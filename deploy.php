<?php
namespace Deployer;

require 'recipe/yii2-app-basic.php';

// Project name
set('application', 'Warehouse Aggregator');

// Project repository
set('repository', 'git@gitlab.globus-ltd.com:warehouse-aggregator/app.git');
set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest');

// [Optional] Allocate tty for git clone. Default value is false.
// set('git_tty', false);

// Shared files/dirs between deploys 
add('shared_files', []);

add('shared_dirs', [
    'runtime',
    'web/assets',
    'web/uploads',
]);

// Writable dirs by web server 
add('writable_dirs', []);

// Hosts
inventory('hosts.yml');

// Tasks
task('deploy:copy_env', function () {
    upload('/tmp/.env', '{{release_path}}/.env');
});

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:copy_env',
    'deploy:vendors',
    'deploy:writable',
    'deploy:run_migrations',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');


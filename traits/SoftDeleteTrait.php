<?php

namespace app\traits;

use app\models\Advert;
use Yii;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use app\models\User;
use yii\db\Transaction;
use app\models\Organization;
use app\models\Reserve;
use app\models\Warehouse;

/**
 * Trait SoftDeleteTrait
 * @package app\traits
 */
trait SoftDeleteTrait
{
    /**
     * @param string|int $id
     * @param bool       $soft
     *
     * @return bool
     * @throws NotFoundHttpException
     */
    public function delete($soft = true)
    {
        /** @var ActiveRecord $class */
        $class = get_called_class();

        if ($soft) {

            if ($this->beforeDelete()){

                $timeDelete = time();
                $this->deleted_at = $timeDelete;

                $countRows = $class::updateAll(['deleted_at' => $timeDelete], ['id' => $this->id]);

                $this->afterDelete();

                return $countRows;
            }

            return false;

        }

        if (isset($this->created_by)) {

            if ($class == Organization::class) {
                $user = User::findModel($this->created_by);
                $user->organization_id = null;
                if (!$user->save()) {
                    return false;
                }
            }

            $this->created_by = null;
        }

        return parent::delete();
    }
}

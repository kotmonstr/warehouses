<?php

namespace app\traits;

use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Trait FindModelTrait
 * @package app\traits
 */
trait FindModelTrait
{
    /**
     * @param mixed $condition
     * @param bool  $checkDeletedAt
     * @param bool  $throwException
     *
     * @return ActiveRecord|self|null
     * @throws NotFoundHttpException
     */
    public static function findModel($condition, $checkDeletedAt = true, $throwException = true)
    {
        /** @var ActiveRecord $class */
        $class = get_called_class();

        $model = $class::findOne($condition);

        if (!$model || ($checkDeletedAt && $model->getAttribute('deleted_at'))) {
            if ($throwException) {
                throw new NotFoundHttpException('Запрашиваемый объект не найден!');
            }

            return null;
        }

        return $model;
    }
}

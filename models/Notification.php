<?php

namespace app\models;

use app\traits\FindModelTrait;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table 'notification'.
 *
 * @property      int $id
 * @property      int $user_id
 * @property      string $title
 * @property      string $text
 * @property      int $created_at
 * @property      int $viewed_at
 * @property-read User $user
 */
class Notification extends ActiveRecord
{
    use FindModelTrait;

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'notification';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['user_id', 'title', 'text'], 'required'],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['viewed_at'], 'default', 'value' => null],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'title' => 'Уведомление',
            'text' => 'Текст',
            'created_at' => 'Дата',
            'viewed_at' => 'Просмотрен',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Function for check is set unreaded notifications
     *
     * @return bool
     */
    public static function isSetUnReaded()
    {
        $query = ['user_id' => Yii::$app->user->identity->id, 'viewed_at' => null];
        return Notification::find()->where($query)->exists();
    }

    /**
     * Function for read notifications
     *
     * @param string|int $id notification
     *
     * @return bool
     */
    public static function readNotification($id)
    {
        if ($id) {
            $query = ['id' => $id, 'user_id' => Yii::$app->user->identity->id];
            $notification = Notification::findOne($query);
            if ($notification) {
                $notification->viewed_at = time();
                if ($notification->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Function for send notification
     *
     * @param string $role to send
     * @param string $title notification
     * @param string $text notification
     *
     * @return bool
     */
    public static function notifyRoleGroup($role, $title, $text)
    {
        $users = User::findAll(['role' => $role]);

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($users as $user) {
            $notification = new Notification();
            $notification->user_id = $user->id;
            $notification->title = $title;
            $notification->text = $text;

            if (!$notification->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }

    /**
     * @param $id
     * @return Notification|null
     * @throws NotFoundHttpException
     * @throws \yii\base\ErrorException
     */
    public static function findByIdAndAccessToken($id)
    {
        $notification = Notification::find()->where(['id' => $id])->one();
        if (!$notification) {
            throw new NotFoundHttpException('Сообщение не найдено');
        }

        if ($id) {
            $query = ['id' => $id, 'user_id' => Yii::$app->user->identity->id];
            $model = Notification::findOne($query);
        }

        return $model;
    }


    /**
     * @return array|ActiveRecord[]
     * @throws NotFoundHttpException
     * @throws \yii\base\ErrorException
     */
    public static function findAllNotifications()
    {
        $model = Notification::find()->where(['user_id' => Yii::$app->user->identity->id])->all();

        return $model;
    }

}

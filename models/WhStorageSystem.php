<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'wh_storage_system'.
 *
 * @property      int $id
 * @property      int $warehouse_id
 * @property      string $storage_system_sn
 * @property      int $created_at
 * @property-read Warehouse $warehouse
 */
class WhStorageSystem extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'wh_storage_system';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['warehouse_id', 'storage_system_sn'], 'required'],
            [['warehouse_id'], 'integer'],
            [['storage_system_sn'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => 'ID склада',
            'storage_system_sn' => 'Система хранения',
            'created_at' => 'Создан',
        ];
    }

    /**
     * Relations with 'warehouse'
     *
     * @property-read Warehouse $warehouse
     * @return        Warehouse $warehouse
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }
}

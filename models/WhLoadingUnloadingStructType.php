<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'wh_loading_unloading_struct_type'.
 *
 * @property      int $id
 * @property      int $warehouse_id
 * @property      string $loading_unloading_struct_type_sn
 * @property      int $created_at
 * @property-read Warehouse $warehouse
 */
class WhLoadingUnloadingStructType extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'wh_loading_unloading_struct_type';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }


    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['warehouse_id', 'loading_unloading_struct_type_sn'], 'required'],
            [['warehouse_id'], 'integer'],
            [['loading_unloading_struct_type_sn'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => 'ID склада',
            'loading_unloading_struct_type_sn' => 'Разгрузочно/погрузочные конструкции',
            'created_at' => 'Создан',
        ];
    }

    /**
     * Relations with 'warehouse'
     *
     * @property-read Warehouse $warehouse
     * @return        Warehouse $warehouse
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }
}

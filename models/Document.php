<?php

namespace app\models;

use app\traits\FindModelTrait;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table 'document'.
 *
 * @property      int $id
 * @property      string $entity_field
 * @property      string $entity_classname
 * @property      int $entity_id
 * @property      int $created_by
 * @property      int $created_at
 * @property      UploadedFile $file
 * @property-read User $user
 * @property-read File $file_db
 */
class Document extends ActiveRecord
{
    use FindModelTrait;

    public $file;

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'document';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['entity_field', 'entity_classname', 'created_by', 'file'], 'required'],
            [['entity_field', 'entity_classname'], 'string', 'max' => 255],
            [['entity_id', 'created_by'], 'integer'],
            ['file', 'file'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'entity_field' => 'Поле',
            'entity_classname' => 'Класс',
            'entity_id' => 'ID класса',
            'created_by' => 'ID создателя',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFile_db()
    {
        return $this->hasOne(File::class, ['document_id' => 'id']);
    }

    /**
     * @return false|string|null
     */
    public function getFile_path()
    {
        $uploadPath = $this->hasOne(File::class, ['document_id' => 'id'])->select('path')->scalar();

        if (!$uploadPath) {
            return null;
        }

        return Yii::$app->params['domainUrl'] . $uploadPath;
    }

    /**
     * Find all user's documents by entity
     *
     * @param string $entityId
     * @param string $entityClassName
     *
     * @return Document[]
     */
    public static function findAllByEntity($entityId, $entityClassName)
    {
        return self::findAll([
            'entity_id' => $entityId,
            'entity_classname' => $entityClassName,
        ]);
    }

    /**
     * Create new document with File
     *
     * @param bool $runValidation
     * @param array $attributes
     *
     * @return bool
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function save($runValidation = true, $attributes = null)
    {
        $transaction = Yii::$app->db->beginTransaction();

        //create row in table document
        if (!parent::save($runValidation, $attributes)) {
            $transaction->rollBack();
            $this->addErrors(parent::getErrors());
            return false;
        }

        //create directory and path for file
        if (!is_dir(Yii::getAlias('@webroot') . "/uploads")) {
            mkdir(Yii::getAlias('@webroot') . "/uploads");
        }
        $path = "/" . "uploads/" . Yii::$app->security->generateRandomString(11) . ".{$this->file->extension}";

        //create row in table file
        $file = new File();
        $file->document_id = $this->id;
        $file->name = $this->file->name;
        $file->path = $path;
        $file->size = $this->file->size;
        $file->mime_type = $this->file->type;

        if (!$file->save()) {
            $transaction->rollBack();
            $this->addErrors($file->errors);
            return false;
        }

        //save file
        if (!$this->file->saveAs(Yii::getAlias('@webroot') . $path)) {
            $transaction->rollBack();
            $this->addErrors(['file' => 'Error saving file']);
            return false;
        }

        $transaction->commit();

        return true;
    }

    /**
     * @param $id
     * @param $docId
     * @throws NotFoundHttpException
     */
    public static function markDocumentEntityId($id,$docId){

        $model = Document::findOne($docId);
        if (!$model) {
            throw new NotFoundHttpException('Документ с id = '.$docId. ' не неайден');
        }
        $model->entity_id = $id;

        $model->updateAttributes(['entity_id']);
    }

}

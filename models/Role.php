<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\traits\SoftDeleteTrait;

/**
 * This is the model class for table 'role'.
 *
 * @property      int $id
 * @property      string $title
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 * @property-read UserRole $user_role
 */
class Role extends ActiveRecord
{
    use SoftDeleteTrait;

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'role';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['deleted_at'], 'default', 'value' =>  null],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * Relations with 'user_role'
     *
     * @property-read UserRole $user_role
     * @return        UserRole $user_role
     */
    public function getUser_role()
    {
        return $this->hasMany(UserRole::class, ['role_id' => 'id']);
    }

}

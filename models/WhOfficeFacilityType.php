<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'wh_office_facility_type'.
 *
 * @property      int $id
 * @property      int $warehouse_id
 * @property      string $types
 * @property      string $office_facility_types_sn
 * @property      int $created_at
 * @property-read Warehouse $warehouse
 */
class WhOfficeFacilityType extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'wh_office_facility_type';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['warehouse_id', 'types', 'office_facility_types_sn'], 'required'],
            [['warehouse_id'], 'integer'],
            [['types', 'office_facility_types_sn'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => 'ID склада',
            'types' => 'Типы офисных и пособных помещений',
            'office_facility_types_sn' => 'Системное имя',
            'created_at' => 'Создан',
        ];
    }

    /**
     * Relations with 'warehouse'
     *
     * @property-read Warehouse $warehouse
     * @return        Warehouse $warehouse
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }
}

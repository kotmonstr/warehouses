<?php

namespace app\models;

use app\modules\panel\models\forms\warehouse\DocumentsForm;
use app\modules\panel\models\forms\warehouse\ExternalForm;
use app\modules\panel\models\forms\warehouse\GeneralForm;
use app\modules\panel\models\forms\warehouse\InternalForm;
use app\traits\FindModelTrait;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\traits\SoftDeleteTrait;
use app\models\Advert;

/**
 * This is the model class for table "warehouse".
 *
 * @property int $id
 * @property int $organization_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $address_ym_geocoder
 * @property string|null $address_lat
 * @property string|null $address_lon
 * @property bool|null $work_hours_24_7
 * @property string|null $work_hours_weekdays_from
 * @property string|null $work_hours_weekdays_to
 * @property string|null $work_hours_weekends_from
 * @property string|null $work_hours_weekends_to
 * @property string|null $accounting_system_sn
 * @property string|null $classification_sn
 * @property float|null $square_area
 * @property string|null $square_area_type_sn
 * @property float|null $built_up_area
 * @property string|null $built_up_area_type_sn
 * @property float|null $ceiling_height
 * @property string|null $ceiling_height_type_sn
 * @property int|null $floors
 * @property bool|null $maneuvering_area
 * @property int|null $entries_amount
 * @property bool|null $loading_ramp
 * @property int|null $automatic_gates_amount
 * @property bool|null $outer_lighting
 * @property bool|null $outer_security
 * @property bool|null $outer_beautification
 * @property bool|null $outer_fencing
 * @property int|null $outer_entries_amount
 * @property bool|null $truck_maneuvering_area
 * @property bool|null $rail_road
 * @property float|null $distance_from_highway
 * @property string|null $distance_from_highway_type_sn
 * @property float|null $vehicle_max_length
 * @property string|null $vehicle_max_length_type_sn
 * @property float|null $vehicle_max_height
 * @property string|null $vehicle_max_height_type_sn
 * @property float|null $vehicle_max_width
 * @property string|null $vehicle_max_width_type_sn
 * @property float|null $vehicle_max_weight
 * @property string|null $vehicle_max_weight_type_sn
 * @property float|null $column_distance
 * @property string|null $column_distance_type_sn
 * @property float|null $span_distance
 * @property string|null $span_distance_type_sn
 * @property bool|null $column_protection
 * @property string|null $floor_covering_sn
 * @property bool|null $anti_dust_covering
 * @property float|null $height_above_ground
 * @property string|null $height_above_ground_type_sn
 * @property float|null $load_per_square_meter
 * @property string|null $load_per_square_meter_type_sn
 * @property bool|null $ventilation
 * @property bool|null $conditioning
 * @property string|null $temperature_mode_sn
 * @property float|null $min_temperature
 * @property string|null $min_temperature_type_sn
 * @property float|null $max_temperature
 * @property string|null $max_temperature_type_sn
 * @property bool|null $security_guard
 * @property bool|null $security_video
 * @property bool|null $security_alarm
 * @property bool|null $fire_alarm
 * @property string|null $fire_ext_system_sn
 * @property bool|null $fire_charging
 * @property bool|null $fire_tanks_and_pump
 * @property string|null $electricity_grid_sn
 * @property string|null $heating_system_sn
 * @property bool|null $water_hot
 * @property bool|null $water_cold
 * @property bool|null $water_sewerage
 * @property bool|null $internet
 * @property bool|null $phone
 * @property bool|null $fiber_optic
 * @property int $created_by
 * @property int|null $verified_at
 * @property int $created_at
 * @property int|null $deleted_at
 * @property int|null $updated_at
 * @property bool|null $general_is_set
 * @property bool|null $external_is_set
 * @property bool|null $internal_is_set
 * @property bool|null $documents_is_set
 * @property string|null $car_parking_sn
 * @property string|null $truck_parking_sn
 * @property string|null $sb_verification_status_sn
 * @property string|null $verification_comment
 * @property string|null $address_city_ym_geocoder
 * @property string|null $dsl_verification_status_sn
 * @property string|null $status_sn
 * @property string|null $inn
 * @property string|null $kpp
 * @property bool|null $from_the_owner
 *
 * @property Advert[] $adverts
 * @property Organization $organization
 * @property User $createdBy
 * @property WhAccountingControlSystem[] $whAccountingControlSystems
 * @property WhLoadUnloadCranage[] $whLoadUnloadCranages
 * @property WhLoadUnloadEquipment[] $whLoadUnloadEquipments
 * @property WhLoadingUnloadingStructType[] $whLoadingUnloadingStructTypes
 * @property WhOfficeFacilityType[] $whOfficeFacilityTypes
 * @property WhStorageSystem[] $whStorageSystems
 */
class Warehouse extends ActiveRecord
{
    use FindModelTrait;
    use SoftDeleteTrait;

    const TABS = [
        GeneralForm::ALIAS => 'Основное',
        ExternalForm::ALIAS => 'Внешние параметры',
        InternalForm::ALIAS => 'Внутренние параметры',
        DocumentsForm::ALIAS => 'Документы',
    ];

    /**
     * Constants for verification
     */
    const VERIFICATION_IN_PROGRESS = 'in_progress';
    const VERIFICATION_VERIFIED = 'verified';
    const VERIFICATION_FAILED = 'failed';
    const VERIFICATION_RESTRICTED = 'restricted';

    /**
     * Constants for status_sn
     */
    const STATUS_DRAFT = 'draft';
    const STATUS_VERIFICATION = 'verification';
    const STATUS_VERIFIED = 'verified';
    const STATUS_RESTRICTED = 'restricted';


    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'warehouse';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [
                [
                    'organization_id', 'title',
                    'created_by'
                ], 'required'
            ],
            [
                [
                    'organization_id', 'floors',
                    'entries_amount', 'automatic_gates_amount', 'outer_entries_amount',
                    'created_by'
                ], 'integer'
            ],
            [
                [
                    'title', 'address_ym_geocoder', 'address_lat', 'address_lon',
                    'accounting_system_sn', 'classification_sn', 'square_area_type_sn',
                    'work_hours_weekdays_from', 'work_hours_weekdays_to',
                    'work_hours_weekends_from', 'work_hours_weekends_to',
                    'built_up_area_type_sn', 'ceiling_height_type_sn',
                    'car_parking_sn', 'truck_parking_sn', 'distance_from_highway_type_sn',
                    'vehicle_max_length_type_sn', 'vehicle_max_height_type_sn', 'vehicle_max_width_type_sn',
                    'vehicle_max_weight_type_sn', 'column_distance_type_sn', 'span_distance_type_sn',
                    'floor_covering_sn', 'height_above_ground_type_sn', 'load_per_square_meter_type_sn',
                    'temperature_mode_sn', 'min_temperature_type_sn', 'max_temperature_type_sn',
                    'fire_ext_system_sn', 'electricity_grid_sn', 'heating_system_sn',
                    'sb_verification_status_sn', 'dsl_verification_status_sn',
                    'organizationName', 'storage_type_sn', 'address_city_ym_geocoder', 'status_sn', 'inn', 'kpp'
                ], 'string', 'max' => 255
            ],
            [['description', 'verification_comment'], 'string'],
            [
                [
                    'general_is_set', 'external_is_set', 'internal_is_set', 'documents_is_set',
                    'work_hours_24_7', 'maneuvering_area', 'loading_ramp',
                    'outer_lighting', 'outer_security', 'outer_beautification',
                    'outer_fencing', 'truck_maneuvering_area', 'rail_road',
                    'column_protection', 'anti_dust_covering', 'ventilation',
                    'conditioning', 'security_guard', 'security_video',
                    'security_alarm', 'fire_alarm', 'fire_charging',
                    'fire_tanks_and_pump', 'water_hot', 'water_cold',
                    'water_sewerage', 'internet', 'phone', 'fiber_optic', 'from_the_owner'
                ], 'boolean'
            ],
            [
                [
                    'square_area', 'built_up_area', 'ceiling_height',
                    'distance_from_highway', 'vehicle_max_length', 'vehicle_max_height',
                    'vehicle_max_width', 'vehicle_max_weight', 'column_distance',
                    'span_distance', 'height_above_ground', 'load_per_square_meter',
                    'min_temperature', 'max_temperature'
                ], 'double'
            ],
            [['verified_at', 'deleted_at'], 'default', 'value' => null],
            [
                [
                    'general_is_set', 'external_is_set', 'internal_is_set', 'documents_is_set'
                ], 'default', 'value' => false
            ],
            [['sb_verification_status_sn', 'dsl_verification_status_sn'], 'default', 'value' => Warehouse::VERIFICATION_IN_PROGRESS],

            [['status_sn'], 'default', 'value' => self::STATUS_DRAFT]
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'organization_id' => 'ID организации',
            'title' => 'Название склада',
            'description' => 'Описание',
            'general_is_set' => 'Основное заполнено',
            'external_is_set' => 'Внешние параметры заполнено',
            'internal_is_set' => 'Внутренние параметры заполнено',
            'documents_is_set' => 'Документы заполнено',
            'address_ym_geocoder' => 'Адрес склада',
            'address_lat' => 'GPS-координаты склада (широта)',
            'address_lon' => 'GPS-координаты склада (долгота)',
            'work_hours_24_7' => 'Круглосуточно',
            'work_hours_weekdays_from' => 'Будни, с',
            'work_hours_weekdays_to' => 'Будни, по',
            'work_hours_weekends_from' => 'Выходные, с',
            'work_hours_weekends_to' => 'Выходные, по',
            'accounting_system_sn' => 'Система учета на складе',
            'classification_sn' => 'Класс помещения',
            'square_area' => 'Площадь помещения',
            'square_area_type_sn' => 'Площадь помещения (единица измерения)',
            'built_up_area' => 'Площадь застройки',
            'built_up_area_type_sn' => 'Площадь застройки (единица измерения)',
            'ceiling_height' => 'Высота потолков',
            'ceiling_height_type_sn' => 'Высота потолков (единица измерения)',
            'floors' => 'Кол-во этажей',
            'maneuvering_area' => 'Наличие зоны маневрирования',
            'entries_amount' => 'Кол-во въездов/выездов на территорию',
            'loading_ramp' => 'Наличие пандуса для разгрузки автотранспорта',
            'automatic_gates_amount' => 'Кол-во автоматических ворот докового типа',
            'outer_lighting' => 'Освещение',
            'outer_security' => 'Охрана',
            'outer_beautification' => 'Благоустройство',
            'outer_fencing' => 'Ограждение',
            'outer_entries_amount' => 'Количество въездов/выездов на территорию (прил.)',
            'car_parking_sn' => 'Парковка для легкового транспорта ',
            'truck_parking_sn' => 'Для отстоя большегрузного транспорта ',
            'truck_maneuvering_area' => 'Зона маневрирования грузовиков',
            'rail_road' => 'ЖД ветка',
            'distance_from_highway' => 'Удаленность',
            'distance_from_highway_type_sn' => 'Удаленность (единица измерения)',
            'vehicle_max_length' => 'Максимальная длина',
            'vehicle_max_length_type_sn' => 'Максимальная длина (единица измерения)',
            'vehicle_max_height' => 'Максимальная высота',
            'vehicle_max_height_type_sn' => 'Максимальная высота (единица измерения)',
            'vehicle_max_width' => 'Максимальная ширина',
            'vehicle_max_width_type_sn' => 'Максимальная ширина (единица измерения)',
            'vehicle_max_weight' => 'Максимальная масса',
            'vehicle_max_weight_type_sn' => 'Максимальная масса (единица измерения)',
            'column_distance' => 'Шаг колонн',
            'column_distance_type_sn' => 'Шаг колонн (единица измерения)',
            'span_distance' => 'Расстояние между пролетами',
            'span_distance_type_sn' => 'Расстояние между пролетами (единица измерения)',
            'column_protection' => 'Наличие защитных сооружений вокруг колонн',
            'floor_covering_sn' => 'Материал покрытия',
            'anti_dust_covering' => 'Антипылевое покрытие',
            'height_above_ground' => 'Высота над землей',
            'height_above_ground_type_sn' => 'Высота над землей (единица измерения)',
            'load_per_square_meter' => 'Нагрузка на квадратный метр',
            'load_per_square_meter_type_sn' => 'Нагрузка на квадратный метр (единица измерения)',
            'ventilation' => 'Вентиляция',
            'conditioning' => 'Кондиционирование',
            'temperature_mode_sn' => 'Температурный режим',
            'min_temperature' => 'Температура мин',
            'min_temperature_type_sn' => 'Температура мин (единица измерения)',
            'max_temperature' => 'Температура макс',
            'max_temperature_type_sn' => 'Температура макс (единица измерения)',
            'security_guard' => 'Охрана',
            'security_video' => 'Видеонаблюдение',
            'security_alarm' => 'Сигнализация',
            'fire_alarm' => 'Система предупреждения о пожаре',
            'fire_ext_system_sn' => 'Система автоматического пожаротушения',
            'fire_charging' => 'Наличие зарядных комнат',
            'fire_tanks_and_pump' => 'Наличие резервуаров пожаротушения и насосной станции',
            'electricity_grid_sn' => 'Электросеть',
            'heating_system_sn' => 'Система отопления',
            'water_hot' => 'Горячее',
            'water_cold' => 'Холодное',
            'water_sewerage' => 'Канализация',
            'internet' => 'Интернет',
            'phone' => 'Телефонная связь',
            'fiber_optic' => 'Оптоволоконные телекоммуникации',
            'created_by' => 'ID создателя',
            'verified_at' => 'Проверен',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
            'sb_verification_status_sn' => 'Статус СБ',
            'verification_comment' => 'Комментарий',
            'dsl_verification_status_sn' => 'Статус ДСЛ',
            'organizationName' => 'Компания',
            'storage_type_sn' => 'Список систем хранения',
            'address_city_ym_geocoder' => 'Город',
            'status_sn' => 'Статус',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'from_the_owner' => 'Я владелец',
        ];
    }

    /**
     * Relations with 'user'
     *
     * @return        User $user
     * @property-read User $user
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Relations with 'organization'
     *
     * @return        Organization $organization
     * @property-read Organization $organization
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    /**
     * @return   string $organizationName
     * @property string $organizationName
     */
    public function getOrganizationName()
    {
        return Organization::findOne(['id' => $this->organization_id])['title'];
    }

    /**
     * Relations with 'wh_load_unload_equipment'
     *
     * @return        WhLoadUnloadEquipment $wh_load_unload_equipment
     * @property-read WhLoadUnloadEquipment $wh_load_unload_equipment
     */
    public function getWh_load_unload_equipment()
    {
        return $this->hasMany(
            WhLoadUnloadEquipment::class,
            ['warehouse_id' => 'id']
        );
    }

    /**
     * Relations with 'wh_storage_system'
     *
     * @return        WhStorageSystem $wh_storage_system
     * @property-read WhStorageSystem $wh_storage_system
     */
    public function getWh_storage_system()
    {
        return $this->hasMany(WhStorageSystem::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return   string $storage_type_sn
     * @property string $storage_type_sn
     */
    public function getStorage_type_sn()
    {
        $storageSystems = WhStorageSystem::findAll(['warehouse_id' => $this->id]);
        $systemNames = ArrayHelper::getColumn($storageSystems, 'storage_system_sn');
        $namesArray = [];
        $dictionaryItems = Dictionary::findDictItems('storage_system');
        foreach ($systemNames as $name) {
            $namesArray[] = $dictionaryItems[$name];
        }
        return implode(", ", $namesArray);
    }

    /**
     * Relations with 'wh_loading_unloading_struct_type'
     *
     * @return        WhLoadingUnloadingStructType $wh_loading_unloading_struct_type
     * @property-read WhLoadingUnloadingStructType $wh_loading_unloading_struct_type
     */
    public function getWh_loading_unloading_struct_type()
    {
        return $this->hasMany(
            WhLoadingUnloadingStructType::class,
            ['warehouse_id' => 'id']
        );
    }

    /**
     * Relations with 'wh_office_facility_type'
     *
     * @return        WhOfficeFacilityType $wh_office_facility_type
     * @property-read WhOfficeFacilityType $wh_office_facility_type
     */
    public function getWh_office_facility_type()
    {
        return $this->hasMany(WhOfficeFacilityType::class, ['warehouse_id' => 'id']);
    }

    /**
     * Relations with 'wh_load_unload_cranage'
     *
     * @return        WhLoadUnloadCranage $wh_load_unload_cranage
     * @property-read WhLoadUnloadCranage $wh_load_unload_cranage
     */
    public function getWh_load_unload_cranage()
    {
        return $this->hasMany(
            WhLoadUnloadCranage::class,
            ['warehouse_id' => 'id']
        );
    }

    /**
     * Relations with 'wh_accounting_control_system'
     *
     * @return        WhAccountingControlSystem $wh_accounting_control_system
     * @property-read WhAccountingControlSystem $wh_accounting_control_system
     */
    public function getWh_accounting_control_system()
    {
        return $this->hasMany(WhAccountingControlSystem::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounting_control_system()
    {
        return $this->hasMany(WhAccountingControlSystem::class, ['warehouse_id' => 'id']);
    }

    /**
     * Relations with 'advert'
     *
     * @return        Advert $advert
     * @property-read Advert $advert
     */
    public function getAdvert()
    {
        return $this->hasMany(Advert::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFirstEmptyTab()
    {
        $aliases = array_keys(self::TABS);

        foreach ($aliases as $alias) {
            if (!$this->{$alias . '_is_set'}) {
                return $alias;
            }
        }

        return end($aliases);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $classification
     * @property-read DictionaryItem $classification
     */
    public function getClassification()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'classification_sn']);
    }

    /**
     * Function to get list of cities
     *
     * @param string|int $organization_id of current user
     * @param bool $verified_only
     *
     * @return string[] $cities
     */
    public static function getCities($organization_id = null, $verified_only = false)
    {
        if ($organization_id) {

            //cities for holders in panel module
            $cities = Warehouse::find()
                ->joinWith('advert')
                ->where(
                    [
                        'warehouse.deleted_at' => null,
                        'warehouse.organization_id' => $organization_id,
                        'advert.deleted_at' => null,
                    ]
                )->all();

        } else {

            //cities for main pages
            if ($verified_only) {
                $cities = Warehouse::find()
                    ->joinWith('organization')
                    ->joinWith('advert')
                    ->where(
                        [
                            'warehouse.deleted_at' => null,
                            'organization.deleted_at' => null,
                            'advert.deleted_at' => null,
                            'organization.verification_status_sn' => Organization::VERIFICATION_VERIFIED,
                            'warehouse.sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                            'warehouse.dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                            'advert.status_sn' => Advert::STATUS_PUBLISHED,
                        ]
                    )->andWhere(['>=', 'advert.available_to', time()])->all();

            } else {

                //cities for users in admin module
                $cities = Warehouse::find()
                    ->joinWith('organization')
                    ->innerJoin('advert', 'advert.warehouse_id=warehouse.id')
                    ->where(
                        [
                            'warehouse.deleted_at' => null,
                            'organization.deleted_at' => null,
                            'advert.deleted_at' => null,
                        ]
                    )->all();
            }
        }


        $citiesArr = ArrayHelper::getColumn($cities, 'address_city_ym_geocoder');
        $citiesArr = array_unique($citiesArr);

        $citiesReturn = [];
        foreach ($citiesArr as $city) {
            $citiesReturn [$city] = $city;
        }

        return $citiesReturn;
    }

    /**
     * Логика:
     *
     * Обе пройдены = пройдено,
     * Хоть одна отклонена = отклонено,
     * Хоть одна заблокирована = заблокировано,
     * Иначе = в процессе
     *
     * @return DictionaryItem
     */
    public function getVerification_status()
    {
        $status = self::VERIFICATION_IN_PROGRESS;

        if ($this->sb_verification_status_sn == self::VERIFICATION_VERIFIED && $this->dsl_verification_status_sn == self::VERIFICATION_VERIFIED) {
            $status = self::VERIFICATION_VERIFIED;
        } elseif ($this->sb_verification_status_sn == self::VERIFICATION_FAILED || $this->dsl_verification_status_sn == self::VERIFICATION_FAILED) {
            $status = self::VERIFICATION_FAILED;
        } elseif ($this->sb_verification_status_sn == self::VERIFICATION_RESTRICTED || $this->dsl_verification_status_sn == self::VERIFICATION_RESTRICTED) {
            $status = self::VERIFICATION_RESTRICTED;
        }

        return DictionaryItem::findOne(['dictionary_sn' => 'warehouse_verification_status', 'system_name' => $status]);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $accounting_system_sn
     * @property-read DictionaryItem $accounting_system_sn
     */
    public function getAccounting_system()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'accounting_system_sn']);
    }    /**
 * Relations with 'dictionaryItem'
 * @return        DictionaryItem $accounting_system_sn
 * @property-read DictionaryItem $accounting_system_sn
 */

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $square_area_type
     * @property-read DictionaryItem $square_area_type
     */
    public function getSquare_area_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'square_area_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $built_up_area_type
     * @property-read DictionaryItem $built_up_area_type
     */
    public function getBuilt_up_area_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'built_up_area_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $ceiling_height_type
     * @property-read DictionaryItem $ceiling_height_type
     */
    public function getCeiling_height_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'ceiling_height_type']);
    }


    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $car_parking
     * @property-read DictionaryItem $car_parking
     */
    public function getCar_parking()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'car_parking']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $truck_parking
     * @property-read DictionaryItem $truck_parking
     */
    public function getTruck_parking()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'truck_parking']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $distance_from_highway_type
     * @property-read DictionaryItem $distance_from_highway_type
     */
    public function getDistance_from_highway_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'distance_from_highway_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $vehicle_max_length_type
     * @property-read DictionaryItem $vehicle_max_length_type
     */
    public function getVehicle_max_length_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'vehicle_max_length_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $vehicle_max_height_type
     * @property-read DictionaryItem $vehicle_max_height_type
     */
    public function getVehicle_max_height_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'vehicle_max_height_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $vehicle_max_width_type
     * @property-read DictionaryItem $vehicle_max_width_type
     */
    public function getvehicle_max_width_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'vehicle_max_width_type']);
    }

    /**
     * Relations with 'dictionaryItem'
     * @return        DictionaryItem $vehicle_max_weight_type
     * @property-read DictionaryItem $vehicle_max_weight_type
     */
    public function getvehicle_max_weight_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'vehicle_max_weight_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'status_sn'])->andWhere(['dictionary_sn' => 'warehouse_status']);
    }


}

<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'dictionary_item'.
 *
 * @property      int $id
 * @property      string $dictionary_sn
 * @property      string $type
 * @property      string $system_name
 * @property      string $title
 * @property      string $title_short
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 * @property-read Dictionary $dictionary
 */
class DictionaryItem extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'dictionary_item';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['dictionary_sn', 'type', 'system_name', 'title'], 'required'],
            [['dictionary_sn', 'type', 'system_name', 'title', 'title_short'], 'string', 'max' => 255],
            [['deleted_at'], 'default', 'value' =>  null],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'dictionary_sn' => 'Системное имя справочника',
            'type' => 'Название справочника',
            'system_name' => 'Системное имя',
            'title' => 'Название',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDictionary()
    {
        return $this->hasOne(Dictionary::class, ['system_name' => 'dictionary_sn']);
    }

    /**
     * @return string
     */
    public function getTitle_short()
    {
        return $this->title_short ?? $this->title;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use app\traits\FindModelTrait;
use app\traits\SoftDeleteTrait;

/**
 * This is the model class for table 'user'.
 *
 * @property      int $id
 * @property      string|null $first_name
 * @property      int $organization_id
 * @property      string $organization_position
 * @property      string $email
 * @property      string $email_verification_token
 * @property      bool $email_verified
 * @property      string $phone_number
 * @property      string $password_hash
 * @property      string $password_reset_token
 * @property      string $verification_status_sn
 * @property      string $verification_comment
 *
 * @property      int $access_token
 * @property      int $access_token_expired_at
 * @property      int $refresh_token
 * @property      int $refresh_token_expired_at
 *
 * @property string|null $last_name
 * @property string|null $middle_name
 *
 * @property      int $verified_at
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 *
 * @property      string $role
 * @property-read UserRole $user_role
 * @property-read Organization $organization
 * @property-read Warehouse $warehouse
 */
class User extends ActiveRecord implements IdentityInterface
{
    use FindModelTrait;
    use SoftDeleteTrait;

    const ACCESS_TOKEN_TTL = 60 * 60 * 24;      // 24 hrs
    const REFRESH_TOKEN_TTL = 60 * 60 * 24 * 30; // 30 days

    /**
     * Constants for roles
     */
    const ROLE_ADMIN = 'admin';
    const ROLE_HOLDER = 'holder';
    const ROLE_RENTER = 'renter';
    const ROLE_SB = 'sb';
    const ROLE_DSL = 'dsl';

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'user';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['first_name', 'email', 'password_hash'], 'required'],
            [
                [
                    'first_name', 'email', 'organization_position', 'email_verification_token',
                    'phone_number', 'password_reset_token', 'verification_status_sn',
                    'password_hash', 'last_name', 'middle_name'
                ], 'string', 'max' => 255
            ],
            [['email'], 'email'],
            [['organization_id'], 'integer'],
            [['organization_id', 'deleted_at'], 'default', 'value' => null],
            [['email_verified'], 'boolean'],
            [['verification_comment'], 'string'],
            [['role'], 'string', 'max' => 50],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'organization_id' => 'ID организации',
            'organization_position' => 'Должность',
            'email' => 'Email',
            'email_verification_token' => 'Токен Email',
            'email_verified' => 'Email подтвержден',
            'phone_number' => 'Телефон',
            'password_hash' => 'Хэш пароля',
            'password_reset_token' => 'Токен пароля',
            'verification_status_sn' => 'Статус проверки',
            'verification_comment' => 'Комментарий проверки',
            'verified_at' => 'Проверен',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
            'role' => 'Роль',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser_role()
    {
        return $this->hasOne(UserRole::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasMany(Warehouse::class, ['created_by' => 'id']);
    }

    /**
     * Function for checking if user is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role == $this::ROLE_ADMIN;
    }

    /**
     * Function for checking if user is holder
     *
     * @return bool
     */
    public function isHolder()
    {
        return $this->role == $this::ROLE_HOLDER;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Receive current user ID
     *
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
    }

    public function validateAuthKey($authKey)
    {
    }

    /**
     * Finds user by email
     *
     * @param string $email name of user
     * @param bool $checkDeleted
     *
     * @return static|null
     */
    public static function findByUsername($email, $checkDeleted = true)
    {
        if ($checkDeleted) {
            return static::findOne(['email' => $email, 'deleted_at' => null]);
        }
        return static::findOne(['email' => $email]);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password password of user
     *
     * @return null
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates new password reset token
     *
     * @return null
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * Removes password reset token
     *
     * @return null
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        return static::findOne(['password_reset_token' => $token]);
    }

    /**
     * Finds user by email verification token
     *
     * @param string $token email verification token
     *
     * @return static|null
     */
    public static function findByEmailVerificationToken($token)
    {
        return static::findOne(['email_verification_token' => $token]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return array|ActiveRecord|IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->where(['access_token' => $token])
            ->andWhere(['>', 'access_token_expired_at', time()])
            ->one();
    }

    /**
     * @param $token
     * @return array|ActiveRecord|null
     */
    public static function findIdentityByRefreshToken($token)
    {
        return static::find()
            ->where(['refresh_token' => $token])
            ->andWhere(['>', 'refresh_token_expired_at', time()])
            ->one();
    }

    /**
     * @param $token
     * @return bool
     */
    public function validateRefreshToken($token)
    {
        return $this->refresh_token === $token;
    }

    /**
     * @throws Exception
     */
    public function refreshToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
        $this->access_token_expired_at = time() + self::ACCESS_TOKEN_TTL;

        $this->refresh_token = Yii::$app->security->generateRandomString();
        $this->refresh_token_expired_at = time() + self::REFRESH_TOKEN_TTL;
    }

    public function logout()
    {
        return (bool)User::updateAll([
            'access_token' => null,
            'access_token_expired_at' => null,
            'refresh_token' => null,
            'refresh_token_expired_at' => null,
        ], ['id' => $this->id,]);
    }

    /**
     * @param $user
     */
    public static function sendVerificationEmail($user)
    {
        $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(
            ['user/sign-up-confirm', 'token' => $user->email_verification_token]
        );
        Yii::$app->mailer
            ->compose(
                [
                    'html' => 'user/email_confirm_html',
                    'text' => 'user/email_confirm_text',
                ],
                [
                    'user' => $user,
                    'confirmLink' => $confirmLink,
                ]
            )
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Подтверждение регистрации')
            ->send();
    }

    /**
     * @param $user
     */
    public static function sendResetPasswordEmail($user)
    {
        $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(
            ['user/set-password', 'token' => $user->generatePasswordResetToken()]
        );
        Yii::$app->mailer
            ->compose(
                [
                    'html' => 'user/password_reset_html',
                    'text' => 'user/password_reset_text',
                ],
                [
                    'user' => $user,
                    'confirmLink' => $confirmLink,
                ]
            )
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Сброс пароля')
            ->send();
    }

    /**
     * @param $accessTokenString
     * @return array|ActiveRecord|null
     */
    public static function getUserByAccessTokenFromHeader(){
        $accessTokenString = Yii::$app->request->getHeaders()['authorization'];
        if (empty($accessTokenString)){
            throw new ErrorException('No access token provided.');
        }
        $accessToken = substr($accessTokenString, 7);
        $user = User::find()->where(['access_token'=> $accessToken])->one();

        return $user;
    }

}

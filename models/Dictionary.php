<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table 'dictionary'.
 *
 * @property      int $id
 * @property      string $system_name
 * @property      string $title
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 * @property-read DictionaryItem $items
 */
class Dictionary extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'dictionary';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['system_name', 'title'], 'required'],
            [['system_name', 'title'], 'string', 'max' => 255],
            [['deleted_at'], 'default', 'value' =>  null],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'system_name' => 'Системное имя',
            'title' => 'Название',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(
            DictionaryItem::class,
            ['dictionary_sn' => 'system_name']
        );
    }

    /**
     * Find items of dictionary
     *
     * @param string      $dictionary_sn Системное имя справочника
     * @param string|null $type          Название справочника
     * @param bool|null   $titleShort
     *
     * @return array
     */
    public static function findDictItems($dictionary_sn, $type = null, $titleShort = false)
    {
        $query = ['dictionary_sn' => $dictionary_sn];

        if ($type) {
            $query['type'] = $type;
        }

        $dictItems = DictionaryItem::find()
            ->where($query)
            ->orderBy([
                'created_at' => SORT_ASC,
                'id' => SORT_ASC,
            ])->all();

        if ($titleShort) {
            return ArrayHelper::map($dictItems, 'system_name', 'title_short');
        }

        return ArrayHelper::map($dictItems, 'system_name', 'title');
    }
}

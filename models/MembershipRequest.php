<?php
namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class MembershipRequest
 * @package app\models
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $organization_id
 * @property integer $created_at
 */
class MembershipRequest extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'membership_request';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'organization_id'], 'required'],
            [['user_id', 'organization_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'organization_id' => 'ID организации',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return MembershipRequest|null
     */
    public static function findByCurrentUser()
    {
        return self::findOne(['user_id' => Yii::$app->user->identity->id]);
    }

    /**
     * @param $membershipRequestId
     * @param $organizationId
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public static function addUser($membershipRequestId, $organizationId)
    {
        $membershipRequest = Membership::findOne([['id' => $membershipRequestId]]);
        if (!$membershipRequest) {
            throw new NotFoundHttpException('Membership request not founded');
        }
        $user = User::findOne(['id' => $membershipRequest->user_id]);
        if (!$user) {
            throw new NotFoundHttpException('User not founded');
        }

        $transaction = Yii::$app->db->beginTransaction();

        $user->organization_id = $organizationId;
        if (!$user->save()) {
            $transaction->rollBack();
            return false;
        }

        if (!$membershipRequest->delete()) {
            $transaction->rollBack();
            return false;
        }

        $organization = Organization::findModel($organizationId);
        $notificationUser = new Notification();
        $notificationUser->user_id = $user->id;
        $notificationUser->title = "Одобрена заявка на присоединение к компании";
        $notificationUser->text = "Одобрена Ваша заявка на присоединение к компании {$organization->title}";
        if (!$notificationUser->save()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();

        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws Exception
     * @throws NotFoundHttpException
     */
//    public function afterSave($insert, $changedAttributes)
//    {
//        parent::afterSave($insert, $changedAttributes);
//
//        $transaction = Yii::$app->db->beginTransaction();
//
//        $organization = Organization::findModel($this->organization_id);
//        $owner = User::findModel($organization->created_by);
//        $user = User::findModel(Yii::$app->user->identity->id);
//
//        $notificationOwner = new Notification();
//        $notificationOwner->user_id = $owner->id;
//        $notificationOwner->title = "Заявка на присоединение к компании";
//        $notificationOwner->text = "Вашей компании направлена заявка на присоединение от пользователя <a href='/panel/team/index'>{$user->name}</a>";
//        if (!$notificationOwner->save()) {
//            $transaction->rollBack();
//        }
//
//        $notificationUser = new Notification();
//        $notificationUser->user_id = $user->id;
//        $notificationUser->title = "Заявка на присоединение к компании";
//        $notificationUser->text = "Вами создана заявка на присоединение к компании {$organization->title}";
//        if (!$notificationUser->save()) {
//            $transaction->rollBack();
//        }
//
//        $transaction->commit();
//    }

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
//    public function afterDelete()
//    {
//        parent::afterDelete();
//
//        $organization = Organization::findModel($this->organization_id);
//        $user = User::findModel($this->user_id);
//
//        if ($user->organization_id != $organization->id) {
//            $transaction = Yii::$app->db->beginTransaction();
//
//            $notificationUser = new Notification();
//            $notificationUser->user_id = $user->id;
//            $notificationUser->title = "Отклонена заявка на присоединение к компании";
//            $notificationUser->text = "Отклонена Ваша заявка на присоединение к компании {$organization->title}";
//            if (!$notificationUser->save()) {
//                $transaction->rollBack();
//            }
//
//            $transaction->commit();
//        }
//    }
}
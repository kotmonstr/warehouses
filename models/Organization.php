<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Yii;
use app\traits\FindModelTrait;
use app\traits\SoftDeleteTrait;

/**
 * This is the model class for table 'organization'.
 *
 * @property int    $id
 * @property string $legal_type
 * @property string $title
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $okpo
 * @property string $main_activity
 * @property string $taxation_type
 * @property string $address_ym_geocoder
 *
 * @property string $shareholders
 * @property string $edm_type
 * @property string $post_address
 * @property string $fact_address
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $start_capital
 * @property string $registration_date
 * @property int    $employee_count
 * @property string $bank_details
 * @property string $authorized_head_contact
 * @property string $authorized_sign_contact
 * @property string $authorized_security_contact
 * @property string $partner_type
 *
 * @property string $verification_status_sn
 * @property string $verification_comment
 * @property int $created_by
 * @property int $verified_at
 * @property int $created_at
 * @property int $deleted_at
 * @property int $updated_at
 *
 * @property-read User $user
 */
class Organization extends ActiveRecord
{
    use FindModelTrait;
    use SoftDeleteTrait;

    /**
     * Constants for verification
     */
    const VERIFICATION_IN_PROGRESS = 'in_progress';
    const VERIFICATION_VERIFIED = 'verified';
    const VERIFICATION_FAILED = 'failed';
    const VERIFICATION_RESTRICTED = 'restricted';

    const PARTNER_TYPE_REGIONAL = 'regional';
    const PARTNER_TYPE_FEDERAL = 'federal';

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'organization';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'inn', 'ogrn', 'created_by'], 'required'],
            [
                [
                    'shareholders', 'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'employee_count', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'required', 'when' => function (self $model) {
                return $model->legal_type == 'UL';
            }
            ],
            [
                [
                    'title', 'inn', 'ogrn',
                    'kpp', 'okpo', 'main_activity',
                    'taxation_type', 'address_ym_geocoder',
                    'verification_status_sn', 'legal_type',
                    'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'string', 'max' => 255
            ],
            [['verification_comment', 'shareholders'], 'string'],
            [['created_by', 'employee_count'], 'integer'],
            [['deleted_at'], 'default', 'value' => null],
            [['verification_status_sn'], 'default', 'value' => Organization::VERIFICATION_IN_PROGRESS],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'legal_type' => 'Юридический тип',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'kpp' => 'КПП',
            'okpo' => 'ОКПО',
            'main_activity' => 'Основной вид деятельности',
            'taxation_type' => 'Налоговый режим',
            'address_ym_geocoder' => 'Юридический адрес',

            'shareholders' => 'Учредители',
            'edm_type' => 'Применяемая платформа ЭДО',
            'post_address' => 'Почтовый адрес',
            'fact_address' => 'Фактическое местоположение',
            'contact_phone' => 'Телефоны (с указанием кода города)',
            'contact_email' => 'Адрес электронной почты',
            'start_capital' => 'Размер уставного капитала',
            'registration_date' => 'Дата регистрации',
            'employee_count' => 'Численность сотрудников',
            'bank_details' => 'Банковские реквизиты',
            'authorized_head_contact' => 'ФИО руководителя',
            'authorized_sign_contact' => 'ФИО уполномоченного лица',
            'authorized_security_contact' => 'ФИО сотрудника безопасности',
            'partner_type' => 'Вид контрагента',

            'verification_status_sn' => 'Статус',
            'verification_comment' => 'Комментарий проверки',
            'created_by' => 'ID создателя',
            'verified_at' => 'Проверен',
            'created_at' => 'Дата регистрации',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return string[]
     */
    public static function partnerTypeList()
    {
        return [
            self::PARTNER_TYPE_FEDERAL => 'Федеральный',
            self::PARTNER_TYPE_REGIONAL => 'Региональный',
        ];
    }

    /**
     * Relations with 'user'
     *
     * @return        User $user
     * @property-read User $user
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * find organization by inn
     *
     * @param string $inn
     * @return Organization
     */
    public static function findByINN($inn)
    {
        return self::findOne(['inn' => $inn]);
    }

    /**
     * Find organization by current user
     *
     * @return Organization
     */
    public static function findByCurrentUser()
    {
        return self::findOne(['id' => Yii::$app->user->identity->organization_id]);
    }

    /**
     * Find organization by membership request of current user
     *
     * @return Organization
     */
    public static function findByMembemshipRequest()
    {
        $membershipRequest = MembershipRequest::findOne(['user_id' => Yii::$app->user->identity->id]);
        return self::findOne(['id' => $membershipRequest['organization_id']]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArendators_list()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'arendators_list']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNo_debts_reference()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'no_debts_reference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent_contract()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'rent_contract']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse_cadastral_passport()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'warehouse_cadastral_passport']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty_rights()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'property_rights']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax_stamp()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'tax_stamp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax_registration_ul()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'tax_registration_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutive_unit_definition()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'executive_unit_definition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoa_for_signing()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'poa_for_signing']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHead_signature()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'head_signature']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancial_report()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'financial_report']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport_ip()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'passport_ip']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax_registration_ip()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'tax_registration_ip']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerification_status()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'warehouse_verification_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerificationcomment()
    {
        return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'verification_comment']);
    }

}

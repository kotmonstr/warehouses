<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\validators\DictItemValidator;
use app\models\WhLoadUnloadEquipment;

/**
 * This is the model class for table 'wh_load_unload_equipment_lu_type'.
 *
 * @property      int $id
 * @property      int $equipment_id
 * @property      string $loading_unloading_method_sn
 * @property      int $created_at
 * @property      int $updated_at
 * @property-read WhLoadUnloadEquipment $equipment
 */
class WhLoadUnloadEquipmentLuType extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'wh_load_unload_equipment_lu_type';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['equipment_id', 'loading_unloading_method_sn'], 'required'],
            [['equipment_id'], 'integer'],
            [['loading_unloading_method_sn'], DictItemValidator::class, 'dictionary_sn' => 'loading_unloading_methods'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'equipment_id' => 'ID техники',
            'loading_unloading_method_sn' => 'Способ загрузки',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * Relations with 'wh_load_unload_equipment'
     *
     * @property-read WhLoadUnloadEquipment $equipment
     * @return        WhLoadUnloadEquipment $equipment
     */
    public function getEquipment()
    {
        return $this->hasOne(WhLoadUnloadEquipment::class, ['id' => 'equipment_id']);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\AttributesBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\UploadedFile;
use app\traits\SoftDeleteTrait;
use app\traits\FindModelTrait;
use yii\helpers\ArrayHelper;
use app\validators\NumberValidator;

/**
 * This is the model class for table 'reserve'.
 *
 * @property      int $id
 * @property      int $advert_id
 * @property      float $amount
 * @property      string $amount_type_sn
 * @property      float $cost
 * @property      int $rent_from
 * @property      int $rent_to
 * @property      string $contact_fio
 * @property      string $contact_phone_number
 * @property      string $contact_email
 * @property      string $comment
 * @property      string $status_sn
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 * @property-read Advert $advert
 */
class Reserve extends ActiveRecord
{
    use SoftDeleteTrait;
    use FindModelTrait;

    const STATUS_ACTIVE = 'active';
    const STATUS_CANCELLED = 'canceled';

    const FILES_MAX_COUNT = 30;

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'reserve';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => AttributesBehavior::class,
                'attributes' => [
                    'rent_from' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_BEFORE_UPDATE => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_AFTER_FIND    => function ($event, $attribute) {return Yii::$app->formatter->asDate($this->$attribute, 'yyyy-MM-dd'); },
                    ],
                    'rent_to' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_BEFORE_UPDATE => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_AFTER_FIND    => function ($event, $attribute) {return Yii::$app->formatter->asDate($this->$attribute, 'yyyy-MM-dd'); },
                    ],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['status_sn'], 'default', 'value' => self::STATUS_ACTIVE],
            [
                [
                     'amount', 'amount_type_sn', 'cost', 'contact_phone_number',
                    'rent_from', 'rent_to', 'contact_fio', 'contact_email', 'status_sn', 'advert_id'
                ], 'required'
            ],
            [['advert_id'], 'integer'],
            //[['advert_id'], 'validatePublished'],
            [['amount', 'cost'], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'],
            [['contact_phone_number'], 'string', 'min' => 18, 'message' => 'Введите номер телефона полностью'],
            [
                [
                    'amount_type_sn', 'contact_fio',
                    'contact_phone_number', 'contact_email'
                ], 'string', 'max' => 255
            ],
            [['contact_email'], 'email'],
            [['comment', 'status_sn'], 'string'],
            [['deleted_at'], 'default', 'value' =>  null],
            [['rent_to'], 'compare', 'compareAttribute' => 'rent_from', 'operator' => '>', 'skipOnEmpty' => true],
            [['amount'], 'validateSquare'],
            [['rent_to', 'rent_from'], 'validateDates'],
        ];
    }

    /**
     * Validates dates,
     * this method serves as the inline validation for dates.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateDates($attribute, $params) {
        switch ($attribute) {
        case 'rent_from':
            if (strtotime($this->$attribute) < (time() - (60 * 60 * 24))) {
                $this->addError($attribute, 'Дата должна быть не раньше сегодня');
            }
            if (strtotime($this->$attribute) < strtotime($this->advert->available_from)) {
                $this->addError($attribute, 'Дата должна быть не раньше даты начала объявления ' . $this->advert->available_from);
                return false;
            }
            break;
        case 'rent_to':
            if (strtotime($this->$attribute) > (time() + (60 * 60 * 24 * 365 * 5))) {
                $this->addError($attribute, 'Дата должна быть не позже, чем через 5 лет');
            }
            if (strtotime($this->$attribute) > strtotime($this->advert->available_to)) {
                $this->addError($attribute, 'Дата должна быть не позже даты окончания объявления ' . $this->advert->available_to);
                return false;
            }
            break;
        }
    }

    /**
     * Validates square,
     * this method serves as the inline validation for availabel square.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateSquare($attribute, $params) {
        if ($this->advert->available_space <= 0) {
            $this->addError($attribute, 'У объявления не осталось свободных площадей');
            return;
        }

        if ($this->$attribute == 0) {
            $this->addError($attribute, "Бронируемая площадь должна быть больше 0");
            return;
        }

        if ($this->$attribute > $this->advert->available_space) {
            $this->addError($attribute, "У объявления осталось всего {$this->advert->available_space} м² для бронирования");
            return;
        }
    }

    /**
     * Validates published,
     * this method serves as the inline validation for advert status.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validatePublished($attribute, $params) {
        if ($this->advert->status_sn != Advert::STATUS_PUBLISHED) {
            $this->addError($attribute, 'Можно добавить бронь только к опубликованному объявлению.');
            return;
        }
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'advert_id' => 'Объявление',
            'amount' => 'Кол-во',
            'amount_type_sn' => 'ед.',
            'cost' => 'Стоимость',
            'rent_from' => 'С',
            'rent_to' => 'По',
            'contact_fio' => 'ФИО',
            'contact_phone_number' => 'Телефон',
            'contact_email' => 'Email',
            'comment' => 'Комментарий',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
            'status_sn' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['id' => 'advert_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'status_sn']);
    }

    /**
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function cancel()
    {
        if ($this->status_sn == self::STATUS_CANCELLED) {
            return true;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if (!self::updateAll(['status_sn' => self::STATUS_CANCELLED], ['id' => $this->id])) {
            $transaction->rollBack();
            throw new Exception('Can\'t cancel reserve');
        }

        $advert = Advert::findOne($this->advert_id);
        $advert->status_sn = Advert::STATUS_ADJUSTMENT;
        if (!$advert->save()) {
            $transaction->rollBack();
            throw new Exception('Can\'t change advert status');
        }

        $notification = new Notification();
        $notification->user_id = $this->advert->created_by;
        $notification->title = 'Бронь вашего объявления была отменена';
        $notification->text = '<a href="/panel/reserve/view?id=' . $this->id . '">' .
                                'Бронь</a> по вашему ' .
                                '<a href="/panel/advert/update?id=' . $advert->id . '">' .
                                'объявлению</a>' .
                                ' была отменена. Пожалуйста скорректируйте доступную площадь.';

        if (!$notification->save()) {
            $transaction->rollBack();
            throw new Exception('Can\'t notify user about reserve cancelation');
        }

        $this->status_sn = self::STATUS_CANCELLED;

        $transaction->commit();

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null )
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        Advert::updateAll(['status_sn' => Advert::STATUS_ADJUSTMENT], ['id' => $this->advert_id]);

        if (!parent::save($runValidation, $attributeNames)) {
            $transaction->rollBack();
            throw new Exception('Can\'t save reserve');
        }

        $notification = new Notification();
        $notification->user_id = $this->advert->created_by;
        $notification->title = 'Объявление забронировано';
        $notification->text = 'Забронировано ' .
                                '<a href="/panel/advert/update?id=' . $this->advert_id . '">' .
                                'объявление</a> для склада ' . $this->advert->warehouseName .
                                '. Перейти к ' .
                                '<a href="/panel/reserve/view?id=' . $this->id . '">' .
                                'брони</a>.';
        if (!$notification->save()) {
            $transaction->rollBack();
            throw new Exception('Can\'t notify user about reserve');
        }

        $transaction->commit();

        return true;
    }
}

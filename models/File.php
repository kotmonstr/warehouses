<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'file'.
 *
 * @property integer $id
 * @property integer $document_id
 * @property string  $name
 * @property string  $path
 * @property integer $size
 * @property string  $mime_type
 * @property integer $created_at
 *
 * @property-read Document $document
 */
class File extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'file';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['document_id', 'name', 'path', 'size', 'mime_type'], 'required'],
            [['document_id', 'size'], 'integer'],
            [['name', 'path', 'mime_type'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'document_id' => 'ID документа',
            'name' => 'Название',
            'path' => 'Путь',
            'size' => 'Размер',
            'mime_type' => 'MIME',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::class, ['id' => 'document_id']);
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return (file_exists(Yii::getAlias('@webroot') . $this->path));
    }
}

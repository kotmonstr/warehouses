<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table 'user_role'.
 *
 * @property      int $id
 * @property      int $user_id
 * @property      int $role_id
 * @property      int $created_at
 * @property-read Role $role
 * @property-read User $user
 */
class UserRole extends ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'user_role';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['user_id', 'role_id'], 'integer'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID пользователя',
            'role_id' => 'ID роли',
            'created_at' => 'Создан',
        ];
    }

    /**
     * Relations with 'role'
     *
     * @property-read Role $role
     * @return        Role $role
     */
    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }

    /**
     * Relations with 'user'
     *
     * @property-read User $user
     * @return        User $user
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}

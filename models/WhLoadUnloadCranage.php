<?php

namespace app\models;

use app\validators\NumberValidator;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\validators\DictItemValidator;
use app\models\WhLoadUnloadCranageLuType;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table 'wh_load_unload_cranage'.
 *
 * @property      int $id
 * @property      int $warehouse_id
 * @property      string $loading_unloading_tech_type_sn
 * @property      int $amount
 * @property      float $capacity
 * @property      string $capacity_type_sn
 * @property      string[] $loading_unloading_method_sn
 * @property      int $created_at
 * @property      int $updated_at
 * @property-read Warehouse $warehouse
 * @property-read WhLoadUnloadCranageLuType[] $lu_type
 */
class WhLoadUnloadCranage extends ActiveRecord
{
    public $loading_unloading_method_sn;

    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'wh_load_unload_cranage';
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [
                [
                    'warehouse_id', 'loading_unloading_tech_type_sn', 'amount',
                    'capacity', 'capacity_type_sn', 'loading_unloading_method_sn',
                ], 'required'
            ],
            [['warehouse_id', 'amount'], 'integer'],
            [
                [
                    'loading_unloading_tech_type_sn', 'capacity_type_sn',
                ], 'string', 'max' => 255
            ],
            [['loading_unloading_tech_type_sn'], DictItemValidator::class, 'dictionary_sn' => 'cranage_types'],
            [['capacity_type_sn'], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'mass'],
            [['loading_unloading_method_sn'], 'each', 'rule' =>  [DictItemValidator::class, 'dictionary_sn' => 'loading_unloading_methods']],
            [['capacity'], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'
            ],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => 'ID склада',
            'loading_unloading_tech_type_sn' => 'Вид',
            'amount' => 'Кол-во',
            'capacity' => 'Грузоподъемность',
            'capacity_type_sn' => 'Грузоподъемность (единица измерения)',
            'loading_unloading_method_sn' => 'Способ загрузки',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * Relations with 'warehouse'
     *
     * @property-read Warehouse $warehouse
     * @return        Warehouse $warehouse
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }

    /**
     * Relations with 'wh_load_unload_cranage_lu_type'
     *
     * @property-read WhLoadUnloadCranageLuType $lu_type
     * @return        WhLoadUnloadCranageLuType $lu_type
     */
    public function getLu_type()
    {
        return $this->hasMany(WhLoadUnloadCranageLuType::class, ['equipment_id' => 'id']);
    }

    /**
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null )
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if (!parent::save($runValidation, $attributeNames)) {
            $transaction->rollBack();
            throw new Exception('Can\'t save cranage');
        }

        if (count($this->loading_unloading_method_sn)) {
            foreach ($this->loading_unloading_method_sn as $lu_method) {
                $wh_luclt = new WhLoadUnloadCranageLuType();
                $wh_luclt->loading_unloading_method_sn = $lu_method;
                $wh_luclt->equipment_id = $this->id;
                if (!$wh_luclt->save()) {
                    $transaction->rollBack();
                    throw new Exception(
                        'Can\'t save equipment lu type'
                    );
                }
            }
        }

        $transaction->commit();

        return true;
    }
}

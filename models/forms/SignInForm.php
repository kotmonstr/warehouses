<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * SignInForm is the model behind the SignInForm form.
 *
 * @property      string $username
 * @property      string $password
 * @property      bool $rememberMe
 * @property-read User|null $user
 */
class SignInForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['rememberMe'], 'boolean'],
            [['username', 'password'], 'string', 'max' => 255],
            [['username'], 'email'],
            [['password'], 'validatePassword'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'username' => 'Email',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить',
        ];
    }

    /**
     * Validates the password,
     * this method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findByUsername($this->username);

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $user = User::findByUsername($this->username);
        if ($user && $this->validate()) {
            return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Reset password function
     *
     * @param string $email for sending reseyed password
     *
     * @return bool
     */
    public function resetPassword($email)
    {
        $user = User::findByUsername($email);
        if ($user) {

            $user->generatePasswordResetToken();

            if ($user->save()) {

                $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(
                    [
                        'user/reset-password-confirm'
                    ]
                ) . '?token=' . $user->password_reset_token;
                $sent = Yii::$app->mailer
                    ->compose(
                        [
                            'html' => 'user/password_reset_html',
                            'text' => 'user/password_reset_text',
                        ],
                        [
                            'user' => $user,
                            'confirmLink' => $confirmLink,
                        ]
                    )
                    ->setTo($user->email)
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject('Сброс пароля')
                    ->send();

                if ($sent) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Confirm by token
     *
     * @param string $token for confirm
     * @param string $password new password
     *
     * @return bool|User
     */
    public function confirm($token, $password)
    {
        $user = User::findByPasswordResetToken($token);
        if ($user) {
            $user->removePasswordResetToken();
            $user->setPassword($password);
            if ($user->save()) {
                return $user;
            }
        }
        return false;
    }
}

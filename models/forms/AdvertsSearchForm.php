<?php

namespace app\models\forms;

use app\models\Organization;
use app\models\Warehouse;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Advert;
use Yii;

/**
 * This is the model class for search in table 'advert'
 *
 * @property integer $available_from
 * @property integer $available_to
 * @property integer $unit_cost_from
 * @property integer $unit_cost_to
 * @property string  $city
 */
class AdvertsSearchForm extends Model
{
    public $available_from;
    public $available_to;
    public $unit_cost_from;
    public $unit_cost_to;
    public $city;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>=', 'skipOnEmpty' => true],
            [['unit_cost_to'], 'compare', 'compareAttribute' => 'unit_cost_from', 'operator' => '>=', 'skipOnEmpty' => true],
            [['unit_cost_from', 'unit_cost_to'], 'double'],
            [['available_from', 'available_to', 'city'], 'string'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'available_from' => 'Доступно с',
            'available_to' => 'Доступно по',
            'unit_cost_from' => 'Цена за м², от',
            'unit_cost_to' => 'Цена за м², до',
            'city' => 'Город',
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = Advert::find();

        $query
            ->joinWith('organization')
            ->joinWith('warehouse')
            ->where([
                'organization.verification_status_sn' => Organization::VERIFICATION_VERIFIED,
                'warehouse.dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                'warehouse.sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                'advert.status_sn' => Advert::STATUS_PUBLISHED,
                'organization.deleted_at' => null,
                'warehouse.deleted_at' => null,
                'advert.deleted_at' => null,
            ])
            ->andWhere(['>=', 'advert.available_to', time()]);


        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10
                ]
            ]
        );


        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        if ($this->unit_cost_from) {
            $query->andFilterWhere(['>=', 'advert.unit_cost_per_day', $this->unit_cost_from]);
        }
        if ($this->unit_cost_to) {
            $query->andFilterWhere(['<=', 'advert.unit_cost_per_day', $this->unit_cost_to]);
        }
        $query->andFilterWhere(['warehouse.address_city_ym_geocoder' => $this->city])
            ->andFilterWhere(['<=', 'available_from', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['>=', 'available_to', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['<=', 'available_from', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null])
            ->andFilterWhere(['>=', 'available_to', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null]);

        return $dataProvider;
    }
}

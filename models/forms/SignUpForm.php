<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * SignUpForm is the model behind the SignUpForm form.
 *
 * @property      string $username
 * @property      string $password
 * @property      string $name
 * @property      bool $accept
 * @property      bool $rememberMe
 * @property-read User|null $user
 */
class SignUpForm extends Model
{
    public $username;
    public $password;
    public $name;
    public $accept = false;
    public $rememberMe = true;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'accept'], 'required'],
            [['username', 'password', 'name'], 'string', 'max' => 255],
            ['username', 'email'],
            ['username','validateUsername'],
            [['accept', 'rememberMe'], 'boolean'],
            ['accept', 'compare', 'compareValue' => true, 'message' => 'You should accept.'],
        ];
    }

    /**
     * Check if the user email is unique
     *
     * @param string       $attribute email of user for sign up
     * @param string|array $params    parameters
     *
     * @return null
     */
    public function validateUsername($attribute, $params)
    {
        if (User::findByUsername($this->$attribute, false) != null) {
            $this->addError($attribute, 'Email already exist. Please try another one.');
        }
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'username' => 'Email',
            'password' => 'Пароль',
            'name' => 'Имя',
            'rememberMe' => 'Запомнить',
            'accept' => 'Я даю согласие на обработку персональных данных',
        ];
    }


    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            $user = new User();
            $user->setPassword($this->password);
            $user->role = User::ROLE_HOLDER;
            $user->first_name = $this->name;
            $user->email = $this->username;
            $user->email_verification_token = Yii::$app->getSecurity()->generateRandomString();
            $user->email_verified = false;

            if ($user->save()) {

                $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(
                    ['user/sign-up-confirm', 'token' => $user->email_verification_token]
                );
                $sent = Yii::$app->mailer
                    ->compose(
                        [
                            'html' => 'user/email_confirm_html',
                            'text' => 'user/email_confirm_text',
                        ],
                        [
                            'user' => $user,
                            'confirmLink' => $confirmLink,
                        ]
                    )
                    ->setTo($user->email)
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject('Подтверждение регистрации')
                    ->send();

                if ($sent) {
                    return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
                }
            }
        }
        return false;
    }

    /**
     * Confirm by token
     *
     * @param string $token for confirm
     *
     * @return bool|User
     */
    public function confirm($token)
    {
        $user = User::findByEmailVerificationToken($token);
        if ($user) {
            $user->email_verification_token = null;
            $user->email_verified = true;
            if ($user->save()) {
                return $user;
            }
        }
        return false;
    }
}

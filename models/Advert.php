<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributesBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Organization;
use app\traits\FindModelTrait;
use app\traits\SoftDeleteTrait;
use app\models\Reserve;
use yii\helpers\ArrayHelper;
use app\models\Warehouse;
use app\validators\NumberValidator;

/**
 * This is the model class for table 'advert'
 *
 * @property      int $id
 * @property      int $warehouse_id
 * @property      string $storage_type_sn
 * @property      float $available_space
 * @property      string $available_space_type_sn
 * @property      float $unit_cost_per_day
 * @property      int $available_from
 * @property      int $available_to
 * @property      string $manager_name
 * @property      string $manager_phone_number
 * @property      string $manager_email
 * @property      string $comment
 * @property      string $status_sn
 * @property      int $created_by
 * @property      int $created_at
 * @property      int $deleted_at
 * @property      int $updated_at
 * @property-read User $user
 * @property-read Warehouse $warehouse
 * @property-read DictionaryItem $available_space_type
 *
 * @property string $organizationName
 * @property string $warehouseName
 * @property string $classification
 *
 */
class Advert extends ActiveRecord
{
    use FindModelTrait;
    use SoftDeleteTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advert';
    }

    /**
     * Constants for status
     */
    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAFT = 'draft';
    const STATUS_ADJUSTMENT = 'adjustment';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => AttributesBehavior::class,
                'attributes' => [
                    'available_from' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_BEFORE_UPDATE => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_AFTER_FIND    => function ($event, $attribute) {return Yii::$app->formatter->asDate($this->$attribute, 'yyyy-MM-dd'); },
                    ],
                    'available_to' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_BEFORE_UPDATE => function ($event, $attribute) {return Yii::$app->formatter->format($this->$attribute, 'timestamp'); },
                        ActiveRecord::EVENT_AFTER_FIND    => function ($event, $attribute) {return Yii::$app->formatter->asDate($this->$attribute, 'yyyy-MM-dd'); },
                    ],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [
                [
                    'warehouse_id', 'storage_type_sn', 'available_space',
                    'available_space_type_sn', 'unit_cost_per_day', 'available_from',
                    'available_to', 'manager_name', 'manager_email', 'manager_phone_number',
                    'status_sn', 'created_by'
                ], 'required'
            ],
            [['warehouse_id', 'created_by'], 'integer'],
            [
                [
                    'storage_type_sn', 'available_space_type_sn', 'manager_name',
                    'manager_phone_number', 'manager_email', 'status_sn',
                    'organizationName', 'warehouseName', 'classification'
                ], 'string', 'max' => 255
            ],
            [['manager_phone_number'], 'string', 'min' => 18, 'message' => 'Введите номер телефона полностью'],
            [['available_space', 'unit_cost_per_day'], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'],
            [['manager_email'], 'email'],
            [['comment'], 'string'],
            [['deleted_at'], 'default', 'value' =>  null],
            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>', 'skipOnEmpty' => true],
            [['available_to', 'available_from'], 'validateDates'],
            [['available_space'], 'validateSquare'],
        ];
    }

    /**
     * Validates dates,
     * this method serves as the inline validation for dates.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateDates($attribute, $params) {
        if ($this->status_sn == Advert::STATUS_PUBLISHED) {
            switch ($attribute) {
            case 'available_from':
                if (strtotime($this->$attribute) < (time() - (60 * 60 * 24))) {
                    $this->addError($attribute, 'Дата должна быть не раньше сегодня');
                }
                break;
            case 'available_to':
                if (strtotime($this->$attribute) > (time() + (60 * 60 * 24 * 365 * 5))) {
                    $this->addError($attribute, 'Дата должна быть не позже, чем через 5 лет');
                }
                break;
            }
        }
    }

    /**
     * Validates square,
     * this method serves as the inline validation for availabel square.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateSquare($attribute, $params) {
        if ($this->status_sn == Advert::STATUS_PUBLISHED) {
            $advertsQuery = Advert::find()->where(['warehouse_id' => $this->warehouse_id, 'deleted_at' => null])
                ->andWhere(['<=', 'available_from', Yii::$app->formatter->format($this->available_to, 'timestamp')])
                ->andWhere(['>=', 'available_to', Yii::$app->formatter->format($this->available_from, 'timestamp')]);

            if ($this->id) {
                $advertsQuery->andWhere('id != ' . $this->id);
            }

            $advertSquares = ArrayHelper::map($advertsQuery->all(), 'id', 'available_space');

            $totalAdvertSquares = 0;
            $advertIdxs = [];

            if (count($advertSquares) > 0) {
                $totalAdvertSquares = array_sum($advertSquares);
                $advertIdxs = array_keys($advertSquares);
            }

            if ($this->id) {
                array_push($advertIdxs, $this->id);
            }

            if (count($advertIdxs)) {
                $reserveSquares = ArrayHelper::getColumn(
                    Reserve::find()->where(
                        [
                            'deleted_at' => null,
                            'status_sn' => Reserve::STATUS_ACTIVE,
                            'advert_id' => $advertIdxs
                        ])
                        ->andWhere(['<=', 'rent_from', Yii::$app->formatter->format($this->available_to, 'timestamp')])
                        ->andWhere(['>=', 'rent_to', Yii::$app->formatter->format($this->available_from, 'timestamp')])
                        ->all(),
                    'amount'
                );

                if (count($reserveSquares)) {
                    $totalAdvertSquares += array_sum($reserveSquares);
                }
            }

            $restSquare = Warehouse::findModel($this->warehouse_id)->square_area - $totalAdvertSquares;

            if ($restSquare <= 0) {
                $this->addError($attribute, 'У склада не осталось свободных площадей');
                return;
            }

            if ($this->$attribute == 0) {
                $this->addError($attribute, "Сдаваемая площадь должна быть больше 0");
                return;
            }

            if ($this->$attribute > $restSquare) {
                $this->addError($attribute, "У склада осталось всего {$restSquare} м² под сдачу");
                return;
            }
        }
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'warehouse_id' => 'Склад',
            'storage_type_sn' => 'Тип',
            'available_space' => 'Площадь',
            'available_space_type_sn' => 'ед.',
            'unit_cost_per_day' => '₽/м²/день',
            'available_from' => 'Доступно с',
            'available_to' => 'Доступно по',
            'manager_name' => 'Имя менеджера',
            'manager_phone_number' => 'Телефон',
            'manager_email' => 'email',
            'comment' => 'Комментарий',
            'status_sn' => 'Статус',
            'created_by' => 'ID создателя',
            'created_at' => 'Создан',
            'deleted_at' => 'Удален',
            'updated_at' => 'Обновлен',
            'organizationName' => 'Компания',
            'warehouseName' => 'Склад',
            'classification' => 'Класс помещения',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id'])->via('warehouse');
    }

    /**
     * @return ActiveQuery
     */
    public function getStorage_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'storage_type_sn']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAvailable_space_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'available_space_type_sn']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'status_sn']);
    }

    /**
     * @property string $organizationName
     * @return   string $organizationName
     */
    public function getOrganizationName()
    {
        return Organization::findOne(['id' => $this->warehouse->organization_id])['title'];
    }

    /**
     * @property string $warehouseName
     * @return   string $warehouseName
     */
    public function getWarehouseName()
    {
        return $this->warehouse->title;
    }

    /**
     * @property string $classification
     * @return   string $classification
     */
    public function getClassification()
    {
        return $this->warehouse->classification_sn;
    }
}

<?php

namespace app\modules\api\docs\controllers;

use yii\web\Controller;

/**
 * Class DocsController
 * @package app\modules\api\docs\controllers
 */
class DocsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $name
     * @return string
     */
    public function actionView($name)
    {
        return $this->render('yaml/' . $name . '.yaml');
    }
}
<?php /* @var $this \yii\web\View */ ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XPL Temporary API</title>
</head>
<body>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.17.3/swagger-ui.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.17.3/swagger-ui-bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.17.3/swagger-ui-standalone-preset.js"></script>

<div id="swagger-ui-container">

</div>


</body>
</html>

<script>
    window.swaggerUi = new SwaggerUIBundle({
        dom_id: '#swagger-ui-container',
        urls: [
            {url: '/docs/dictionary', name: 'Справочник'},
            {url: '/docs/user', name: 'Пользователь'},
            {url: '/docs/organization', name: 'Профиль: Компания'},
            {url: '/docs/team', name: 'Профиль: Команда'},
            {url: '/docs/warehouse', name: 'Склад'},
            {url: '/docs/notification', name: 'Уведомление'},
            {url: '/docs/advert', name: 'Объявление'},
            {url: '', name: '------------------'},
            {url: '/docs/adm_user', name: 'Админ: Пользователи'},
            {url: '/docs/adm_organization', name: 'Админ: Компании'},
        ],
        presets: [
            SwaggerUIBundle.presets.apis,
            SwaggerUIStandalonePreset
        ],
        plugins: [
            SwaggerUIBundle.plugins.DownloadUrl,
        ],
        layout: "StandaloneLayout"
    });
</script>


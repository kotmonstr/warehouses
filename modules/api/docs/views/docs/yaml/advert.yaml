openapi: 3.0.0
info:
  title: "Объявление"
  version: 0.0.0
  description: "Описание внешнего API, Объявление"
paths:

  ## Создание / редактирование

  /v0/advrt/create:
    post:
      tags:
        - Создание / редактирование
      summary: Создание объявления (черновик)
      security:
        - jwt: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '/docs/_forms#/components/AdvertUpdateForm'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    description: Идентификатор созданного объявления
                    example: 76
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'
  /v0/advrt/<id>/update:
    post:
      tags:
        - Создание / редактирование
      summary: Редактирование объявления (черновик)
      security:
        - jwt: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '/docs/_forms#/components/AdvertUpdateForm'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    description: Идентификатор созданного объявления
                    example: 76
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'
  /v0/advrt/<id>/publish:
    post:
      tags:
        - Создание / редактирование
      summary: Опубликовать объявление
      security:
        - jwt: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    description: Идентификатор созданного объявления
                    example: 76
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'

  ## Список / просмотр

  /v0/advrt/index:
    post:
      tags:
        - Список / просмотр
      summary: Список складов
      security:
        - jwt: []
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                warehouse_id:
                  type: integer
                  description: Фильтр по складу
                  example: 76
                city:
                  type: string
                  description: Фильтр по городу склада (для списка есть временный метод /v0/wh_cities/my, описан в разделе "Справочник")
                  example: Москва
                available_from:
                  type: string
                  description: Фильтр по датам брони (от)
                  example: "2020-08-01"
                available_to:
                  type: string
                  description: Фильтр по датам брони (до)
                  example: "2020-10-01"
                storage_type_sn:
                  type: string
                  description: Фильтр по типу хранения (dictionary_sn = storage_system, type = default)
                  example: some_dictionary_item_system_name
                status_sn:
                  type: string
                  description: Фильтр по статусу (dictionary_sn = advert_status, type = default)
                  example: some_dictionary_item_system_name
                classification_sn:
                  type: string
                  description: Фильтр по классу склада (dictionary_sn = classification, type = default)
                  example: some_dictionary_item_system_name
                limit:
                  type: integer
                  description: Количество элементов
                  example: 20
                  default: 20
                offset:
                  type: integer
                  description: Смещение получаемого результата
                  example: 0
                  default: 0
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    type: string
                    description: Количество запросов
                    example: 1
                  items:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: string
                          description: Идентификатор объявления
                          example: 44
                        warehouse:
                          $ref: '/docs/_models#/components/WarehouseShort'
                        storage_type:
                          $ref: '/docs/_models#/components/DictionaryItem'
                        available_space:
                          type: string
                          description: Доступная площадь
                          example: "50.00"
                        available_space_type:
                          $ref: '/docs/_models#/components/DictionaryItem'
                        unit_cost_per_day:
                          type: string
                          description: Цена за м²/день
                          example: "26.00"
                        available_from:
                          type: string
                          description: Доступно с
                          example: "2020-08-01"
                        available_to:
                          type: string
                          description: Доступно по
                          example: "2020-12-31"
                        status:
                          $ref: '/docs/_models#/components/DictionaryItem'
                        created_at:
                          type: integer
                          description: Дата создания объявления
                          example: 1594727349
  /v0/advrt/<id>/view:
    post:
      tags:
        - Список / просмотр
      summary: Просмотр объявления
      security:
        - jwt: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: string
                    description: Идентификатор объявления
                    example: 44
                  warehouse:
                    $ref: '/docs/_models#/components/WarehouseShort'
                  storage_type:
                    $ref: '/docs/_models#/components/DictionaryItem'
                  available_space:
                    type: string
                    description: Доступная площадь
                    example: "50.00"
                  available_space_type:
                    $ref: '/docs/_models#/components/DictionaryItem'
                  unit_cost_per_day:
                    type: string
                    description: Цена за м²/день
                    example: "26.00"
                  available_from:
                    type: string
                    description: Доступно с
                    example: "2020-08-01"
                  available_to:
                    type: string
                    description: Доступно по
                    example: "2020-12-31"
                  manager_name:
                    type: string
                    description: Имя менеджера
                    example: "Петров Петр Петрович"
                  manager_phone_number:
                    type: string
                    description: Телефон менеджера
                    example: "+7 (111) 111-11-11"
                  manager_email:
                    type: string
                    description: Email менеджера
                    example: "test_manager_email@test_manager_email.com"
                  comment:
                    type: string
                    description: Комментарий
                    example: ""
                  status:
                    $ref: '/docs/_models#/components/DictionaryItem'
                  user:
                    $ref: '/docs/_models#/components/UserShort'
                  created_at:
                    type: integer
                    description: Дата создания объявления
                    example: 1594727349
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'

  ## Удаление

  /v0/advrt/<id>/delete:
    post:
      tags:
        - Удаление
      summary: Удалить объявление
      security:
        - jwt: []
      responses:
        '200':
          description: OK
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'
openapi: 3.0.0
info:
  title: "Справочник"
  version: 0.0.0
  description: "Описание внешнего API, Справочник"
paths:

  ## Список / просмотр

  /v0/dictionary/<system_name>/items:
    post:
      tags:
        - Список / просмотр
      summary: Список элементов справочника
      description: 'Метод возвращает список элементов запрошенного справочника для вывода инпута в виде селекта или набора чекбоксов.<br>
                    system_name справочника (параметр в урле) для конкретного селектора вы сможете узнать из описания этого каждого конкретного поля в swagger.<br>
                    В случае, когда имя селекта содержит "_sn" - в виде значения вам необходимо передавать именно system_name выбранного элемента.<br>
                    В иных случаях (пока в системе не встечаются) - в виде значения вам необходимо передавать значение id выбранного элемента.<br>'
      security:
        - jwt: []
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                type:
                  type: string
                  description: Тип элемента справочника
                  example: square
                limit:
                  type: integer
                  description: Количество элементов
                  example: 20
                  default: 20
                offset:
                  type: integer
                  description: Смещение получаемого результата
                  example: 0
                  default: 0
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    type: string
                    description: Количество элементов
                    example: 1
                  items:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: string
                          description: Идентификатор элемента
                          example: 76
                        system_name:
                          type: string
                          description: Системное имя элемента
                          example: meter
                        title:
                          type: string
                          description: Название элемента
                          example: Метр квадратный
                        title_short:
                          type: string
                          description: Сокращённое название элемента
                          example: м2
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'

  ## Города (временные методы)

  /v0/wh_cities/all:
    post:
      tags:
        - Города (временные методы)
      summary: Список городов по всем складам системы
      security:
        - jwt: []
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                title:
                  type: string
                  description: Поиск по части названия
                  example: Москв
                limit:
                  type: integer
                  description: Количество элементов
                  example: 20
                  default: 20
                offset:
                  type: integer
                  description: Смещение получаемого результата
                  example: 0
                  default: 0
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    type: string
                    description: Количество элементов
                    example: 1
                  items:
                    type: array
                    items:
                      type: string
                      description: Название города
                      example: Москва
  /v0/wh_cities/my:
    post:
      tags:
        - Города (временные методы)
      summary: Список городов по складам пользователя
      security:
        - jwt: []
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                title:
                  type: string
                  description: Поиск по части названия
                  example: Москв
                limit:
                  type: integer
                  description: Количество элементов
                  example: 20
                  default: 20
                offset:
                  type: integer
                  description: Смещение получаемого результата
                  example: 0
                  default: 0
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    type: string
                    description: Количество элементов
                    example: 1
                  items:
                    type: array
                    items:
                      type: string
                      description: Название города
                      example: Москва
        '400':
          $ref: '/docs/_responses#/components/BadRequest'
        '403':
          $ref: '/docs/_responses#/components/Unauthorized'
        '422':
          $ref: '/docs/_responses#/components/ValidationError'

  ## Геокодер

  /v0/geocoder/suggest:
    post:
      tags:
        - Геокодер
      summary: Список городов по всем складам системы
      description: "Метод позволяет передать строку (мин 3 символа), по которой будет возвращён набор (10) возможных вариантов адресов из Яндекс карт.<br>
        Так же метод позволяет передать текущие координаты пользователя (опционально), для того, чтобы результаты были отсортированы по удалённости от пользователя."
      security:
        - jwt: []
      requestBody:
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                query:
                  type: string
                  description: Поиск по части названия
                  example: Мира 13
                lat:
                  type: number
                  description: Координаты пользователя (Широта)
                  example: 34.134184
                lon:
                  type: number
                  description: Координаты пользователя (Долгота)
                  example: 44.921987
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  description: Название города
                  example: Казахстан, Акмолинская область, Кокшетау, улица Мира, 13
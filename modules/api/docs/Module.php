<?php

namespace app\modules\api\docs;

use Yii;
use yii\web\Response;

/**
 * Class Module
 * @package app\modules\api\docs
 */
class Module extends \yii\base\Module
{
    public $layout = false;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\docs\controllers';
}

<?php

namespace app\modules\api\v0\models;

use app\models\User;
use yii\db\ActiveQuery;

/**
 * Class UserMemberShip
 * @package app\modules\api\v0\models
 */
class UserMembershipRequest extends \app\models\User
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'user', // Todo: переделать swagger и формат ответа
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id'])->select([
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'organization_position'
        ])->asArray();
    }
}
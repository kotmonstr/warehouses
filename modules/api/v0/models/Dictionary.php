<?php

namespace app\modules\api\v0\models;

use yii\db\ActiveQuery;

/**
 * Class Dictionary
 * @package app\modules\api\v0\models
 */
class Dictionary extends \app\models\Dictionary
{
    /**
     * @param $dictionary_sn
     * @param string $type
     * @return ActiveQuery
     */
    public static function findDictItemsBySystemName($dictionary_sn, $type )
    {
        $type = $type ?: 'default';
        $dictItems = \app\modules\api\v0\models\DictionaryItem::find()
            ->where(['dictionary_sn' => $dictionary_sn, 'type' => $type])
            ->orderBy([
                'created_at' => SORT_ASC,
                'id' => SORT_ASC,
            ]);

        return $dictItems;
    }
}
<?php


namespace app\modules\api\v0\models;

/**
 * Class UserShort
 * @package app\modules\api\v0\models
 */
class UserShort extends \app\models\User
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'organization_position',
        ];
    }
}
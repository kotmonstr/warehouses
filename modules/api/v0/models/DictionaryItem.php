<?php

namespace app\modules\api\v0\models;

/**
 * Class DictionaryItem
 * @package app\modules\api\v0\models
 */
class DictionaryItem extends \app\models\DictionaryItem
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'system_name',
            'title',
            'title_short',
        ];
    }
}

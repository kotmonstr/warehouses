<?php


namespace app\modules\api\v0\models;


use app\models\File;

class Document extends \app\models\Document
{
    public function fields()
    {
        return [
            'id',
            'entity_field',
            'file_name',
            'file_path'
        ];
    }

    /**
     * @return false|string|null
     */
    public function getFile_name()
    {
        return $this->hasOne(File::class, ['document_id' => 'id'])->select('name')->scalar();
    }
}
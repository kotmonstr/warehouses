<?php

namespace app\modules\api\v0\models;

class WarehouseGeneral extends \app\models\Warehouse
{

    /**
     * @return array|false
     */
    public function fields()
    {

        return [
            'id',
            'title',
            'description',
            'address_ym_geocoder',
            'address_lat',
            'address_lon',
            'work_hours_24_7',
            'work_hours_weekdays_from',
            'work_hours_weekdays_to',
            'work_hours_weekends_from',
            'work_hours_weekends_to',
            'photo',
            'status',
            'verification_comment'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
         return $this->hasMany(Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'photo']);
    }

    public function getStatus()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'status_sn'])->andWhere(['dictionary_sn' => 'warehouse_status']);
    }


}

<?php

namespace app\modules\api\v0\models;

class WarehouseShort extends \app\models\Warehouse
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'description',
            'classification',
            'address_city_ym_geocoder',
            'address_ym_geocoder',
            'address_lat',
            'address_lon',
            'work_hours_24_7',
            'work_hours_weekdays_from',
            'work_hours_weekdays_to',
            'work_hours_weekends_from',
            'work_hours_weekends_to',
            'verification_status',
            'verification_comment',

            //extra filds:
            'photos',
            'status',
            'created_at'
        ];
    }
    //Warehouse index

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
       return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'photo']);
    }
}

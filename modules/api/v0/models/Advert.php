<?php

namespace app\modules\api\v0\models;

use yii\db\ActiveQuery;

/**
 * Class Warehouse
 * @package app\modules\api\v0\models
 */
class Advert extends \app\models\Advert
{
    public function fields()
    {
        $fields = parent::fields();

        return [
            'id',
            'warehouse',
            'storage_type',
            'available_space',
            'available_space_type',
            'unit_cost_per_day',
            'available_from',
            'available_to',
            'status',
            'created_at',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getStorage_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'storage_type_sn']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAvailable_space_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'available_space_type_sn']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(WarehouseShort::class, ['id' => 'warehouse_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'status_sn']);
    }
}
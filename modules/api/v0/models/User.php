<?php


namespace app\modules\api\v0\models;

use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\modules\api\v0\models
 */
class User extends \app\models\User
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'organization_id',
            'organization_position',
            'email',
            'email_verification_token',
            'email_verified',
            'phone_number',
            'password_hash',
            'password_reset_token',
            'verification_status_sn',
            'verification_comment',
            'verified_at',
            'created_at',
            'deleted_at',
            'updated_at',
            'role',
            'access_token',
            'access_token_expired_at',
            'refresh_token',
            'refresh_token_expired_at'
        ];
    }
}
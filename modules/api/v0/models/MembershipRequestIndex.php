<?php


namespace app\modules\api\v0\models;


use app\models\User;
use yii\db\ActiveQuery;
use app\models\MembershipRequest;


/**
 * Class UserMemberShip
 * @package app\modules\api\v0\models
 */
class MembershipRequestIndex extends MembershipRequest
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'user',
            'created_at'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->select([
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'organization_position'
        ])->asArray();
    }
}
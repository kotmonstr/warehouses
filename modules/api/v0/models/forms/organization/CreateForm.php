<?php

namespace app\modules\api\v0\models\forms\organization;

use app\models\Organization;
use app\models\Document;
use app\models\User;
use yii\base\Model;
use yii\base\ErrorException;
use Yii;

/**
 * Class CreateForm
 * @package app\modules\api\v0\models\forms\organization
 *
 * @property integer $id;
 *
 * @property string  $legal_type;
 * @property string  $inn;
 * @property string  $title;
 * @property string  $ogrn;
 * @property string  $kpp;
 * @property string  $okpo;
 * @property string  $partner_type;
 * @property string  $main_activity;
 * @property string  $address_ym_geocoder;
 * @property string  $post_address;
 * @property string  $fact_address;
 * @property string  $taxation_type;
 * @property string  $shareholders;
 * @property string  $start_capital;
 * @property string  $registration_date;
 * @property string  $edm_type;
 * @property integer $employee_count;
 * @property string  $bank_details;
 * @property string  $authorized_head_contact;
 * @property string  $authorized_sign_contact;
 * @property string  $authorized_security_contact;
 * @property string  $contact_phone;
 * @property string  $contact_email;
 */
class CreateForm extends Model
{
    public $id;

    public $legal_type;
    public $inn;
    public $title;
    public $ogrn;
    public $kpp;
    public $okpo;
    public $partner_type;
    public $main_activity;
    public $address_ym_geocoder;
    public $post_address;
    public $fact_address;
    public $taxation_type;
    public $shareholders;
    public $start_capital;
    public $registration_date;
    public $edm_type;
    public $employee_count;
    public $bank_details;
    public $authorized_head_contact;
    public $authorized_sign_contact;
    public $authorized_security_contact;
    public $contact_phone;
    public $contact_email;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['inn', 'title', 'ogrn', 'kpp', 'okpo', 'partner_type', 'main_activity', 'address_ym_geocoder', 'post_address',
                'fact_address', 'taxation_type', 'shareholders', 'start_capital', 'registration_date', 'edm_type', 'employee_count',
                'bank_details', 'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'contact_phone',
                'contact_email'], 'required'],

            [['inn', 'title', 'ogrn', 'kpp', 'okpo', 'partner_type', 'main_activity', 'address_ym_geocoder', 'post_address',
                'fact_address', 'taxation_type', 'shareholders', 'start_capital', 'registration_date',  'edm_type',
                'bank_details', 'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'contact_phone',
                'contact_email'], 'string'],

            [['employee_count'],'integer']
        ];
    }

    /** {@inheritDoc} */
    public function fields()
    {
        return ['id'];
    }
}

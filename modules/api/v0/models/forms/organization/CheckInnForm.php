<?php

namespace app\modules\api\v0\models\forms\organization;

use app\components\KonturApi;
use app\modules\api\v0\models\OrganizationShort;
use Yii;
use yii\base\Model;

/**
 * Class CheckInnForm
 * @package app\modules\api\v0\models\forms\organization
 *
 * @property string inn
 */
class CheckInnForm extends Model
{
    public $inn;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['inn'], 'required'],
            [['inn'], 'string'],

        ];
    }

    /**
     * @return OrganizationShort|bool
     */
    public function search()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = OrganizationShort::findOne(['inn' => $this->inn]);

        if (!$model) {
            $model = new OrganizationShort();

            /** @var KonturApi $konturApi */
            $konturApi = Yii::$app->konturApi;

            $data = $konturApi->findOrganizationByINN($this->inn);

            if (isset($data['error'])) {
                $this->addError('inn', 'Некорректный ИНН');

                return false;
            }

            $model->load($data, '');
        }

        return $model;
    }
}

<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\Warehouse;
use yii\base\ErrorException;
use yii\base\Model;
use Yii;

/**
 * Class UpdateFormInternal
 * @package app\modules\api\v0\models\forms\warehouse
 */
class UpdateFormInternal extends Model
{
    public $storage_system_sn;
    public $id;
    public $office_facility_types_inside_sn;
    public $office_facility_types_outside_sn;
    public $column_distance_type_sn;
    public $span_distance_type_sn;
    public $floor_covering_sn;
    public $height_above_ground_type_sn;
    public $load_per_square_meter_type_sn;
    public $fire_ext_system_sn;
    public $temperature_mode_sn;
    public $min_temperature_type_sn;
    public $max_temperature_type_sn;
    public $electricity_grid_sn;
    public $heating_system_sn;
    public $column_distance;
    public $span_distance;
    public $height_above_ground;
    public $load_per_square_meter;
    public $min_temperature;
    public $max_temperature;
    public $column_protection;
    public $anti_dust_covering;
    public $security_guard;
    public $security_video;
    public $security_alarm;
    public $ventilation;
    public $conditioning;
    public $fire_alarm;
    public $fire_charging;
    public $fire_tanks_and_pump;
    public $water_hot;
    public $water_sewerage;
    public $internet;
    public $phone;
    public $fiber_optic;
    public $water_cold;
    public $loading_unloading_struct_types_sn;
    public $cranage_type;
    public $loading_unloading_tech_type;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['storage_system_sn'], 'required'],

            [['storage_system_sn', 'office_facility_types_inside_sn', 'office_facility_types_outside_sn',
                'loading_unloading_struct_types_sn', 'cranage_type', 'loading_unloading_tech_type'], 'each', 'rule' => ['string']],

            [['column_distance_type_sn', 'span_distance_type_sn', 'floor_covering_sn', 'height_above_ground_type_sn', 'load_per_square_meter_type_sn',
                'fire_ext_system_sn', 'temperature_mode_sn', 'min_temperature_type_sn', 'max_temperature_type_sn', 'electricity_grid_sn',
                'heating_system_sn'], 'string'],

            [['column_distance', 'span_distance', 'height_above_ground', 'load_per_square_meter', 'min_temperature', 'max_temperature'], 'number'],

            [['column_protection', 'anti_dust_covering', 'security_guard', 'security_video', 'security_alarm', 'ventilation', 'conditioning',
                'fire_alarm', 'fire_charging', 'fire_tanks_and_pump', 'water_hot', 'water_cold', 'water_sewerage', 'internet',
                'phone', 'fiber_optic'], 'boolean'],


        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = Warehouse::findOne($id);
        if (!$model) {
            $this->addError('id', 'Склад не найден.');
            return false;
        }

        $model->load(Yii::$app->request->getBodyParams(), '');

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }
}
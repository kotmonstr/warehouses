<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\User;
use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\models\Warehouse;
use Yii;

/**
 * Class SearchForm
 * @package app\modules\api\v0\models\forms\warehouse
 */
class SearchForm extends BasicSearchForm
{
    public $organization_id;

    /** {@inheritDoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['organization_id'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function search()
    {
        $query = Warehouse::find()->where(['deleted_at' => null]);

        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        if (in_array($identity->role, [User::ROLE_ADMIN, User::ROLE_DSL, User::ROLE_SB])) {
            $query->andFilterWhere(['organization_id' => $this->organization_id]);
        } else {
            $query->andWhere(['organization_id' => $identity->organization_id]);
        }

        // Фильтр
//        if($this->verification_status){
//            $query->andWhere(['verification_status' => $this->verification_status]);
//        }

        $totalCount = $query->count();

        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }
}

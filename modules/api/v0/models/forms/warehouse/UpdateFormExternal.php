<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\Warehouse;
use yii\base\ErrorException;
use yii\base\Model;
use Yii;

/**
 * Class UpdateFormExternal
 * @package app\modules\api\v0\models\forms\warehouse
 */
class UpdateFormExternal extends Model
{
    public $id;
    public $accounting_system_sn;
    public $classification_sn;
    public $square_area;
    public $square_area_type_sn;
    public $built_up_area;
    public $built_up_area_type_sn;
    public $ceiling_height;
    public $ceiling_height_type_sn;
    public $floors;
    public $entries_amount;
    public $maneuvering_area;
    public $car_parking_sn;
    public $truck_parking_sn;
    public $automatic_gates_amount;
    public $loading_ramp;
    public $accounting_control_system_sn;
    public $outer_lighting;
    public $outer_security;
    public $outer_beautification;
    public $outer_fencing;
    public $distance_from_highway;
    public $distance_from_highway_type_sn;
    public $rail_road;
    public $truck_maneuvering_area;
    public $outer_entries_amount;
    public $vehicle_max_length;
    public $vehicle_max_length_type_sn;
    public $vehicle_max_height;
    public $vehicle_max_height_type_sn;
    public $vehicle_max_width;
    public $vehicle_max_width_type_sn;
    public $vehicle_max_weight;
    public $vehicle_max_weight_type_sn;
    public $from_the_owner;
    public $inn;
    public $kpp;
    public $address_city_ym_geocoder;

    /**
     * @return array
     */
    public function rules()
    {
        return [

            [['accounting_system_sn', 'classification_sn', 'square_area', 'square_area_type_sn', 'built_up_area',
                'built_up_area_type_sn', 'ceiling_height', 'ceiling_height_type_sn', 'floors', 'entries_amount', 'maneuvering_area',
                'car_parking_sn', 'truck_parking_sn', 'automatic_gates_amount', 'loading_ramp',
                'outer_lighting', 'outer_security', 'outer_security', 'outer_beautification', 'outer_fencing', 'distance_from_highway',
                'distance_from_highway_type_sn', 'rail_road'], 'required'],

            [['accounting_system_sn', 'classification_sn', 'square_area_type_sn', 'built_up_area_type_sn', 'ceiling_height_type_sn', 'car_parking_sn',
                'truck_parking_sn', 'distance_from_highway_type_sn', 'vehicle_max_length_type_sn',
                'vehicle_max_height_type_sn', 'vehicle_max_width_type_sn', 'inn', 'kpp', 'address_city_ym_geocoder'], 'string'],

            [['floors', 'entries_amount', 'automatic_gates_amount', 'outer_entries_amount'], 'integer'],

            [['square_area', 'built_up_area', 'ceiling_height', 'distance_from_highway', 'vehicle_max_length', 'vehicle_max_height',
                'vehicle_max_width', 'vehicle_max_weight'], 'number'],

            [['outer_lighting', 'outer_security', 'outer_beautification', 'outer_fencing', 'rail_road', 'truck_maneuvering_area', 'from_the_owner'], 'boolean'],

            [['accounting_control_system_sn'], 'each', 'rule' => ['string']],
        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = Warehouse::findOne($id);
        if (!$model) {
            $this->addError('id', 'Склад не найден.');
            return false;
        }

        $model->load(Yii::$app->request->getBodyParams(), '');

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }
}
<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\Document;
use app\modules\api\v0\models\Warehouse;
use yii\base\Model;
use yii\base\ErrorException;
use Yii;

/**
 * Class CreateForm
 * @package app\modules\api\v0\models\forms\warehouse
 */
class CreateForm extends Model
{
    public $id;
    public $title;
    public $description;
    public $address_ym_geocoder;
    public $address_lat;
    public $address_lon;
    public $work_hours_24_7;
    public $work_hours_weekdays_from;
    public $work_hours_weekdays_to;
    public $work_hours_weekends_from;
    public $work_hours_weekends_to;
    public $photo;
    public $organization_id;
    public $from_the_owner;
    public $address_city_ym_geocoder;
    public $inn;
    public $kpp;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['title', 'description', 'address_ym_geocoder', 'address_lat', 'address_lon', 'work_hours_24_7', 'from_the_owner', 'address_city_ym_geocoder'], 'required'],

            [['title', 'description', 'address_ym_geocoder', 'address_lat', 'address_lon', 'work_hours_weekdays_from',
                'work_hours_weekdays_to', 'work_hours_weekends_from', 'work_hours_weekends_to', 'inn', 'kpp', 'address_city_ym_geocoder'], 'string'],

            [['organization_id'], 'integer'],

            [['photo'], 'each', 'rule' => ['integer']],

            [['work_hours_24_7', 'from_the_owner'], 'boolean'],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        if (Yii::$app->user->identity->organization_id == null) {
            $this->addError('organization_id', 'У пользователя нет организации.');
            return false;
        }

        $model = new Warehouse();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->organization_id = Yii::$app->user->identity->organization_id;
        $model->created_by = Yii::$app->user->identity->id;

        // Проверяю что я собственник и включаю обьязательные поля к заполнению
        if ($model->from_the_owner) {
            if (!$model->inn) {
                $this->addError('inn', 'Поле обьязательное к заполнению.');
            }
            if (!$model->kpp) {
                $this->addError('kpp', 'Поле обьязательное к заполнению.');
            }
        }

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        Document::updateAll(['entity_id' => $model->id], ['id' => $this->photo]);

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }

}

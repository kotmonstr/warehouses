<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\User;
use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\models\WarehouseShort;
use Yii;

/**
 * Class IndexForm
 * @package app\modules\api\v0\models\forms\warehouse
 */
class IndexForm extends BasicSearchForm
{
    public $organization_id;
    public $status_sn;

    /** {@inheritDoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['organization_id','status_sn'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function search()
    {
        $query = WarehouseShort::find()->where(['deleted_at' => null]);

        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        if (in_array($identity->role, [User::ROLE_ADMIN, User::ROLE_DSL, User::ROLE_SB])) {
            $query->andFilterWhere(['organization_id' => $this->organization_id]);
        } else {
            $query->andWhere(['organization_id' => $identity->organization_id]);
        }

       // Фильтр
        if(!empty($this->status_sn)){
            $query->andFilterWhere(['status_sn' => $this->status_sn]);
        }

        $totalCount = $query->count();

        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }

}

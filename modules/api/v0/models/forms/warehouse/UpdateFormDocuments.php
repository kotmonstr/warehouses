<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\Document;

use app\models\Warehouse;
use Yii;
use yii\base\ErrorException;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class UpdateFormDocuments
 * @package app\modules\api\v0\models\forms\warehouse
 */
class UpdateFormDocuments extends Model
{
    public $id;
    public $property_certificate;
    public $lease_copy;
    public $cadastral_passport;
    public $tenants_list;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['property_certificate', 'lease_copy', 'cadastral_passport', 'tenants_list'], 'file']
        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = Warehouse::findOne($id);
        if (!$model) {
            $this->addError('id', 'Склад не найден.');
            return false;
        }

        $model->status_sn = Warehouse::STATUS_VERIFICATION;
        $arrListFiles = ['property_certificate', 'lease_copy', 'cadastral_passport', 'tenants_list'];

        foreach ($arrListFiles as $doc) {
            $document = Document::findOne(['entity_field' => $doc, 'entity_id' => $id]);
            if (empty($document)) {
                $document = new Document();

                $document->entity_id = $id;
                $document->entity_classname = Warehouse::class;
                $document->entity_field = $doc;
                $document->created_by = Yii::$app->user->id;
            }

            $document->file = UploadedFile::getInstanceByName($doc);

            if ($document->file) {
                if (!$document->save()) {
                    throw new NotFoundHttpException('Что то пошло не так при обновлении ' . $doc);
                }
            }
        }

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }
}
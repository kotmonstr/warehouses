<?php

namespace app\modules\api\v0\models\forms\warehouse;

use app\models\Document;
use app\models\Warehouse;
use yii\base\ErrorException;
use yii\base\Model;
use Yii;

/**
 * Class UpdateFormGeneral
 * @package app\modules\api\v0\models\forms\warehouse
 */
class UpdateFormGeneral extends Model
{
    public $id;
    public $title;
    public $description;
    public $address_ym_geocoder;
    public $address_lat;
    public $address_lon;
    public $work_hours_24_7;
    public $work_hours_weekdays_from;
    public $work_hours_weekdays_to;
    public $work_hours_weekends_from;
    public $work_hours_weekends_to;
    public $photo;
    public $from_the_owner;
    public $inn;
    public $kpp;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['title', 'description', 'address_ym_geocoder', 'address_lat', 'address_lon', 'work_hours_24_7'], 'required'],

            [['title', 'description', 'address_ym_geocoder', 'address_lat', 'address_lon', 'work_hours_weekdays_from',
                'work_hours_weekdays_to', 'work_hours_weekends_from', 'work_hours_weekends_to', 'inn', 'kpp'], 'string'],

            [['photo'], 'each', 'rule' => ['integer']],

            [['work_hours_24_7', 'from_the_owner'], 'boolean']
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = Warehouse::findOne($id);
        if (!$model) {
            $this->addError('id', 'Склад не найден.');
            return false;
        }

        $model->load(Yii::$app->request->getBodyParams(), '');

        // Проверяю что я собственник и включаю обьязательные поля к заполнению
        if ($model->from_the_owner) {

            if (empty($model->inn)) {
                $this->addError('inn', 'Поле обьязательное к заполнению.');
                return false;
            }

            if (empty($model->kpp)) {
                $this->addError('kpp', 'Поле обьязательное к заполнению.');
                return false;
            }

        }

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }


        //старые удалить те которые не прислались
        Document::deleteAll(['AND', 'entity_id = :id', ['NOT IN', 'id', $this->photo]], [':id' => $model->id]);

        Document::updateAll(['entity_id' => $model->id], ['id' => $this->photo]);

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }
}
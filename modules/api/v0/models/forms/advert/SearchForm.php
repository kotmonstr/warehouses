<?php

namespace app\modules\api\v0\models\forms\advert;

use app\models\Warehouse;
use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\models\Advert;
use Yii;

/**
 * Class SearchForm
 * @package app\modules\api\v0\models\forms\advert
 *
 * @property string $warehouse_id
 * @property string $status_sn
 */
class SearchForm extends BasicSearchForm
{
    public $warehouse_id;
    public $status_sn;
    public $city;
    public $available_from;
    public $available_to;
    public $classification_sn;
    public $storage_type_sn;

    /** {@inheritDoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['warehouse_id', 'status_sn', 'city', 'available_from', 'available_to', 'classification_sn','storage_type_sn'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function search()
    {

        $query = Advert::find()->where(['deleted_at' => null, 'created_by' => Yii::$app->user->identity->id]);

        // city
        if (!empty($this->city)) {
            $query->join(
                'JOIN',
                'warehouse',
                'advert.warehouse_id = warehouse.id'
            )->where(['ilike', 'address_city_ym_geocoder', $this->city]);

            if(!empty($this->classification_sn)){
                $query->where(['warehouse.classification_sn' => $this->classification_sn]);
            }
        }

        //classification_sn
        if (!empty($this->classification_sn && empty($this->city))) {
            $query->join(
                'JOIN',
                'warehouse',
                'advert.warehouse_id = warehouse.id'
            )->where(['warehouse.classification_sn' => $this->classification_sn]);
        }

        $query->andFilterWhere(['warehouse_id' => $this->warehouse_id]);
        $query->andFilterWhere(['status_sn' => $this->status_sn]);
        $query->andFilterWhere(['>=', 'available_from', $this->available_from]);
        $query->andFilterWhere(['<=', 'available_to', $this->available_to]);
        $query->andFilterWhere(['storage_type_sn' => $this->storage_type_sn]);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }
}

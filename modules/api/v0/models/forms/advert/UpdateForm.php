<?php

namespace app\modules\api\v0\models\forms\advert;

use app\models\Warehouse;
use yii\base\Model;
use yii\base\ErrorException;
use Yii;

/**
 * Class UpdateForm
 * @package app\modules\api\v0\models\forms\advert
 */
class UpdateForm extends Model
{
    public $id;
    public $warehouse_id;
    public $storage_type_sn;
    public $available_space;
    public $available_space_type_sn;
    public $unit_cost_per_day;
    public $available_from;
    public $available_to;
    public $manager_name;
    public $manager_phone_number;
    public $manager_email;
    public $comment;


    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['warehouse_id', 'storage_type_sn', 'available_space', 'available_space_type_sn', 'unit_cost_per_day', 'available_from',
                'available_to', 'manager_name', 'manager_phone_number', 'manager_email' ], 'required'],

            [['comment', 'storage_type_sn', 'available_space_type_sn', 'available_from', 'available_to', 'manager_name',
               'manager_email'], 'string'],

            [['unit_cost_per_day', 'available_space', 'warehouse_id'], 'integer'],

            [['manager_phone_number'],'string','min'=>18],

            [['manager_email'],'email'],

            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>', 'skipOnEmpty' => true],

            [['warehouse_id'], 'exist', 'targetClass' => Warehouse::class, 'targetAttribute' => 'id']



        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = \app\models\Advert::findOne($id);
        if(!$model){
            $this->addError('id', 'Обьявление не найден.');
            return false;
        }

        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->status_sn = \app\models\Advert::STATUS_DRAFT;

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
        ];
    }

}

<?php

namespace app\modules\api\v0\models\forms\teamMember;

use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\models\MembershipRequestIndex;
use app\modules\api\v0\models\UserMembershipRequest;
use app\models\MembershipRequest;
use yii;
use yii\web\NotFoundHttpException;

/**
 * Class SearchForm
 * @package app\modules\api\v0\models\forms\teamMember
 */
class SearchForm extends BasicSearchForm
{
    /**
     * @return array
     */
    public function searchTeamMemberRequests()
    {
        $query = MembershipRequestIndex::find()
            ->where(['organization_id' => Yii::$app->user->identity->organization_id]);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function searchTeamMembers()
    {
        if (Yii::$app->user->identity->organization_id == null) {
            return [];
            //throw new NotFoundHttpException('Пользователь не состоит в организации.'); ToDo не точно! уточняю что вернуть
        }

        $query = UserMembershipRequest::find()->where(['organization_id' => Yii::$app->user->identity->organization_id]);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }
}

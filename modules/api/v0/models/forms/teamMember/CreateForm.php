<?php

namespace app\modules\api\v0\models\forms\teamMember;

use yii;
use app\models\MembershipRequest;
use yii\base\ErrorException;

/**
 * Class CreateForm
 * @package app\modules\api\v0\models\forms\teamMember
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $organization_id
 */
class CreateForm extends yii\base\Model
{
    public $id;
    public $user_id;
    public $organization_id;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['organization_id', 'user_id'], 'required'],
            [['organization_id', 'user_id'], 'integer'],

            [['user_id'], 'unique', 'targetClass' => MembershipRequest::class, 'targetAttribute' => 'user_id',
                'message' => 'У пользователя уже есть активный запрос на членство в компанию.'
            ],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new MembershipRequest();

        $model->user_id = $this->user_id;
        $model->organization_id = $this->organization_id;

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        return true;
    }

    /** {@inheritDoc} */
    public function fields()
    {
        return ['id'];
    }
}

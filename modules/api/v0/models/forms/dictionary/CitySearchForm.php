<?php

namespace app\modules\api\v0\models\forms\dictionary;

use app\models\Warehouse;
use app\modules\api\v0\models\forms\BasicSearchForm;

/**
 * Class CitySearchForm
 * @package app\modules\api\v0\models\dictionary
 *
 * @property string  $title
 * @property integer $organization_id
 */
class CitySearchForm extends BasicSearchForm
{
    public $title;
    public $organization_id;

    /** {@inheritDoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title'], 'string'],
            [['organization_id'], 'integer'],
        ]);
    }

    /**
     * @param $item
     * @return array
     */
    public function search()
    {
        $query = Warehouse::find()
            ->select(['address_city_ym_geocoder'])
            ->andFilterWhere(['organization_id' => $this->organization_id])
            ->andFilterWhere(['ilike', 'address_city_ym_geocoder', $this->title])
            ->groupBy(['address_city_ym_geocoder']);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->column(),
        ];
    }
}

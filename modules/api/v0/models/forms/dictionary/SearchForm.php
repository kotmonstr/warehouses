<?php

namespace app\modules\api\v0\models\forms\dictionary;


use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\models\Dictionary;

/**
 * Class SearchForm
 * @package app\modules\api\v0\models\dictionary
 */
class SearchForm extends BasicSearchForm
{
    public $type;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['type'], 'string'],
        ];
    }

    /**
     * @param $item
     * @return array
     */
    public function search($item)
    {
        $query = Dictionary::findDictItemsBySystemName($item, $this->type);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }

}

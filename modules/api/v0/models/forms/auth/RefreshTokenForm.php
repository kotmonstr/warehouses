<?php

namespace app\modules\api\v0\models\forms\auth;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class RefreshTokenForm
 * @package app\modules\api\v0\models\forms\auth
 *
 * @property string $access_token
 * @property integer $access_token_expired_at
 *
 * @property string $refresh_token
 * @property integer $refresh_token_expired_at
 */
class RefreshTokenForm extends Model
{
    public $access_token;
    public $access_token_expired_at;

    public $refresh_token;
    public $refresh_token_expired_at;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['refresh_token'], 'required'],
            [['refresh_token'], 'string', 'min' => 32, 'max' => 32],
            [['refresh_token'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'refresh_token']
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findIdentityByRefreshToken($this->refresh_token);
        $user->refreshToken();

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->access_token = $user->access_token;
        $this->access_token_expired_at = $user->access_token_expired_at;

        $this->refresh_token = $user->refresh_token;
        $this->refresh_token_expired_at = $user->refresh_token_expired_at;

        return true;
    }
}
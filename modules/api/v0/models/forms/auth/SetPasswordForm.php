<?php

namespace app\modules\api\v0\models\forms\auth;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class SetPasswordForm
 * @package app\modules\api\v0\models\forms\auth
 *
 * @property string $password
 * @property string $password_reset_token
 *
 */
class SetPasswordForm extends Model
{
    public $password;
    public $password_reset_token;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['password', 'password_reset_token'], 'required'],
            [['password', 'password_reset_token'], 'string'],
            [['password_reset_token'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'password_reset_token']
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findByPasswordResetToken($this->password_reset_token);
        $user->setPassword($this->password);

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        return true;
    }

}
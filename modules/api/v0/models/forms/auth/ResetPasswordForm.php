<?php

namespace app\modules\api\v0\models\forms\auth;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class ResetPasswordForm
 * @package app\modules\api\v0\models\forms\auth
 * @property string $email
 *
 */
class ResetPasswordForm extends Model
{
    public $email;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'email']
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findByUsername($this->email);

        User::sendResetPasswordEmail($user);

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        return true;
    }
}
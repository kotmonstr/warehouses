<?php

namespace app\modules\api\v0\models\forms\auth;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class SignUpForm
 * @package app\modules\api\v0\models\forms\auth
 * @property string $email
 * @property string $password
 * @property string $first_name
 */
class SignUpForm extends Model
{
    public $user_id;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $role;
    public $access_token;
    public $access_token_expired_at;
    public $refresh_token;
    public $refresh_token_expired_at;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['email', 'password', 'first_name', 'last_name'], 'required'],
            [['email', 'password', 'first_name', 'last_name', 'middle_name', 'role'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            [['role'], 'default', 'value' => 'holder'],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User;
        $user->load(Yii::$app->request->getBodyParams(), '');
        $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $user->email_verification_token = Yii::$app->getSecurity()->generateRandomString();
        $user->email_verified = false;
        $user->role = 'holder';
        $user->refreshToken();

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->user_id = $user->id;
        $this->role = $user->role;
        $this->access_token = $user->access_token;
        $this->access_token_expired_at = $user->access_token_expired_at;
        $this->refresh_token = $user->refresh_token;
        $this->refresh_token_expired_at = $user->refresh_token_expired_at;

        User::sendVerificationEmail($user);

        return true;

    }

    public function fields()
    {
        return [
            'user_id',
            'email',
            'first_name',
            'last_name',
            'middle_name',
            'role',
            'access_token',
            'access_token_expired_at',
            'refresh_token',
            'refresh_token_expired_at',
        ];
    }
}
<?php

namespace app\modules\api\v0\models\forms\auth;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class SignInForm
 * @package app\modules\api\v0\models\forms\auth
 *
 * @property string  $user_id
 *
 * @property string  $email
 * @property string $password
 *
 * @property string $access_token
 * @property integer $access_token_expired_at
 *
 * @property string $refresh_token
 * @property integer $refresh_token_expired_at
 */
class SignInForm extends Model
{
    public $user_id;

    public $email;
    public $password;

    public $access_token;
    public $access_token_expired_at;

    public $refresh_token;
    public $refresh_token_expired_at;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email', 'password'], 'string'],
            [['email'], 'email'],
            [['email'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'email']
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findByUsername($this->email);

        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError('email', 'Wrong Email or password.');
            return false;
        }

        $user->refreshToken();

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->user_id = $user->id;

        $this->access_token = $user->access_token;
        $this->access_token_expired_at = $user->access_token_expired_at;

        $this->refresh_token = $user->refresh_token;
        $this->refresh_token_expired_at = $user->refresh_token_expired_at;

        return true;
    }

    public function fields()
    {
        return [
            'user_id',
            'access_token',
            'access_token_expired_at',
            'refresh_token',
            'refresh_token_expired_at',
        ];
    }
}
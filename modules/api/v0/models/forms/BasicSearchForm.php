<?php

namespace app\modules\api\v0\models\forms;

use app\models\User;
use app\modules\api\v0\models\Warehouse;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class BasicSearchForm
 * @package app\modules\api\v0\models\forms
 *
 * @property integer $limit
 * @property integer $offset
 */
class BasicSearchForm extends Model
{
    public $limit;
    public $offset;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['limit', 'offset'], 'integer'],

            [['limit'], 'default', 'value' => 20],
            [['offset'], 'default', 'value' => 0],
       ];
    }
}
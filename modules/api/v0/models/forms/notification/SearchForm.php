<?php

namespace app\modules\api\v0\models\forms\notification;

use app\models\Notification;
use app\models\User;
use app\modules\api\v0\models\forms\BasicSearchForm;

/**
 * Class SearchForm
 * @package app\modules\api\v0\models\notification
 */
class SearchForm extends BasicSearchForm
{
    /**
     * @return array
     */
    public function search()
    {
         $_model = $query = Notification::find()
            ->where(['user_id' => \Yii::$app->user->identity->id])
            ->orderBy('viewed_at DESC');

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        // Помечаю все уведомления как прочитанные
        if ($_model) {
            $model = $_model->all();

            foreach ($model as $notification) {
                $notification->viewed_at = time();
                $notification->updateAttributes(['viewed_at']);
            }
        }

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }

    /**
     * @return array
     */
    public function searchUnread()
    {
        $query = Notification::find()
            ->where(['user_id' => \Yii::$app->user->identity->id])
            ->andWhere(['viewed_at' => null]);

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
        ];
    }
}

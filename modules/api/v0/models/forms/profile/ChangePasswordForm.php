<?php

namespace app\modules\api\v0\models\forms\profile;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class ChangePasswordForm
 * @package app\modules\api\v0\models\forms\profile
 *
 * @property string $password
 * @property string $password_reset_token
 *
 */
class ChangePasswordForm extends Model
{
    public $id;
    public $password;
    public $password_current;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $organization_id;
    public $organization_position;
    public $email;
    public $email_verification_token;
    public $email_verified;
    public $phone_number;
    public $password_hash;
    public $password_reset_token;
    public $verification_status_sn;
    public $verification_comment;
    public $verified_at;
    public $created_at;
    public $deleted_at;
    public $updated_at;
    public $role;
    public $access_token;
    public $access_token_expired_at;
    public $refresh_token;
    public $refresh_token_expired_at;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['password','password_current'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findIdentity($this->id);

        if (!$user->validatePassword($this->password_current)) {
            $this->addError('password_current', 'Wrong Email or password.');
            return false;
        }

        $user->setPassword($this->password);

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->last_name = $user->last_name;
        $this->first_name = $user->first_name;
        $this->middle_name = $user->middle_name;
        $this->organization_id = $user->organization_id;
        $this->organization_position = $user->organization_position;
        $this->email = $user->email;
        $this->email_verification_token = $user->email_verification_token;
        $this->email_verified = $user->email_verified;
        $this->phone_number = $user->phone_number;
        $this->password_hash = $user->password_hash;
        $this->password_reset_token = $user->password_reset_token;
        $this->verification_status_sn = $user->verification_status_sn;
        $this->verification_comment = $user->verification_comment;
        $this->verified_at = $user->verified_at;
        $this->created_at = $user->created_at;
        $this->deleted_at = $user->deleted_at;
        $this->updated_at = $user->updated_at;
        $this->role = $user->role;
        $this->access_token = $user->access_token;
        $this->access_token_expired_at = $user->access_token_expired_at;
        $this->refresh_token = $user->refresh_token;
        $this->refresh_token_expired_at = $user->refresh_token_expired_at;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'password',
            'middle_name',
            'organization_id',
            'organization_position',
            'email',
            'email_verification_token',
            'email_verified',
            'phone_number',
            'password_hash',
            'password_reset_token',
            'verification_status_sn',
            'verification_comment',
            'verified_at',
            'created_at',
            'deleted_at',
            'updated_at',
            'role',
            'access_token',
            'access_token_expired_at',
            'refresh_token',
            'refresh_token_expired_at'
        ];
    }

}
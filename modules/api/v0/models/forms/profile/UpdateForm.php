<?php

namespace app\modules\api\v0\models\forms\profile;

use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class UpdateForm
 * @package app\modules\api\v0\models\forms\profile
 *
 * @property integer $id
 * @property string $first_name
 * @property integer $organization_id
 * @property string $organization_position
 * @property string $email
 * @property string $email_verification_token
 * @property string $email_verified
 * @property string $phone_number
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_status_sn
 * @property string $verification_comment
 * @property integer $verified_at
 * @property integer $created_at
 * @property integer $deleted_at
 * @property integer $updated_at
 * @property string $role
 * @property string $access_token
 * @property integer $access_token_expired_at
 * @property string $refresh_token
 * @property integer $refresh_token_expired_at
 */
class UpdateForm extends Model
{
    public $id;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $organization_id;
    public $organization_position;
    public $email;
    public $email_verification_token;
    public $email_verified;
    public $phone_number;
    public $password_hash;
    public $password_reset_token;
    public $verification_status_sn;
    public $verification_comment;
    public $verified_at;
    public $created_at;
    public $deleted_at;
    public $updated_at;
    public $role;
    public $access_token;
    public $access_token_expired_at;
    public $refresh_token;
    public $refresh_token_expired_at;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            [['first_name', 'middle_name', 'last_name','phone_number','organization_position'], 'string'],
            [['role'], 'default', 'value' => 'holder'],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findIdentity($this->id);
        $user->load(Yii::$app->request->getBodyParams(), '');
        $user->role = 'holder';

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->email = $user->email;
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        $this->middle_name = $user->middle_name;
        $this->organization_id = $user->organization_id;
        $this->organization_position = $user->organization_position;
        $this->email_verification_token = $user->email_verification_token;
        $this->email_verified = $user->email_verified;
        $this->phone_number = $user->phone_number;
        $this->password_hash = $user->password_hash;
        $this->password_reset_token = $user->password_reset_token;
        $this->verification_status_sn = $user->verification_status_sn;
        $this->verification_comment = $user->verification_comment;
        $this->verified_at = $user->verified_at;
        $this->created_at = $user->created_at;
        $this->deleted_at = $user->deleted_at;
        $this->updated_at = $user->updated_at;
        $this->role = 'holder';
        $this->access_token = $user->access_token;
        $this->access_token_expired_at = $user->access_token_expired_at;
        $this->refresh_token = $user->refresh_token;
        $this->refresh_token_expired_at = $user->refresh_token_expired_at;

        return true;
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'organization_id',
            'organization_position',
            'email',
            'email_verification_token',
            'email_verified',
            'phone_number',
            'password_hash',
            'password_reset_token',
            'verification_status_sn',
            'verification_comment',
            'verified_at',
            'created_at',
            'deleted_at',
            'updated_at',
            'role',
            'access_token',
            'access_token_expired_at',
            'refresh_token',
            'refresh_token_expired_at'
        ];
    }
}
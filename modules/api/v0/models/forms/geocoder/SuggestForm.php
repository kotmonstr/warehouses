<?php

namespace app\modules\api\v0\models\forms\geocoder;

use app\components\YMaps;
use app\modules\api\v0\models\forms\BasicSearchForm;
use Yii;
use yii\httpclient\Exception;

/**
 * Class SuggestForm
 * @package app\modules\api\v0\models\geocoder
 */
class SuggestForm extends BasicSearchForm
{
    public $query;

    public $lat;
    public $lon;

    /** {@inheritDoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['query'], 'required'],
            [['query'], 'string', 'min' => 3, 'max' => 255],
            [['lat', 'lon'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function search()
    {
        /** @var YMaps $ymaps */
        $ymaps = Yii::$app->ymaps;

        return $ymaps->suggest($this->query, $this->lat, $this->lon);
    }
}

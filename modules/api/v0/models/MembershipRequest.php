<?php

namespace app\modules\api\v0\models;

use app\models\Notification;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class MembershipRequest extends  \app\models\MembershipRequest
{
    /**
     * @param $id
     * @return MembershipRequest|null
     * @throws NotFoundHttpException
     * @throws \yii\base\ErrorException
     */
    public static function findByIdAndAccessToken($id)
    {
        $memberShipRequest = self::find()->where(['id' => $id])->one();
        if (!$memberShipRequest) {
            throw new NotFoundHttpException('Сообщение не найдено');
        }

        if ($id ) {
            $query = ['id' => $id, 'user_id' => Yii::$app->user->identity->id];
            $model = self::findOne($query);
        }

        return $model;
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserShort::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(OrganizationShort::class, ['id' => 'organization_id']);
    }

    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'user',
            'organization',
            'created_at'
        ];
    }
}

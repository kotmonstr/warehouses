<?php

namespace app\modules\api\v0\models;

/**
 * Class Organization
 * @package app\modules\api\v0\models
 */
class Organization extends \app\models\Organization
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'legal_type',
            'inn',
            'title',
            'ogrn',
            'kpp',
            'partner_type',
            'main_activity',
            'address_ym_geocoder',
            'post_address',
            'fact_address',
            'taxation_type',
            'shareholders',
            'start_capital',
            'registration_date',
            'edm_type',
            'employee_count',
            'bank_details',
            'authorized_head_contact',
            'authorized_sign_contact',
            'authorized_security_contact',
            'contact_phone',
            'contact_email',
            'arendators_list',
            'no_debts_reference',
            'rent_contract',
            'warehouse_cadastral_passport',
            'property_rights',
            'tax_stamp',
            'tax_registration_ul',
            'executive_unit_definition',
            'poa_for_signing',
            'head_signature',
            'financial_report',
            'passport_ip',
            'tax_registration_ip',
            'verification_comment',
            'verified_at',
            'created_at'
        ];
    }

    /**
     * @param $id
     * @return \yii\db\ActiveQuery
     */
    public static function findById($id)
    {
        $model = Organization::find()->where(['id' => $id])->one();

        return $model;
    }
}
<?php

namespace app\modules\api\v0\models;

use app\models\WhLoadingUnloadingStructType;
use app\models\WhLoadUnloadCranage;
use app\models\WhLoadUnloadEquipment;
use app\models\WhOfficeFacilityType;
use app\models\WhStorageSystem;

class WarehouseViewInternal extends \app\models\Warehouse
{
    public $column_distance_type;
    public $span_distance_type;
    public $floor_covering;
    public $height_above_ground_type;
    public $load_per_square_meter_type;
    public $temperature_mode;
    public $min_temperature_type;
    public $max_temperature_type;
    public $fire_ext_system;
    public $electricity_grid;
    public $heating_system;

    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'load_unload_equipment',
            'load_unload_cranage',
            'storage_system',
            'column_distance',
            'column_distance_type',
            'span_distance',
            'span_distance_type',
            'column_protection',
            'floor_covering',
            'anti_dust_covering',
            'height_above_ground',
            'height_above_ground_type',
            'load_per_square_meter',
            'ventilation',
            'conditioning',
            'temperature_mode',
            'min_temperature',
            'min_temperature_type',
            'max_temperature',
            'max_temperature_type',
            'security_guard',
            'security_video',
            'security_alarm',
            'fire_alarm',
            'fire_charging',
            'fire_tanks_and_pump',
            'fire_ext_system',
            'electricity_grid',
            'heating_system',
            'water_hot',
            'water_cold',
            'water_sewerage',
            'loading_unloading_struct_types',
            'office_facility_types_inside',
            'internet',
            'phone',
            'fiber_optic',
            'verification_status',
            'verification_comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoad_unload_equipment(){
        return $this->hasOne( WhLoadUnloadEquipment::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoad_unload_cranage(){
        return $this->hasOne( WhLoadUnloadCranage::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage_system(){
        return $this->hasOne( WhStorageSystem::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColumn_distance_type(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'column_distance_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpan_distance_type(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'span_distance_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFloor_covering(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'floor_covering']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeight_above_ground_type(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'height_above_ground_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemperature_mode(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'temperature_mode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMin_temperature_type(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'min_temperature_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMax_temperature_type(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'max_temperature_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFire_ext_system(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'fire_ext_system']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElectricity_grid(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'electricity_grid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeating_system(){
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'heating_system']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoading_unloading_struct_types(){
        return $this->hasOne( WhLoadingUnloadingStructType::class, ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice_facility_types_inside(){
        return $this->hasOne( WhOfficeFacilityType::class, ['warehouse_id' => 'id']);
    }

}

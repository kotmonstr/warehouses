<?php

namespace app\modules\api\v0\models;

class WarehouseViewDocument extends \app\models\Warehouse
{
    /**
     * @return array|false
     */
    public function fields()
    {

        return [
            'id',
            'title',
            'property_certificate',
            'lease_copy',
            'cadastral_passport',
            'tenants_list',
            'verification_status',
            'verification_comment',
        ];
    }

    /**
     * Relations with 'Document'
     * @property-read Document $property_certificate
     * @return        Document $property_certificate
     */
    public function getProperty_certificate()
    {
        return $this->hasOne( Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'property_certificate']);
    }

    /**
     * Relations with 'Document'
     * @property-read Document $lease_copy
     * @return        Document $lease_copy
     */
    public function getLease_copy()
    {
        return $this->hasOne( Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'lease_copy']);
    }

    /**
     * Relations with 'Document'
     * @property-read Document $cadastral_passport
     * @return        Document $cadastral_passport
     */
    public function getCadastral_passport()
    {
        return $this->hasOne( Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'cadastral_passport']);
    }

    /**
     * Relations with 'Document'
     * @property-read Document $tenants_list
     * @return        Document $tenants_list
     */
    public function getTenants_list()
    {
        return $this->hasOne( Document::class, ['entity_id' => 'id'])->andWhere(['entity_field' => 'tenants_list']);
    }




}

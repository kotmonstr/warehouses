<?php


namespace app\modules\api\v0\models;


/**
 * Class Notification
 * @package app\modules\api\v0\models
 */
class Notification extends \app\models\Notification
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'text',
            'created_at',
            'viewed_at',
        ];
    }


}
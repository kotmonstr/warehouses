<?php

namespace app\modules\api\v0\models;

/**
 * Class OrganizationShort
 * @package app\modules\api\v0\models
 */
class OrganizationShort extends \app\models\Organization
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'inn',
            'legal_type',
            'title',
            'ogrn',
            'kpp',
            'okpo',
            'main_activity',
            'taxation_type',
            'address_ym_geocoder',
            'shareholders',
            'start_capital',
            'registration_date',
            'created_at',
        ];
    }
}
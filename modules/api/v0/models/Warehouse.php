<?php

namespace app\modules\api\v0\models;

use app\models\Advert;
use app\models\Organization;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class Warehouse
 * @package app\modules\api\v0\models
 */
class Warehouse extends \app\models\Warehouse
{
    public $photos;

    public function fields()
    {

        return [
            'id',
            'title',
            'description',
            'classification',
            'address_city_ym_geocoder',
            'address_ym_geocoder',
            'address_lat',
            'address_lon',
            'work_hours_24_7',
            'work_hours_weekdays_from',
            'work_hours_weekdays_to',
            'work_hours_weekends_from',
            'work_hours_weekends_to',
            'verification_status',
            'verification_comment'
        ];
    }


    /**
     * @return ActiveQuery
     */
    public function getOrganization()
    {
       // return $this->hasOne(Organization::class, ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return [];
        //return $this->hasMany(\app\modules\api\v0\models\Document::class, ['entity_id' => 'organization_id'])->andWhere(['entity_field' => 'photo']);
    }


    /**
     * Function to get list of cities
     *
     * @param string|int $organization_id of current user
     * @param bool $verified_only
     *
     * @return string[] $cities
     */
    public function getAddress_city_ym_geocoder()
    {

        if ( Yii::$app->user->identity->organization_id) {

            //cities for holders in panel module
            $cities = \app\models\Warehouse::find()
                ->joinWith('advert')
                ->where(
                    [
                        'warehouse.deleted_at' => null,
                        'warehouse.organization_id' =>Yii::$app->user->identity->organization_id,
                        'advert.deleted_at' => null,
                    ]
                )->all();

        }

        //else {

            //cities for main pages
//            if ($verified_only) {
//                $cities = Warehouse::find()
//                    ->joinWith('organization')
//                    ->joinWith('advert')
//                    ->where(
//                        [
//                            'warehouse.deleted_at' => null,
//                            'organization.deleted_at' => null,
//                            'advert.deleted_at' => null,
//                            'organization.verification_status_sn' => Organization::VERIFICATION_VERIFIED,
//                            'warehouse.sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
//                            'warehouse.dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
//                            'advert.status_sn' => Advert::STATUS_PUBLISHED,
//                        ]
//                    )->andWhere(['>=', 'advert.available_to', time()])->all();
//
//            } else {
//
//                //cities for users in admin module
//                $cities = Warehouse::find()
//                    ->joinWith('organization')
//                    ->innerJoin('advert', 'advert.warehouse_id=warehouse.id')
//                    ->where(
//                        [
//                            'warehouse.deleted_at' => null,
//                            'organization.deleted_at' => null,
//                            'advert.deleted_at' => null,
//                        ]
//                    )->all();
  //          }
  //      }


        $citiesArr = ArrayHelper::getColumn($cities, 'address_city_ym_geocoder');
        $citiesArr = array_unique($citiesArr);

        $citiesReturn = [];
        foreach ($citiesArr as $city) {
            $citiesReturn [$city] = $city;
        }

        return $citiesReturn;
    }

    /**
     * @return \app\models\DictionaryItem|ActiveQuery
     */
    public function getClassification()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'classification_sn']);
    }
}

<?php

namespace app\modules\api\v0\models;

class WarehouseExternal extends \app\models\Warehouse
{

    public $square_area_type;
    public $built_up_area_type;
    public $ceiling_height_type;
    public $car_parking;
    public $truck_parking;
    public $distance_from_highway_type;
    public $vehicle_max_length_type;
    public $vehicle_max_height_type;
    public $vehicle_max_width_type;
    public $vehicle_max_weight_type;
    /**
     * @return array|false
     */
    public function fields()
    {

        return [
            'id',
            'title',
            'accounting_system',
            'classification',
            'square_area',
            'square_area_type',
            'built_up_area',
            'built_up_area_type',
            'ceiling_height',
            'ceiling_height_type',
            'floors',
            'maneuvering_area',
            'entries_amount',
            'loading_ramp',
            'automatic_gates_amount',
            'accounting_control_system',
            'outer_lighting',
            'outer_security',
            'outer_beautification',
            'outer_fencing',
            'outer_entries_amount',
            'car_parking',
            'truck_parking',
            'truck_maneuvering_area',
            'rail_road',
            'distance_from_highway',
            'distance_from_highway_type',
            'vehicle_max_length',
            'vehicle_max_length_type',
            'vehicle_max_height',
            'vehicle_max_height_type',
            'vehicle_max_width',
            'vehicle_max_width_type',
            'vehicle_max_weight',
            'vehicle_max_weight_type',
            'verification_status',
            'verification_comment'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
         return $this->hasMany( Document::class, ['entity_id' => 'id']);
    }


}

<?php

namespace app\modules\api\v0\controllers;

use app\modules\api\v0\models\forms\geocoder\SuggestForm;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\httpclient\Exception;

class GeocoderController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only'  => ['suggest'],
            'rules' => [
                [
                    'actions' => ['suggest'],
                    'allow'   => true,
                    'roles'   => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class'   => VerbFilter::class,
            'actions' => [
                'suggest' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return SuggestForm|array
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function actionSuggest()
    {
        $form = new SuggestForm();
        $form->load(Yii::$app->request->getBodyParams(), '');

        if (!$form->validate()) {
            return $form;
        }

        return $form->search();
    }
}
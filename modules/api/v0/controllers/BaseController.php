<?php

namespace app\modules\api\v0\controllers;

use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Class BaseController
 * @package app\modules\api\v0\controllers
 */
class BaseController extends Controller
{

    public function init()
    {
        parent::init();
        \Yii::$app->language = 'ru';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'cors' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Max-Age' => 3600,
                ]
            ],
        ];
    }
}

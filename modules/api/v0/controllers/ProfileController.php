<?php

namespace app\modules\api\v0\controllers;

use app\modules\api\v0\models\forms\profile\UpdateForm;
use app\modules\api\v0\models\forms\profile\ChangePasswordForm;
use app\modules\api\v0\models\User;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;

/**
 * Class ProfileController
 * @package app\modules\api\v0\controllers
 */
class ProfileController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['view', 'update', 'change-password'],
            'rules' => [
                [
                    'actions' => ['view', 'update', 'change-password'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'view' => ['post'],
                'update' => ['post'],
                'change-password' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @param $id
     * @return User
     */
    public function actionView()
    {
        return User::findOne(Yii::$app->user->id);
    }

    /**
     * @param $id
     * @return UpdateForm
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    public function actionUpdate()
    {
        $model = new UpdateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->id = Yii::$app->user->id;
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return ChangePasswordForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionChangePassword()
    {
        $model = new ChangePasswordForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->id = Yii::$app->user->id;
        $model->save();

        return $model;
    }
}
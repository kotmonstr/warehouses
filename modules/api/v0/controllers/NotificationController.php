<?php

namespace app\modules\api\v0\controllers;

use app\models\Notification;
use app\modules\api\v0\models\Notification as NotificationView;
use app\modules\api\v0\models\forms\notification\SearchForm;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class NotificationController
 * @package app\modules\api\v0\controllers
 */
class NotificationController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index', 'unread-count', 'view', 'mark-read', 'mark-all-read', 'delete'],
            'rules' => [
                [
                    'actions' => ['index', 'unread-count', 'view', 'mark-read', 'mark-all-read', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'index' => ['post'],
                'unread-count' => ['post'],
                'view' => ['post'],
                'mark-read' => ['post'],
                'mark-all-read' => ['post'],
                'delete' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return SearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }

    /**
     * @return SearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUnreadCount()
    {
        $searchForm = new SearchForm();

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->searchUnread();
    }

    /**
     * @param $id
     * @return Notification
     * @throws NotFoundHttpException
     * @throws ErrorException
     */
    public function actionView($id)
    {
        $model = NotificationView::findByIdAndAccessToken($id);
        if (!$model) {
            throw new NotFoundHttpException('Cобщение не найдено или не принадлежит пользователю');
        }
        return $model;
    }

    /**
     * @param $id
     * @return \yii\db\ActiveRecord|null
     */
    public function actionMarkRead($id)
    {
        $model = NotificationView::findOne(['id' => $id, 'user_id' => Yii::$app->user->identity->id]);
        if (!$model) {
            throw new NotFoundHttpException('Cобщение не найдено или не принадлежит пользователю');
        }
        $model->viewed_at = time();
        $model->updateAttributes(['viewed_at']);

        return $model;
    }

    /**
     * @throws NotFoundHttpException
     * @throws ErrorException
     */
    public function actionMarkAllRead()
    {
        $model = NotificationView::findAllNotifications();
        if (!$model) {
            throw new NotFoundHttpException('Cобщения не найдены или не принадлежат пользователю');
        }

        if ($model) {
            foreach ($model as $notification) {
                $notification->viewed_at = time();
                $notification->updateAttributes(['viewed_at']);
            }
        }
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws ErrorException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = NotificationView::findByIdAndAccessToken($id);
        if (!$model) {
            throw new NotFoundHttpException('Cобщение не найдено или не принадлежит пользователю');
        }
        $model->delete();
    }

}
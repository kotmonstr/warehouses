<?php

namespace app\modules\api\v0\controllers;

use app\modules\api\v0\models\Document;
use app\modules\api\v0\models\OrganizationShort;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use app\modules\api\v0\models\forms\organization\CheckInnForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class OrganizationController
 * @package app\modules\api\v0\controllers
 */
class OrganizationController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['check-inn', 'create_document'],
            'rules' => [
                [
                    'actions' =>  ['check-inn', 'create_document'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'check-inn' => ['post'],
                'create_document' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return CheckInnForm|OrganizationShort
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCheckInn()
    {
        $form = new CheckInnForm();
        $form->load(Yii::$app->request->getBodyParams(), '');

        /** @var OrganizationShort $organization */
        if (!($organization = $form->search()) && $form->hasErrors()) {
            return $form;
        }

        return $organization;
    }

    /**
     * @param $type
     * @return Document
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionCreate_document($type)
    {
        $model = new Document();
        $model->file = UploadedFile::getInstanceByName('file');
        if (!$model->file) {
            $model->addError('file', 'Wrong file or file format not correct. ');
            return $model;
        }
        $model->entity_field = $type;
        $model->entity_classname = \app\models\Organization::class;
        $model->created_by = Yii::$app->user->id;

        if (!$model->save()) {
            throw new NotFoundHttpException('Что то пошло не так');
        }

        return $model;
    }
}
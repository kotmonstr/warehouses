<?php

namespace app\modules\api\v0\controllers;

use app\models\Document;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Warehouse;

/**
 * Class WarehouseController
 * @package app\modules\api\v0\controllers
 */
class WarehouseController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['new_photo'],
            'rules' => [
                [
                    'actions' => ['new_photo'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'new_photo' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return Document[]
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function actionNew_photo()
    {
        $files = UploadedFile::getInstancesByName('file');

        if (empty($files)) {
            return [];
        }

        $transaction = Yii::$app->db->beginTransaction();

        $ids = [];

        foreach ($files as $file) {
            $model = new Document();
            $model->file = $file;
            $model->entity_field = 'photo';
            $model->entity_classname = Warehouse::class;
            $model->created_by = Yii::$app->user->id;

            if (!$model->save()) {
                $transaction->rollBack();
                throw new NotFoundHttpException('Что то пошло не так');
            }

            $ids[] = $model->id;
        }

        $transaction->commit();

        return $ids;
    }
}
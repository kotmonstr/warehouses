<?php

namespace app\modules\api\v0\controllers;

use app\models\User;
use app\modules\api\v0\models\forms\dictionary\CitySearchForm;
use app\modules\api\v0\models\forms\dictionary\SearchForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;

class DictionaryController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only'  => ['items', 'all-cities', 'my-cities'],
            'rules' => [
                [
                    'actions' => ['items', 'all-cities', 'my-cities'],
                    'allow'   => true,
                    'roles'   => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class'   => VerbFilter::class,
            'actions' => [
                'items'      => ['post'],
                'all-cities' => ['post'],
                'my-cities'  => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @param $item
     * @return SearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionItems($item)
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search($item);
    }

    /**
     * @return CitySearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAllCities()
    {
        $searchForm = new CitySearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }

    /**
     * @return CitySearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionMyCities()
    {
        $searchForm = new CitySearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        /** @var User $identity */
        $identity = Yii::$app->user->identity;
        $searchForm->organization_id = $identity->organization_id;

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }
}
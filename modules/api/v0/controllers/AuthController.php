<?php

namespace app\modules\api\v0\controllers;

use app\modules\api\v0\models\forms\auth\RefreshTokenForm;
use app\modules\api\v0\models\forms\auth\ResetPasswordForm;
use app\modules\api\v0\models\forms\auth\SetPasswordForm;
use app\modules\api\v0\models\forms\auth\SignInForm;
use app\modules\api\v0\models\forms\auth\SignUpForm;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;

/**
 * Class AuthController
 * @package app\modules\api\v0\controllers
 */
class AuthController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => ['sign-up', 'sign-in', 'reset-password', 'set-password', 'refresh-token'],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['sign-up', 'sign-in', 'logout', 'refresh-token', 'reset-password', 'set-password'],
            'rules' => [
                [
                    'actions' => ['sign-up', 'sign-in', 'reset-password', 'set-password', 'refresh-token'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => ['logout'],
                    'allow' => true,
                    'roles' => ['@'],
                ],

            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'sign-up' => ['post'],
                'sign-in' => ['post'],
                'logout' => ['post'],
                'refresh-token' => ['post'],
                'reset-password' => ['post'],
                'set-password' => ['post'],
            ],
        ];

        return $behaviors;
    }


    /**
     * Registration
     * @return SignUpForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionSignUp()
    {
        $model = new SignUpForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * Login
     * @return SignInForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionSignIn()
    {
        $model = new SignInForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * @return array
     * @throws ErrorException
     */
    public function actionLogout()
    {
        if (!Yii::$app->user->identity->logout()) {
            throw new ErrorException('Something went wrong.');
        }

        return [];
    }

    /**
     * @return RefreshTokenForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionRefreshToken()
    {
        $model = new RefreshTokenForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * @return ResetPasswordForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionResetPassword()
    {
        $model = new ResetPasswordForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * @return SetPasswordForm
     * @throws ErrorException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function actionSetPassword()
    {
        $model = new SetPasswordForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }
}
<?php

namespace app\modules\api\v0\modules\adm\controllers;

use app\models\User;
use app\modules\api\v0\controllers\BaseController;
use app\modules\api\v0\modules\adm\models\AdmUserFull;
use app\modules\api\v0\modules\adm\models\forms\user\SearchForm;
use app\modules\api\v0\modules\adm\models\forms\user\SignUpForm;
use app\modules\api\v0\modules\adm\models\forms\user\UpdateForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class UserController
 * @package app\modules\api\v0\modules\adm\controllers
 */
class UserController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index', 'view', 'update', 'change-password', 'create', 'delete'],
            'rules' => [
                [
                    'actions' => ['index', 'view', 'update', 'change-password', 'create', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        return true;

                        // Временное решение
                        // Todo: убрать при реализации ролевой модели

                        /** @var User $identity */
                        $identity = Yii::$app->user->identity;

                        if (!in_array($identity, [User::ROLE_ADMIN, User::ROLE_SB, User::ROLE_DSL])) {
                            return false;
                        }

                        return true;
                    }
                ],

            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'index' => ['post'],
                'view' => ['post'],
                'update' => ['post'],
                'change-password' => ['post'],
                'create' => ['post'],
                'delete' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }

    /**
     * @return SignUpForm
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new SignUpForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return User
     */
    public function actionView($id)
    {
        $model = AdmUserFull::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        return $model;
    }

    /**
     * @param $id
     * @return UpdateForm
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $model = new UpdateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionChangePassword($id)
    {
        return 'Coming soon';
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDelete($id){
        $model = User::findOne(['id'=> $id, 'deleted_at' => null]);
        if (!$model) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        if($model->delete()){
            return [];
        }
    }
}
<?php

namespace app\modules\api\v0\modules\adm\controllers;

use app\modules\api\v0\controllers\BaseController;
use app\modules\api\v0\models\forms\organization\CreateForm;
use app\modules\api\v0\models\Organization;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class OrganizationController
 * @package app\modules\api\v0\modules\adm\controllers
 */
class OrganizationController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'index', 'view', 'update'],
            'rules' => [
                [
                    'actions' =>  ['create', 'index', 'view', 'update'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'create' => ['post'],
                'index' => ['post'],
                'view' => ['post'],
                'update' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return CreateForm
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return  $model;
    }

    /**
     * @param $id
     * @return ActiveQuery
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = Organization::findById($id);

        if (!$model) {
            throw new NotFoundHttpException('Организация не найдена');
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return 'Coming soon';
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        return 'Coming soon';
    }
}
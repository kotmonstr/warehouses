<?php

namespace app\modules\api\v0\modules\adm\controllers;

use app\modules\api\v0\controllers\BaseController;
use app\modules\api\v0\models\Organization;
use app\modules\api\v0\models\Warehouse;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use Yii;

/**
 * Class WarehouseController
 * @package app\modules\api\v0\modules\adm\controllers
 */
class WarehouseController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['organization-report-url'],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'organization-report-url' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionOrganizationReportUrl($id)
    {
        $report= [];
        $model = Warehouse::findOne($id);

        if ($model->from_the_owner) {
            $modelOrganization = Organization::findOne($model->organization_id);
            $report['url'] = Yii::$app->konturApi->downloadBriefReport(['inn' => $modelOrganization->inn]);
        } else {
            $report['url']  = Yii::$app->konturApi->downloadBriefReport(['inn' => $model->inn]);
        }

        return $report;
    }


}
<?php

namespace app\modules\api\v0\modules\adm\models;

use app\modules\api\v0\models\OrganizationShort;

/**
 * Class AdmOrganizationShort
 * @package app\modules\api\v0\modules\adm\models;
 */
class AdmOrganizationShort extends OrganizationShort
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'inn',
            'legal_type',
            'title',
            'registration_date',
            'verification_status',
            'created_at',
        ];
    }
}
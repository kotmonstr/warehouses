<?php

namespace app\modules\api\v0\modules\adm\models;

use app\modules\api\v0\models\UserShort;

/**
 * Class AdmUserFull
 * @package app\modules\api\v0\modules\adm\models;
 */
class AdmUserFull extends UserShort
{
    /**
     * @return array|string[]
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'phone_number',
            'organization',
            'organization_position',
            'warehouses',
            'adverts',
            'created_at'
        ];
    }

    /**
     * @return bool|int|string|null
     */
    public function getOrganization()
    {
        return $this->hasMany(AdmOrganizationShort::class, ['id' => 'organization_id']);
    }

    /**
     * @return bool|int|string|null
     */
    public function getWarehouses()
    {
        return $this->hasMany(AdmWarehouseView::class, ['created_by' => 'id']);
    }

    /**
     * @return bool|int|string|null
     */
    public function getAdverts()
    {
        return $this->hasMany(AdmAdvertView::class, ['created_by' => 'id']);
    }

}
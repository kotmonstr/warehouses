<?php

namespace app\modules\api\v0\modules\adm\models;

use app\models\Advert;
use app\models\Warehouse;
use app\modules\api\v0\models\UserShort;

/**
 * Class AdmUserShort
 * @package app\modules\api\v0\modules\adm\models;
 */
class AdmUserShort extends UserShort
{
    /**
     * @return array|string[]
     */
    public function fields()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'organization',
            'organization_position',
            'warehouse_count',
            'advert_count',
            'created_at'
        ];
    }

    /**
     * @return bool|int|string|null
     */
    public function getWarehouse_count()
    {
        return $this->hasMany(Warehouse::class, ['created_by' => 'id'])->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getAdvert_count()
    {
        return $this->hasMany(Advert::class, ['created_by' => 'id'])->count();
    }

    /**
     * @return bool|int|string|null
     */
    public function getOrganization()
    {
        return $this->hasMany(AdmOrganizationShort::class, ['id' => 'organization_id']);
    }

}
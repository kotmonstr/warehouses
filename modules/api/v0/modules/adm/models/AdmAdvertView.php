<?php

namespace app\modules\api\v0\modules\adm\models;

use app\modules\api\v0\models\DictionaryItem;
use yii\db\ActiveQuery;

/**
 * Class AdmAdvertView
 * @package app\modules\api\v0\modules\adm\models;
 */
class AdmAdvertView extends \app\models\Advert
{
    public function fields()
    {
        return [
            'id',
            'warehouse',
            'storage_type',
            'available_space',
            'available_space_type',
            'unit_cost_per_day',
            'available_from',
            'available_to',
            'status',
            'created_at'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(AdmWarehouseView::class, ['id' => 'warehouse_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStorage_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'storage_type_sn']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAvailable_space_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'available_space_type_sn']);
    }
}
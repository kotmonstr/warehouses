<?php

namespace app\modules\api\v0\modules\adm\models;

use app\models\WhStorageSystem;

/**
 * Class AdmWarehouseView
 * @package app\modules\api\v0\modules\adm\models
 */
class AdmWarehouseView extends \app\models\Warehouse
{
    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'organization',
            'address_city_ym_geocoder',
            'storage_system',
            'status',
            'created_at'
        ];
    }
    //Warehouse index

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
       return $this->hasOne(AdmOrganizationShort::class, ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage_system(){
        return $this->hasOne( WhStorageSystem::class, ['warehouse_id' => 'id']);
    }
}

<?php

namespace app\modules\api\v0\modules\adm\models\forms\organization;

/**
 * Class CreateForm
 * @package app\modules\api\v0\modules\adm\models\forms\organization
 *
 * @property integer $user_id
 */
class CreateForm extends \app\modules\api\v0\models\forms\organization\CreateForm
{
    public $user_id;

    /**
     * Todo
     * @return bool
     */
    public function save()
    {
        return false;
    }
}
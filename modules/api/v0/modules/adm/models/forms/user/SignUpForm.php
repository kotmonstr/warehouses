<?php

namespace app\modules\api\v0\modules\adm\models\forms\user;

use app\models\Organization;
use app\models\User;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class SignUpForm
 * @package app\modules\api\v0\modules\adm\models\forms\user;
 * @property string $email
 * @property string $password
 * @property string $first_name
 */
class SignUpForm extends Model
{
    public $id;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $role;
    public $access_token;
    public $access_token_expired_at;
    public $refresh_token;
    public $refresh_token_expired_at;
    public $phone_number;
    public $organization_id;
    public $organization_position;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name'], 'required'],
            [['email', 'password', 'first_name', 'last_name', 'middle_name', 'role',
                'phone_number', 'organization_id', 'organization_position'], 'string'],
            [['email'], 'email'],
            [['role'], 'default', 'value' => 'holder'],

            [['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            [['organization_id'], 'exist', 'targetClass' => Organization::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User;
        $user->load(Yii::$app->request->getBodyParams(), '');
        $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($user->setPassword(Yii::$app->security->generateRandomString(8)));
        $user->email_verification_token = Yii::$app->getSecurity()->generateRandomString();
        $user->email_verified = false;
        $user->role = 'holder';
        $user->refreshToken();

        if (!$user->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $user->id;

        User::sendVerificationEmail($user);

        return true;

    }

    public function fields()
    {
        return [
            'id',
        ];
    }
}
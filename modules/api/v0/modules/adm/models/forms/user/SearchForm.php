<?php

namespace app\modules\api\v0\modules\adm\models\forms\user;

use app\modules\api\v0\models\forms\BasicSearchForm;
use app\modules\api\v0\modules\adm\models\AdmUserShort;

/**
 * Class SearchForm
 * @package app\modules\api\v0\modules\adm\models\forms\user;
 *
 */
class SearchForm extends BasicSearchForm
{
    public $first_name;
    public $last_name;
    public $middle_name;
    public $query;

    /**
     * @return array|array[]
     */
    public function rules()
    {
        return [
            [['query'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function search()
    {
        $query = AdmUserShort::find()->where(['deleted_at' => null]);

        // query
        if (!empty($this->query)) {
            $query->andFilterWhere(['ilike', 'first_name', $this->query]);
            $query->orFilterWhere(['ilike', 'last_name', $this->query]);
            $query->orFilterWhere(['ilike', 'middle_name', $this->query]);
        }

        $totalCount = $query->count();
        $query->limit($this->limit)->offset($this->offset);

        return [
            'count' => $totalCount,
            'items' => $query->all(),
        ];
    }

}

<?php

namespace app\modules\api\v0\modules\adm\models\forms\user;

use app\models\User;
use app\modules\api\v0\models\Organization;
use yii\base\ErrorException;
use yii\base\Model;
use Yii;

/**
 * Class UpdateForm
 * @package app\modules\api\v0\modules\adm\models\forms\user
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 */
class UpdateForm extends Model
{
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $middle_name;
    public $phone_number;
    public $organization_position;
    public $organization_id;

    /**
     * @return array|array[]
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'required'],
            [['first_name', 'last_name', 'middle_name', 'email', 'phone_number', 'organization_position'], 'string'],
            [['organization_id'], 'integer'],
            [['email'], 'email'],
            [['organization_id'], 'exist', 'targetClass' => Organization::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = User::findOne($id);
        if (!$model) {
            $this->addError('id', 'Пользователь не найден.');
            return false;
        }

        $model->load(Yii::$app->request->getBodyParams(), '');

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        return true;
    }

    /**
     * @return array|string[]
     */
    public function fields()
    {
        return [
            'id',
        ];
    }
}
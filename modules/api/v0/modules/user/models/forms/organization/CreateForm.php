<?php

namespace app\modules\api\v0\modules\user\models\forms\organization;

use app\models\Organization;
use app\models\User;
use yii\base\ErrorException;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class CreateForm
 * @package app\modules\api\v0\modules\user\models\forms\organization
 */
class CreateForm extends \app\modules\api\v0\models\forms\organization\CreateForm
{
    /**
     * @return bool
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new Organization;

        $model->legal_type                  = $this->legal_type;
        $model->title                       = $this->title;
        $model->inn                         = $this->inn;
        $model->ogrn                        = $this->ogrn;
        $model->kpp                         = $this->kpp;
        $model->okpo                        = $this->okpo;
        $model->main_activity               = $this->main_activity;
        $model->taxation_type               = $this->taxation_type;
        $model->address_ym_geocoder         = $this->address_ym_geocoder;

        $model->shareholders                = $this->shareholders;
        $model->edm_type                    = $this->edm_type;
        $model->post_address                = $this->post_address;
        $model->fact_address                = $this->fact_address;
        $model->contact_phone               = $this->contact_phone;
        $model->contact_email               = $this->contact_email;
        $model->start_capital               = $this->start_capital;
        $model->registration_date           = $this->registration_date;
        $model->employee_count              = $this->employee_count;
        $model->bank_details                = $this->bank_details;
        $model->authorized_head_contact     = $this->authorized_head_contact;
        $model->authorized_sign_contact     = $this->authorized_sign_contact;
        $model->authorized_security_contact = $this->authorized_security_contact;
        $model->partner_type                = $this->partner_type;

        $model->created_by = Yii::$app->user->id;

        if (!$model->save()) {
            throw new ErrorException('Something went wrong.');
        }

        $this->id = $model->id;

        // прикрепляю организацию к пользователю
        User::updateAll(['organization_id' => $model->id], ['id' => Yii::$app->user->identity->id]);

        return true;
    }
}
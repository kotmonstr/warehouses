<?php

namespace app\modules\api\v0\modules\user\controllers;

use app\models\User;
use app\modules\api\v0\models\forms\teamMember\CreateForm;
use app\modules\api\v0\models\forms\teamMember\SearchForm;
use app\modules\api\v0\models\MembershipRequest;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use Yii;
use app\models\Organization;
use app\models\Notification;
use yii\web\NotFoundHttpException;
use app\modules\api\v0\controllers\BaseController;

/**
 * Class TeamMembersController
 * @package app\modules\api\v0\modules\user\controllers
 */
class TeamMembersController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['request-index', 'request-my', 'request-cancel', 'request-accept', 'request-reject', 'index', 'delete', 'request-create', 'delete'],
            'rules' => [
                [
                    'actions' => ['request-index', 'request-my', 'request-cancel', 'request-accept', 'request-reject', 'index', 'delete', 'request-create', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'post' => ['request-index', 'request-my', 'request-cancel', 'request-accept', 'request-reject', 'index', 'delete', 'request-create', 'delete'],

            ],
        ];

        return $behaviors;
    }

    /**
     * @return SearchForm|array
     */
    public function actionRequestIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->searchTeamMemberRequests();
    }

    /**
     * @return MembershipRequest|null
     */
    public function actionRequestMy()
    {
        return MembershipRequest::findOne(['user_id' => Yii::$app->user->id]);
    }

    /**
     * @return array
     */
    public function actionRequestCancel()
    {
        MembershipRequest::deleteAll(['user_id' => Yii::$app->user->id]);

        return [];
    }

    /**
     * @return CreateForm
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRequestCreate()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->user_id = Yii::$app->user->id;
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\db\StaleObjectException
     */
    public function actionRequestAccept($id)
    {
        $model = MembershipRequest::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Сообщение не найдено');
        }

        $organization = Organization::find()->where(['id' => $model->organization_id])->one();
        $notificationUser = new Notification();
        $notificationUser->user_id = Yii::$app->user->identity->id;
        $notificationUser->title = "Одобрена заявка на присоединение к компании";
        $notificationUser->text = "Одобрена Ваша заявка на присоединение к компании" . $organization->title;

        $user = User::findOne($model->user_id);
        $user->organization_id = $model->organization_id;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (!$notificationUser->save()) {
                $transaction->rollBack();
            }

            if (!$user->save()) {
                $transaction->rollBack();
            }
            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        if (!$model->delete()) {
            throw new NotFoundHttpException('Oшибка удаления запроса');
        }

    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\db\StaleObjectException
     */
    public function actionRequestReject($id)
    {
        $model = MembershipRequest::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Сообщение не найдено');
        }

        $organization = Organization::find()->where(['id' => $model->organization_id])->one();
        $notificationUser = new Notification();
        $notificationUser->user_id = Yii::$app->user->identity->id;
        $notificationUser->title = "Заявка пользователя на вступление не одобренна";
        $notificationUser->text = "Ваша заявка на присоединение к компании " . $organization->title . " не одобрена";

        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (!$notificationUser->save()) {
                $transaction->rollBack();
                throw new NotFoundHttpException('Oшибка отправки сообщения');
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        if (!$model->delete()) {
            $transaction->rollBack();
            throw new NotFoundHttpException('Oшибка удаления запроса');
        }

    }


    /**
     * @return SearchForm|array
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->searchTeamMembers();
    }

    /**
     * @param $user_id
     * @return User|array|null
     * @throws NotFoundHttpException
     */
    public function actionDelete($user_id)
    {
        $user = User::findOne($user_id);
        if (!$user) {
            throw new NotFoundHttpException('Пользователь с индефикатором '.$user_id .' не найден.');
        }

        if($user->organization_id != Yii::$app->user->identity->organization_id){
            $user->addError('user_id', 'Пользователь с индефикатором '.$user_id .' не cосотоит в команде '. Yii::$app->user->identity->organization_id);
            return $user;
        }

        $user->organization_id = '';
        $user->updateAttributes(['organization_id']);

        return [];
    }

}
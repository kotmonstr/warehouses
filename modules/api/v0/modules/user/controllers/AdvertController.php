<?php

namespace app\modules\api\v0\modules\user\controllers;

use app\models\Advert;
use app\modules\api\v0\models\AdvertView;
use app\modules\api\v0\models\forms\advert\CreateForm;
use app\modules\api\v0\models\forms\advert\SearchForm;
use app\modules\api\v0\models\forms\advert\UpdateForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use app\modules\api\v0\controllers\BaseController;
use yii\web\NotFoundHttpException;

/**
 * Class AdvertController
 * @package app\modules\api\v0\modules\user\controllers
 */
class AdvertController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index', 'view', 'create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' =>  ['index', 'view', 'create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'index' => ['post'],
                'view' => ['post'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }

    /**
     * @return CreateForm
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        $model = new UpdateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPublish($id)
    {
        $model = Advert::updateAll(['status_sn' => Advert::STATUS_PUBLISHED], ['id' => $id, 'created_by' => Yii::$app->user->identity->id]);

        if (!$model) {
            throw new NotFoundHttpException('Обьявление не найденo');
        }

        return $id;
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = AdvertView::findOne(['id'=>$id,  'created_by' => Yii::$app->user->identity->id]);

        if (!$model) {
            throw new NotFoundHttpException('Обьявление не найденo');
        }

        return $model;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDelete($id)
    {
        Advert::updateAll(['deleted_at' => time()], ['id' => $id, 'created_by' => Yii::$app->user->identity->id]);

        return [];
    }
}

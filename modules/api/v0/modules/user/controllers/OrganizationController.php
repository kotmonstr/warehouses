<?php

namespace app\modules\api\v0\modules\user\controllers;

use app\models\User;
use app\modules\api\v0\controllers\BaseController;
use app\modules\api\v0\modules\user\models\forms\organization\CreateForm;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use Yii;
use app\modules\api\v0\models\Organization;
use yii\web\NotFoundHttpException;

/**
 * Class OrganizationController
 * @package app\modules\api\v0\modules\user\controllers
 */
class OrganizationController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['create', 'view'],
            'rules' => [
                [
                    'actions' =>  ['create', 'view'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'create' => ['post'],
                'view' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return CreateForm
     * @throws ErrorException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return  $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        $model = Organization::findById($identity->organization_id);

        if (!$model) {
            throw new NotFoundHttpException('Организация не найдена');
        }

        return $model;
    }
}
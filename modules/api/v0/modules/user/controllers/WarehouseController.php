<?php

namespace app\modules\api\v0\modules\user\controllers;

use app\models\Document;
use app\modules\api\v0\controllers\BaseController;
use app\modules\api\v0\models\forms\warehouse\CreateForm;
use app\modules\api\v0\models\forms\warehouse\IndexForm;
use app\modules\api\v0\models\forms\warehouse\SearchForm;
use app\modules\api\v0\models\forms\warehouse\UpdateFormDocuments;
use app\modules\api\v0\models\forms\warehouse\UpdateFormExternal;
use app\modules\api\v0\models\forms\warehouse\UpdateFormGeneral;
use app\modules\api\v0\models\forms\warehouse\UpdateFormInternal;
use app\modules\api\v0\models\WarehouseExternal;
use app\modules\api\v0\models\WarehouseGeneral;
use app\modules\api\v0\models\WarehouseInternal;
use app\modules\api\v0\models\WarehouseViewDocument;
use app\modules\api\v0\models\WarehouseViewInternal;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Warehouse;

/**
 * Class WarehouseController
 * @package app\modules\api\v0\modules\user\controllers
 */
class WarehouseController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => [
                'create',
                'index',
                'view-general',
                'view-external',
                'view-internal',
                'view-documents',
                'update-general',
                'update-external',
                'update-internal',
                'update-documents',
                'new_photo',
                'delete'
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'index',
                        'view-general',
                        'view-external',
                        'view-internal',
                        'view-documents',
                        'update-general',
                        'update-external',
                        'update-internal',
                        'update-documents',
                        'new_photo',
                        'delete'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'create' => ['post'],
                'index' => ['post'],
                'view-general' => ['post'],
                'view-external' => ['post'],
                'view-internal' => ['post'],
                'view-documents' => ['post'],
                'update-general' => ['post'],
                'update-external' => ['post'],
                'update-intarnal' => ['post'],
                'update-documents' => ['post'],
                'new_photo' => ['post'],
                'delete' => ['post'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return CreateForm
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new CreateForm();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->save();

        return $model;
    }


    /**
     * @return SearchForm|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchForm = new IndexForm();
        $searchForm->load(Yii::$app->request->getBodyParams(), '');

        if (!$searchForm->validate()) {
            return $searchForm;
        }

        return $searchForm->search();
    }

    /**
     * @param $id
     * @return Warehouse|null
     */
    public function actionViewGeneral($id)
    {
        $model = WarehouseGeneral::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Склад не найден');
        }

        return $model;

    }

    /**
     * @param $id
     * @return Warehouse|null
     */
    public function actionViewExternal($id)
    {
        $model = WarehouseExternal::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Склад не найден');
        }

        return $model;
    }

    /**
     * @param $id
     * @return WarehouseViewInternal|null
     * @throws NotFoundHttpException
     */
    public function actionViewInternal($id)
    {
        $model = WarehouseViewInternal::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException('Склад не найден');
        }

        return $model;
    }

    /**
     * @param $id
     * @return WarehouseViewDocument|null
     * @throws NotFoundHttpException
     */
    public function actionViewDocuments($id)
    {
        $model = WarehouseViewDocument::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Склад не найден');
        }

        return $model;
    }

    /**
     * @param $id
     * @return UpdateFormGeneral
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateGeneral($id)
    {
        $model = new UpdateFormGeneral();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @param $id
     * @return UpdateFormExternal
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateExternal($id)
    {

        $model = new UpdateFormExternal();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateInternal($id)
    {
        $model = new UpdateFormInternal();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateDocuments($id)
    {
        $model = new UpdateFormDocuments();
        $model->load(Yii::$app->request->getBodyParams(), '');
        $model->update($id);

        return $model;
    }

    /**
     * @return Document
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionNew_photo()
    {
        $model = new Document();
        $model->file = UploadedFile::getInstanceByName('file');
        if (!$model->file) {
            $model->addError('file', 'Wrong file or file format not correct. ');
            return $model;
        }

        $model->entity_field = 'photo';
        $model->entity_classname = Warehouse::class;
        $model->created_by = Yii::$app->user->id;
        if (!$model->save()) {
            throw new NotFoundHttpException('Что то пошло не так');
        }

        return $model;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = Warehouse::findOne(['id'=> $id, 'deleted_at' => null]);
        if (!$model) {
            throw new NotFoundHttpException('Склад не найден');
        }

        if($model->delete()){
            return [];
        }

    }

}
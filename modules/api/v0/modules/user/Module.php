<?php

namespace app\modules\api\v0\modules\user;

use Yii;
use yii\web\Response;

/**
 * Class Module
 * @package app\modules\api\v0\modules\user
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\v0\modules\user\controllers';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->user->enableSession = false;
        Yii::$app->language = 'en_US';
        Yii::$app->response->format = Response::FORMAT_JSON;
    }
}

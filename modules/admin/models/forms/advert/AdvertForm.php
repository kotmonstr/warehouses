<?php

namespace app\modules\admin\models\forms\advert;

use app\models\Advert;
use yii\base\Model;

/**
 * Class AdvertForm
 *
 * @property int|string $id
 */
class AdvertForm extends Model
{
    public $id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return array_merge($scenarios, [
            self::SCENARIO_CREATE => $scenarios[self::SCENARIO_DEFAULT],
            self::SCENARIO_UPDATE => array_merge(['id'], $scenarios[self::SCENARIO_DEFAULT]),
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return (new Advert)->attributeLabels();
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
        ]);
    }
}

<?php
/**
 * This is the file with model class for search in table 'advert'
 * on title organization and warehouse
 * on address and classification
 * on dates from and to
 * on storage type and status
 * php version 7
 */
namespace app\modules\admin\models\forms\advert;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\Advert;
use app\models\Warehouse;

/**
 * This is the model class for search in table 'advert'
 *
 * @property string $organizationName
 * @property string $warehouseName
 * @property string $city
 * @property string $available_from
 * @property string $available_to
 * @property string $storage_type_sn
 * @property string $status_sn
 */
class SearchForm extends Advert
{
    public $organizationName;
    public $warehouseName;
    public $city;
    public $storage_type_sn;
    public $status_sn;
    public $available_from;
    public $available_to;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'organizationName', 'warehouseName', 'address',
                    'available_from', 'available_to', 'storage_type_sn', 'status_sn', 'city'
                ], 'safe'
            ],
            [['available_from', 'available_to'], 'string'],
            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>=', 'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = Advert::find();
        $query->joinWith('warehouse')
            ->joinWith('organization')
            ->where(
                [
                    'organization.deleted_at' => null,
                    'warehouse.deleted_at' => null,
                    'advert.deleted_at' => null,
                ]
            );

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]
        );

        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['warehouse.organization_id' => $this->organizationName])
            ->andFilterWhere(['advert.warehouse_id' => $this->warehouseName])
            ->andFilterWhere(['warehouse.address_city_ym_geocoder' => $this->city])
            ->andFilterWhere(['advert.storage_type_sn' => $this->storage_type_sn])
            ->andFilterWhere(['advert.status_sn' => $this->status_sn])
            ->andFilterWhere(['<=', 'advert.available_from', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['>=', 'advert.available_to', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['<=', 'advert.available_from', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null])
            ->andFilterWhere(['>=', 'advert.available_to', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null]);

        return $dataProvider;
    }
}

<?php
/**
 * This is the file with form for creating 'advert'
 * php version 7
 */
namespace app\modules\admin\models\forms\advert;

use app\modules\admin\models\forms\advert\AdvertForm;
use Yii;
use app\models\Advert;
use app\models\Warehouse;
use yii\helpers\ArrayHelper;
use app\models\DictionaryItem;
use app\models\User;
use app\models\Reserve;
use app\validators\NumberValidator;

/**
 * Create Advert Form
 *
 * @property int $id
 * @property int $warehouse_id
 * @property string $storage_type_sn
 * @property float $available_space
 * @property string $available_space_type_sn
 * @property float $unit_cost_per_day
 * @property int $available_from
 * @property int $available_to
 * @property string $manager_name
 * @property string $manager_phone_number
 * @property string $manager_email
 * @property string $comment
 * @property string $status_sn
 *
 * @property-read Warehouse $warehouse
 * @property-read User $user
 */
class Form extends AdvertForm
{
    public $warehouse_id;
    public $storage_type_sn;
    public $available_space;
    public $available_space_type_sn;
    public $unit_cost_per_day;
    public $available_from;
    public $available_to;
    public $manager_name;
    public $manager_phone_number;
    public $manager_email;
    public $comment;
    public $status_sn;

    /**
     * {@inheritdoc}
     *
     * @return array the validation rules.
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'warehouse_id', 'storage_type_sn', 'available_space',
                    'available_space_type_sn', 'unit_cost_per_day', 'available_from',
                    'available_to', 'manager_name', 'manager_email',
                    'status_sn', 'manager_phone_number'
                ], 'required'
            ],
            [['warehouse_id'], 'integer'],
            [
                [
                    'storage_type_sn', 'available_space_type_sn', 'manager_name',
                    'manager_email', 'status_sn', 'manager_phone_number'
                ], 'string', 'max' => 255
            ],
            [['manager_phone_number'], 'filter', 'filter' => function ($value) {
                if (!empty($value)) {
                    return str_replace("_", "", $value);
                }
            }],
            [['manager_phone_number'], 'string', 'length' => 18, 'message' => 'Введите номер телефона полностью'],
            [['available_space', 'unit_cost_per_day'], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'],
            [['manager_email'], 'email'],
            [['comment'], 'string'],
            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>', 'skipOnEmpty' => true],
            [['available_to', 'available_from'], 'validateDates'],
            [['available_space'], 'validateSquare'],
        ]);
    }

    /**
     * Validates dates,
     * this method serves as the inline validation for dates.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateDates($attribute, $params) {
        if ($this->status_sn == Advert::STATUS_PUBLISHED) {
            switch ($attribute) {
            case 'available_from':
                if (strtotime($this->$attribute) < (time() - (60 * 60 * 24))) {
                    $this->addError($attribute, 'Дата должна быть не раньше сегодня');
                }
                break;
            case 'available_to':
                if (strtotime($this->$attribute) > (time() + (60 * 60 * 24 * 365 * 5))) {
                    $this->addError($attribute, 'Дата должна быть не позже, чем через 5 лет');
                }
                break;
            }
        }
    }

    /**
     * Validates square,
     * this method serves as the inline validation for availabel square.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validateSquare($attribute, $params) {
        if ($this->status_sn == Advert::STATUS_PUBLISHED) {
            $advertsQuery = Advert::find()->where(['warehouse_id' => $this->warehouse_id, 'deleted_at' => null])
                ->andWhere(['<=', 'available_from', Yii::$app->formatter->format($this->available_to, 'timestamp')])
                ->andWhere(['>=', 'available_to', Yii::$app->formatter->format($this->available_from, 'timestamp')]);

            if ($this->id) {
                $advertsQuery->andWhere('id != ' . $this->id);
            }

            $advertSquares = ArrayHelper::map($advertsQuery->all(), 'id', 'available_space');

            $totalAdvertSquares = 0;
            $advertIdxs = [];

            if (count($advertSquares) > 0) {
                $totalAdvertSquares = array_sum($advertSquares);
                $advertIdxs = array_keys($advertSquares);
            }

            if ($this->id) {
                array_push($advertIdxs, $this->id);
            }

            if (count($advertIdxs)) {
                $reserveSquares = ArrayHelper::getColumn(
                    Reserve::find()->where(
                        [
                            'deleted_at' => null,
                            'status_sn' => Reserve::STATUS_ACTIVE,
                            'advert_id' => $advertIdxs
                        ])
                        ->andWhere(['<=', 'rent_from', Yii::$app->formatter->format($this->available_to, 'timestamp')])
                        ->andWhere(['>=', 'rent_to', Yii::$app->formatter->format($this->available_from, 'timestamp')])
                        ->all(),
                    'amount'
                );

                if (count($reserveSquares)) {
                    $totalAdvertSquares += array_sum($reserveSquares);
                }
            }


            $restSquare = Warehouse::findModel($this->warehouse_id)->square_area - $totalAdvertSquares;

            if ($restSquare <= 0) {
                $this->addError($attribute, 'У склада не осталось свободных площадей');
                return;
            }

            if ($this->$attribute == 0) {
                $this->addError($attribute, "Сдаваемая площадь должна быть больше 0");
                return;
            }

            if ($this->$attribute > $restSquare) {
                $this->addError($attribute, "У склада осталось всего {$restSquare} м² под сдачу");
                return;
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
        ]);
    }

    /**
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $model = Advert::findModel($this->id);
        } else {
            $model = new Advert();
            $model->warehouse_id = $this->warehouse_id;
            $model->created_by = Warehouse::findModel($this->warehouse_id)->created_by;
        }

        $model->storage_type_sn = $this->storage_type_sn;
        $model->available_space = $this->available_space;
        $model->available_space_type_sn = $this->available_space_type_sn;
        $model->unit_cost_per_day = $this->unit_cost_per_day;
        $model->available_from = $this->available_from;
        $model->available_to = $this->available_to;
        $model->manager_name = $this->manager_name;
        $model->manager_email = $this->manager_email;
        $model->status_sn = $this->status_sn;

        if ($this->comment) {
            $model->comment = $this->comment;
        }
        if ($this->manager_phone_number) {
            $model->manager_phone_number = $this->manager_phone_number;
        }

        if (!$model->save()) {
            $transaction->rollBack();
            $this->addErrors($model->errors);
            return false;
        }

        $this->id = $model->id;

        $transaction->commit();

        return true;

    }

    /**
     * Function for get list of warehouses
     *
     * @return string[]
     */
    public function warehouses()
    {
        $warehouses = Warehouse::findAll(
            [
                'deleted_at' => null,
                'sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                'dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
            ]
        );
        return ArrayHelper::map($warehouses, 'id', 'title');
    }

    /**
     * @return ActiveQuery
     */
    public function getAvailable_space_type()
    {
        return $this->hasOne(DictionaryItem::class, ['system_name' => 'available_space_type_sn']);
    }

    /**
     * Function for get warehouse
     * @property-read Warehouse $warehouse
     * @return        Warehouse
     */
    public function getWarehouse()
    {
        return Warehouse::findModel($this->warehouse_id);
    }

    /**
     * Function for get user
     * @property-read User $user
     * @return        User
     */
    public function getUser()
    {
        $advert = Advert::findModel($this->id);
        return User::findOne(['id' => $advert->created_by, 'deleted_at' => null]);
    }
}

<?php

namespace app\modules\admin\models\forms\warehouse;

use Yii;
use app\models\Warehouse;
use app\models\Document;
use app\models\File;
use yii\web\UploadedFile;
/**
 * Class DocumentsForm
 * @package app\modules\admin\models\forms\warehouse
 *
 * @property UploadedFile $documents_file_1
 * @property UploadedFile $documents_file_2
 * @property UploadedFile $documents_file_3
 */
class DocumentsForm extends WarehouseForm
{
    const ALIAS = 'documents';

    public $documents_file_1;
    public $documents_file_2;
    public $documents_file_3;

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'documents_file_1', 'documents_file_2', 'documents_file_3'
                ], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'
            ],
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'documents_file_1' => 'Документ 1',
            'documents_file_2' => 'Документ 2',
            'documents_file_3' => 'Документ 3',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем основную модель */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        $warehouse = Warehouse::findModel($this->id);

        $warehouse->documents_is_set = true;
        $warehouse->internal_is_set = true;
        $warehouse->external_is_set = true;
        $warehouse->general_is_set = true;

        if (!$warehouse->save()) {
            $transaction->rollBack();
            $this->addErrors($warehouse->errors);
            return false;
        }


        /*~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем документы */
        /*~~~~~~~~~~~~~~~~~~~~~*/

        if ($this->documents_file_1) {
            $this->createDocWithFile(
                $this->documents_file_1,
                'documents_file_1',
                $transaction
            );
        }
        if ($this->documents_file_2) {
            $this->createDocWithFile(
                $this->documents_file_2,
                'documents_file_2',
                $transaction
            );
        }
        if ($this->documents_file_3) {
            $this->createDocWithFile(
                $this->documents_file_3,
                'documents_file_3',
                $transaction
            );
        }

        $transaction->commit();

        return true;
    }

    /**
     * Function for save document with file
     *
     * @param UploadedFile       $uploadedFile
     * @param string             $entity_field
     * @param yii\db\Transaction $transaction
     *
     * @return bool|null
     */
    function createDocWithFile($uploadedFile, $entity_field, $transaction)
    {
        if ($uploadedFile) {
            $document = new Document();
            $document->entity_classname = Warehouse::class;
            $document->entity_field = $entity_field;
            $document->entity_id = $this->id;
            $document->created_by = $this->created_by;
            $document->file = $uploadedFile;

            if (!$document->save()) {
                $transaction->rollBack();
                $this->addErrors($document->errors);
                return false;
            }
        }
    }

    /**
     * Function for get paths of uploaded files
     *
     * @return array
     */
    public function findFilePaths()
    {
        $documents = Document::findAllByEntity($this->id, Warehouse::class);
        $documents_paths = [];
        if ($documents) {

            foreach ($documents as $document) {
                if ($document->entity_field && (substr_compare($document->entity_field, 'photo_', 0, 6) != 0)) {
                    $file = File::findOne(['document_id' => $document['id']]);
                    if ($file) {
                        $documents_paths[$document->entity_field] = [
                            'name' => $file->name,
                            'path' => $file->path
                        ];
                    }
                }
            }

        }
        return $documents_paths;
    }

}

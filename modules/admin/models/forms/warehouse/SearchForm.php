<?php

/**
 * This is the file with model class for search in table 'warehouse'
 * on title and address,
 * on organization title and storage system
 * php version 7
 */
namespace app\modules\admin\models\forms\warehouse;

use yii\data\ActiveDataProvider;
use app\models\Warehouse;
use app\models\WhStorageSystem;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for search in table 'warehouse'
 *
 * @property string $title
 * @property string $address_ym_geocoder
 * @property int $organization_id
 * @property string $organizationName
 * @property string $storage_s
 */
class SearchForm extends Warehouse
{
    public $organizationName;
    public $storage_s;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'address_ym_geocoder', 'organization_id', 'organizationName', 'storage_s'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = Warehouse::find()->where(['warehouse.deleted_at' => null]);
        $query->joinWith('organization')
            ->andFilterWhere(['organization.id' => $this->organization_id])
            ->andWhere([
                'warehouse.general_is_set' => true,
                'warehouse.external_is_set' => true,
                'warehouse.internal_is_set' => true,
                'warehouse.documents_is_set' => true,
                'organization.deleted_at' => null,
                'warehouse.deleted_at' => null,
            ]);

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]
        );

        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ILIKE', 'organization.title', $this->organizationName])
            ->andFilterWhere(['ILIKE', 'warehouse.title', $this->title])
            ->andFilterWhere(['ILIKE', 'warehouse.address_ym_geocoder', $this->address_ym_geocoder]);

        if ($this->storage_s) {
            $wh_ss = WhStorageSystem::findAll(['storage_system_sn' => $this->storage_s]);
            $ids = ArrayHelper::getColumn($wh_ss, 'warehouse_id');
            $query->andFilterWhere(['warehouse.id' => $ids]);
        }

        return $dataProvider;
    }
}

<?php

namespace app\modules\admin\models\forms\warehouse;

use app\models\Warehouse;
use yii\base\Model;
use yii\base\Exception;
use app\models\Organization;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class WarehouseForm
 *
 * @property int $id
 * @property int $organization_id
 * @property int $created_at
 * @property int $created_by
 * @property string $sb_verification_status_sn
 * @property string $verification_comment
 * @property string $dsl_verification_status_sn
 *
 * @property-read User $user
 */
class WarehouseForm extends Model
{
    public $id;
    public $organization_id;
    public $created_at;
    public $sb_verification_status_sn;
    public $verification_comment;
    public $dsl_verification_status_sn;
    public $created_by;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return array_merge($scenarios, [
            self::SCENARIO_CREATE => $scenarios[self::SCENARIO_DEFAULT],
            self::SCENARIO_UPDATE => array_merge(['id'], $scenarios[self::SCENARIO_DEFAULT]),
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return (new Warehouse)->attributeLabels();
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['organization_id', 'created_at', 'created_by'], 'integer'],
            [['sb_verification_status_sn', 'dsl_verification_status_sn'], 'string', 'max' => 255],
            [['verification_comment'], 'string'],
        ]);
    }

    /**
     * Relations with 'organization'
     *
     * @property-read Organization $organization
     * @return        Organization $organization
     */
    public function getOrganization()
    {
        return Organization::findOne(['id' => $this->organization_id, 'deleted_at' => null]);
    }

    /**
     * Function for get list of users id=>email
     *
     * @return string[]
     */
    public function users()
    {
        $users = User::findAll(['deleted_at' => null]);
        $usersArray = ArrayHelper::map($users, function ($element) {
            if ($element['organization_id'] && ($element['role'] == User::ROLE_HOLDER)) {
                return $element['id'];
            }
        }, function ($element) {
            if ($element['organization_id'] && ($element['role'] == User::ROLE_HOLDER)) {
                return $element['email'];
            }
        });
        return array_filter($usersArray);
    }

    /**
     * Relations with 'user'
     *
     * @property-read User $user
     * @return        User $user
     */
    public function getUser()
    {
        return User::findOne(['id' => $this->created_by, 'deleted_at' => null]);
    }
}

<?php

namespace app\modules\admin\models\forms\warehouse;

use app\models\Warehouse;
use app\models\WhLoadUnloadEquipment;
use app\models\WhLoadUnloadCranage;
use app\models\WhStorageSystem;
use app\models\WhLoadingUnloadingStructType;
use app\models\WhOfficeFacilityType;
use app\validators\NumberValidator;
use matrozov\yii2multipleField\validators\ModelValidator;
use Yii;
use app\validators\DictItemValidator;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class InternalForm
 * @package app\modules\admin\models\forms\warehouse
 *
 * @property WhLoadUnloadEquipment[] $loading_unloading_tech_types
 * @property WhLoadUnloadCranage[] $cranage_types
 *
 * @property float $column_distance
 * @property string $column_distance_type_sn
 * @property float $span_distance
 * @property string $span_distance_type_sn
 * @property bool $column_protection
 * @property string $floor_covering_sn
 * @property bool $anti_dust_covering
 * @property float $height_above_ground
 * @property string $height_above_ground_type_sn
 * @property float $load_per_square_meter
 * @property string $load_per_square_meter_type_sn
 * @property bool $ventilation
 * @property bool $conditioning
 * @property string $temperature_mode_sn
 * @property float $min_temperature
 * @property string $min_temperature_type_sn
 * @property float $max_temperature
 * @property string $max_temperature_type_sn
 * @property bool $security_guard
 * @property bool $security_video
 * @property bool $security_alarm
 * @property bool $fire_alarm
 * @property string $fire_ext_system_sn
 * @property bool $fire_charging
 * @property bool $fire_tanks_and_pump
 * @property string $electricity_grid_sn
 * @property string $heating_system_sn
 * @property bool $water_hot
 * @property bool $water_cold
 * @property bool $water_sewerage
 * @property bool $internet
 * @property bool $phone
 * @property bool $fiber_optic
 * @property string[] $loading_unloading_struct_types_sn
 * @property string[] $office_facility_types_inside_sn
 * @property string[] $office_facility_types_outside_sn
 * @property string[] $storage_system_sns
 */
class InternalForm extends WarehouseForm
{
    const ALIAS = 'internal';

    public $column_distance;
    public $column_distance_type_sn;
    public $span_distance;
    public $span_distance_type_sn;
    public $column_protection;
    public $floor_covering_sn;
    public $anti_dust_covering;
    public $height_above_ground;
    public $height_above_ground_type_sn;
    public $load_per_square_meter;
    public $load_per_square_meter_type_sn;
    public $ventilation;
    public $conditioning;
    public $temperature_mode_sn;
    public $min_temperature;
    public $min_temperature_type_sn;
    public $max_temperature;
    public $max_temperature_type_sn;
    public $security_guard;
    public $security_video;
    public $security_alarm;
    public $fire_alarm;
    public $fire_ext_system_sn;
    public $fire_charging;
    public $fire_tanks_and_pump;
    public $electricity_grid_sn;
    public $heating_system_sn;
    public $water_hot;
    public $water_cold;
    public $water_sewerage;
    public $internet;
    public $phone;
    public $fiber_optic;
    public $storage_system_sns;
    public $loading_unloading_struct_types_sn;
    public $office_facility_types_inside_sn;
    public $office_facility_types_outside_sn;
    public $loading_unloading_tech_types;
    public $cranage_types;

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'column_distance_type_sn', 'span_distance_type_sn',
                    'floor_covering_sn', 'height_above_ground_type_sn',
                    'load_per_square_meter_type_sn', 'temperature_mode_sn',
                    'min_temperature_type_sn', 'max_temperature_type_sn',
                    'fire_ext_system_sn', 'electricity_grid_sn',
                    'heating_system_sn'
                ], 'string', 'max' => 255
            ],
            [
                [
                    'column_distance_type_sn', 'span_distance_type_sn',
                    'height_above_ground_type_sn'
                ], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'distance'
            ],
            [['floor_covering_sn'], DictItemValidator::class, 'dictionary_sn' => 'floor_covering'],
            [['load_per_square_meter_type_sn'], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'mass'],
            [['temperature_mode_sn'], DictItemValidator::class, 'dictionary_sn' => 'temperature_mode'],
            [['min_temperature_type_sn', 'max_temperature_type_sn'], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'temperature'],
            [['fire_ext_system_sn'], DictItemValidator::class, 'dictionary_sn' => 'fire_ext_system_type'],
            [['electricity_grid_sn'], DictItemValidator::class, 'dictionary_sn' => 'electricity_grid_type'],
            [['heating_system_sn'], DictItemValidator::class, 'dictionary_sn' => 'heating_system_type'],
            [
                [
                    'column_protection', 'anti_dust_covering', 'ventilation',
                    'conditioning', 'security_guard', 'security_video',
                    'security_alarm', 'fire_alarm', 'fire_charging',
                    'fire_tanks_and_pump', 'water_hot', 'water_cold',
                    'water_sewerage', 'internet', 'phone', 'fiber_optic'
                ], 'boolean'
            ],
            [
                [
                    'column_distance', 'span_distance',
                    'height_above_ground', 'load_per_square_meter'
                ], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'
            ],
            [
                [
                    'min_temperature', 'max_temperature'
                ], NumberValidator::class,
                'min' => -100,
                'max' => 100,
                'numberPattern' => '/^(-)?\d+([\.\,]\d{1,2})?$/',
                'message' => 'Число максимум с двумя знаками после запятой'
            ],
            [['max_temperature'], 'compare', 'compareAttribute' => 'min_temperature', 'operator' => '>=', 'skipOnEmpty' => true, 'type' => NumberValidator::class],
            [
                [
                    'storage_system_sns', 'loading_unloading_struct_types_sn',
                    'office_facility_types_inside_sn', 'office_facility_types_outside_sn'
                ], 'each', 'rule' => ['string', 'max' => 255]
            ],
            [['storage_system_sns'], 'each', 'rule' => [
                DictItemValidator::class,
                'dictionary_sn' => 'storage_system'
            ]],
            [['loading_unloading_struct_types_sn'], 'each', 'rule' => [
                DictItemValidator::class,
                'dictionary_sn' => 'loading_unloading_struct_types'
            ]],
            [['office_facility_types_inside_sn', 'office_facility_types_outside_sn'], 'each', 'rule' => [
                DictItemValidator::class,
                'dictionary_sn' => 'office_facility_types'
            ]],
            [['loading_unloading_tech_types'], 'each', 'rule' => [ModelValidator::class, 'model' => WhLoadUnloadEquipment::class]],
            [['cranage_types'], 'each', 'rule' => [ModelValidator::class, 'model' => WhLoadUnloadCranage::class]],
            [
                [
                    'storage_system_sns', 'loading_unloading_struct_types_sn',
                    'office_facility_types_inside_sn', 'office_facility_types_outside_sn',
                    'loading_unloading_tech_types', 'cranage_types'
                ], 'default', 'value' => []
            ],
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'storage_system_sns' => 'Системы хранения',
            'loading_unloading_struct_types_sn' => 'Разгрузочно/погрузочные конструкции',
            'office_facility_types_inside_sn' => 'на территории',
            'office_facility_types_outside_sn' => 'не на территории',
        ]);
    }

    /**
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем основную модель */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        $warehouse = Warehouse::findModel($this->id);

        $warehouse->column_distance = $this->column_distance;
        $warehouse->column_distance_type_sn = $this->column_distance_type_sn;
        $warehouse->span_distance = $this->span_distance;
        $warehouse->span_distance_type_sn = $this->span_distance_type_sn;
        $warehouse->column_protection = $this->column_protection;
        $warehouse->floor_covering_sn = $this->floor_covering_sn;
        $warehouse->anti_dust_covering = $this->anti_dust_covering;
        $warehouse->height_above_ground = $this->height_above_ground;
        $warehouse->height_above_ground_type_sn = $this->height_above_ground_type_sn;
        $warehouse->load_per_square_meter = $this->load_per_square_meter;
        $warehouse->load_per_square_meter_type_sn = $this->load_per_square_meter_type_sn;
        $warehouse->ventilation = $this->ventilation;
        $warehouse->conditioning = $this->conditioning;
        $warehouse->temperature_mode_sn = $this->temperature_mode_sn;
        $warehouse->min_temperature = $this->min_temperature;
        $warehouse->min_temperature_type_sn = $this->min_temperature_type_sn;
        $warehouse->max_temperature = $this->max_temperature;
        $warehouse->max_temperature_type_sn = $this->max_temperature_type_sn;
        $warehouse->security_guard = $this->security_guard;
        $warehouse->security_video = $this->security_video;
        $warehouse->security_alarm = $this->security_alarm;
        $warehouse->fire_alarm = $this->fire_alarm;
        $warehouse->fire_ext_system_sn = $this->fire_ext_system_sn;
        $warehouse->fire_charging = $this->fire_charging;
        $warehouse->fire_tanks_and_pump = $this->fire_tanks_and_pump;
        $warehouse->electricity_grid_sn = $this->electricity_grid_sn;
        $warehouse->heating_system_sn = $this->heating_system_sn;
        $warehouse->water_cold = $this->water_cold;
        $warehouse->water_hot = $this->water_hot;
        $warehouse->water_sewerage = $this->water_sewerage;
        $warehouse->internet = $this->internet;
        $warehouse->phone = $this->phone;
        $warehouse->fiber_optic = $this->fiber_optic;

        $warehouse->internal_is_set = true;
        $warehouse->external_is_set = true;
        $warehouse->general_is_set = true;
        $warehouse->documents_is_set = true;

        if (!$warehouse->save()) {
            $transaction->rollBack();
            $this->addErrors($warehouse->errors);
            return false;
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанные "Системы хранения" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhStorageSystem::deleteAll(['warehouse_id' => $warehouse->id]);

        foreach ($this->storage_system_sns as $sn) {
            $wh_ss = new WhStorageSystem();

            $wh_ss->warehouse_id = $warehouse->id;
            $wh_ss->storage_system_sn = $sn;

            if (!$wh_ss->save()) {
                $transaction->rollBack();
                $this->addError('storage_system_sns', 'Указано неверное значение');
                return false;
            }
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанные "Разгрузочно/погрузочные конструкции" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhLoadingUnloadingStructType::deleteAll(['warehouse_id' => $warehouse->id]);

        foreach ($this->loading_unloading_struct_types_sn as $sn) {
            $wh_lust = new WhLoadingUnloadingStructType();

            $wh_lust->warehouse_id = $warehouse->id;
            $wh_lust->loading_unloading_struct_type_sn = $sn;

            if (!$wh_lust->save()) {
                $transaction->rollBack();
                $this->addError('loading_unloading_struct_types_sn', 'Указано неверное значение');
                return false;
            }
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанные "Офисные помещения на территории" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhOfficeFacilityType::deleteAll(['warehouse_id' => $warehouse->id, 'types' => 'inside']);

        foreach ($this->office_facility_types_inside_sn as $sn) {
            $wh_ofti = new WhOfficeFacilityType();

            $wh_ofti->warehouse_id = $warehouse->id;
            $wh_ofti->office_facility_types_sn = $sn;
            $wh_ofti->types = 'inside';

            if (!$wh_ofti->save()) {
                $transaction->rollBack();
                $this->addError('office_facility_types_inside_sn', 'Указано неверное значение');
                return false;
            }
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанные "Офисные помещения не на территории" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhOfficeFacilityType::deleteAll(['warehouse_id' => $warehouse->id, 'types' => 'outside']);

        foreach ($this->office_facility_types_outside_sn as $sn) {
            $wh_ofto = new WhOfficeFacilityType();

            $wh_ofto->warehouse_id = $warehouse->id;
            $wh_ofto->office_facility_types_sn = $sn;
            $wh_ofto->types = 'outside';

            if (!$wh_ofto->save()) {
                $transaction->rollBack();
                $this->addError('office_facility_types_outside_sn', 'Указано неверное значение');
                return false;
            }
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанную "Погрузочно-разгрузочную технику" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhLoadUnloadEquipment::deleteAll(['warehouse_id' => $warehouse->id]);

        if (count($this->loading_unloading_tech_types) > 0) {
            foreach ($this->loading_unloading_tech_types as $subModel) {
                /** @var WhLoadUnloadEquipment $subModel */
                if ($subModel['warehouse_id'] && $subModel['amount'] && $subModel['capacity']) {
                    if (!$subModel->save()) {
                        $transaction->rollBack();
                        $this->addErrors($subModel->errors); // todo: Need to check
                        return false;
                    }
                }
            }
        }


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанную "Крановове оборудование" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhLoadUnloadCranage::deleteAll(['warehouse_id' => $warehouse->id]);

        if (count($this->cranage_types) > 0) {
            foreach ($this->cranage_types as $subModel) {
                /** @var WhLoadUnloadCranage $subModel */
                if ($subModel['warehouse_id'] && $subModel['amount'] && $subModel['capacity']) {
                    if (!$subModel->save()) {
                        $transaction->rollBack();
                        $this->addErrors($subModel->errors); // todo: Need to check
                        return false;
                    }
                }
            }
        }

        $transaction->commit();

        return true;
    }
}

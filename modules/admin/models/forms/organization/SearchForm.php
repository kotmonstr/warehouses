<?php

/**
 * This is the file with model class for search in table 'organization'
 * on title and inn
 * php version 7
 */
namespace app\modules\admin\models\forms\organization;

use yii\data\ActiveDataProvider;
use app\models\Organization;

/**
 * This is the model class for search in table 'organization'
 *
 * @property string $title
 * @property string $inn
 */
class SearchForm extends Organization
{
    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'inn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = Organization::find()->where(['deleted_at' => null]);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 7,
                ],
            ]
        );

        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ILIKE', 'title', $this->title])
            ->andFilterWhere(['like', 'inn', $this->inn]);

        return $dataProvider;
    }
}

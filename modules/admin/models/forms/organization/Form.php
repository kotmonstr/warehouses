<?php

namespace app\modules\admin\models\forms\organization;

use app\models\Document;
use app\models\User;
use Yii;
use app\models\Organization;
use Exception;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Warehouse;

/**
 * Create Organization Form
 *
 * @property string $title
 * @property string $legal_type
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $okpo
 * @property string $main_activity
 * @property string $taxation_type
 * @property string $address_ym_geocoder
 * @property string $verification_status_sn
 * @property int $created_at
 * @property int $created_by
 *
 * @property string $shareholders
 * @property string $edm_type
 * @property string $post_address
 * @property string $fact_address
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $start_capital
 * @property string $registration_date
 * @property int    $employee_count
 * @property string $bank_details
 * @property string $authorized_head_contact
 * @property string $authorized_sign_contact
 * @property string $authorized_security_contact
 * @property string $partner_type
 *
 * @property UploadedFile $arendatorsList;
 * @property UploadedFile $noDebtsReference;
 * @property UploadedFile $rentContractCopy;
 * @property UploadedFile $warehouseCadastralPassport;
 * @property UploadedFile $propertyRightsCopy;
 * @property UploadedFile $taxStampCopy;
 * @property UploadedFile $taxRegistrationCopyUL;
 * @property UploadedFile $naznachenieIspolnitelyaCopy;
 * @property UploadedFile $doverennostNaPodpisanieCopy;
 * @property UploadedFile $headSignatureCopy;
 * @property UploadedFile $financialReportCopy;
 * @property UploadedFile $passportCopyIP;
 * @property UploadedFile $taxRegistrationCopyIP;
 *
 * @property-read User $user
 */
class Form extends OrganizationForm
{
    public $title;
    public $legal_type;
    public $inn;
    public $ogrn;
    public $kpp;
    public $okpo;
    public $main_activity;
    public $taxation_type;
    public $address_ym_geocoder;
    public $verification_status_sn;
    public $created_at;
    public $created_by;

    public $shareholders;
    public $edm_type;
    public $post_address;
    public $fact_address;
    public $contact_phone;
    public $contact_email;
    public $start_capital;
    public $registration_date;
    public $employee_count;
    public $bank_details;
    public $authorized_head_contact;
    public $authorized_sign_contact;
    public $authorized_security_contact;
    public $partner_type;

    // Common Files
    // Список арендаторов, имеющих право пользоваться складом (если их несколько)
    public $arendatorsList;
    // Справка налогового органа об отсутствии задолженностей по налогам и сборам.
    public $noDebtsReference;
    // Копия договора аренды или свидетельства о собственности на помещение (либо выписка из ЕГРН) по месту регистрации и фактическому месту нахождения.
    public $rentContractCopy;
    // Кадастровый паспорт складского помещения, сдаваемого в аренду
    public $warehouseCadastralPassport;
    // Копия свидетельства о праве собственности на складское помещение, сдаваемое в аренду
    public $propertyRightsCopy;

    // UL Files
    // Копия устава со штампом налогового органа (актуальная редакция, с учетом внесенных изменений).
    public $taxStampCopy;
    // Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРЮЛ).
    public $taxRegistrationCopyUL;
    // Копия решения и копия приказа о назначении единоличного исполнительного органа (либо о передаче полномочий единоличного исполнительного органа управляющей компании).
    public $naznachenieIspolnitelyaCopy;
    // Копия доверенности на право подписи договоров и первичных документов: актов, накладных, счетов, счетов-фактур, УПД от имени контрагента (в случае подписания таких документов лицами, не имеющими права действовать от имени юридического лица без доверенности).
    public $doverennostNaPodpisanieCopy;
    // Копия образца подписи руководителя, заверенная нотариально или банком (заверенная копия банковской карточки) либо копии страниц 2, 3 паспорта руководителя.
    public $headSignatureCopy;
    // Копия бухгалтерской отчетности на последнюю отчетную дату перед заключением договора (форма 1,2,3,4, пояснения) с квитанций об электронной сдаче отчетности.
    public $financialReportCopy;

    // IP Files
    // Копии страниц 2, 3 паспорта индивидуального предпринимателя.
    public $passportCopyIP;
    // Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРИП).
    public $taxRegistrationCopyIP;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'inn', 'ogrn'], 'required'],
            [
                [
                    'shareholders', 'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'employee_count', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'required', 'when' => function (self $model) {
                return $model->legal_type == 'UL';
            }
            ],
            [
                [
                    'kpp', 'okpo', 'main_activity',
                    'taxation_type', 'address_ym_geocoder', 'legal_type',
                    'verification_status_sn',
                    'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'string', 'max' => 255
            ],
            [['shareholders'], 'string'],
            [
                [
                    'taxStampCopy',
                    'taxRegistrationCopyUL',
                    'naznachenieIspolnitelyaCopy',
                    'doverennostNaPodpisanieCopy',
                    'headSignatureCopy',
                    'financialReportCopy',
                    'arendatorsList',
                    'noDebtsReference',
                    'rentContractCopy',
                    'warehouseCadastralPassport',
                    'propertyRightsCopy',
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'pdf',
                'minSize' => 1,
                'when' => function ($model) {
                    return $model->legal_type == 'UL';
                }
            ],
            [
                [
                    'passportCopyIP',
                    'taxRegistrationCopyIP',
                    'arendatorsList',
                    'noDebtsReference',
                    'rentContractCopy',
                    'warehouseCadastralPassport',
                    'propertyRightsCopy',
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'pdf',
                'minSize' => 1,
                'when' => function ($model) {
                    return $model->legal_type == 'IP';
                }
            ],
            [['created_at', 'created_by', 'employee_count'], 'integer'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'arendatorsList' => 'Список арендаторов, имеющих право пользоваться складом',
            'noDebtsReference' => 'Справка налогового органа об отсутствии задолженностей по налогам и сборам',
            'rentContractCopy' => 'Копия договора аренды или свидетельства о собственности на помещение (либо выписка из ЕГРН) по месту регистрации и фактическому месту нахождения',
            'warehouseCadastralPassport' => 'Кадастровый паспорт складского помещения',
            'propertyRightsCopy' => 'Копия свидетельства о праве собственности на складское помещение, сдаваемое в аренду',
            'taxStampCopy' => 'Копия устава со штампом налогового органа (актуальная редакция, с учетом внесенных изменений)',
            'taxRegistrationCopyUL' => 'Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРЮЛ)',
            'naznachenieIspolnitelyaCopy' => 'Копия решения и копия приказа о назначении единоличного исполнительного органа (либо о передаче полномочий единоличного исполнительного органа управляющей компании)',
            'doverennostNaPodpisanieCopy' => 'Копия доверенности на право подписи договоров и первичных документов: актов, накладных, счетов, счетов-фактур, УПД от имени контрагента (в случае подписания таких документов лицами, не имеющими права действовать от имени юридического лица без доверенности)',
            'headSignatureCopy' => 'Копия образца подписи руководителя, заверенная нотариально или банком (заверенная копия банковской карточки) либо копии страниц 2, 3 паспорта руководителя',
            'financialReportCopy' => 'Копия бухгалтерской отчетности на последнюю отчетную дату перед заключением договора (форма 1,2,3,4, пояснения) с квитанций об электронной сдаче отчетности',
            'passportCopyIP' => 'Копии страниц 2, 3 паспорта индивидуального предпринимателя',
            'taxRegistrationCopyIP' => 'Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРИП)',
        ]);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $model = Organization::findModel($this->id);
        } else {
            $model = new Organization();
            $model->created_by = $this->created_by;
            $user = $this->user;
        }

        $model->title = $this->title;
        $model->legal_type = $this->legal_type;
        $model->inn = $this->inn;
        $model->ogrn = $this->ogrn;
        $model->kpp = $this->kpp;
        $model->okpo = $this->okpo;
        $model->main_activity = $this->main_activity;
        $model->taxation_type = $this->taxation_type;
        $model->address_ym_geocoder = $this->address_ym_geocoder;

        $model->shareholders = $this->shareholders;
        $model->edm_type = $this->edm_type;
        $model->post_address = $this->post_address;
        $model->fact_address = $this->fact_address;
        $model->contact_phone = $this->contact_phone;
        $model->contact_email = $this->contact_email;
        $model->start_capital = $this->start_capital;
        $model->registration_date = $this->registration_date;
        $model->employee_count = $this->employee_count;
        $model->bank_details = $this->bank_details;
        $model->authorized_head_contact = $this->authorized_head_contact;
        $model->authorized_sign_contact = $this->authorized_sign_contact;
        $model->authorized_security_contact = $this->authorized_security_contact;
        $model->partner_type = $this->partner_type;

        if (!$model->save()) {
            $transaction->rollBack();
            $this->addErrors($model->errors);
            return false;
        }

        $this->id = $model->id;

        $this->createDocument($model->id, 'arendatorsList', $this->arendatorsList, $transaction, $model->created_by);
        $this->createDocument($model->id, 'noDebtsReference', $this->noDebtsReference, $transaction, $model->created_by);
        $this->createDocument($model->id, 'rentContractCopy', $this->rentContractCopy, $transaction, $model->created_by);
        $this->createDocument($model->id, 'warehouseCadastralPassport', $this->warehouseCadastralPassport, $transaction, $model->created_by);
        $this->createDocument($model->id, 'propertyRightsCopy', $this->propertyRightsCopy, $transaction, $model->created_by);

        if ($this->legal_type == 'UL') {
            $this->createDocument($model->id, 'taxStampCopy', $this->taxStampCopy, $transaction, $model->created_by);
            $this->createDocument($model->id, 'taxRegistrationCopyUL', $this->taxRegistrationCopyUL, $transaction, $model->created_by);
            $this->createDocument($model->id, 'naznachenieIspolnitelyaCopy', $this->naznachenieIspolnitelyaCopy, $transaction, $model->created_by);
            $this->createDocument($model->id, 'doverennostNaPodpisanieCopy', $this->doverennostNaPodpisanieCopy, $transaction, $model->created_by);
            $this->createDocument($model->id, 'headSignatureCopy', $this->headSignatureCopy, $transaction, $model->created_by);
            $this->createDocument($model->id, 'financialReportCopy', $this->financialReportCopy, $transaction, $model->created_by);
        } else {
            $this->createDocument($model->id, 'passportCopyIP', $this->passportCopyIP, $transaction, $model->created_by);
            $this->createDocument($model->id, 'taxRegistrationCopyIP', $this->taxRegistrationCopyIP, $transaction, $model->created_by);
        }

        if ($this->scenario == self::SCENARIO_CREATE) {
            $user->organization_id = $this->id;
            if (!$user->save()) {
                $transaction->rollBack();
                $this->addErrors($user->errors);
                return false;
            }
        }

        $transaction->commit();

        return true;

    }

    /**
     * Create new document
     *
     * @param string|int         $orgId        organization id
     * @param string             $entityField  relationship to field of the form
     * @param UploadedFile       $uploadedFile
     * @param yii\db\Transaction $transaction
     * @param string|int         $userId       user id
     *
     * @return void
     */
    public function createDocument($orgId, $entityField, $uploadedFile, $transaction, $userId)
    {
        if ($uploadedFile) {
            $doc = new Document();

            $doc->file = $uploadedFile;
            $doc->entity_field = $entityField;
            $doc->entity_classname = Organization::class;
            $doc->entity_id = $orgId;
            $doc->created_by = $userId;

            if (!$doc->save()) {
                $transaction->rollBack();
                $this->addErrors($doc->errors);
                return false;
            }
        }
    }

    /**
     * Verify organization
     *
     * @param string|int $orgId organization id
     *
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public static function verify($orgId)
    {
        $organization = Organization::findModel($orgId);

        if ($organization->verification_status_sn == Organization::VERIFICATION_RESTRICTED) {
            throw new Exception('Verification was cancel forever');
        }
        $organization->verification_status_sn = Organization::VERIFICATION_VERIFIED;
        $organization->verification_comment = '';
        return $organization->save();
    }

    /**
     * Cancel verify organization
     *
     * @param string|int $orgId   organization id
     * @param string     $comment for cancel
     * @param bool       $forever or not verify cancel
     *
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public static function verifyCancel($orgId, $comment, $forever)
    {
        $organization = Organization::findModel($orgId);

        if ($forever == 'true') {
            $organization->verification_status_sn = Organization::VERIFICATION_RESTRICTED;
        } else {
            if ($organization->verification_status_sn == Organization::VERIFICATION_RESTRICTED) {
                throw new Exception('Verification was cancel forever');
            }
            $organization->verification_status_sn = Organization::VERIFICATION_FAILED;
        }
        $organization->verification_comment = $comment;

        return $organization->save();
    }

    /**
     * Relations with 'user'
     *
     * @property-read User $user
     * @return        User $user
     */
    public function getUser()
    {
        return User::findOne(['id' => $this->created_by, 'deleted_at' => null]);
    }
}

<?php

namespace app\modules\admin\models\forms\organization;

use app\models\Organization;
use yii\base\Model;
use app\models\User;
use yii\helpers\ArrayHelper;

/**
 * Class OrganizationForm
 *
 * @property int|string $id
 */
class OrganizationForm extends Model
{
    public $id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return array_merge($scenarios, [
            self::SCENARIO_CREATE => $scenarios[self::SCENARIO_DEFAULT],
            self::SCENARIO_UPDATE => array_merge(['id'], $scenarios[self::SCENARIO_DEFAULT]),
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return (new Organization)->attributeLabels();
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
        ]);
    }

    /**
     * Function for get list of users id=>email
     *
     * @return string[]
     */
    public function users()
    {
        $users = User::findAll(['deleted_at' => null]);
        $usersArray = ArrayHelper::map($users, function ($element) {
            if (!$element['organization_id'] && ($element['role'] == User::ROLE_HOLDER)) {
                return $element['id'];
            }
        }, function ($element) {
            if (!$element['organization_id'] && ($element['role'] == User::ROLE_HOLDER)) {
                return $element['email'];
            }
        });
        return array_filter($usersArray);
    }

}

<?php

namespace app\modules\admin\models\forms\user;

use app\models\User;
use yii\base\Model;

/**
 * Class UserForm
 *
 * @property int|string $id
 */
class UserForm extends Model
{
    public $id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return array_merge($scenarios, [
            self::SCENARIO_CREATE => $scenarios[self::SCENARIO_DEFAULT],
            self::SCENARIO_UPDATE => array_merge(['id'], $scenarios[self::SCENARIO_DEFAULT]),
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return (new User)->attributeLabels();
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
        ]);
    }
}

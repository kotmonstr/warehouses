<?php
/**
 * This is the file with form for creating 'user'
 * php version 7
 */
namespace app\modules\admin\models\forms\user;

use app\models\Organization;

use Yii;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\Warehouse;
use app\models\Advert;
use yii\web\NotFoundHttpException;

/**
 * Create User Form
 *
 * @property      int $id
 * @property      string $name
 * @property      int $organization_id
 * @property      string $organization_position
 * @property      string $email
 * @property      string $phone_number
 * @property      string $password
 * @property      string $verification_status_sn
 * @property      string $verification_comment
 * @property      int $verified_at
 * @property      string $role
 * @property      int $created_at
 * @property-read Organization $organization
 * @property-read Warehouse $warehouse
 * @property-read Advert $advert
 */
class Form extends UserForm
{
    public $name;
    public $organization_id;
    public $organization_position;
    public $email;
    public $phone_number;
    public $password;
    public $verification_status_sn;
    public $verification_comment;
    public $verified_at;
    public $role;
    public $created_at;

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['organization_id'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'email'], 'required'],
            [
                [
                    'name', 'email', 'organization_position',
                    'verification_status_sn', 'password'
                ], 'string', 'max' => 255
            ],
            [['email'], 'email'],
            [['organization_id', 'verified_at', 'created_at'], 'integer'],
            [['verification_comment'], 'string'],
            [['role'], 'string', 'max' => 50],
            [['phone_number'], 'filter', 'filter' => function ($value) {
                if (!empty($value)) {
                    return str_replace("_", "", $value);
                }
            }],
            [['phone_number'], 'string', 'length' => 18, 'message' => 'Введите номер телефона полностью'],
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'password' => 'Пароль',
        ]);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $model = User::findModel($this->id);
        } else {
            $model = new User();
            $model->organization_id = $this->organization_id;
            $model->role = User::ROLE_HOLDER;
        }

        $model->name = $this->name;
        $model->organization_position = $this->organization_position;
        $model->email = $this->email;
        $model->phone_number = $this->phone_number;
        if ($this->password) {
            $model->setPassword($this->password);
        }

        if (!$model->save()) {
            $transaction->rollBack();
            $this->addErrors($model->errors);
            return false;
        }

        $this->id = $model->id;

        $transaction->commit();

        return true;

    }

    /**
     * Relations with 'organization'
     *
     * @property-read Organization $organization
     * @return        Organization $organization
     */
    public function getOrganization()
    {
        return Organization::findOne(['id' => $this->organization_id, 'deleted_at' =>null]);
    }

    /**
     * Relations with 'warehouse'
     *
     * @property-read Warehouse $warehouse
     * @return        Warehouse $warehouse
     */
    public function getWarehouse()
    {
        return Warehouse::findAll(['created_by' => $this->id, 'deleted_at' => null]);
    }
    /**
     * Function for get list of organizations
     *
     * @return string[]
     */
    public function organizations()
    {
        $organizations = Organization::findAll(['deleted_at' => null]);
        return ArrayHelper::map($organizations, 'id', 'title');
    }

    /**
     * Relations with 'advert'
     *
     * @property-read Advert $advert
     * @return        Advert $advert
     */
    public function getAdvert()
    {
        return Advert::findAll(['created_by' => $this->id, 'deleted_at' => null]);
    }
}

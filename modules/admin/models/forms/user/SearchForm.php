<?php

namespace app\modules\admin\models\forms\user;

use yii\data\ActiveDataProvider;
use app\models\User;
use Yii;

/**
 * This is the model class for search in table 'user'
 *
 * @property string $name
 */
class SearchForm extends User
{
    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = User::find()->where(['deleted_at' => null]);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 7,
                ],
            ]
        );

        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ILIKE', 'name', $this->name]);

        return $dataProvider;
    }
}

<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\modules\admin\models\forms\advert\SearchForm;
use app\modules\admin\models\forms\advert\Form;
use app\models\Advert;

/**
 * Class AdvertController
 * @package app\modules\admin\controllers
 */
class AdvertController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'advert' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $dataProvider = $searchForm->search(Yii::$app->request->get());
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchForm
            ]
        );
    }

    /**
     * Advert action for create or update advert
     *
     * @param null|string|int $id advert
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionAdvert($id = null)
    {
        $form = new Form();
        $form->scenario = Form::SCENARIO_CREATE;

        if ($id) {
            $model = Advert::findModel($id);
            $form = new Form();
            $form->scenario = Form::SCENARIO_UPDATE;
            $form->load($model->attributes, '');
        }

        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Изменения успешно сохранены');
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $form,
        ]);
    }

    /**
     * Delete action
     *
     * @param string|int $id advert
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        if (!Advert::findModel($id)->delete()) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Объявление не удалено');
        } else {
            Yii::$app->getSession()->addFlash('bg-success', 'Объявление удалено');
        }
        return $this->redirect(['index']);
    }
}

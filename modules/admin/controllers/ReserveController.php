<?php

namespace app\modules\admin\controllers;

use app\models\Document;
use app\models\Reserve;
use app\modules\admin\models\forms\reserve\ReserveDocumentForm;
use app\modules\admin\models\forms\reserve\ReserveSearchForm;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Organization;
use yii\base\Exception;
use yii\web\Response;

/**
 * Class ReserveController
 * @package app\modules\admin\controllers
 */
class ReserveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['GET', 'POST'],
                    'update' => ['POST', 'GET'],
                    'delete' => ['GET', 'POST'],
                    'view' => ['GET', 'POST'],
                    'index' => ['GET'],
                    'delete-file' => ['GET', 'POST'],
                    'cancel' => ['GET', 'POST'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Reserve models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReserveSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reserve model.
     *
     * @param integer $id
     *
     * @return Response|string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $reserve = Reserve::findOne($id);
        $documentsModel = new ReserveDocumentForm();

        $documents = ArrayHelper::index(Document::findAllByEntity($id, Reserve::class), 'entity_field');
        $documentsModel->documents = $documents;

        if ($documentsModel->load(Yii::$app->request->post())) {

            foreach (ReserveDocumentForm::DOC_TYPES as $type) {
                $documentsModel->$type = UploadedFile::getInstance($documentsModel, $type);
            }

            if ($documentsModel->upload($reserve->id)) {
                Yii::$app->session->addFlash('bg-success', "Документы успешно загружены");
                return $this->redirect('index');
            }
        }

        return $this->render('view', [
            'documentModel' => $documentsModel,
            'documents' => $documents,
            'model' => $reserve,
        ]);
    }

    /**
     * Creates a new Reserve model.
     *
     * @return Response|string
     * @throws
     */
    public function actionCreate()
    {
        $reserve = new Reserve();
        $documentsModel = new ReserveDocumentForm();

        $organizations = Organization::find()
            ->where(['deleted_at' => null])
            ->all();

        if ($reserve->load(Yii::$app->request->post()) && $reserve->save()) {
            Yii::$app->session->addFlash('bg-success', 'Бронь успешно добавлена');

            foreach (ReserveDocumentForm::DOC_TYPES as $type) {
                $documentsModel->$type = UploadedFile::getInstance($documentsModel, $type);
            }

            if (!$documentsModel->upload($reserve->id)) {
                return $this->render('create', [
                    'model' => $reserve,
                    'organizations' => ArrayHelper::map($organizations, 'id', 'title'),
                    'documentModel' => $documentsModel,
                ]);
            }

            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $reserve,
            'organizations' => ArrayHelper::map($organizations, 'id', 'title'),
            'documentModel' => $documentsModel,
        ]);
    }

    /**
     * Updates an existing Reserve model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $reserve = Reserve::findOne($id);
        $documentsModel = new ReserveDocumentForm();

        $documents = ArrayHelper::index(Document::findAllByEntity($id, Reserve::class), 'entity_field');
        $documentsModel->documents = $documents;

        if ($reserve->load(Yii::$app->request->post()) && $reserve->save()) {
            Yii::$app->session->addFlash('bg-success', 'Бронь успешно изменена');

            foreach (ReserveDocumentForm::DOC_TYPES as $type) {
                $documentsModel->$type = UploadedFile::getInstance($documentsModel, $type);
            }

            if (!$documentsModel->upload($reserve->id)) {
                return $this->render('update', [
                    'documentModel' => $documentsModel,
                    'documents' => $documents,
                    'model' => $reserve,
                ]);
            }

            return $this->redirect('index');
        }

        return $this->render('update', [
            'documentModel' => $documentsModel,
            'documents' => $documents,
            'model' => $reserve,
        ]);
    }

    /**
     * Deletes an existing Reserve model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Reserve::findModel($id)->delete()) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Бронь не удалена');
        } else {
            Yii::$app->getSession()->addFlash('bg-success', 'Бронь удалена');
        }
        return $this->redirect(['index']);
    }

    /**
     * DownloadFile action
     *
     * @param string|int $id document
     *
     * @return Response|string
     */
    public function actionDownloadFile($id)
    {
        $document = Document::findOne($id);
        $file = $document->file_db;

        if (!$file->exists()) {
            throw new NotFoundHttpException('File not found');
        }

        Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $file->path, $file->name);
    }

    /**
     * Delete file action.
     *
     * @param string $id document
     *
     * @return Response|string
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionDeleteFile($id)
    {
        $document = Document::findOne(['id' => $id]);

        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }

        $path = $document->file_db->path;

        if ($document->delete()) {
            unlink(Yii::getAlias('@webroot') . $path);
            Yii::$app->session->addFlash('bg-success', "Документ успешно удален");
            return $this->redirect(Yii::$app->request->referrer ?? Yii::$app->homeUrl);
        }
    }

    /**
     * Cancel reserve action.
     *
     * @param string $id reserve
     *
     * @return Response|string
     * @throws NotFoundHttpException
     */
    public function actionCancel($id)
    {
        $reserve = Reserve::findModel($id);

        if ($reserve->cancel()) {
            Yii::$app->session->addFlash('bg-success', 'Бронь успешно отменена');
        } else {
            Yii::$app->session->addFlash('bg-danger', 'При отмене брони произошла ошибка');
        }

        return $this->redirect(['/admin/reserve/view?id=' . $id]);
    }
}

<?php

namespace app\modules\admin\controllers;

use PHPExcel_Cell_DataType;
use PHPExcel_Style_Alignment;
use yii\db\Exception;
use yii\helpers\Json;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\User;
use app\modules\admin\models\forms\organization\SearchForm;
use app\modules\admin\models\forms\organization\Form;
use app\models\Organization;
use yii\web\UploadedFile;
use app\models\Document;
use app\models\File;
use yii\web\NotFoundHttpException;
use app\models\Notification;

/**
 * Class OrganizationController
 * @package app\modules\admin\controllers
 */
class OrganizationController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'organization' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'download-file' => ['GET', 'POST'],
                    'delete-file' => ['GET', 'POST'],
                    'verify' => ['GET', 'POST'],
                    'verify-cancel' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $dataProvider = $searchForm->search(Yii::$app->request->get());
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchForm
            ]
        );
    }

    /**
     * Organization action for create or update organization
     *
     * @param null|string|int $id organization
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionOrganization($id = null)
    {
        $form = new Form();
        $form->scenario = Form::SCENARIO_CREATE;
        $documents = [];

        if ($id) {
            $model = Organization::findModel($id);
            $form = new Form();
            $form->scenario = Form::SCENARIO_UPDATE;
            $form->load($model->attributes, '');
            $data = Document::findAllByEntity($id, Organization::class);
            foreach ($data as $doc) {
                $documents[$doc['entity_field']] = $doc;
            }
        }

        if ($form->load(Yii::$app->request->post())) {
            $form->arendatorsList = UploadedFile::getInstance($form, 'arendatorsList');
            $form->noDebtsReference = UploadedFile::getInstance($form, 'noDebtsReference');
            $form->rentContractCopy = UploadedFile::getInstance($form, 'rentContractCopy');
            $form->warehouseCadastralPassport = UploadedFile::getInstance($form, 'warehouseCadastralPassport');
            $form->propertyRightsCopy = UploadedFile::getInstance($form, 'propertyRightsCopy');

            if ($form->legal_type == 'UL') {
                $form->taxStampCopy = UploadedFile::getInstance($form, 'taxStampCopy');
                $form->taxRegistrationCopyUL = UploadedFile::getInstance($form, 'taxRegistrationCopyUL');
                $form->naznachenieIspolnitelyaCopy = UploadedFile::getInstance($form, 'naznachenieIspolnitelyaCopy');
                $form->doverennostNaPodpisanieCopy = UploadedFile::getInstance($form, 'doverennostNaPodpisanieCopy');
                $form->headSignatureCopy = UploadedFile::getInstance($form, 'headSignatureCopy');
                $form->financialReportCopy = UploadedFile::getInstance($form, 'financialReportCopy');
            } else {
                $form->passportCopyIP = UploadedFile::getInstance($form, 'passportCopyIP');
                $form->taxRegistrationCopyIP = UploadedFile::getInstance($form, 'taxRegistrationCopyIP');
            }
            if ($form->save()) {
                Yii::$app->getSession()->addFlash('bg-info', 'Организация успешно создана');
                return $this->redirect(['/admin/organization/index']);
            }
        }

        return $this->render('form', [
            'model'      => $form,
            'documents' => $documents,
        ]);
    }

    /**
     * Delete action for organization
     *
     * @param string|int $id organization
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        if (!Organization::findModel($id)->delete()) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Организация не удалена');
        } else {
            Yii::$app->getSession()->addFlash('bg-success', 'Организация удалена');
        }
        return $this->redirect(['index']);
    }

    /**
     * Delete file action.
     *
     * @param string|int $id of document
     *
     * @return Response|string
     */
    public function actionDeleteFile($id)
    {
        $document = Document::findOne(['id' => $id]);
        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }
        $file = File::findOne(['document_id' => $id]);
        if (!$file) {
            throw new NotFoundHttpException('File not founded');
        }
        $path = $file->path;

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($file->delete() && $document->delete()) {
            unlink(Yii::getAlias('@webroot') . $path);
            Yii::$app->getSession()->addFlash('bg-info', 'Файл удален');
            $resp = (object) ['status' => 200, 'text' => 'success'];
        } else {
            $resp = (object) ['status' => 400, 'text' => 'error'];
        }

        return Json::encode($resp);
    }

    /**
     * Download file action.
     *
     * @param string|int $id of document
     *
     * @return Response|string
     */
    public function actionDownloadFile($id)
    {
        $file = File::findOne(['document_id' => $id]);
        if (!$file) {
            throw new NotFoundHttpException('File not founded');
        }

        if (file_exists(Yii::getAlias('@webroot') . $file->path)) {
            Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $file->path);
        }
    }

    /**
     * Verify action.
     *
     * @param string|int $id   organization
     *
     * @return Response|string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionVerify($id)
    {
        Form::verify($id);
        $notification = new Notification();
        $notification->user_id = Organization::findModel($id)->created_by;
        $notification->title = 'Верификация компании';
        $notification->text = 'Ваша ' .
                                '<a href="/panel/organization/index">' .
                                'компания</a> была успешно верифицирована';
        if (!$notification->save()) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Notification not send');
        }
        return $this->redirect(['index']);
    }

    /**
     * Cancel verify action.
     *
     * @param string|int $id      organization
     * @param string     $comment for cancel
     * @param bool       $forever or not verify cancel
     *
     * @return Response|string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionVerifyCancel($id, $comment, $forever)
    {
        if (Form::verifyCancel($id, $comment, $forever)) {
            Yii::$app->getSession()->addFlash('bg-info', 'Верификация отменена');
        }
        return $this->redirect(['index']);
    }

    /**
     * Cancel verify action.
     *
     * @param string|int $id      organization
     * @param string     $comment for cancel
     * @param bool       $forever or not verify cancel
     *
     * @return Response|string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDownloadExcelForm($id)
    {
        $data = [];

        $organization = Organization::findOne($id);
        $lables = $organization->attributeLabels();

        foreach ($organization->attributes as $attribute => $value) {
            if (in_array($attribute, ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'verification_status_sn', 'verification_comment', 'verified_at'])) {
                continue;
            }

            $data[] = [
                $lables[$attribute], (string)$value
            ];
        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Users' => [
                    'data' => $data,
                    'titles' => [
                        'Параметр',
                        'Значение',
                    ],
                    'types' => [
                        PHPExcel_Cell_DataType::TYPE_STRING,
                        PHPExcel_Cell_DataType::TYPE_STRING,
                    ],
                    'formats' => [
                        'C' => '#,##0.00',
                    ],
                    'styles' => [
                        'A1:Z1000' => [
                            'alignment' => [
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            ],
                        ],
                        'A1:Z1' => [
                            'font' => [
                                'bold' => true,
                            ],
                        ],
                    ],
                ]
            ]
        ]);

        $file->send('Анкета ' . $organization->title . '.xlsx');
    }
}

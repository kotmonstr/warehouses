<?php

namespace app\modules\admin\controllers;

use yii\db\Exception;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\User;
use app\modules\admin\models\forms\user\SearchForm;
use app\modules\admin\models\forms\user\Form;

/**
 * Class UserController
 * @package app\modules\admin\controllers
 */
class UserController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'user' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'info' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $dataProvider = $searchForm->search(Yii::$app->request->get());
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchForm
            ]
        );
    }

    /**
     * User action for create or update user
     *
     * @param null|string|int $id user
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUser($id = null)
    {
        $form = new Form();
        $form->scenario = Form::SCENARIO_CREATE;

        if ($id) {
            $model = User::findModel($id);

            if ($model->role == User::ROLE_ADMIN) {
                throw new NotFoundHttpException();
            }

            if ($model->role == User::ROLE_DSL || $model->role == User::ROLE_SB) {
                if (Yii::$app->user->identity->role != User::ROLE_ADMIN) {
                    throw new NotFoundHttpException();
                }
            }

            $form = new Form();
            $form->scenario = Form::SCENARIO_UPDATE;
            $form->load($model->attributes, '');
        }

        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Изменения успешно сохранены');
            return $this->redirect(['index']);
        }

        return $this->render('form', [
            'model' => $form,
        ]);
    }

    /**
     * Delete action
     *
     * @param string|int $id user
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $user = User::findModel($id);

        if ($user->role == User::ROLE_ADMIN) {
            throw new NotFoundHttpException();
        }

        if ($user->role == User::ROLE_DSL || $user->role == User::ROLE_SB) {
            if (Yii::$app->user->identity->role != User::ROLE_ADMIN) {
                throw new NotFoundHttpException();
            }
        }

        if ($user->delete()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Пользователь удален');
        }

        return $this->redirect(['index']);
    }

    /**
     * Info action
     *
     * @param string|int $id user
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionInfo($id)
    {
        $model = User::findModel($id);
        $form = new Form();
        $form->scenario = Form::SCENARIO_UPDATE;
        $form->load($model->attributes, '');

        return $this->render('info', ['model' => $form]);
    }
}

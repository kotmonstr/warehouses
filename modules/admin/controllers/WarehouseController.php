<?php

namespace app\modules\admin\controllers;

use app\models\Warehouse;
use app\modules\admin\models\forms\warehouse\GeneralForm;
use app\modules\admin\models\forms\warehouse\InternalForm;
use app\modules\admin\models\forms\warehouse\ExternalForm;
use app\modules\admin\models\forms\warehouse\DocumentsForm;
use app\modules\admin\models\forms\warehouse\WarehouseForm;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\modules\admin\models\forms\warehouse\SearchForm;
use yii\web\UploadedFile;
use app\models\File;
use app\models\Document;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\models\Notification;
use app\models\WhLoadUnloadCranageLuType;
use app\models\WhLoadUnloadEquipmentLuType;

/**
 * Class WarehouseController
 * @package app\modules\admin\controllers
 */
class WarehouseController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'general' => ['GET', 'POST'],
                    'internal' => ['GET', 'POST'],
                    'external' => ['GET', 'POST'],
                    'documents' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'delete-file' => ['GET', 'POST'],
                    'download-file' => ['GET', 'POST'],
                    'verify' => ['GET', 'POST'],
                    'verify-cancel' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchModel = new SearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]
        );
    }

    /**
     * @param null $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionGeneral($id = null)
    {
        $form = new GeneralForm();
        $form->scenario = GeneralForm::SCENARIO_CREATE;
        $documents = [];

        if ($id) {
            $model = Warehouse::findModel($id);
            $form = new GeneralForm();
            $form->scenario = GeneralForm::SCENARIO_UPDATE;
            $form->load($model->attributes, '');
            $documents = $form->findFilePaths();
        }

        if ($form->load(Yii::$app->request->post())
            && !in_array($form->dsl_verification_status_sn, [Warehouse::VERIFICATION_VERIFIED, Warehouse::VERIFICATION_RESTRICTED])
            && !in_array($form->sb_verification_status_sn, [Warehouse::VERIFICATION_VERIFIED, Warehouse::VERIFICATION_RESTRICTED])
        ) {
            $form->photo_1 = UploadedFile::getInstance($form, 'photo_1');
            $form->photo_2 = UploadedFile::getInstance($form, 'photo_2');
            $form->photo_3 = UploadedFile::getInstance($form, 'photo_3');
            $form->photo_4 = UploadedFile::getInstance($form, 'photo_4');
            $form->photo_5 = UploadedFile::getInstance($form, 'photo_5');
            if ($form->save()) {
                return $this->redirect([ExternalForm::ALIAS, 'id' => $form->id]);
            }
        }

        return $this->render('form', [
            'current'    => GeneralForm::ALIAS,
            'title'      => isset($model) ? $model->title : null,
            'model'      => $form,
            'documents' => $documents,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionExternal($id)
    {
        $model = Warehouse::findModel($id);

        $form = new ExternalForm();
        $form->scenario = ExternalForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');

        /* Предзагрузка связанных сущностей */
        $form->accounting_control_system_sn = ArrayHelper::getColumn($model->wh_accounting_control_system, 'accounting_control_system_sn');


        if ($form->load(Yii::$app->request->post())
            && in_array($form->dsl_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
            && in_array($form->sb_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
        ) {
            if ($form->save()) {
                return $this->redirect([InternalForm::ALIAS, 'id' => $form->id]);
            }
        }

        return $this->render('form', [
            'current'    => ExternalForm::ALIAS,
            'title'      => $model->title,
            'model'      => $form,
            'documents' => [],
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionInternal($id)
    {
        $model = Warehouse::findModel($id);

        $form = new InternalForm();
        $form->scenario = InternalForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');

        if ($form->load(Yii::$app->request->post())
            && in_array($form->dsl_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
            && in_array($form->sb_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
        ) {
            if ($form->save()) {
                return $this->redirect([DocumentsForm::ALIAS, 'id' => $form->id]);
            }
        }

        /* Предзагрузка связанных сущностей */
        $form->storage_system_sns = ArrayHelper::getColumn($model->wh_storage_system, 'storage_system_sn');
        $form->loading_unloading_struct_types_sn = ArrayHelper::getColumn($model->wh_loading_unloading_struct_type, 'loading_unloading_struct_type_sn');
        $form->office_facility_types_inside_sn = ArrayHelper::getColumn(
            $model->wh_office_facility_type,
            function ($element) {
                if ($element['types'] == 'inside') {
                    return $element['office_facility_types_sn'];
                }
            }
        );
        $form->office_facility_types_outside_sn = ArrayHelper::getColumn(
            $model->wh_office_facility_type,
            function ($element) {
                if ($element['types'] == 'outside') {
                    return $element['office_facility_types_sn'];
                }
            }
        );

        $form->loading_unloading_tech_types = $model->wh_load_unload_equipment;
        foreach ($form->loading_unloading_tech_types as $tech) {
            $tech->loading_unloading_method_sn = ArrayHelper::getColumn(
                WhLoadUnloadEquipmentLuType::findAll(['equipment_id' => $tech->id]),
                'loading_unloading_method_sn'
            );
        }

        $form->cranage_types = $model->wh_load_unload_cranage;
        foreach ($form->cranage_types as $cranage) {
            $cranage->loading_unloading_method_sn = ArrayHelper::getColumn(
                WhLoadUnloadCranageLuType::findAll(['equipment_id' => $cranage->id]),
                'loading_unloading_method_sn'
            );
        }

        return $this->render('form', [
            'current'    => InternalForm::ALIAS,
            'title'      => $model->title,
            'model'      => $form,
            'documents' => [],
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocuments($id)
    {
        $model = Warehouse::findModel($id);

        $form = new DocumentsForm();
        $form->scenario = DocumentsForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');
        $documents = $form->findFilePaths();

        if ($form->load(Yii::$app->request->post())
            && in_array($form->dsl_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
            && in_array($form->sb_verification_status_sn, [Warehouse::VERIFICATION_IN_PROGRESS, Warehouse::VERIFICATION_FAILED])
        ) {
            $form->documents_file_1 = UploadedFile::getInstance($form, 'documents_file_1');
            $form->documents_file_2 = UploadedFile::getInstance($form, 'documents_file_2');
            $form->documents_file_3 = UploadedFile::getInstance($form, 'documents_file_3');
            if ($form->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('form', [
            'current'    => DocumentsForm::ALIAS,
            'title'      => $model->title,
            'model'      => $form,
            'documents' => $documents,
        ]);
    }

    /**
     * Delete action.
     *
     * @param $id
     *
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        if (!Warehouse::findModel($id)->delete()) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Склад не удален');
        } else {
            Yii::$app->getSession()->addFlash('bg-success', 'Склад удален');
        }
        return $this->redirect(['index']);
    }

    /**
     * Delete file action.
     *
     * @param string $path to file
     *
     * @return Response|string
     */
    public function actionDeleteFile($path)
    {
        $file = File::findOne(['path' => $path]);
        if (!$file) {
            throw new NotFoundHttpException('File not founded');
        }
        $document = Document::findOne(['id' => $file->document_id]);
        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($file->delete() && $document->delete()) {
            unlink(Yii::getAlias('@webroot') . $path);
            Yii::$app->getSession()->addFlash('bg-info', 'Файл успешно удален');
            $resp = (object)['status' => 200, 'text' => 'success'];
        } else {
            $resp = (object)['status' => 400, 'text' => 'error'];
        }
        return Json::encode($resp);
    }

    /**
     * Download file action.
     *
     * @param string $path to file
     *
     * @return Response|string
     */
    public function actionDownloadFile($path)
    {
        if (file_exists(Yii::getAlias('@webroot') . $path)) {
            Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $path);
        }
    }

    /**
     * Verify action.
     *
     * @param string|int $id warehouse
     * @param string     $type of verify sb_verification_status_sn /dsl_verification_status_sn
     *
     * @return Response|string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionVerify($id, $type)
    {
        $warehouse = Warehouse::findModel($id);

        if ($type == 'sb_verification_status_sn' && $warehouse->dsl_verification_status_sn != Warehouse::VERIFICATION_VERIFIED) {
            throw new Exception('Verification DSL not verified');
        }

        if ($warehouse->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            || $warehouse->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
        ) {
            throw new Exception('Verification was cancel forever');
        }

        $warehouse[$type] = Warehouse::VERIFICATION_VERIFIED;
        $warehouse->verification_comment = '';

        if (!$warehouse->save()) {
            Yii::$app->session->addFlash('bg-danger', "Ошибка. Действие не применено");

        } else {
            Yii::$app->getSession()->addFlash('bg-info',  ($type == 'sb_verification_status_sn' ? 'Склад верифицирован' : 'Склад аттестован'));

            if ($type == 'sb_verification_status_sn') {

                $notification = new Notification();
                $notification->user_id = $warehouse->created_by;
                $notification->title = 'Верификация склада';
                $notification->text = "Ваш склад " .
                                        '<a href="/panel/warehouse/general?id=' . $warehouse->id . '">' .
                                        "{$warehouse->title}</a> был успешно верифицирован";
                if (!$notification->save()) {
                    Yii::$app->getSession()->addFlash('bg-danger', 'Notification not send');
                }

            } else {

                if (!Notification::notifyRoleGroup(
                    User::ROLE_SB,
                    'Склад ожидает проверку',
                    "Склад " . '<a href="/admin/warehouse/general?id=' . $warehouse->id . '">' . "{$warehouse->title}</a> ожидает проверку"
                )
                ) {
                    Yii::$app->getSession()->addFlash('bg-danger', 'Notification SB not send');
                }
            }
        }


        return $this->redirect(['warehouse/general', 'id' => $id]);
    }

    /**
     * Cancel verify action.
     *
     * @param string|int $id warehouse
     * @param string     $comment for cancel
     * @param bool       $forever or not verify cancel
     * @param string     $type of verify sb_verification_status_sn /dsl_verification_status_sn
     *
     * @return Response|string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionVerifyCancel($id, $comment, $forever, $type)
    {
        $warehouse = Warehouse::findModel($id);

        if ($type == 'sb_verification_status_sn' && $warehouse->dsl_verification_status_sn != Warehouse::VERIFICATION_VERIFIED) {
            throw new Exception('Verification DSL not verified');
        }

        if ($warehouse[$type] != Warehouse::VERIFICATION_VERIFIED) {

            if ($forever == 'true') {
                $warehouse[$type] = Warehouse::VERIFICATION_RESTRICTED;

            } else {

                if ($warehouse->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                    || $warehouse->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                ) {
                    throw new Exception('Verification was cancel forever');
                }

                $warehouse[$type] = Warehouse::VERIFICATION_FAILED;
            }

            $warehouse->verification_comment = $comment;

            if (!$warehouse->save()) {
                Yii::$app->session->addFlash('bg-danger', "Ошибка. Действие не применено");

            } else {
                Yii::$app->getSession()->addFlash('bg-info',  ($type == 'sb_verification_status_sn' ? 'Верификация отменена' : 'Аттестация отменена'));

                $notification = new Notification();
                $notification->user_id = $warehouse->created_by;
                $notification->title = 'Верификация склада отменена';
                $notification->text = "Верификация склада " .
                                        '<a href="/panel/warehouse/general?id=' . $warehouse->id . '">' .
                                        "{$warehouse->title}</a> была отменена";
                if (!$notification->save()) {
                    Yii::$app->getSession()->addFlash('bg-danger', 'Notification not send');
                }
            }

        } else {
            throw new Exception('You try cancel verification of verified warehouse');
        }

        return $this->redirect(['warehouse/general', 'id' => $id]);
    }
}

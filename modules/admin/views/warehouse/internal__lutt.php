<?php

use yii\bootstrap\Html;
use app\models\Dictionary;
use yii\web\View;
use app\models\Warehouse;
use dosamigos\multiselect\MultiSelect;

/* @var $this View */
/* @var $model app\modules\admin\models\forms\warehouse\InternalForm*/
/* @var $lutt app\models\WhLoadUnloadEquipment */

$internalIsSet = Warehouse::findOne(['id' => $model->id])->internal_is_set;

?>

<?php if (count($model->loading_unloading_tech_types)) { ?>

    <?php foreach ($model->loading_unloading_tech_types as $idx => $lutt) { ?>

        <div class="loading_unloading_tech_type lu-block">
            <?= Html::hiddenInput(Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Вид', ['class' => 'control-label']) ?>
                        <?= Html::dropDownList(
                            Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][loading_unloading_tech_type_sn]'),
                            $lutt->loading_unloading_tech_type_sn,
                            Dictionary::findDictItems('loading_unloading_tech_types'),
                            ['class' => 'form-control', 'required' => true]) ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                        <?php
                        echo MultiSelect::widget([
                            "id" => $idx . "-multiselect-equipment",
                            "options" => ['multiple'=>"multiple", "class" => "form-control"],
                            "clientOptions" => ['nonSelectedText' => 'Ничего не выбрано', 'allSelectedText' => 'Все варианты'],
                            "data" => Dictionary::findDictItems('loading_unloading_methods'),
                            "value" =>  $lutt->loading_unloading_method_sn,
                            "name" => Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][loading_unloading_method_sn]'),
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">

                                <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                                <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][capacity]'), $lutt->capacity, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>

                        </div>
                        <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">

                                <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                                <?= Html::dropDownList(Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][capacity_type_sn]'), $lutt->capacity_type_sn, Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= Html::label('Количество', ['class' => 'control-label']) ?>
                                <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[' . $idx . '][amount]'), $lutt->amount, ['class' => 'form-control', 'required' => true]) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div style="margin-top: 1.5em; text-align: right;">
                                <div class="btn btn-danger equipment-delete delete-block">
                                    Удалить
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>

    <?php } ?>

<?php } elseif (!$internalIsSet) { ?>

    <div class="loading_unloading_tech_type lu-block">

        <?= Html::hiddenInput(Html::getInputName($model, 'loading_unloading_tech_types[0][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Вид', ['class' => 'control-label']) ?>
                    <?= Html::dropDownList(
                        Html::getInputName($model, 'loading_unloading_tech_types[0][loading_unloading_tech_type_sn]'),
                        [],
                        Dictionary::findDictItems('loading_unloading_tech_types'),
                        ['class' => 'form-control', 'required' => true]) ?>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                    <?php
                    echo MultiSelect::widget([
                        "id" => "0-multiselect-equipment",
                        "options" => ['multiple'=>"multiple", "class" => "form-control"],
                        "clientOptions" => ['nonSelectedText' => 'Ничего не выбрано', 'allSelectedText' => 'Все варианты'],
                        "data" => Dictionary::findDictItems('loading_unloading_methods'),
                        "value" =>  [],
                        "name" => Html::getInputName($model, 'loading_unloading_tech_types[0][loading_unloading_method_sn]'),
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">

                            <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                            <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[0][capacity]'), null, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>

                    </div>
                    <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">

                            <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                            <?= Html::dropDownList(Html::getInputName($model, 'loading_unloading_tech_types[0][capacity_type_sn]'), [], Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= Html::label('Количество', ['class' => 'control-label']) ?>
                            <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[0][amount]'), null, ['class' => 'form-control', 'required' => true]) ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div style="margin-top: 1.5em; text-align: right;">
                            <div class="btn btn-danger equipment-delete delete-block">
                                Удалить
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php

use dosamigos\multiselect\MultiSelectAsset;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use app\models\Dictionary;
use app\models\Warehouse;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\forms\warehouse\InternalForm */

MultiSelectAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        $('.clone-block').click(function() {
            let ethalonClass = '.ethalon__holder .' + $(this).data('type');
            let targetClass = '.' + $(this).data('type');

            let clone = $(ethalonClass).first().clone();
            let holder = $(targetClass + '__holder');

            holder.append(clone);

            let count = $(holder).find(targetClass).length;
            
            clone.find('.form-control').each(function(key, element){
                element.name = element.name.replace(/\[\d+\]/, '[' + (count - 1) + ']');
                element.value = null
            });

            clone.find('.hidden-control').each(function(key, element){
                element.name = element.name.replace(/\[\d+\]/, '[' + (count - 1) + ']');
            });
            
            clone.find('SELECT.multiselect').multiselect({nonSelectedText:'Ничего не выбрано', allSelectedText: 'Все варианты'});
            
            clone.find('.delete-block').click(deleteBlock);
        });
        
        $('.delete-block').click(deleteBlock);
        
        function deleteBlock() {
            $(this).closest('.lu-block').remove();
        }
    });
JS;

$this->registerJs($js);

?>

<div class="hidden ethalon__holder">
    <?= $this->render('internal__eth', [
        'model' => $model,
    ]) ?>
</div>

<?php $internalForm = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <p style="font-weight: bold;">Погрузочно-разгрузочная техника</p>
    <div class="loading_unloading_tech_type__holder">
        <?= $this->render('internal__lutt', [
            'model' => $model,
        ]) ?>
    </div>
    <?= Html::button('Добавить', ['class' => 'btn btn-default clone-block', 'data' => ['type' => 'loading_unloading_tech_type']]) ?>
    <hr>

    <p style="font-weight: bold;">Крановое оборудование</p>
    <div class="cranage_types__holder">
        <?= $this->render('internal__luct', [
            'model' => $model,
        ]) ?>
    </div>
    <?= Html::button('Добавить', ['class' => 'btn btn-default clone-block', 'data' => ['type' => 'cranage_types']]) ?>
    <hr>

    <?= $internalForm->field($model, 'storage_system_sns')->checkboxList(Dictionary::findDictItems('storage_system')); ?>
    <hr>

    <p style="font-weight: bold;">Внутренние конструкции</p>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'column_distance')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'column_distance_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'span_distance')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'span_distance_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $internalForm->field($model, 'column_protection')->checkbox(); ?>
    <hr>

    <p style="font-weight: bold;">Пол</p>
    <?php echo $internalForm->field($model, 'floor_covering_sn')->inline()->radioList(Dictionary::findDictItems('floor_covering')); ?>
    <?= $internalForm->field($model, 'anti_dust_covering')->checkbox(); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'height_above_ground')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'height_above_ground_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'load_per_square_meter')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'load_per_square_meter_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'mass', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Системы вентиляции и кондиционирования</p>
    <?= $internalForm->field($model, 'ventilation')->checkbox(); ?>
    <?= $internalForm->field($model, 'conditioning')->checkbox(); ?>
    <hr>

    <?php echo $internalForm->field($model, 'temperature_mode_sn')->inline()->radioList(Dictionary::findDictItems('temperature_mode')); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'min_temperature')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'min_temperature_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'temperature', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $internalForm->field($model, 'max_temperature')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $internalForm->field($model, 'max_temperature_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'temperature', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Система безопасности</p>
    <?= $internalForm->field($model, 'security_guard')->checkbox(); ?>
    <?= $internalForm->field($model, 'security_video')->checkbox(); ?>
    <?= $internalForm->field($model, 'security_alarm')->checkbox(); ?>
    <hr>

    <p style="font-weight: bold;">Пожарная безопасность</p>
    <?= $internalForm->field($model, 'fire_alarm')->checkbox(); ?>
    <?= $internalForm->field($model, 'fire_charging')->checkbox(); ?>
    <?= $internalForm->field($model, 'fire_tanks_and_pump')->checkbox(); ?>
    <?php echo $internalForm->field($model, 'fire_ext_system_sn')->inline()->radioList(Dictionary::findDictItems('fire_ext_system_type')); ?>
    <hr>

    <?php echo $internalForm->field($model, 'electricity_grid_sn')->inline()->radioList(Dictionary::findDictItems('electricity_grid_type')); ?>
    <hr>

    <?php echo $internalForm->field($model, 'heating_system_sn')->inline()->radioList(Dictionary::findDictItems('heating_system_type')); ?>
    <hr>

    <p style="font-weight: bold;">Водоснабжение</p>
    <?= $internalForm->field($model, 'water_cold')->checkbox(); ?>
    <?= $internalForm->field($model, 'water_hot')->checkbox(); ?>
    <?= $internalForm->field($model, 'water_sewerage')->checkbox(); ?>
    <hr>

    <?= $internalForm->field($model, 'loading_unloading_struct_types_sn')->checkboxList(Dictionary::findDictItems('loading_unloading_struct_types')); ?>
    <hr>

    <p style="font-weight: bold;">Офисные и подсобные помещения</p>
    <div class="row">
        <div class="col-sm-6">
            <?= $internalForm->field($model, 'office_facility_types_inside_sn')->checkboxList(Dictionary::findDictItems('office_facility_types')); ?>
        </div>
        <div class="col-sm-6">
            <?= $internalForm->field($model, 'office_facility_types_outside_sn')->checkboxList(Dictionary::findDictItems('office_facility_types')); ?>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Телекоммуникации</p>
    <?= $internalForm->field($model, 'internet')->checkbox(); ?>
    <?= $internalForm->field($model, 'phone')->checkbox(); ?>
    <?= $internalForm->field($model, 'fiber_optic')->checkbox(); ?>
    <hr>

    <div class="form-group btns-wrapper">

        <?php if (($model->sb_verification_status_sn == Warehouse::VERIFICATION_VERIFIED
                        && $model->dsl_verification_status_sn == Warehouse::VERIFICATION_VERIFIED)
                    || ($model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                        || $model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED)
        ) {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next', 'disabled' => 'disabled']);
        } else {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next']);
        } ?>

        <?php ActiveForm::end() ?>

        <?= Html::a('На предыдущий шаг', ['/admin/warehouse/external?id=' . $model->id], ['class' => 'btn btn-prev']) ?>

    </div>




<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\widgets\geocoder\Geocoder;
use yii\helpers\Url;
use app\models\Warehouse;

/* @var $this View */
/* @var $model app\modules\admin\models\forms\warehouse\GeneralForm */
/* @var $documents string[]  */

$scriptToggleFields = <<< JS
    var chkbx = document.getElementById('generalform-work_hours_24_7');

    if (chkbx.checked) {
        disableWdWe();
    }

    chkbx.addEventListener( 'change', function() {
        if(this.checked) {
            disableWdWe();
        } else {
            enableWdWe();
        }
    });

    function disableWdWe() {
        document.getElementById('wh_wd_from').value = null;
        document.getElementById('wh_wd_from').readOnly = true;
        document.getElementById('wh_wd_to').value = null;
        document.getElementById('wh_wd_to').readOnly = true;
        document.getElementById('wh_we_from').value = null;
        document.getElementById('wh_we_from').readOnly = true;
        document.getElementById('wh_we_to').value = null;
        document.getElementById('wh_we_to').readOnly = true;
    }

    function enableWdWe() {
        document.getElementById('wh_wd_from').readOnly = false;
        document.getElementById('wh_wd_to').readOnly = false;
        document.getElementById('wh_we_from').readOnly = false;
        document.getElementById('wh_we_to').readOnly = false;
    }

    function deleteFileAdminGeneral(path) {
        $.ajax({
            url: '/admin/warehouse/delete-file',
            method: "GET",
            data: {"path": path},
            success: function(data) {
                console.log(data);
                window.location.reload();
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    var photos = [];
    photos.push(document.getElementById('photo-cell-1_delete'));
    photos.push(document.getElementById('photo-cell-2_delete'));
    photos.push(document.getElementById('photo-cell-3_delete'));
    photos.push(document.getElementById('photo-cell-4_delete'));
    photos.push(document.getElementById('photo-cell-5_delete'));
    photos.map(function (photo) {
        if (photo) {
            photo.addEventListener('click', function () {
                deleteFileAdminGeneral(photo.dataset.path);
                photo.parentNode.style.display = 'none';
            });
        }
    });

    var inputs = document.getElementsByClassName('photo-field');
    for (var i = 0; i < inputs.length; i++) {
        inputs.item(i).addEventListener('change', function () {
            var wrapper = this.parentNode;
            var paragraphs = wrapper.getElementsByTagName('p');
            for (var i = 0; i < paragraphs.length; i++) {
                wrapper.removeChild(paragraphs[i]);
            }
            var p = document.createElement('p');
            p.innerText = this.files[0].name;
            p.style.fontSize = '0.7em';
            p.style.position = 'relative';
            p.style.top = '-2em';
            wrapper.appendChild(p);
        });
    }

    $(document).ready(function(){
        var errorMessageDivs = document.getElementsByClassName('help-block');
        for (var i = 0; i < errorMessageDivs.length; i++){
            if (errorMessageDivs[i].previousSibling.previousSibling.classList
                && errorMessageDivs[i].previousSibling.previousSibling.classList.contains('photo-field')) {
                    errorMessageDivs[i].style.width = errorMessageDivs[i].parentNode.parentNode.parentNode.offsetWidth + 'px';
                    errorMessageDivs[i].style.position = 'relative';
                    errorMessageDivs[i].style.top = '2em';
            }
        }
    });

JS;
$this->registerJs($scriptToggleFields, yii\web\View::POS_END);
?>

<?php $generalForm = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

    <?php if (!$model->id) { ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $generalForm->field($model, 'created_by')->dropDownList($model->users())->label('Владелец') ?>
            </div>
        </div>
    <?php } else { ?>
        <h4 style="font-weight: bold;">Владелец</h4>
        <h5>
            <?php if ($model->user) {
                echo Html::a($model->user->name . ' ' . $model->user->email, ['/admin/user/user/', 'id' => $model->user->id]);
            } else {
                echo 'нет';
            } ?>
        </h5>
    <?php } ?>

    <?= $generalForm->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $generalForm->field($model, 'description')->textarea() ?>

    <hr>

    <div class="row">
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'work_hours_weekdays_from')->textInput(['type'=> 'time', 'id' => 'wh_wd_from']) ?>
        </div>
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'work_hours_weekdays_to')->textInput(['type'=> 'time', 'id' => 'wh_wd_to']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'work_hours_weekends_from')->textInput(['type'=> 'time', 'id' => 'wh_we_from']) ?>
        </div>
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'work_hours_weekends_to')->textInput(['type'=> 'time', 'id' => 'wh_we_to']) ?>
        </div>
    </div>

    <?= $generalForm->field($model, 'work_hours_24_7')->checkbox() ?>

    <hr>

    <?= $generalForm->field($model, 'address_ym_geocoder')->textInput(['readonly'=> true, 'placeholder' => 'Укажите адрес на карте', 'id' => 'input_address_ym_geocoder']) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'address_lat')->textInput(['readonly'=> true, 'id' => 'input_address_lat']) ?>
        </div>
        <div class="col-sm-6">
            <?= $generalForm->field($model, 'address_lon')->textInput(['readonly'=> true, 'id' => 'input_address_lon']) ?>
        </div>
    </div>

    <?= $generalForm->field($model, 'address_city_ym_geocoder')->hiddenInput(['readonly'=> true, 'id' => 'input_address_city_ym_geocoder']) ?>

    <div class="form-group">
        <?= Geocoder::widget(['model' => $model]) ?>
    </div>

    <div class="row photo-wrapper" style="margin-bottom: 3.5em;">
<?php if (isset($documents) && count($documents)) {
    $idx = 0;
    $photo_names = ['photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5'];
    $photo_exists = [];
    foreach ($documents as $photo_N => $photo_path) {
        echo '<div class="photo-cell" id="photo-cell-' . ($idx + 1) . '"><img src="' .
        Url::to('@web' . $photo_path) .
        '" alt="warehouse photo ' . ($idx + 1) . '">' .
        '<div class="photo-delete" id="photo-cell-' . ($idx + 1) . '_delete"' .
        'data-path="' . $photo_path . '">' .
        '&times;</div></div>';
        $idx++;
        $photo_exists[] = $photo_N;
    }
    $photo_last = array_diff($photo_names, $photo_exists);
    foreach ($photo_last as $photo) {
        echo '<div class="photo-cell">' . $generalForm->field($model, $photo)->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
    }

} else {
    echo '<div class="photo-cell">' . $generalForm->field($model, 'photo_1')->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
    echo '<div class="photo-cell">' . $generalForm->field($model, 'photo_2')->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
    echo '<div class="photo-cell">' . $generalForm->field($model, 'photo_3')->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
    echo '<div class="photo-cell">' . $generalForm->field($model, 'photo_4')->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
    echo '<div class="photo-cell">' . $generalForm->field($model, 'photo_5')->fileInput(['class' => 'photo-field'])->label('Добавить', ['class' => 'photo-label']) . '</div>';
} ?>
    </div>
    <p style="color: gray; margin-top: 2em;">5 фотографий в формате JPG или PNG. Максимальный размер - 20MB</p>

    <div class="form-group" style="text-align: right;">

        <?php if ($model->id
                    && (($model->sb_verification_status_sn == Warehouse::VERIFICATION_VERIFIED
                            && $model->dsl_verification_status_sn == Warehouse::VERIFICATION_VERIFIED)
                        || ($model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                            || $model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED))
        ) {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next', 'disabled' => 'disabled']);
        } else {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next']);
        } ?>

        <?php ActiveForm::end() ?>

    </div>

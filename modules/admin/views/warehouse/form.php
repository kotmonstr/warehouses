<?php

use app\models\Warehouse;
use app\modules\admin\models\forms\warehouse\WarehouseForm;
use yii\bootstrap\Html;
use yii\web\View;
use app\models\User;

/* @var $this View */
/* @var $current string */
/* @var $title string */
/* @var $model app\modules\admin\models\forms\warehouse\WarehouseForm */
/* @var $documents string[]  */

$this->title = $title ? : 'Новый склад';
$this->params['breadcrumbs'][] = ['label' => 'Склады', 'url' => ['/admin/warehouse/index']];
$this->params['breadcrumbs'][] = $this->title;

$tabs = [];
$firstEmptyPassed = false;

foreach (Warehouse::TABS as $alias => $title) {
    $options = [];
    $content = Html::a($title, [$alias, 'id' => $model->id]);

    if (!$model->id) {
        $options['class'] = 'disabled';
        $options['title'] = 'Склад еще не создан, параметр id отсутствует';
    }

    if ($alias == $current) {
        $options['class'] = 'active';
    }

    $tabs[] = Html::tag('li', $content, $options);
}

function showBtnVerify($model, $type, $text) {
    echo Html::beginForm('/admin/warehouse/verify?id=' . $model->id . '&type=' . $type, 'get');
    echo '<button type="submit" class="btn btn-primary" style="width: 12em; margin-bottom: .5em;"';
    if ($model[$type] == Warehouse::VERIFICATION_RESTRICTED
        || $model[$type] == Warehouse::VERIFICATION_VERIFIED
        || ($type == 'sb_verification_status_sn' && $model->dsl_verification_status_sn != Warehouse::VERIFICATION_VERIFIED)
    ) {
            echo 'disabled="disabled"';
    }
    echo ">{$text}</button>";
    echo Html::endForm();
}

function showBtnVerifyCancel($model, $type, $text, $id) {
    echo '<button type="button" style="width: 16em; margin-bottom: .5em;"';
    echo "class='btn btn-danger' id='{$id}'";
    echo 'data-toggle="modal" data-target="#verifyCancelModalWarehouse"';
    if ($model[$type] == Warehouse::VERIFICATION_RESTRICTED || $model[$type] == Warehouse::VERIFICATION_VERIFIED
        || ($type == 'sb_verification_status_sn' && $model->dsl_verification_status_sn != Warehouse::VERIFICATION_VERIFIED)
    ) {
        echo 'disabled="disabled"';
    }
    echo ">{$text}</button>";
}

$scriptSetTypeForVerifyCancel = <<< JS

    var btnVerifyCancelModal = document.getElementById('verifyCancelWarehouseBtnSend');
    var btnVerifyCancelSb = document.getElementById('sb_verify_cancel_button');
    var btnVerifyCancelDsl = document.getElementById('dsl_verify_cancel_button');
    if (btnVerifyCancelSb) {
        btnVerifyCancelSb.addEventListener('click', function () {
            btnVerifyCancelModal.dataset.canceltype = 'sb_verification_status_sn';
        });
    }
    if (btnVerifyCancelDsl) {
        btnVerifyCancelDsl.addEventListener('click', function () {
            btnVerifyCancelModal.dataset.canceltype = 'dsl_verification_status_sn';
        });
    }

JS;
$this->registerJs($scriptSetTypeForVerifyCancel, yii\web\View::POS_END);
?>

<?php if ($model->id) {?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('verify_cancel_modal', ['id' => $model->id]) ?>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <div class="row">
        <div class="col-sm-3">
            <h5 style="color: gray;">Дата добавления</h5>
            <p>
                <?= isset($model->created_at) ? date('d.m.Y', $model->created_at) : 'не задано' ?>
            </p>
        </div>

        <div class="col-sm-3">
            <h5 style="color: gray;">Статус ДСЛ</h5>
            <p>
                <?php
                    switch ($model->dsl_verification_status_sn) {
                    case Warehouse::VERIFICATION_IN_PROGRESS:
                        echo '<span style="color: orange;">Ожидает</span>';
                        break;
                    case Warehouse::VERIFICATION_VERIFIED:
                        echo '<span style="color: green;">Аттестован</span>';
                        break;
                    case Warehouse::VERIFICATION_FAILED:
                        echo '<span style="color: red;">Отменен</span>';
                        break;
                    case Warehouse::VERIFICATION_RESTRICTED:
                        echo '<span style="color: red;">Отмена навсегда</span>';
                        break;
                    default:
                        echo '<span style="color: orange;">Ожидает</span>';
                        }
                ?>
            </p>
        </div>

        <div class="col-sm-3">
            <h5 style="color: gray;">Статус СБ</h5>
            <p>
                <?php
                    switch ($model->sb_verification_status_sn) {
                    case Warehouse::VERIFICATION_IN_PROGRESS:
                        echo '<span style="color: orange;">Ожидает</span>';
                        break;
                    case Warehouse::VERIFICATION_VERIFIED:
                        echo '<span style="color: green;">Верифицирован</span>';
                        break;
                    case Warehouse::VERIFICATION_FAILED:
                        echo '<span style="color: red;">Отменен</span>';
                        break;
                    case Warehouse::VERIFICATION_RESTRICTED:
                        echo '<span style="color: red;">Отмена навсегда</span>';
                        break;
                    default:
                        echo '<span style="color: orange;">Ожидает</span>';
                        }
                ?>
            </p>
        </div>

        <div class="col-sm-3">
            <h5 style="color: gray;">Компания</h5>
            <p>
                <?= $model->organization ?
                    Html::a($model->organization->title, ['/admin/organization/organization/', 'id' => $model->organization_id]):
                    'не задано' ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h5 style="color: gray;">Комментарий</h5>
            <?= Warehouse::findModel($model->id)->verification_comment ?? '' ?>
        </div>
    </div>
    <hr>

    <div class="row">

    <?php switch (Yii::$app->user->identity->role) {

        case User::ROLE_DSL:
            echo "<div class='col-sm-3'>";
            showBtnVerify($model, 'dsl_verification_status_sn', 'Аттестовать');
            echo "</div>";

            echo '<div class="col-sm-3">';
            showBtnVerifyCancel($model, 'dsl_verification_status_sn', 'Отклонить аттестацию', 'dsl_verify_cancel_button');
            echo '</div>';
            break;

        case User::ROLE_SB:
            echo "<div class='col-sm-3'>";
            showBtnVerify($model, 'sb_verification_status_sn', 'Верифицировать');
            echo "</div>";

            echo '<div class="col-sm-3">';
            showBtnVerifyCancel($model, 'sb_verification_status_sn', 'Отклонить верификацию', 'sb_verify_cancel_button');
            echo '</div>';
            break;

        case User::ROLE_ADMIN:
            echo "<div class='col-sm-3'>";
            showBtnVerify($model, 'dsl_verification_status_sn', 'Аттестовать ДСЛ');
            showBtnVerify($model, 'sb_verification_status_sn', 'Верифицировать СБ');
            echo "</div>";

            echo '<div class="col-sm-3">';
            showBtnVerifyCancel($model, 'dsl_verification_status_sn', 'Отклонить аттестацию ДСЛ', 'dsl_verify_cancel_button');
            showBtnVerifyCancel($model, 'sb_verification_status_sn', 'Отклонить верификацию СБ', 'sb_verify_cancel_button');
            echo '</div>';
            break;

    }?>

        <div class="col-sm-3"></div>

        <div class="col-sm-3" style="text-align: right;">
            <?= Html::beginForm('/admin/warehouse/delete?id=' . $model->id, 'get') ?>
                <?= Html::submitButton('Удалить склад',
                    [
                        'class' => 'btn',
                        'style' => 'border: 1px solid red; border-radius: 5px; color: red; width: 12em;',
                        "data" => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                        ],
                    ]
                ) ?>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>
<?php } ?>

<div class="row">
    <div class="col-lg-8 col-md-10">

        <ul class="nav nav-tabs">
            <?= implode('', $tabs) ?>
        </ul>

        <div class="panel panel-default">
            <div class="panel-body">

                <?= $this->render($current, [
                    'model' => $model,
                    'documents' => $documents,
                ]) ?>

            </div>
        </div>
    </div>
</div>

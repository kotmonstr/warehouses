<?php

/* @var $this View */

use yii\web\View;

/* @var $id string|int id of warehouse */

$scriptVerifyCancel = <<< JS

    function verifyCancelWarehouse(id, comment, forever, type) {
        $.ajax({
            url: '/admin/warehouse/verify-cancel',
            method: "GET",
            data: {"id": id, 'comment': comment, 'forever': forever, 'type': type},
            success: function(data) {
                console.log(data);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    var btnVerifyCancelWarehouse = document.getElementById('verifyCancelWarehouseBtnSend');
    if (btnVerifyCancelWarehouse) {
        btnVerifyCancelWarehouse.addEventListener('click', function () {
            var verifyCancelWarehouseComment = document.getElementById('verifyCancelWarehouseComment').value;
            var verifyCancelWarehouseChkbx = document.getElementById('verifyCancelWarehouseChkbx');
            var id = this.dataset.id;
            var canceltype = this.dataset.canceltype;
            if (verifyCancelWarehouseChkbx.checked) {
                verifyCancelWarehouse(id, verifyCancelWarehouseComment && verifyCancelWarehouseComment != '' ? verifyCancelWarehouseComment : 'no comments', true, canceltype);
            }else{
                verifyCancelWarehouse(id, verifyCancelWarehouseComment && verifyCancelWarehouseComment != '' ? verifyCancelWarehouseComment : 'no comments', false, canceltype);
            }
        });
    }

JS;
$this->registerJs($scriptVerifyCancel, yii\web\View::POS_END);

?>
<!-- Verify cancel modal begin-->
<div class="modal fade" id="verifyCancelModalWarehouse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 15%; left: 25%;">
    <div class="modal-content" style="width: 50%;">


        <div class="modal-c-tabs">
            <div class="options text-right">
                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">&#10005;</button>
            </div>

            <h4 style="padding:0 1em;">Причина отклонения</h4>

            <div class="modal-body mb-1" style="padding: 1em;">
                <div class="form-group">
                    <textarea id="verifyCancelWarehouseComment" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="checkbox" id="verifyCancelWarehouseChkbx"/>
                    <label for="verifyCancelWarehouseChkbx">Запретить навсегда</label>
                </div>
            </div>

            <div class="modal-footer" style="padding: 1em;">
                <button type="button" class="btn btn-primary"
                    id="verifyCancelWarehouseBtnSend"
                    style="display: block; padding: 1em auto; width: 100%;"
                    data-id="<?= $id ?>">Отправить</button>
                <button type="button" class="btn "
                    style="display: block; padding: 1em auto; color: blue; width: 100%; margin: 0;"
                    data-dismiss="modal">Отмена</button>
            </div>

        </div>
    </div>
</div>
<!-- Verify cancel modal end-->

<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $searchModel app\modules\admin\models\forms\warehouse\SearchForm */

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Warehouse;
use app\models\Dictionary;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Склады';
$this->params['breadcrumbs'][] = $this->title;

$scriptSearchStorageTypes = <<< JS

    var selectStorageSystemWarehouse = document.getElementById('select_storage_system_warehouse');
    if (selectStorageSystemWarehouse) {
        selectStorageSystemWarehouse.addEventListener('change', function(){
            if (this.value) {
                window.location.href = '/admin/warehouse/index?SearchForm[storage_s]=' + this.value;
            }
        });
    }

    var btnStorageSystemWarehouse = document.getElementById('btn_storage_system_warehouse');
    if (btnStorageSystemWarehouse) {
        btnStorageSystemWarehouse.addEventListener('click', function(){
            window.location.href = '/admin/warehouse/index';
        });
    }

JS;

$this->registerJs($scriptSearchStorageTypes, yii\web\View::POS_END);

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/admin/warehouse/general?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

    <div style="width: 15%; position:relative; top:1em; left:.5em;">
        <button type="button" id="btn_storage_system_warehouse" class="btn" style="width: 90%; color:blue; border:1px solid blue; border-radius:5px;">Все</button>
    </div>

    <div style="width: 40%; position:relative; top:2.4em; left:38%;">
        <?php
            $data = Dictionary::findDictItems('storage_system');
            echo '<select id="select_storage_system_warehouse" class="form-control" style="width:100%;" name="SearchForm[storage_s]">';
            echo "<option value=''>Тип хранения</option>";
            foreach ($data as $system_name => $title) {
                echo "<option value='{$system_name}'>{$title}</option>";
            }
            echo '</select>';
        ?>
    </div>


    <div style="width: 20%; position:relative; top:0em; left:78%; text-align:right;">
        <?= Html::beginForm('/admin/warehouse/general', 'get') ?>
        <?= Html::submitButton('Новый склад', ['class' => 'btn btn-primary', 'style' => 'width: 90%;']) ?>
        <?= Html::endForm() ?>
    </div>


<div class="row" style="padding: 1em; text-align: center; margin-top:-4em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterPosition' => GridView::FILTER_POS_HEADER,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => [
                'style' => 'border: none; cursor: pointer;',
                'onmouseover' => 'this.style.backgroundColor="lightblue";',
                'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
            ],
            'columns' => [
                [
                    'attribute' => 'title',
                    'headerOptions' => ['style' => 'width: 15%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'placeholder' => 'Название склада'
                    ],
                    'content' => function ($model, $key, $index, $column) {
                        return '<p>' .
                            $model->title . '</p><p style="font-size: .7em; color: gray;">' .
                            $model->organizationName . '</p>';
                    }

                ],
                [
                    'attribute' => 'organizationName',
                    'headerOptions' => ['style' => 'width: 15%; display:none;'],
                    'contentOptions' => [ 'style' => 'text-align: left; display:none;' ],
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'style'       => 'position: relative; top: -3.9em; width:86%;',
                        'placeholder' => 'Название организации'
                    ]
                ],
                [
                    'attribute' => 'address_ym_geocoder',
                    'headerOptions' => ['style' => 'width: 25%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'style'       => 'position: relative; left: -86%; width:70%;',
                        'placeholder' => 'Адрес склада'
                    ]
                ],
                [
                    'attribute' => 'storage_type_sn',
                    'headerOptions' => ['style' => 'width: 30%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                ],
                [
                    'attribute' => 'created_at',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'header' => 'Дата регистрации',
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'format' => ['date', 'php:d.m.Y']
                ],
                [
                    'attribute' => 'dsl_verification_status_sn',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'content' => function ($model, $key, $index, $column) {
                        switch ($model->dsl_verification_status_sn) {
                        case Warehouse::VERIFICATION_IN_PROGRESS:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        case Warehouse::VERIFICATION_FAILED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отменен</div>';
                        case Warehouse::VERIFICATION_RESTRICTED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отмена навсегда</div>';
                        case Warehouse::VERIFICATION_VERIFIED:
                            return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Аттестован</div>';
                        default:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        }
                    }
                ],
                [
                    'attribute' => 'sb_verification_status_sn',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'content' => function ($model, $key, $index, $column) {
                        switch ($model->sb_verification_status_sn) {
                        case Warehouse::VERIFICATION_IN_PROGRESS:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        case Warehouse::VERIFICATION_VERIFIED:
                            return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Верифицирован</div>';
                        case Warehouse::VERIFICATION_FAILED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отменен</div>';
                        case Warehouse::VERIFICATION_RESTRICTED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отмена навсегда</div>';
                        default:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        }
                    }
                ],
            ],
        ]
    ) ?>
</div>

</div>

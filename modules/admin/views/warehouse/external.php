<?php

use app\modules\admin\models\forms\warehouse\ExternalForm;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use app\models\Dictionary;
use app\models\Warehouse;

/* @var $this yii\web\View*/
/* @var $model ExternalForm */
/* @var $model app\modules\admin\models\forms\warehouse\ExternalForm */

?>

<?php $externalForm = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $externalForm->field($model, 'accounting_system_sn')->inline()->radioList(Dictionary::findDictItems('accounting_system_type')); ?>
    <hr>

    <?php echo $externalForm->field($model, 'classification_sn')->inline()->radioList(Dictionary::findDictItems('classification')); ?>
    <hr>

    <p style="font-weight: bold;">Характеристики здания</p>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'square_area')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'square_area_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'built_up_area')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'built_up_area_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'ceiling_height')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'ceiling_height_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $externalForm->field($model, 'floors')->textInput() ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Организация движения</p>
    <?= $externalForm->field($model, 'maneuvering_area')->checkbox(); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $externalForm->field($model, 'entries_amount')->textInput() ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Зона разгрузки</p>
    <?= $externalForm->field($model, 'loading_ramp')->checkbox(); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $externalForm->field($model, 'automatic_gates_amount')->textInput() ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <?= $externalForm->field($model, 'accounting_control_system_sn')->checkboxList(Dictionary::findDictItems('accounting_control_systems')); ?>
    <hr>

    <p style="font-weight: bold;">Прилегающая территория</p>
    <?= $externalForm->field($model, 'outer_lighting')->checkbox(); ?>
    <?= $externalForm->field($model, 'outer_security')->checkbox(); ?>
    <?= $externalForm->field($model, 'outer_beautification')->checkbox(); ?>
    <?= $externalForm->field($model, 'outer_fencing')->checkbox(); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <?= $externalForm->field($model, 'outer_entries_amount')->textInput() ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Площадки для автотранспорта</p>
    <?php echo $externalForm->field($model, 'car_parking_sn')->inline()->radioList(Dictionary::findDictItems('parking_type')); ?>
    <?php echo $externalForm->field($model, 'truck_parking_sn')->inline()->radioList(Dictionary::findDictItems('parking_type')); ?>
    <?= $externalForm->field($model, 'truck_maneuvering_area')->checkbox(); ?>
    <hr>

    <p style="font-weight: bold;">ЖД</p>
    <?= $externalForm->field($model, 'rail_road')->checkbox(); ?>
    <hr>

    <p style="font-weight: bold;">Расположение относительно магистралей</p>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'distance_from_highway')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'distance_from_highway_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <p style="font-weight: bold;">Ограничения для ТС</p>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_length')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_length_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_height')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_height_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_width')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_width_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'distance', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_weight')->textInput(['style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= $externalForm->field($model, 'vehicle_max_weight_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'mass', true), ['style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;'])->label('Ед. изм.'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr>


    <div class="form-group btns-wrapper">

        <?php if (($model->sb_verification_status_sn == Warehouse::VERIFICATION_VERIFIED
                        && $model->dsl_verification_status_sn == Warehouse::VERIFICATION_VERIFIED)
                    || ($model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                        || $model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED)
        ) {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next', 'disabled' => 'disabled']);
        } else {
            echo Html::submitButton('Далее', ['class' => 'btn btn-primary btn-next']);
        } ?>

        <?php ActiveForm::end() ?>

        <?= Html::a('На предыдущий шаг', ['/admin/warehouse/general?id=' . $model->id], ['class' => 'btn btn-prev']) ?>

    </div>




<?php

/* @var $this \yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\forms\advert\SearchForm */

use app\models\Organization;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Dictionary;
use yii\helpers\Url;
use app\models\Advert;
use app\models\Warehouse;

$this->title = 'Объявления';
$this->params['breadcrumbs'][] = $this->title;

$cityArray = Warehouse::getCities();
$organizationArray = ArrayHelper::map(Organization::find()->where(['deleted_at' => null])->all(), 'id', 'title');
$warehouseArray = ArrayHelper::map(
    Warehouse::find()->where(['deleted_at' => null])
        ->andFilterWhere(['organization_id' => $searchModel->organizationName ? $searchModel->organizationName : null])
        ->all(),
    'id',
    'title'
);


function getFilterFromArray($array, $labelText, $model, $attribute) {
    $filterArray = ['' => $labelText] + $array;
    return Html::activeDropDownList($model, $attribute, $filterArray, ['class' => 'form-control']);
}

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/admin/advert/advert?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="margin: 1em 5em 1em 0; padding: 1em;">
<div class="row">
    <div class="col-sm-6" style="padding-left: 0;">
        <?= Html::beginForm('/admin/advert/index', 'get') ?>
        <?= Html::submitButton('Все', ['class' => 'btn btn-default', 'style' => 'width: 12em; color: blue; border: 1px solid blue; border-radius: 5px;']) ?>
        <?= Html::endForm() ?>
    </div>

    <div class="col-sm-6" style="text-align: right; padding-right: 0;">
        <?= Html::a('Новое объявление', ['/admin/advert/advert'], ['class' => 'btn btn-primary', 'style' => 'width: 12em;']) ?>
    </div>
</div>
</div>


<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

<div class="row" style="padding: 1em; margin-top:-1em;">

        <?= GridView::widget(
            [
                'id' => 'grid-id-ob-admin',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
                'layout' => "{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table'
                ],
                'rowOptions' => [
                    'style' => 'border: none; cursor: pointer;',
                    'onmouseover' => 'this.style.backgroundColor="lightblue";',
                    'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
                ],
                'columns' => [
                    [
                        'attribute' => 'organizationName',
                        'filter' => getFilterFromArray($organizationArray, 'Организации', $searchModel, 'organizationName'),
                        'headerOptions' => ['style' => 'width:17%;'],
                        'value' => 'organization.title',
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:17%;'],
                    ],
                    [
                        'attribute' => 'warehouseName',
                        'filter' => getFilterFromArray($warehouseArray, 'Склады', $searchModel, 'warehouseName'),
                        'headerOptions' => ['style' => 'width:15%;'],
                        'value' => 'warehouse.title',
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:15%;'],
                    ],
                    [
                        'attribute' => 'city',
                        'header' => 'Город',
                        'headerOptions' => ['style' => 'width:9%;'],
                        'filter' => getFilterFromArray($cityArray, 'Города', $searchModel, 'city'),
                        'value' => function ($model, $key, $index, $column) {
                            return $model->warehouse->address_city_ym_geocoder;
                        },
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:9%;'],
                    ],
                    [
                        'attribute' => 'storage_type_sn',
                        'filter' => getFilterFromArray(Dictionary::findDictItems('storage_system'), 'Типы систем хранения', $searchModel, 'storage_type_sn'),
                        'value' => 'storage_type.title',
                        'headerOptions' => ['style' => 'width:30%;'],
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:30%;'],
                    ],
                    [
                        'label' => 'Площадь',
                        'attribute' => 'available_space',
                        'headerOptions' => ['style' => 'width:5%;'],
                        'value' => function (Advert $model) {
                            return $model->available_space . ' ' . $model->available_space_type->title_short;
                        },
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:5%;'],
                    ],
                    [
                        'label' => '₽/м²/день',
                        'attribute' => 'unit_cost_per_day',
                        'headerOptions' => ['style' => 'width:5%;'],
                        'value' => function (Advert $model) {
                            return $model->unit_cost_per_day . ' ₽';
                        },
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:5%;'],
                    ],
                    [
                        'attribute' => 'available_from',
                        'filterInputOptions' => [
                            'type' => 'date',
                            'class' => 'form-control',
                            'style' => ['width' => '120px'],
                            'id' => null,
                        ],
                        'format' => ['date', 'php:d.m.Y'],
                        'headerOptions' => ['style' => 'width:5%;'],
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:5%;'],
                    ],
                    [
                        'attribute' => 'available_to',
                        'filterInputOptions' => [
                            'type' => 'date',
                            'class' => 'form-control',
                            'style' => ['width' => '120px'],
                            'id' => null,
                        ],
                        'format' => ['date', 'php:d.m.Y'],
                        'headerOptions' => ['style' => 'width:5%;'],
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:5%;'],
                    ],
                    [
                        'attribute' => 'status_sn',
                        'filter' => getFilterFromArray(Dictionary::findDictItems('advert_status'), 'Статус', $searchModel, 'status_sn'),
                        'headerOptions' => ['style' => 'width:8%;'],
                        'value' => function ($model, $key, $index, $column) {
                            switch ($model->status_sn) {
                                case Advert::STATUS_PUBLISHED:
                                    return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Опубликовано</div>';
                                case Advert::STATUS_DRAFT:
                                    return '<div class="alert" style="margin: 0; padding: .2em .5em; background-color: gray;">Черновик</div>';
                                default:
                                    return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Корректировка</div>';
                            }
                        },
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'grid-table-row', 'style' => 'width:8%;'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['style' => 'width:3em; border: none; position: relative; left: -1.5em;'],
                        'contentOptions' => ['style' => 'border: none; text-align: left; width:3em;  position: relative; left: -1em; padding-top: 1em;'],
                        'template' => '{delete}',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'delete') {
                                return Url::to(['/admin/advert/delete?id=' . $model->id]);
                            }
                        },
                    ],
                ],
            ]
        ) ?>
</div>

</div>

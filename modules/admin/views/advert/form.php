<?php

/* @var $this \yii\web\View */
/* @var $model app\modules\admin\models\forms\advert\Form */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Advert;
use app\models\Dictionary;
use yii\widgets\MaskedInput;

$this->title = $model->id && $model->warehouse ? 'Объявление для склада "' . $model->warehouse->title . '"' : 'Новое объявление';
$this->params['breadcrumbs'][] = ['label' => 'Объявления', 'url' => ['/admin/advert/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<?php if (isset($model->id)) { ?>
<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
        <div class="row">
            <div class="col-sm-4">
                <h4>
                Склад:
                <?= Html::a($model->warehouse->title, ['/admin/warehouse/general/', 'id' => $model->warehouse_id]) ?>
                </h4>
            </div>

<?php
    switch ($model->status_sn) {
    case Advert::STATUS_PUBLISHED:
        echo '<div class="alert alert-success col-sm-2" style="margin: 0; padding: .2em .5em; text-align: center;">Опубликовано</div>';
        break;
    case Advert::STATUS_DRAFT:
        echo'<div class="alert col-sm-2" style="margin: 0; padding: .2em .5em; background-color: gray; text-align: center;">Черновик</div>';
        break;
    default:
        echo'<div class="alert alert-warning col-sm-2" style="margin: 0; padding: .2em .5em; text-align: center;">Корректировка</div>';
    }
    echo '<div class="col-sm-6" style="text-align: right;">' .
        Html::a('Смотреть на сайте', ['/site/advert-info', 'id' => $model->id], ['class' => 'btn btn-primary', 'style' => 'width: 14em;']) . '</div>';
    echo '</div>';

    echo '<div class="row"><div class="col-sm-12"><h4>Владелец: </h4><p>';
    if ($model->user) {
        echo Html::a($model->user->name . ' ' . $model->user->email, ['/admin/user/user/', 'id' => $model->user->id]);
    } else {
        echo 'нет';
    }
    echo '</p></div></div>';

    $advertForm = ActiveForm::begin();
} else {
?>


<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <h4>Новое объявление</h4>
    <?php $advertForm = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $advertForm->field($model, 'warehouse_id')->dropDownList($model->warehouses())->label('Склад') ?>
        </div>
    </div>

<?php } ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $advertForm->field($model, 'storage_type_sn')->dropDownList(Dictionary::findDictItems('storage_system'), ['value' => $model->storage_type_sn ? $model->storage_type_sn : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $advertForm->field($model, 'available_space')->textInput(['value' => $model->available_space ? $model->available_space : '']) ?>
        </div>
        <div class="col-sm-3">
            <?= $advertForm->field($model, 'available_space_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true), ['value' => $model->storage_type_sn ? $model->storage_type_sn : '']) ?>
        </div>
        <div class="col-sm-6">
            <?= $advertForm->field($model, 'unit_cost_per_day')->textInput(['value' => $model->unit_cost_per_day ? $model->unit_cost_per_day : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $advertForm->field($model, 'available_from')->textInput(['type' => 'date', 'value' => $model->available_from ? $model->available_from : '']) ?>
        </div>
        <div class="col-sm-6">
            <?= $advertForm->field($model, 'available_to')->textInput(['type' => 'date', 'value' => $model->available_to ? $model->available_to : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $advertForm->field($model, 'manager_name')->textInput(['value' => $model->manager_name ? $model->manager_name : '']) ?>
        </div>
        <div class="col-sm-4">
            <?= $advertForm->field($model, 'manager_phone_number')->widget(MaskedInput::class, ['mask' => '+7 (999) 999-99-99',
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->textInput(['placeholder'=>'+7 (999) 999 9999'])?>
        </div>
        <div class="col-sm-4">
            <?= $advertForm->field($model, 'manager_email')->textInput(['value' => $model->manager_email ? $model->manager_email : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $advertForm->field($model, 'comment')->textarea(['rows' => 5]) ?>
        </div>
    </div>

    <div class="row">

<?php
if ($model->id) {
    if ($model->status_sn == Advert::STATUS_PUBLISHED) {
        echo '<div class="col-sm-3">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'Form[status_sn]', 'style' => 'width: 14em;', 'value' => Advert::STATUS_PUBLISHED]) . '</div>';
        echo '<div class="col-sm-3"></div><div class="col-sm-3" style="text-align: right;">' . Html::submitButton('Снять с публикации', ['class' => 'btn', 'name' => 'Form[status_sn]', 'style' => 'width: 14em; color: red; border: 1px solid red; border-radius: 5px;', 'value' => Advert::STATUS_DRAFT]) . '</div>';
    } else {
        echo '<div class="col-sm-3">' . Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'Form[status_sn]', 'style' => 'width: 14em;', 'value' => Advert::STATUS_DRAFT]) . '</div>';
        echo '<div class="col-sm-3">' . Html::submitButton('Сохранить и опубликовать', ['class' => 'btn', 'name' => 'Form[status_sn]', 'style' => 'width: 14em; color: blue; border: 1px solid blue; border-radius: 5px;', 'value' => Advert::STATUS_PUBLISHED]) . '</div><div class="col-sm-3"></div>';
    }

} else {
    echo '<div class="col-sm-3">' . Html::submitButton('Сохранить и опубликовать', ['class' => 'btn btn-primary', 'name' => 'Form[status_sn]', 'style' => 'width: 14em;', 'value' => Advert::STATUS_PUBLISHED]) . '</div>';
    echo '<div class="col-sm-3">' . Html::submitButton('Сохранить черновик', ['class' => 'btn', 'name' => 'Form[status_sn]', 'style' => 'width: 14em; color: blue; border: 1px solid blue; border-radius: 5px;', 'value' => Advert::STATUS_DRAFT]) . '</div><div class="col-sm-3"></div>';
}
    ActiveForm::end();
?>

<?php
if ($model->id) { ?>
    <div class="col-sm-3" style="text-align: right;">

    <?= Html::a('Удалить объявление', ['/admin/advert/delete?id=' . $model->id], [
        'class' => 'btn btn-danger',
        'style' => 'width: 14em;',
        "data" => [
            'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
        ],
    ]) ?>

    </div>
<?php } ?>


    </div>


</div>

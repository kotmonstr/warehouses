<?php

/* @var $this View */
/* @var $model app\modules\admin\models\forms\organization\Form */
/* @var $documents app\models\Document[] */

use app\models\Organization;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use app\models\User;

$this->title = $model->id ? $model->title : 'Новая компания';

$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['/admin/organization/index']];
$this->params['breadcrumbs'][] = $this->title;


$scriptToggleFileFields = <<< JS

    var selectULIP = document.getElementById('form-legal_type');
    if (selectULIP) {
        selectULIP.addEventListener('change', function () {
            if (selectULIP.value == 'UL') {
                document.getElementById('ul_fields_wrapper').style.display = 'block';
                document.getElementById('ip_fields_wrapper').style.display = 'none';
            } else {
                document.getElementById('ul_fields_wrapper').style.display = 'none';
                document.getElementById('ip_fields_wrapper').style.display = 'block';
            }
        });
    }

    var linksDelete = document.getElementsByClassName('admin_organization_delete_document');
    for (var i = 0; i < linksDelete.length; i++) {
        linksDelete.item(i).addEventListener('click', function () {
            if (this.dataset.id) {
                $.ajax({
                    url: '/admin/organization/delete-file',
                    method: 'GET',
                    data: {"id": this.dataset.id},
                    success: function(data) {
                        console.log(data);
                        window.location.reload();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }
        });
    }

JS;
$this->registerJs($scriptToggleFileFields, yii\web\View::POS_END);

$pdfBriefReportUrl = '#';
if ($model->inn) {
    $responseKontur = Yii::$app->konturApi->downloadBriefReport(['inn' => $model->inn]);
    if (!isset($responseKontur->error)) {
        $pdfBriefReportUrl = $responseKontur;
    }
}
?>

<h1 class="row">
    <div class="col-sm-8">
        <?= Html::encode($this->title) ?>
    </div>

    <?php if ($model->inn) { ?>
        <div class='col-sm-4' style='text-align:right;'>
            <?= Html::a('Анкета компании', ['organization/download-excel-form', 'id' => $model->id], ['class' => 'btn btn-default', 'target' => '_blank', 'style' => '']) ?>
            <?= Html::a('Отчет о компании', $pdfBriefReportUrl, ['class' => 'btn btn-default', 'target' => '_blank', 'style' => '']) ?>
        </div>
    <?php }?>
</h1>

<?= $this->render('verify_cancel_modal', ['id' => isset($model->id) ? $model->id : '']) ?>

<?php
    if (isset($model->id)) { ?>
        <div style="background-color: #F3F3F3; margin: 1em 0; border-radius: 10px; padding: 1em;">
            <div class="row">
                <div class="col-sm-3">
                    <h5 style="color: gray;">Дата отправки документов</h5>
                    <p>
                        <?= isset($model->created_at) ? date('d.m.Y', $model->created_at) : 'не задано' ?>
                    </p>
                </div>

                <div class="col-sm-3">
                    <h5 style="color: gray;">Статус</h5>
                    <p>
                        <?php
                            switch ($model->verification_status_sn) {
                            case Organization::VERIFICATION_IN_PROGRESS:
                                echo '<span style="color: orange;">Ожидает</span>';
                                break;
                            case Organization::VERIFICATION_VERIFIED:
                                echo '<span style="color: blue;">Верифицирована</span>';
                                break;
                            case Organization::VERIFICATION_FAILED:
                                echo '<span style="color: red;">Отменена</span>';
                                break;
                            case Organization::VERIFICATION_RESTRICTED:
                                echo '<span style="color: red;">Отмена навсегда</span>';
                                break;
                            default:
                                echo '<span style="color: orange;">Ожидает</span>';
                            }
                        ?>
                    </p>
                </div>

                <div class="col-sm-6">
                    <h5 style="color: gray;">Комментарий</h5>
                    <?= Organization::findModel($model->id)->verification_comment ?? '' ?>
                </div>
            </div>
            <hr>

            <div class="row">

                <div class="col-sm-3">

                    <?= Html::beginForm('/admin/organization/verify?id=' . $model->id, 'get') ?>
                    <button type="submit" class="btn btn-primary" style="width: 12em;"
                        <?= $model->verification_status_sn == Organization::VERIFICATION_RESTRICTED
                        || $model->verification_status_sn == Organization::VERIFICATION_VERIFIED ?
                        'disabled="disabled"' : "" ?>
                    >
                    Верифицировать
                    </button>
                    <?= Html::endForm() ?>

                </div>
                <div class="col-sm-3">

                    <button type="button" style="width: 15em;"
                        class="btn btn-danger"
                        data-toggle="modal" data-target="#verifyCancelModal"
                            <?= $model->verification_status_sn == Organization::VERIFICATION_RESTRICTED ?
                            'disabled="disabled"' : "" ?>
                        >
                    Отклонить верификацию
                    </button>

                </div>

                <div class="col-sm-3"></div>

                <div class="col-sm-3" style="text-align: right;">
                    <?= Html::beginForm('/admin/organization/delete?id=' . $model->id, 'get') ?>
                    <?= Html::submitButton('Удалить компанию',
                        [
                            'class' => 'btn',
                            'style' => 'border: 1px solid red; border-radius: 5px; color: red; width: 12em;',
                            "data" => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                            ],
                        ]
                    ) ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
<?php $organizationForm = ActiveForm::begin(); ?>

<div style="background-color: #F3F3F3; margin: 1em 0; border-radius: 10px; padding: 1em;">
    <h4>Владелец</h4>
    <h5>
        <?php if ($model->user) {
            echo Html::a($model->user->name . ' ' . $model->user->email, ['/admin/user/user/', 'id' => $model->user->id]);
        } else {
            echo 'нет';
        } ?>
    </h5>
</div>

<?php } else { ?>

<div style="background-color: #F3F3F3; margin: 1em 0; border-radius: 10px; padding: 1em;">
    <h4>Пользователь, который станет управляющим компании</h4>

    <?php $organizationForm = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $organizationForm->field($model, 'created_by')->dropDownList($model->users())->label('Владелец') ?>
        </div>
    </div>
</div>

<?php } ?>

<div style="background-color: #F3F3F3; margin: 1em 0; border-radius: 10px; padding: 1em;">
    <h4>Реквизиты</h4>

        <div class="row">
            <div class="col-sm-8">
                <?= $organizationForm->field($model, 'title')->textInput(['value' => $model->title ? $model->title : '']) ?>
            </div>

            <div class="col-sm-4">
                <?= $organizationForm->field($model, 'inn')->textInput(['value' => $model->inn ? $model->inn : '']) ?>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $organizationForm->field($model, 'ogrn')->textInput(['value' => $model->ogrn ? $model->ogrn : '']) ?>
            </div>
            <div class="col-sm-4">
                <?= $organizationForm->field($model, 'kpp')->textInput(['value' => $model->kpp ? $model->kpp : '']) ?>
            </div>
            <div class="col-sm-4">
                <?= $organizationForm->field($model, 'okpo')->textInput(['value' => $model->okpo ? $model->okpo : '']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'partner_type')->dropDownList(Organization::partnerTypeList()) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'main_activity')->textInput(['value' => $model->main_activity ? $model->main_activity : '']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'address_ym_geocoder')->textInput(['value' => $model->address_ym_geocoder ? $model->address_ym_geocoder : '']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'post_address') ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'fact_address') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'taxation_type')->textInput(['value' => $model->taxation_type ? $model->taxation_type : '']) ?>
            </div>
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'legal_type')->dropDownList([
                    'UL' => 'Юридическое лицо',
                    'IP' => 'Индивидуальный предприниматель'
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'shareholders')->textarea() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'start_capital')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'registration_date')->textInput(['type' => 'date', 'value' => $model->registration_date ? $model->registration_date : '']) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'edm_type')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'employee_count')->textInput(['type' => 'number']) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'bank_details')->textarea(['style' => ['resize' => 'vertical']])->hint('Наименование и адрес банка, номер расчетного счета участника запроса предложений в банке, телефоны банка, прочие банковские реквизиты') ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'authorized_head_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО руководителя, имеющего право подписи согласно учредительным документам, с указанием должности и контактного телефона') ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'authorized_sign_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО уполномоченного лица, имеющего право подписи по доверенности с указанием должности, контактного телефона, эл.почты') ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <?= $organizationForm->field($model, 'authorized_security_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО сотрудника безопасности с указанием контактного телефона, эл.почты') ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'contact_phone')->textarea(['style' => ['resize' => 'vertical']]) ?>
            </div>
            <div class="col-sm-6">
                <?= $organizationForm->field($model, 'contact_email')->textarea(['style' => ['resize' => 'vertical']]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">

                <button type="submit" class="btn btn-primary">Сохранить изменения</button>

            </div>
        </div>


</div>

<div style="background-color: #F3F3F3; margin: 1em 0; border-radius: 10px; padding: 1em;">
    <h4>Документы</h4>

        <?php
            $entitiesUL = [
                'taxStampCopy',
                'taxRegistrationCopyUL',
                'naznachenieIspolnitelyaCopy',
                'doverennostNaPodpisanieCopy',
                'headSignatureCopy',
                'financialReportCopy'
            ];
            $entitiesIP = [
                'passportCopyIP',
                'taxRegistrationCopyIP'
            ];
            $entitiesAll = [
                'arendatorsList',
                'noDebtsReference',
                'rentContractCopy',
                'warehouseCadastralPassport',
                'propertyRightsCopy',
            ];
            $entitiesSet = [];
            if ($documents && count($documents)) {
                foreach ($documents as $entity => $document) {
                    echo '<div class="row" style="padding: .5em 0;">';
                    echo '<div class="col-sm-8">' . $model->attributeLabels()[$entity] . '</div>';
                    echo '<div class="col-sm-2">' . Html::a('Скачать', ['/admin/organization/download-file/', 'id' => $document->id]) . '</div>';
                    echo '<div class="col-sm-2">' .
                        '<span style="color: red; cursor: pointer" class="admin_organization_delete_document" data-id="' .
                        $document->id . '"> ' .
                        'Удалить</span></div>';
                    echo '</div><hr>';
                    $entitiesSet[] = $entity;
                }
            }
            if (count($entitiesSet)) {
                $entitiesIP = array_diff($entitiesIP, $entitiesSet);
                $entitiesUL = array_diff($entitiesUL, $entitiesSet);
                $entitiesAll = array_diff($entitiesAll, $entitiesSet);
            }


            foreach ($entitiesAll as $entity) {
                echo '<div class="row" style="padding: .5em 1em;">';
                echo $organizationForm->field($model, $entity)->fileInput(['class' => 'col-sm-12']);
                echo '</div><hr>';
            }

            $rowIP = '<div style="';
            $rowIP .= $model->legal_type == 'IP' ? '' : 'display: none;';
            $rowIP .=  '" id="ip_fields_wrapper">';
            echo $rowIP;
            foreach ($entitiesIP as $entity) {
                echo '<div class="row" style="padding: .5em 1em;">';
                echo $organizationForm->field($model, $entity)->fileInput(['class' => 'col-sm-12']);
                echo '</div><hr>';
            }
            echo '</div>';

            $rowUL = '<div style="';
            $rowUL .= !isset($model->legal_type) || $model->legal_type == 'UL' ? '' : 'display: none;' ;
            $rowUL .= '" id="ul_fields_wrapper">';
            echo $rowUL;
            foreach ($entitiesUL as $entity) {
                echo '<div class="row" style="padding: .5em 1em;" id="ul_fields_wrapper">';
                echo $organizationForm->field($model, $entity)->fileInput(['class' => 'col-sm-12']);
                echo '</div><hr>';
            }
            echo '</div>';

        ?>
</div>

<?php ActiveForm::end() ?>

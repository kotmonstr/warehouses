<?php

/* @var $this View */

use yii\web\View;

/* @var $id string|int id of organization */

$scriptVerifyCancel = <<< JS

    function verifyCancel(orgId, comment, forever) {
        $.ajax({
            url: '/admin/organization/verify-cancel',
            method: "GET",
            data: {"id": orgId, 'comment': comment, 'forever': forever},
            success: function(data) {
                console.log(data);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    var btnVerifyCancel = document.getElementById('verifyCancelBtnSend');
    if (btnVerifyCancel) {
        btnVerifyCancel.addEventListener('click', function () {
            var verifyCancelComment = document.getElementById('verifyCancelComment').value;
            var verifyCancelChkbx = document.getElementById('verifyCancelChkbx');
            var orgId = this.dataset.id;
            if (verifyCancelChkbx.checked) {
                verifyCancel(orgId, verifyCancelComment && verifyCancelComment != '' ? verifyCancelComment : 'no comments', true);
            }else{
                verifyCancel(orgId, verifyCancelComment && verifyCancelComment != '' ? verifyCancelComment : 'no comments', false);
            }
        });
    }

JS;
$this->registerJs($scriptVerifyCancel, yii\web\View::POS_END);

?>
<!-- Verify cancel modal begin-->
<div class="modal fade" id="verifyCancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 15%; left: 25%;">
    <div class="modal-content" style="width: 50%;">


        <div class="modal-c-tabs">
            <div class="options text-right">
                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">&#10005;</button>
            </div>

            <h4 style="padding:0 1em;">Причина отклонения</h4>

            <div class="modal-body mb-1" style="padding: 1em;">
                <div class="form-group">
                    <textarea id="verifyCancelComment" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="checkbox" id="verifyCancelChkbx"/>
                    <label for="verifyCancelChkbx">Запретить навсегда</label>
                </div>
            </div>

            <div class="modal-footer" style="padding: 1em;">
                <button type="button" class="btn btn-primary"
                    id="verifyCancelBtnSend"
                    style="display: block; padding: 1em auto; width: 100%;"
                    data-id="<?= $id ?>">Отправить</button>
                <button type="button" class="btn "
                    style="display: block; padding: 1em auto; color: blue; width: 100%; margin: 0;"
                    data-dismiss="modal">Отмена</button>
            </div>

        </div>
    </div>
</div>
<!-- Verify cancel modal end-->

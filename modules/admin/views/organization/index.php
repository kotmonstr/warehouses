<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\forms\organization\SearchForm */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Organization;
use yii\web\View;

$this->title = 'Компании';
$this->params['breadcrumbs'][] = $this->title;

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/admin/organization/organization?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

<div style="position: relative; left: 80%; top: 2em; width: 20%; text-align:right;">
    <?= Html::beginForm('/admin/organization/organization', 'get') ?>
    <?= Html::submitButton('Новая компания', ['class' => 'btn btn-primary']) ?>
    <?= Html::endForm() ?>
</div>

<div class="row" style="padding: 1em; text-align: center; margin-top:-2em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterPosition' => GridView::FILTER_POS_HEADER,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => [
                'style' => 'border: none; cursor: pointer;',
                'onmouseover' => 'this.style.backgroundColor="lightblue";',
                'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
            ],
            'columns' => [
                [
                    'attribute' => 'title',
                    'headerOptions' => ['style' => 'width: 40%;'],
                    'filterInputOptions' => [
                        'placeholder' => 'Название организации',
                        'class' => 'form-control'
                    ],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ]
                ],
                [
                    'attribute' => 'inn',
                    'headerOptions' => ['style' => 'width: 20%;'],
                    'filterInputOptions' => [
                        'placeholder' => 'ИНН',
                        'class' => 'form-control'
                    ],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                ],
                [
                    'attribute' => 'created_at',
                    'headerOptions' => ['style' => 'width: 20%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'format' => ['date', 'php:d-m-Y']
                ],
                [
                    'attribute' => 'verification_status_sn',
                    'headerOptions' => ['style' => 'width: 20%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'content' => function ($model, $key, $index, $column) {
                        switch ($model->verification_status_sn) {
                        case Organization::VERIFICATION_IN_PROGRESS:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        case Organization::VERIFICATION_VERIFIED:
                            return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Верифицирована</div>';
                        case Organization::VERIFICATION_FAILED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отменена</div>';
                        case Organization::VERIFICATION_RESTRICTED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отмена навсегда</div>';
                        default:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        }
                    }
                ],
            ],
        ]
    ) ?>
</div>

</div>

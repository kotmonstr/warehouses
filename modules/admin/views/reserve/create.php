<?php

/* @var View $this */
/* @var Reserve $model */
/* @var array $organizations */
/* @var app\modules\admin\models\forms\reserve\ReserveDocumentForm $documentModel */

use app\models\Advert;
use app\models\Dictionary;
use app\models\Reserve;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\MaskedInput;

$this->title = 'Новое бронирование';
$this->params['breadcrumbs'][] = ['label' => 'Брони', 'url' => ['/admin/reserve/index']];
$this->params['breadcrumbs'][] = $this->title;

$whUrl     = Url::to(['/api/warehouse/index'], true);
$advUrl    = Url::to(['/api/advrt/index'], true);
$advStatus = Advert::STATUS_PUBLISHED;

$orgInputId = Html::getInputId($model, 'organization_id');
$whInputId  = Html::getInputId($model, 'warehouse_id');
$advInputId = Html::getInputId($model, 'advert_id');

$script = <<<JS

    $(document).ready(function() {
        refreshWarehouseList();
    });
        
    $("#$orgInputId").change(function() {
        $("#$whInputId option").remove();
        $("#$advInputId option").remove();
        refreshWarehouseList();
    });
        
    $("#reserve-amount").change(function() {
        $('#reserve-cost').val((parseFloat($(this).val()) * parseFloat($("#$advInputId").data('cost'))) || 0);
    });

    $("#$whInputId").change(function() {
        refreshAdvertList();
        $("#$advInputId option").remove();
    });

    function refreshWarehouseList() {
        if ($("#$advInputId option:selected").length != 0) {
            return;
        }
        
        $.ajax({
          type: "POST",
          url: "$whUrl",
          data: {"organization_id": $("#$orgInputId").val()},
          success: function (data) {
              if (data.count == 0) {
                  $("#$whInputId").append($('<option>').text('Нет подходящих записей').prop('disabled', true).prop('selected', true));
                  $('#reserve-cost').val('');
                  return;
              }

              $.each(data.items, function(key, item){
                  $("#$whInputId").append(new Option(item.title, item.id));
              });

              refreshAdvertList();
          },
        });
    }

    function refreshAdvertList() {
       $.ajax({
          type: "POST",
          url: "$advUrl",
          data: {"warehouse_id": $("#$whInputId").val(), "status_sn": "$advStatus"},
          success: function (data) {
              if (data.count == 0) {
                  $("#$advInputId").append($('<option>').text('Нет подходящих записей').prop('disabled', true).prop('selected', true));
                  $('#reserve-cost').val('');
                  return;
              }

              $.each(data.items, function(key, item){
                  $("#$advInputId").append(new Option(item.warehouse.title + ", " + item.available_space + "м², " + item.unit_cost_per_day + "руб в день", item.id));
                  $("#$advInputId").data('cost', item.unit_cost_per_day);
              });
              
              $('#reserve-cost').val((parseFloat($('#reserve-amount').val()) * parseFloat($("#$advInputId").data('cost'))) || 0);
          }
        });
    }

JS;

$selectedOrganizationId = null;
$selectedWarehouseId = null;

if ($model->advert) {
    $selectedWarehouseId = $model->advert->warehouse_id;
    $selectedOrganizationId = $model->advert->warehouse->organization_id;
}

$this->registerJs($script);

$advert = @$model->advert ?: false;
$warehouse = @$advert->warehouse ?: false;
$organization = @$warehouse->organization ?: false;

?>
<?php $form = ActiveForm::begin(); ?>

<h1><?= Html::encode($this->title) ?></h1>

<div style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <?= Html::label('Огранизация', $orgInputId, ['class' => 'control-label']) ?>
                <?= Html::dropDownList(Html::getInputName($model, 'organization_id'), $selectedOrganizationId, $organizations, ['id' => $orgInputId, 'class' => 'form-control']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <?= Html::label('Склад', $whInputId, ['class' => 'control-label']) ?>
                <?= Html::dropDownList(Html::getInputName($model, 'warehouse_id'), $selectedWarehouseId, $advert ? [$advert->warehouse_id => $warehouse->title] : [], ['id' => $whInputId, 'class' => 'form-control']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'advert_id')->dropDownList($advert ? [$model->advert_id => $warehouse->title . ', ' . $advert->available_space . 'м², ' . $advert->unit_cost_per_day . 'руб в день'] : [], ['required' => true, 'data' => ['cost' => @$model->advert->unit_cost_per_day ?: 0]]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'amount')->textInput()->label() ?>
        </div>
        <div class="form-group col-sm-3">
            <?= $form->field($model, 'amount_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true)) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'cost')->textInput()->label() ?>
        </div>
    </div>

    <p style="margin-bottom: 0; color: gray">Интервал</p>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'rent_from')->textInput(['type' => 'date']) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'rent_to')->textInput(['type' => 'date']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'contact_fio')->textInput()->label() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'contact_phone_number')->textInput()->widget(MaskedInput::class, ['mask' => '+7 (999) 999-99-99',
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->textInput(['placeholder'=>'+7 (999) 999 9999'])?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'contact_email')->textInput()->label() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" >
           <?= $form->field($model, 'comment')->textarea(['rows' => 6])->label() ?>
        </div>
    </div>

    <div class="row" style="padding: 1em;">

        <div class="col-sm-12" >
            <h4 style='font-weight: bold;'>Документы</h4>
        </div>

        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'incomingOrder')->fileInput() ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'mx1')->fileInput() ?>
        </div>
        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'mx1Complaint')->fileInput() ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'outgoingOrder')->fileInput() ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'mx3')->fileInput() ?>
        </div>
        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'mx3Complaint')->fileInput() ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?= $form->field($documentModel, 'completionCert')->fileInput() ?>
        </div>

    </div>

    <div class="form-group" style="margin-top: 2rem">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'style' => 'width: 12em;']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>

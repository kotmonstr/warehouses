<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\forms\reserve\ReserveSearchForm */

use app\models\Dictionary;
use app\models\Organization;
use app\models\Warehouse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Брони';
$this->params['breadcrumbs'][] = $this->title;

if ($searchModel->organization_id) {
    $warehouses = ArrayHelper::map(Warehouse::find()->where(['organization_id' => $searchModel->organization_id])->all(), 'id', 'title');
} else {
    $warehouses = ArrayHelper::map(Warehouse::find()->all(), 'id', 'title');
}

$organizations = ArrayHelper::map(Organization::find()->all(), 'id', 'title');

function getFilterFromArray($array, $labelText, $model, $attribute) {
    $filterArray = ['' => $labelText] + $array;
    return Html::activeDropDownList($model, $attribute, $filterArray, ['class' => 'form-control']);
}

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/admin/reserve/view?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div style="text-align: right; margin-bottom: 15px;">
    <?= Html::a('Новое бронирование', ['/admin/reserve/create'], ['class' => 'btn btn-primary']) ?>
</div>

<div style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">
    <div class="row">
        <div class="col-md-12">
            <?php
            echo \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
                'layout'=>"{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table',
                ],
                'rowOptions' => [
                    'style' => 'border: none; cursor: pointer;',
                    'onmouseover' => 'this.style.backgroundColor="lightblue";',
                    'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
                ],
                'columns' => [
                    [
                        'attribute' => 'organization_id',
                        'headerOptions' => ['style' => 'width:20%;'],
                        'filter' => getFilterFromArray($organizations, 'Организации', $searchModel, 'organization_id'),
                        'value' => 'advert.warehouse.organization.title',
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'attribute' => 'warehouse_id',
                        'headerOptions' => ['style' => 'width:20%;'],
                        'filter' => getFilterFromArray($warehouses, 'Склады', $searchModel, 'warehouse_id'),
                        'value' => 'advert.warehouse.title',
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'attribute' => 'storage_type_sn',
                        'headerOptions' => ['style' => 'width:20%;'],
                        'filter' => getFilterFromArray(Dictionary::findDictItems('storage_system'), 'Типы систем хранения', $searchModel, 'storage_type_sn'),
                        'value' => 'advert.storage_type.title',
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'attribute' => 'rent_from',
                        'format' => ['date', 'php:d.m.Y'],
                        'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                        'headerOptions' => ['style' => 'width:10%;'],
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'attribute' => 'rent_to',
                        'format' => ['date', 'php:d.m.Y'],
                        'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                        'headerOptions' => ['style' => 'width:10%;'],
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'attribute' => 'status_sn',
                        'filter'=> getFilterFromArray(Dictionary::findDictItems('reserve_status'), 'Статус', $searchModel, 'status_sn'),
                        'headerOptions' => ['style' => 'width:10%;'],
                        'value' => function ($model, $key, $index, $column) {
                            $items = Dictionary::findDictItems('reserve_status');

                            if (!$model->status_sn) {
                                return null;
                            }

                            return array_key_exists($model->status_sn, $items) ? $items[$model->status_sn] : null;
                        },
                        'contentOptions' => ['class' => 'grid-table-row'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => '',
                        'headerOptions' => ['style' => 'width: 10%;'],
                        'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>

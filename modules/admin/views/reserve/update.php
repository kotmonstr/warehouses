<?php

/**
 * @var \app\models\Reserve $model
 * @var app\modules\admin\models\forms\reserve\ReserveDocumentForm $documentModel
 * @var app\models\Document[] $documents
 */

use app\models\Dictionary;
use app\models\Reserve;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use app\models\File;

$this->title = 'Редактировать бронь';
$this->params['breadcrumbs'][] = ['label' => 'Брони', 'url' => ['/admin/reserve/index']];
$this->params['breadcrumbs'][] = ['label' => $model->advert->warehouse->title, 'url' => ['/admin/reserve/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$advert = $model->advert;
$warehouse = $advert->warehouse;
$organization = $advert->organization;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">
    <p style="margin-bottom: 0; color: gray">Объявление:</p>
    <p><a href="<?= $advert->deleted_at || $warehouse->deleted_at || $organization->deleted_at ? '#' : Url::to(['/admin/advert/advert', 'id' => $model->advert_id]) ?>">
            <?= $model->advert->warehouse->title . ", " . $model->advert->available_space . "м², " . $model->advert->unit_cost_per_day . "руб в день" ?>
    </a></p>

    <p style="margin-bottom: 0; color: gray">Тип хранения</p>
    <p><?php echo $model->advert->storage_type->title ?></p>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'amount')->textInput()->label() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'amount_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true)) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'cost')->textInput()->label() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'rent_from')->textInput(['type' => 'date', 'value' => $model->rent_from ?? '']) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'rent_to')->textInput(['type' => 'date', 'value' => $model->rent_to ?? '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'contact_fio')->textInput()->label() ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'contact_phone_number')->textInput()->widget(MaskedInput::class, ['mask' => '+7 (999) 999-99-99',
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->textInput(['placeholder'=>'+7 (999) 999 9999'])?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'contact_email')->textInput()->label() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" >
           <?= $form->field($model, 'comment')->textarea(['rows' => 6])->label() ?>
        </div>
    </div>

    <div class="row"  style="padding: 1em;">

        <div class="col-sm-12" >
            <h4 style='font-weight: bold;'>Документы</h4>
        </div>

        <div class="col-sm-6" >
            <?php if (empty($documents['incomingOrder'])) { ?>
                <?= $form->field($documentModel, 'incomingOrder')->fileInput() ?>
            <?php } else { ?>
            <div class="row">
                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'incomingOrder') ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['incomingOrder']['id']]) ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['incomingOrder']['id']], ['style' => 'color: red;']) ?></div>
            </div>
            <?php } ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?php if (empty($documents['mx1'])) { ?>
                <?= $form->field($documentModel, 'mx1')->fileInput() ?>
            <?php } else { ?>
            <div class="row">
                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx1') ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['mx1']['id']]) ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['mx1']['id']], ['style' => 'color: red;']) ?></div>
            </div>
            <?php } ?>
        </div>
        <div class="col-sm-6" >
            <?php if (empty($documents['mx1Complaint'])) { ?>
                <?= $form->field($documentModel, 'mx1Complaint')->fileInput() ?>
            <?php } else { ?>
            <div class="row">
                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx1Complaint') ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['mx1Complaint']['id']]) ?></div>
                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['mx1Complaint']['id']], ['style' => 'color: red;']) ?></div>
            </div>
            <?php } ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?php if (empty($documents['outgoingOrder'])) { ?>
                <?= $form->field($documentModel, 'outgoingOrder')->fileInput() ?>
            <?php } else { ?>
                <div class="row">
                    <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'outgoingOrder') ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['outgoingOrder']['id']]) ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['outgoingOrder']['id']], ['style' => 'color: red;']) ?></div>
                </div>
            <?php } ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?php if (empty($documents['mx3'])) { ?>
                <?= $form->field($documentModel, 'mx3')->fileInput() ?>
            <?php } else { ?>
                <div class="row">
                    <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx3') ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['mx3']['id']]) ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['mx3']['id']], ['style' => 'color: red;']) ?></div>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-6" >
            <?php if (empty($documents['mx3Complaint'])) { ?>
                <?= $form->field($documentModel, 'mx3Complaint')->fileInput() ?>
            <?php } else { ?>
                <div class="row">
                    <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx3Complaint') ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['mx3Complaint']['id']]) ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['mx3Complaint']['id']], ['style' => 'color: red;']) ?></div>
                </div>
            <?php } ?>
        </div>

        <div class="col-sm-12" ><hr></div>

        <div class="col-sm-6" >
            <?php if (empty($documents['completionCert'])) { ?>
                <?= $form->field($documentModel, 'completionCert')->fileInput() ?>
            <?php } else { ?>
                <div class="row">
                    <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'completionCert') ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/admin/reserve/download-file/', 'id' => $documents['completionCert']['id']]) ?></div>
                    <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/admin/reserve/delete-file/', 'id' => $documents['completionCert']['id']], ['style' => 'color: red;']) ?></div>
                </div>
            <?php } ?>
        </div>


    </div>

    <div class="form-group" style="margin-top: 2rem">
        <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary', 'name' => 'reserve-update-button', 'style' => 'width: 14em;']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php if ($model->status_sn == Reserve::STATUS_ACTIVE) {

                echo Html::beginForm(['/admin/reserve/cancel', 'id' => $model->id]);
                echo Html::submitButton(
                    'Отменить бронь',
                    [
                        'class' => 'btn btn-default',
                        'style' => 'border-color: red; color: red; width: 14em; margin-top: 1em;',
                        'data' => [
                            'confirm' => 'Вы уверены, что хотите отменить бронь?'
                        ]
                    ]
                );
                echo Html::endForm();

            } else {

                echo "<p style='color: red; margin-top: 1em;'>Бронь отменена</p>";

    }?>



</div>


<?php

/* @var $this View */
/* @var $model app\modules\admin\models\forms\user\Form */

use yii\helpers\Html;
use app\models\User;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\models\Warehouse;
use app\models\Dictionary;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

$dataProviderWarehouse = new ArrayDataProvider([
    'key'=>'id',
    'allModels' => $model->warehouse,
    'pagination' => [
        'pageSize' => 5
    ]
]);

$dataProviderAdvert = new ArrayDataProvider([
    'key'=>'id',
    'allModels' => $model->advert,
    'pagination' => [
        'pageSize' => 5
    ]
]);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/admin/user/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <div class="row">
        <div class="col-sm-3">
            <h5 style="color: gray;">Тип</h5>
            <p>
                <?php
                    switch ($model->role) {
                    case User::ROLE_HOLDER:
                        echo 'Арендодатель';
                        break;
                    case User::ROLE_RENTER:
                        echo 'Арендатор';
                        break;
                    case User::ROLE_ADMIN:
                        echo 'Администратор';
                        break;
                    case User::ROLE_DSL:
                        echo 'ДСЛ';
                        break;
                    case User::ROLE_SB:
                        echo 'СБ';
                        break;
                    }
                ?>
            </p>
        </div>
        <div class="col-sm-3">
            <h5 style="color: gray;">Дата добавления</h5>
            <p>
                <?= $model->created_at ? date('d.m.Y', $model->created_at) : 'не задано' ?>
            </p>
        </div>
        <div class="col-sm-3">
            <h5 style="color: gray;">Компания</h5>
            <p>
                <?= $model->organization ?
                    Html::a($model->organization->title, ['/admin/organization/organization/', 'id' => $model->organization_id]):
                    'не задано' ?>
            </p>
        </div>
        <div class="col-sm-3" style="text-align: right; margin-top: 1em;">

            <?php if (($model->role != User::ROLE_ADMIN && (Yii::$app->user->identity->role == User::ROLE_ADMIN))
                        || (in_array($model->role, [User::ROLE_HOLDER, User::ROLE_RENTER])
                                && in_array(Yii::$app->user->identity->role, [User::ROLE_DSL, User::ROLE_SB]))
            ) { ?>

                <?= Html::beginForm('/admin/user/user?id=' . $model->id) ?>
                <?= Html::submitButton('Редактировать пользователя', ['class' => 'btn btn-primary']) ?>
                <?= Html::endForm() ?>

            <?php } ?>

        </div>
    </div>
</div>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <h4>Склады</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderWarehouse,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'style' => 'border: none; cursor: pointer;',
                    'onmouseover' => 'this.style.backgroundColor="lightblue";',
                    'onmouseout' => 'this.style.backgroundColor="#F3F3F3";',
                    'data-link' => Url::to("/admin/warehouse/general?id={$model->id}"),
                    'onclick' => "var a = document.createElement('a');
                    var link = this.dataset.link;
                    a.setAttribute('href', link);
                    a.click();",
                ];
            },
            'columns' => [
                [
                    'attribute' => 'title',
                    'header' => 'Склад',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'created_at',
                    'header' => 'Дата регистрации',
                    'headerOptions' => ['style' => 'width: 7em;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'format' => ['date', 'php:d.m.Y']
                ],
                [
                    'attribute' => 'address_ym_geocoder',
                    'header' => 'Адрес',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'header' => 'Тип хранения',
                    'value' => function ($model, $key, $index, $column) {
                        $titles = [];
                        $system_names = ArrayHelper::getColumn($model->wh_storage_system, 'storage_system_sn');
                        foreach ($system_names as $name) {
                            $titles[] = Dictionary::findDictItems('storage_system')[$name];
                        }
                        return implode(", ", $titles);
                    }
                ],
                [
                    'attribute' => 'dsl_verification_status_sn',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'header' => 'Статус ДСЛ',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        switch ($model->dsl_verification_status_sn) {
                        case Warehouse::VERIFICATION_IN_PROGRESS:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        case Warehouse::VERIFICATION_FAILED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отменен</div>';
                        case Warehouse::VERIFICATION_RESTRICTED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отмена навсегда</div>';
                        case Warehouse::VERIFICATION_VERIFIED:
                            return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Аттестован</div>';
                        default:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        }
                    }
                ],
                [
                    'attribute' => 'sb_verification_status_sn',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'header' => 'Статус СБ',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        switch ($model->sb_verification_status_sn) {
                        case Warehouse::VERIFICATION_IN_PROGRESS:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        case Warehouse::VERIFICATION_VERIFIED:
                            return '<div class="alert alert-success" style="margin: 0; padding: .2em .5em;">Верифицирован</div>';
                        case Warehouse::VERIFICATION_FAILED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отменен</div>';
                        case Warehouse::VERIFICATION_RESTRICTED:
                            return '<div class="alert alert-warning" style="margin: 0; padding: .2em .5em;">Отмена навсегда</div>';
                        default:
                            return '<div class="alert alert-danger" style="margin: 0; padding: .2em .5em;">Ожидает</div>';
                        }
                    }
                ],
            ],
        ]
    ) ?>
    <?php Pjax::end(); ?>
</div>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <h4>Объявления</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderAdvert,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'style' => 'border: none; cursor: pointer;',
                    'onmouseover' => 'this.style.backgroundColor="lightblue";',
                    'onmouseout' => 'this.style.backgroundColor="#F3F3F3";',
                    'data-link' => Url::to("/admin/advert/advert?id={$model->id}"),
                    'onclick' => "var a = document.createElement('a');
                    var link = this.dataset.link;
                    a.setAttribute('href', link);
                    a.click();",
                ];
            },
            'columns' => [
                [
                    'header' => 'Склад',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'value' => function ($model, $key, $index, $column) {
                        return $model->warehouse->title;
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Город',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'value' => function ($model, $key, $index, $column) {
                        return $model->warehouse->address_city_ym_geocoder;
                    }
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'header' => 'Класс',
                    'value' => function ($model, $key, $index, $column) {
                        $items = Dictionary::findDictItems('classification');

                        if (!$model->warehouse->classification_sn) {
                            return null;
                        }

                        return array_key_exists($model->warehouse->classification_sn, $items) ? $items[$model->warehouse->classification_sn] : null;
                    }
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'header' => 'Тип хранения',
                    'value' => function ($model, $key, $index, $column) {
                        $items = Dictionary::findDictItems('storage_system');

                        if (!$model->storage_type_sn) {
                            return null;
                        }

                        return array_key_exists($model->storage_type_sn, $items) ? $items[$model->storage_type_sn] : null;
                    }
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'header' => 'Количество',
                    'value' => function ($model, $key, $index, $column) {
                        $items = Dictionary::findDictItems('unit');

                        if (!$model->available_space_type_sn) {
                            return null;
                        }

                        return array_key_exists($model->available_space_type_sn, $items) ? $items[$model->available_space_type_sn] : null;
                    }
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'header' => 'Даты',
                    'value' => function ($model, $key, $index, $column) {
                        if (!$model->available_from || !$model->available_to) {
                            return null;
                        }

                        return date('d.m.Y', strtotime($model->available_from)) . ' - ' . date('d.m.Y', strtotime($model->available_to));
                    }
                ],
                [
                    'attribute' => 'unit_cost_per_day',
                    'header' => 'Цена в день',
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'value' => function ($model, $key, $index, $column) {
                        return $model->unit_cost_per_day . '₽';
                    }
                ],
            ],
        ]
    ) ?>
    <?php Pjax::end(); ?>
</div>

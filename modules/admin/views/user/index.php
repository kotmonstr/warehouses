<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\forms\user\SearchForm */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/admin/user/info?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

<div style="position: relative; left: 80%; top: 2em;">
    <?= Html::beginForm('/admin/user/user', 'get') ?>
    <?= Html::submitButton('Новый пользователь', ['class' => 'btn btn-primary']) ?>
    <?= Html::endForm() ?>
</div>

<div class="row" style="padding: 1em; text-align: center; margin-top:-2em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterPosition' => GridView::FILTER_POS_HEADER,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => [
                'style' => 'border: none; cursor: pointer;',
                'onmouseover' => 'this.style.backgroundColor="lightblue";',
                'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
            ],
            'columns' => [
                [
                    'attribute' => 'first_name',
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'filterInputOptions' => [
                        'placeholder' => 'Имя пользователя',
                        'class' => 'form-control'
                    ],
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'header' => 'Компания',
                    'content' => function ($model, $key, $index, $column) {
                        if ($model->organization) {
                            return $model->organization->title;
                        } else {
                            return '-';
                        }
                    }
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'header' => 'Склад',
                    'content' => function ($model, $key, $index, $column) {
                        if ($model->warehouse) {
                            return $model->warehouse[0]->title;
                        } else {
                            return '-';
                        }
                    }
                ],
                [
                    'attribute' => 'organization_position',
                    'header' => 'Должность',
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'content' => function ($model, $key, $index, $column) {
                        if ($model->organization_position) {
                            return $model->organization_position;
                        } else {
                            return '-';
                        }
                    }
                ],
                [
                    'attribute' => 'email',
                    'header' => 'Email',
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                ],
                [
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'header' => 'Город',
                    'content' => function ($model, $key, $index, $column) {
                        if ($model->organization) {
                            $parts = explode(",", $model->organization->address_ym_geocoder);

                            return array_key_exists(1, $parts) ? $parts[1]: '-';
                        } else {
                            return '-';
                        }
                    }
                ],
            ],
        ]
    ) ?>
</div>

</div>

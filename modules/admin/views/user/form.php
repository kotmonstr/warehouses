<?php

/* @var $this View */
/* @var $model app\modules\admin\models\forms\user\Form */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;
use yii\web\View;
use yii\widgets\MaskedInput;

$this->title = $model->id ? 'Редактировать пользователя' : 'Новый пользователь';

$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/admin/user/index']];

if ($model->id) {
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['/admin/user/info', 'id' => $model->id]];
}

$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
    if (isset($model->id)) { ?>
        <div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
            <div class="row">
                <div class="col-sm-3">
                    <h5 style="color: gray;">Тип</h5>
                    <p>
                        <?php
                            switch ($model->role) {
                            case User::ROLE_HOLDER:
                                echo 'Арендодатель';
                                break;
                            case User::ROLE_RENTER:
                                echo 'Арендатор';
                                break;
                            case User::ROLE_ADMIN:
                                echo 'Администратор';
                                break;
                            case User::ROLE_DSL:
                                echo 'ДСЛ';
                                break;
                            case User::ROLE_SB:
                                echo 'СБ';
                                break;
                            }
                        ?>
                    </p>
                </div>
                <div class="col-sm-3">
                    <h5 style="color: gray;">Дата добавления</h5>
                    <p>
                        <?= $model->created_at ? date('d.m.Y', $model->created_at) : 'не задано' ?>
                    </p>
                </div>
                <div class="col-sm-3">
                    <h5 style="color: gray;">Компания</h5>
                    <p>
                        <?= $model->organization_id ?
                            Html::a($model->organization->title, ['/admin/organization/organization/', 'id' => $model->organization_id]):
                            'не задано' ?>
                    </p>
                </div>
                <div class="col-sm-3" style="text-align: right; margin-top: 1em;">

                    <?php if (($model->role != User::ROLE_ADMIN && (Yii::$app->user->identity->role == User::ROLE_ADMIN))
                        || (in_array($model->role, [User::ROLE_HOLDER, User::ROLE_RENTER])
                                && in_array(Yii::$app->user->identity->role, [User::ROLE_DSL, User::ROLE_SB]))
                    ) { ?>

                        <?= Html::beginForm('/admin/user/delete?id=' . $model->id) ?>
                        <?= Html::submitButton('Удалить пользователя',
                            [
                                'class' => 'btn btn-danger',
                                "data" => [
                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                ],
                            ]
                        ) ?>
                        <?= Html::endForm() ?>

                    <?php } ?>

                </div>
            </div>


        </div>
        <div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
            <h4>Данные пользователя</h4>
<?php  $userForm = ActiveForm::begin(); } else {?>


<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">
    <h4>Новый пользователь</h4>
    <?php $userForm = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'organization_id')->dropDownList($model->organizations())->label('Компания') ?>
        </div>
    </div>

<?php } ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'name')->textInput(['value' => $model->name ? $model->name : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'organization_position')->textInput(['value' => $model->organization_position ? $model->organization_position : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'phone_number')
                ->widget(MaskedInput::class,
                [
                    'mask' => '+7 (999) 999-99-99',
                    'options' => ['value' => $model->phone_number ? $model->phone_number : '']
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'email')->textInput(['value' => $model->email ? $model->email : '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $userForm->field($model, 'password')->passwordInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <?php if (($model->role != User::ROLE_ADMIN && (Yii::$app->user->identity->role == User::ROLE_ADMIN))
                        || (in_array($model->role, [User::ROLE_HOLDER, User::ROLE_RENTER])
                                && in_array(Yii::$app->user->identity->role, [User::ROLE_DSL, User::ROLE_SB]))
            ) { ?>
                <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary']) ?>
            <?php } ?>

        </div>
    </div>


</div>

<?php ActiveForm::end() ?>

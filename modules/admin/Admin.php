<?php

namespace app\modules\admin;

use app\models\User;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Admin module definition class
 */
class Admin extends Module
{
    /** {@inheritdoc} */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => (!Yii::$app->user->isGuest && in_array(Yii::$app->user->identity->role, [
                            User::ROLE_ADMIN, User::ROLE_DSL, User::ROLE_SB
                        ])),
                    ],
                ],
            ],
        ];
    }
}

<?php

/* @var $this View */
/* @var $dataProviderRequests yii\data\ActiveDataProvider */
/* @var $dataProviderTeam yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Команда';
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; padding: 1em; border: 1px solid #F3F3F3; border-radius: 5px;">
<h4>Запросы на доступ</h4>
<div class="row" style="padding: 1em; text-align: center; margin-top:-1em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderRequests,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'columns' => [
                [
                    'header' => 'Имя',
                    'headerOptions' => ['style' => 'width:30%;'],
                    'contentOptions' => [ 'style' => 'text-align: left; color: blue;' ],
                    'content' => function ($model, $key, $index, $column) {
                        return $model->user->name;
                    }
                ],
                [
                    'header' => 'Должность',
                    'headerOptions' => ['style' => 'width:15%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        return $model->user->organization_position ?
                                $model->user->organization_position :
                                '<span style="color: gray;">Не указана</span>';
                    }
                ],
                [
                    'header' => 'Email',
                    'headerOptions' => ['style' => 'width:25%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        return $model->user->email;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['style' => 'width:30%;'],
                    'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return '<a href="' . $url . '" style="color: blue; text-decoration: none;" class="team-add-user">Добавить</a>';
                        },
                        'delete' => function ($url, $model, $key) {
                            return '<a href="' . $url . '" style="color: red; text-decoration: none;" class="team-delete-user">Отклонить</a>';
                        },
                    ]
                ],
            ],
        ]
    ) ?>
</div>
</div>

<div class="container" style="background-color: #F3F3F3; padding: 1em; border: 1px solid #F3F3F3; border-radius: 5px; margin-top: 1em;">
<h4>Члены команды</h4>
<div class="row" style="padding: 1em; text-align: center; margin-top:-1em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderTeam,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'columns' => [
                [
                    'attribute' => 'name',
                    'headerOptions' => ['style' => 'width:30%;'],
                    'contentOptions' => [ 'style' => 'text-align: left; color: blue;' ],
                ],
                [
                    'attribute' => 'organization_position',
                    'headerOptions' => ['style' => 'width:15%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        return $model->organization_position ?
                                $model->organization_position :
                                '<span style="color: gray;">Не указана</span>';
                    }
                ],
                [
                    'attribute' => 'email',
                    'headerOptions' => ['style' => 'width:25%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                ],
                [
                    'header' => 'Склад',
                    'headerOptions' => ['style' => 'width:30%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;' ],
                    'content' => function ($model, $key, $index, $column) {
                        $warehouses = ArrayHelper::getColumn($model->warehouse, 'title');
                        $warehouseReturn = '<ul>';
                        if (count($warehouses)) {
                            foreach ($warehouses as $warehouse) {
                                $warehouseReturn .= '<li>' . $warehouse . '</li>';
                            }
                        } else {
                            $warehouseReturn .= 'Не задано';
                        }
                        $warehouseReturn .= '</ul>';
                        return $warehouseReturn;
                    }
                ],
            ],
        ]
    ) ?>
</div>
</div>

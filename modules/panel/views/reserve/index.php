<?php

use app\models\Dictionary;
use app\models\User;
use app\models\Warehouse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Брони';
$this->params['breadcrumbs'][] = $this->title;

$warehousesFilter = ArrayHelper::map(
    Warehouse::find()->where(['organization_id' => Yii::$app->user->identity->organization_id])->all(),
    'id',
    'title'
);

function getFilterFromArray($array, $labelText, $model, $attribute) {
    $filterArray = ['' => $labelText] + $array;
    return Html::activeDropDownList($model, $attribute, $filterArray, ['class' => 'form-control']);
}

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/panel/reserve/view?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="container"  style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">
    <div class="row">
            <div class="col-md-12">
                <?php
                echo \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
                    'layout'=>"{items}\n{pager}",
                    'tableOptions' => [
                        'class' => 'table',
                    ],
                    'rowOptions' => [
                        'style' => 'border: none; cursor: pointer;',
                        'onmouseover' => 'this.style.backgroundColor="lightblue";',
                        'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'warehouse_id',
                            'filter' => getFilterFromArray(
                                $warehousesFilter,
                                'Склады',
                                $searchModel,
                                'warehouse_id'
                            ),
                            'value' => 'advert.warehouse.title',
                            'options' => [
                                'class' => 'form-control',
                            ],
                            'contentOptions' => ['class' => 'grid-table-row'],
                        ],
                        [
                            'attribute' => 'storage_type_sn',
                            'filter' => getFilterFromArray(
                                Dictionary::findDictItems('storage_system'),
                                'Типы систем хранения',
                                $searchModel,
                                'storage_type_sn'
                            ),
                            'value' => 'advert.storage_type.title',
                            'options' => [
                                'class' => 'form-control',
                            ],
                            'contentOptions' => ['class' => 'grid-table-row'],
                        ],
                        [
                            'attribute' => 'rent_from',
                            'format' => ['date', 'php:d.m.Y'],
                            'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                            'headerOptions' => ['style' => 'width:10%;'],
                            'contentOptions' => ['class' => 'grid-table-row'],
                        ],
                        [
                            'attribute' => 'rent_to',
                            'format' => ['date', 'php:d.m.Y'],
                            'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                            'headerOptions' => ['style' => 'width:10%;'],
                            'contentOptions' => ['class' => 'grid-table-row'],
                        ],
                        [
                            'attribute' => 'status_sn',
                            'filter'=> getFilterFromArray(
                                Dictionary::findDictItems('reserve_status'),
                                'Статус',
                                $searchModel,
                                'status_sn'
                            ),
                            'headerOptions' => ['style' => 'width:10%;'],
                            'value' => function ($model, $key, $index, $column) {
                                $items = Dictionary::findDictItems('reserve_status');

                                if (!$model->status_sn) {
                                    return null;
                                }

                                return array_key_exists($model->status_sn, $items) ? $items[$model->status_sn] : null;
                            },
                            'contentOptions' => ['class' => 'grid-table-row'],
                        ]
                    ],
                ]);
                ?>
            </div>
    </div>
</div>

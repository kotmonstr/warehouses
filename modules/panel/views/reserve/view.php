<?php

/**
 * @var \app\models\Reserve $model
 * @var app\modules\panel\models\forms\reserve\ReserveDocumentForm $documentModel
 * @var app\models\Document[] $documents
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\models\File;
use app\models\Reserve;
use app\models\Dictionary;

$this->title = 'Бронь для склада "' . $model->advert->warehouse->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Брони', 'url' => ['/panel/reserve/index']];
$this->params['breadcrumbs'][] = $this->title;

$advert = $model->advert;
$warehouse = $advert->warehouse;
$organization = $advert->organization;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container"  style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">
    <div class="row">
        <div class="col-md-12">
            <div style="
            background-color: #F3F3F3;
            border-radius: 10px;
            ">
                <p class="lead">Детали</p>

                <p style="margin-bottom: 0; color: gray">Объявление</p>
                <p><a href="<?= $advert->deleted_at || $warehouse->deleted_at || $organization->deleted_at ? '#' : Url::to(['/panel/advert/update', 'id' => $model->advert_id]) ?>">
                        <?= $model->advert->warehouse->title . ", " . $model->advert->available_space . "м², " . $model->advert->unit_cost_per_day . "руб в день" ?>
                </a></p>

                <p style="margin-bottom: 0; color: gray">Тип хранения</p>
                <p><?php echo $model->advert->storage_type->title ?></p>

                <div class="row">
                    <div class="col-sm-2">
                        <p style="margin-bottom: 0; color: gray">Количество</p>
                        <p>
                            <?php
                                echo $model->amount . " ";
                                echo Dictionary::findDictItems('unit', 'square', true)[$model->amount_type_sn];
                            ?>
                        </p>
                    </div>
                    <div class="col-sm-2">
                        <p style="margin-bottom: 0; color: gray">Стоимость</p>
                        <p><?php echo $model->cost?> ₽</p>
                    </div>
                </div>
                <p style="margin-bottom: 0; color: gray">Интервал</p>
                <div class="row">
                    <div class="col-sm-2">
                        <p>С <?php echo Yii::$app->formatter->asDate($model->rent_from, 'dd.MM.yyyy')?></p>
                    </div>
                    <div class="col-sm-2">
                        <p>По <?php echo Yii::$app->formatter->asDate($model->rent_to, 'dd.MM.yyyy')?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <p style="margin-bottom: 0; color: gray">ФИО контактного лица</p>
                        <p><?php echo $model->contact_fio?> </p>
                    </div>
                    <div class="col-sm-2">
                        <p style="margin-bottom: 0; color: gray">Телефон</p>
                        <p><?php echo $model->contact_phone_number?> </p>
                    </div>
                    <div class="col-sm-2">
                        <p style="margin-bottom: 0; color: gray">Почта</p>
                        <p><?php echo $model->contact_email?> </p>
                    </div>
                </div>
                <p style="margin-bottom: 0; color: gray">Комментарий</p>
                <div class="row">
                    <div class="col-sm-8" >
                        <pre style="white-space: pre-wrap"><?php echo $model->comment?> </pre>
                    </div>
                </div>

                <div class="row"  style="border-bottom: 1px solid lightgray; padding: 1em;">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                    <div class="col-sm-12" >
                        <h4 style='font-weight: bold;'>Документы</h4>
                    </div>

                    <div class="col-sm-6" >
                        <?php if (empty($documents['incomingOrder'])) { ?>
                            <?= $form->field($documentModel, 'incomingOrder')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'incomingOrder') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['incomingOrder']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['incomingOrder']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12" ><hr></div>

                    <div class="col-sm-6" >
                        <?php if (empty($documents['mx1'])) { ?>
                            <?= $form->field($documentModel, 'mx1')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx1') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['mx1']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['mx1']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6" >
                        <?php if (empty($documents['mx1Complaint'])) { ?>
                            <?= $form->field($documentModel, 'mx1Complaint')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx1Complaint') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['mx1Complaint']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['mx1Complaint']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12" ><hr></div>

                    <div class="col-sm-6" >
                        <?php if (empty($documents['outgoingOrder'])) { ?>
                            <?= $form->field($documentModel, 'outgoingOrder')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'outgoingOrder') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['outgoingOrder']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['outgoingOrder']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12" ><hr></div>

                    <div class="col-sm-6" >
                        <?php if (empty($documents['mx3'])) { ?>
                            <?= $form->field($documentModel, 'mx3')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx3') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['mx3']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['mx3']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6" >
                        <?php if (empty($documents['mx3Complaint'])) { ?>
                            <?= $form->field($documentModel, 'mx3Complaint')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'mx3Complaint') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['mx3Complaint']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['mx3Complaint']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12" ><hr></div>

                    <div class="col-sm-6" >
                        <?php if (empty($documents['completionCert'])) { ?>
                            <?= $form->field($documentModel, 'completionCert')->fileInput() ?>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12"><?= Html::activeLabel($documentModel, 'completionCert') ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Скачать', ['/panel/reserve/download-file/', 'id' => $documents['completionCert']['id']]) ?></div>
                                <div class="col-sm-2 text-left"><?= Html::a('Удалить', ['/panel/reserve/delete-file/', 'id' => $documents['completionCert']['id'], 'reserveId' => $model->id], ['style' => 'color: red;']) ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12" style="margin-top: 20px;">
                        <?= Html::submitButton('Загрузить документы', ['class' => 'btn btn-primary', 'style' => 'width: 14em;']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

                </div>

                <div class="form-group" style="margin-top: 2rem">
                    <div><b>Отмена бронирования</b></div>
                    <div>Чтобы отменить бронирование позвоните по телефону: + 7 (900) 044 44 44<br>или напишите на e-mail: support@example.com</div><br>
                    <div style="color: red">За отмену бронирования будет штраф</div>
                </div>
            </div>
        </div>
    </div>
</div>



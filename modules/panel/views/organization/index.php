<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\panel\models\forms\OrgSearchForm;

$this->title = 'Компания';
$this->params['breadcrumbs'][] = $this->title;

$orgSearchModel = new OrgSearchForm();
?>

<h1><?= Html::encode($this->title) ?></h1>

<div style="width: 60%; background-color: #F3F3F3; padding: 2rem; border-radius: 10px">
    <p class="lead">Найти или создать компанию</p>
    <?php $orgSearchForm = ActiveForm::begin([
        'action' => [Yii::$app->homeUrl . 'panel/organization/search'],
    ]); ?>

        <?= $orgSearchForm->field($orgSearchModel, 'inn')->textInput(['placeholder' => 'ИНН'])->label(false) ?>

        <div class="form-group" style="margin-top: 2rem">
            <?= Html::submitButton('Проверить ИНН', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>

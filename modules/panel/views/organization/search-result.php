<?php
/* @var $existingOrg app\models\Organization */

use yii\helpers\Html;

?>

<div>
  <div style="
    background-color: #F3F3F3;
    border-radius: 10px;
    padding: 1rem;
    margin-bottom: 2rem;
    padding-top: 3rem;
    display: block;
    overflow: auto
  ">
    <div class="col-md-8 lead">Найдена компания с этим ИНН</div>
    <div class="col-md-4">
      <a class="btn" style="margin-right: 3rem" href="/panel/organization">Ввести другой ИНН</a>
      <?= Html::a(
    'Присоединиться',
    ['/panel/organization/send-membership-request'],
    [
          'class' => 'btn btn-primary',
          'data-method' => 'POST',
          'data-params' => ['organizationId' => $existingOrg->id],
        ]
) ?>
    </div>
  </div>

  <div style="
    background-color: #F3F3F3;
    border-radius: 10px;
    padding: 2rem;
    display: block;
    overflow: auto
  ">
    <p class="lead">Реквизиты</p>

    <p style="margin-bottom: 0; color: gray">Название компании</p>
    <p><?php echo $existingOrg->title ?></p>

    <div class="row">
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ИНН</p>
        <p><?php echo $existingOrg->inn ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ОГРН</p>
        <p><?php echo $existingOrg->ogrn ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">КПП</p>
        <p><?php echo $existingOrg->kpp ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ОКПО</p>
        <p><?php echo $existingOrg->okpo ?></p>
      </div>
    </div>

    <p style="margin-bottom: 0; color: gray">Основной вид деятельности</p>
    <p><?php echo $existingOrg->main_activity ?></p>

    <p style="margin-bottom: 0; color: gray">Юредический адрес</p>
    <p><?php echo $existingOrg->address_ym_geocoder ?></p>

    <p style="margin-bottom: 0; color: gray">Налоговый режим</p>
    <p><?php echo $existingOrg->taxation_type ?></p>
  </div>
</div>

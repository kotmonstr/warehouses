<?php
/* @var $model app\models\Organization */
/* @var $documents app\models\Document[] */

use yii\helpers\Html;
use yii\bootstrap\Alert;
use app\models\Organization;

$this->title = 'Компания';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php if ($model->verification_status_sn == Organization::VERIFICATION_RESTRICTED) {
    echo Alert::widget([
        'options' => [
            'class' => 'alert-danger',
        ],
        'body' => "<h4>Заявка навсегда отклонена администратором</h4>Причина: {$model->verification_comment}",
    ]);
}
?>

<h1><?= Html::encode($this->title) ?></h1>

<div>
  <div style="
    background-color: #F3F3F3;
    border-radius: 10px;
    padding: 2rem;
    display: block;
    overflow: auto
  ">
    <p class="lead">Реквизиты</p>

    <p style="margin-bottom: 0; color: gray">Название компании</p>
    <p><?php echo $model->title ?></p>

    <div class="row">
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ИНН</p>
        <p><?php echo $model->inn ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ОГРН</p>
        <p><?php echo $model->ogrn ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">КПП</p>
        <p><?php echo $model->kpp ?></p>
      </div>
      <div class="col-sm-2">
        <p style="margin-bottom: 0; color: gray">ОКПО</p>
        <p><?php echo $model->okpo ?></p>
      </div>
    </div>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['partner_type'] ?></p>
    <p><?php echo Organization::partnerTypeList()[$model->partner_type] ?></p>

    <p style="margin-bottom: 0; color: gray">Основной вид деятельности</p>
    <p><?php echo $model->main_activity ?></p>

    <p style="margin-bottom: 0; color: gray">Юридический адрес</p>
    <p><?php echo $model->address_ym_geocoder ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['post_address'] ?></p>
    <p><?php echo $model->post_address ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['fact_address'] ?></p>
    <p><?php echo $model->fact_address ?></p>

    <p style="margin-bottom: 0; color: gray">Налоговый режим</p>
    <p><?php echo $model->taxation_type ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['shareholders'] ?></p>
    <p><?php echo $model->shareholders ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['start_capital'] ?></p>
    <p><?php echo $model->start_capital ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['registration_date'] ?></p>
    <p><?php echo $model->registration_date ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['edm_type'] ?></p>
    <p><?php echo $model->edm_type ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['employee_count'] ?></p>
    <p><?php echo $model->employee_count ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['bank_details'] ?></p>
    <p><?php echo $model->bank_details ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['authorized_head_contact'] ?></p>
    <p><?php echo $model->authorized_head_contact ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['authorized_sign_contact'] ?></p>
    <p><?php echo $model->authorized_sign_contact ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['authorized_security_contact'] ?></p>
    <p><?php echo $model->authorized_security_contact ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['contact_phone'] ?></p>
    <p><?php echo $model->contact_phone ?></p>

    <p style="margin-bottom: 0; color: gray"><?= $model->attributeLabels()['contact_email'] ?></p>
    <p><?php echo $model->contact_email ?></p>
  </div>

  <div style="
    background-color: #F3F3F3;
    border-radius: 10px;
    padding: 2rem;
    margin-top: 2rem;
    display: block;
    overflow: auto
  ">
    <p class="lead">Документы</p>

    <?php if ($model->legal_type == 'UL'): ?>
      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 1rem 0">
        <div class="col-md-10">
          <p>Копия устава со штампом налогового органа (актуальная редакция, с учетом внесенных изменений).</p>
        </div>
        <div class="col-md-2">
          <?php if (isset($documents['taxStampCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['taxStampCopy']->id]);
          } else {
                echo '<span style="color: red;">отсутствует</span>';
          } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРЮЛ).</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['taxRegistrationCopyUL'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['taxRegistrationCopyUL']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия решения и копия приказа о назначении единоличного исполнительного органа (либо о передаче полномочий единоличного исполнительного органа управляющей компании).</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['naznachenieIspolnitelyaCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['naznachenieIspolnitelyaCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия доверенности на право подписи договоров и первичных документов: актов, накладных, счетов, счетов-фактур, УПД от имени контрагента (в случае подписания таких документов лицами, не имеющими права действовать от имени юридического лица без доверенности).</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['doverennostNaPodpisanieCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['doverennostNaPodpisanieCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия образца подписи руководителя, заверенная нотариально или банком (заверенная копия банковской карточки) либо копии страниц 2, 3 паспорта руководителя.</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['headSignatureCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['headSignatureCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия бухгалтерской отчетности на последнюю отчетную дату перед заключением договора (форма 1,2,3,4, пояснения) с квитанций об электронной сдаче отчетности.</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['financialReportCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['financialReportCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($model->legal_type == 'IP'): ?>
      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 1rem 0">
        <div class="col-md-10">
          <p>Копии страниц 2, 3 паспорта индивидуального предпринимателя.</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['passportCopyIP'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['passportCopyIP']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>

      <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
        <div class="col-md-10">
          <p>Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРИП).</p>
        </div>
        <div class="col-md-2">
            <?php if (isset($documents['taxRegistrationCopyIP'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['taxRegistrationCopyIP']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
        </div>
      </div>
    <?php endif; ?>

    <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
      <div class="col-md-10">
        <p>Список арендаторов, имеющих право пользоваться складом (если их несколько)</p>
      </div>
      <div class="col-md-2">
            <?php if (isset($documents['arendatorsList'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['arendatorsList']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
      </div>
    </div>

    <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
      <div class="col-md-10">
        <p>Справка налогового органа об отсутствии задолженностей по налогам и сборам</p>
      </div>
      <div class="col-md-2">
            <?php if (isset($documents['noDebtsReference'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['noDebtsReference']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
      </div>
    </div>

    <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
      <div class="col-md-10">
        <p>Копия договора аренды или свидетельства о собственности на помещение (либо выписка из ЕГРН) по месту регистрации и фактическому месту нахождения</p>
      </div>
      <div class="col-md-2">
            <?php if (isset($documents['rentContractCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['rentContractCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
      </div>
    </div>

    <div class="row"  style="border-bottom: 1px solid lightgray; padding: 2rem 0 1rem 0">
      <div class="col-md-10">
        <p>Кадастровый паспорт складского помещения, сдаваемого в аренду</p>
      </div>
      <div class="col-md-2">
            <?php if (isset($documents['warehouseCadastralPassport'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['warehouseCadastralPassport']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
      </div>
    </div>

    <div class="row"  style="padding: 2rem 0 0 0">
      <div class="col-md-10">
        <p>Копия свидетельства о праве собственности на складское помещение, сдаваемое в аренду</p>
      </div>
      <div class="col-md-2">
            <?php if (isset($documents['propertyRightsCopy'])) {
                echo Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $documents['propertyRightsCopy']->id]);
            } else {
                echo '<span style="color: red;">отсутствует</span>';
            } ?>
      </div>
    </div>
  </div>
</div>

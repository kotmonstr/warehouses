<?php
/* @var $model app\modules\panel\models\forms\CreateOrgForm */
/* @var $documents app\models\Document[] */

use app\models\Organization;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

$script = <<< JS

    $(document).ready( function() {
        $('#checkbox-id').click(function() {
            if ($(this).is(':checked')) {
                $('#submit-btn').prop("disabled", false);
            } else {
                $('#submit-btn').attr('disabled', true);
            }
        });

        $('#submit-btn').on('click', function(){
            $('input[type=file]').each(function(){
                if (!this.value) {
                    this.parentNode.previousSibling.style.backgroundColor = '#ff7570';
                } else {
                    this.parentNode.previousSibling.style.backgroundColor = 'white';
                }
            });
        });

        $(document).on('change', 'input[type=file]', function() {
            const input = $(this);
            const val = input.val();
            const fileName = val.slice(12);
            input.trigger('fileselect', [fileName]);
        });

        $(':file').on('fileselect', function(event, fileName) {
            const label = $(this).closest('div.form-group').prev('label');
            label.html(fileName);
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

<?php if ($model->id && Organization::findModel($model->id)->verification_status_sn == Organization::VERIFICATION_FAILED) {
?>
    <p class="lead">Отредактируйте компанию</p>
<?php } else { ?>

    <div style="background-color: #F3F3F3; border-radius: 10px; padding: 1rem; margin-bottom: 2rem; padding-top: 3rem; display: block; overflow: auto">
        <div class="col-md-8 lead">Компания по указанному ИНН не найдена</div>
        <div class="col-md-4">
            <a class="btn pull-right" href="/panel/organization">Ввести другой ИНН</a>
        </div>
    </div>

    <p class="lead">Создайте компанию</p>
<?php } ?>

<?php $createOrgForm = ActiveForm::begin([
    'action' => [Yii::$app->homeUrl . 'panel/organization/create'],
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
    <div style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem; display: block; overflow: auto">
        <p class="lead">Реквизиты</p>
        <div style="display: none;">
            <?= $createOrgForm->field($model, 'legal_type')->label(false) ?>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $createOrgForm->field($model, 'inn')->textInput(['readonly' => true])->label('ИНН') ?>
            </div>
            <div class="col-sm-9">
                <?= $createOrgForm->field($model, 'title')->label('Название компании') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <?= $createOrgForm->field($model, 'ogrn')->label('ОГРН') ?>
            </div>
            <div class="col-sm-4">
                <?= $createOrgForm->field($model, 'kpp')->label('КПП') ?>
            </div>
            <div class="col-sm-4">
                <?= $createOrgForm->field($model, 'okpo')->label('ОКПО') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'partner_type')->dropDownList(Organization::partnerTypeList()) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'main_activity')->label('Основной вид деятельности') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'address_ym_geocoder')->label('Юридический адрес') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'post_address') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'fact_address') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'taxation_type')->label('Налоговый режим') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'shareholders')->textarea() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'start_capital')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'registration_date')->textInput(['type' => 'date', 'value' => $model->registration_date ? $model->registration_date : '']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'edm_type')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'employee_count')->textInput(['type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'bank_details')->textarea(['style' => ['resize' => 'vertical']])->hint('Наименование и адрес банка, номер расчетного счета участника запроса предложений в банке, телефоны банка, прочие банковские реквизиты') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'authorized_head_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО руководителя, имеющего право подписи согласно учредительным документам, с указанием должности и контактного телефона') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'authorized_sign_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО уполномоченного лица, имеющего право подписи по доверенности с указанием должности, контактного телефона, эл.почты') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $createOrgForm->field($model, 'authorized_security_contact')->textarea(['style' => ['resize' => 'vertical']])->hint('ФИО сотрудника безопасности с указанием контактного телефона, эл.почты') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'contact_phone')->textarea(['style' => ['resize' => 'vertical']]) ?>
            </div>
            <div class="col-sm-6">
                <?= $createOrgForm->field($model, 'contact_email')->textarea(['style' => ['resize' => 'vertical']]) ?>
            </div>
        </div>

    </div>

    <div style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem; margin-top: 2rem; display: block; overflow: auto">
        <p class="lead">Документы</p>

        <ul style="color: gray; padding-left: 1.5rem; margin-bottom: 3rem;">
            <li>Скан-копия должна быть цветной</li>
            <li>Формат - .pdf</li>
            <li>Один файл - один документ</li>
            <li>Гарантийные письма и Заявления должны быть поданы на официальном бланке компании, адресованы Генеральному директору ООО "САЛАИР", иметь дату и номер исходящего документа, подпись уполномоченного сотрудника и печать организации</li>
            <li>Справка из налоговой оформляется в соответствии с приказом ФНС от 20 января 2017г. № ММВ-7-8/20@</li>
            <li>Срок действия документов - 30 календарных дней с даты выдачи</li>
        </ul>

        <?php
            $documentsUL = [
                'taxStampCopy',
                'taxRegistrationCopyUL',
                'naznachenieIspolnitelyaCopy',
                'doverennostNaPodpisanieCopy',
                'headSignatureCopy',
                'financialReportCopy'
            ];
            $documentsIP = [
                'passportCopyIP',
                'taxRegistrationCopyIP'
            ];
            $documentsAll = [
                'arendatorsList',
                'noDebtsReference',
                'rentContractCopy',
                'warehouseCadastralPassport',
                'propertyRightsCopy',
            ];
            $documentsSet = [];

            if ($documents && count($documents) > 0) {
                foreach ($documents as $entity => $document) {
                    echo '<div class="row"  style="border-bottom: 1px solid lightgray; padding: 1rem 0">';
                    echo '<div class="col-md-10">' . $model->attributeLabels()[$entity] . '</div>';
                    echo '<div class="col-md-1">' . Html::a('Скачать', ['/panel/organization/download-file/', 'id' => $document->id]) . '</div>';
                    echo '<div class="col-md-1">' . Html::a('Удалить', ['/panel/organization/delete-file/', 'id' => $document->id], ['style' => 'color: red;']) . '</div>';
                    echo '</div>';
                    $documentsSet[] = $entity;
                }

                if (count($documentsSet)) {
                    $documentsIP = array_diff($documentsIP, $documentsSet);
                    $documentsUL = array_diff($documentsUL, $documentsSet);
                    $documentsAll = array_diff($documentsAll, $documentsSet);
                }
            }

            foreach ($documentsAll as $entity) {
                $idInput = strtolower($entity);
                echo '<p>' . $model->attributeLabels()[$entity] . '</p>';
                echo '<label for="createorgform-' . $idInput . '" class="btn btn-default" style="width: 100%; text-align: left; padding: 2rem">' .
                        'Выберите файл</label>';
                echo  $createOrgForm->field($model, $entity)->fileInput(['style' => ['visibility' => 'hidden'], 'required' => true])->label(false);
            }

            if ($model->legal_type == 'UL') {
                foreach ($documentsUL as $entity) {
                    $idInput = strtolower($entity);
                    echo '<p>' . $model->attributeLabels()[$entity] . '</p>';
                    echo '<label for="createorgform-' . $idInput . '" class="btn btn-default" style="width: 100%; text-align: left; padding: 2rem">' .
                            'Выберите файл</label>';
                    echo  $createOrgForm->field($model, $entity)->fileInput(['style' => ['visibility' => 'hidden'], 'required' => true])->label(false);
                }
            } else {
                foreach ($documentsIP as $entity) {
                    $idInput = strtolower($entity);
                    echo '<p>' . $model->attributeLabels()[$entity] . '</p>';
                    echo '<label for="createorgform-' . $idInput . '" class="btn btn-default" style="width: 100%; text-align: left; padding: 2rem">' .
                            'Выберите файл</label>';
                    echo  $createOrgForm->field($model, $entity)->fileInput(['style' => ['visibility' => 'hidden'], 'required' => true])->label(false);
                }
            }

        ?>

    <div style="margin-top: 1rem">
        <input type="checkbox" id="checkbox-id" />
        <label for="checkbox-id">Я даю согласие на обработку персональных данных</label>
    </div>
    <div class="form-group" style="margin-top: 2rem">
        <?= Html::submitButton('Отправить на верификацию', [
            'id' => 'submit-btn',
            'class' => 'btn btn-primary',
            'disabled' => true
        ]) ?>
    </div>
<?php ActiveForm::end(); ?>

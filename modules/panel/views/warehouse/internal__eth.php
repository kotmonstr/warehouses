<?php

use yii\bootstrap\Html;
use app\models\Dictionary;
use yii\web\View;
use app\models\Warehouse;

/* @var $this View */
/* @var $model app\modules\admin\models\forms\warehouse\InternalForm*/
/* @var $lutt app\models\WhLoadUnloadEquipment */

?>

<div class="cranage_types lu-block">

    <?= Html::hiddenInput(Html::getInputName($model, 'cranage_types[0][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?= Html::label('Вид', ['class' => 'control-label']) ?>
                <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[0][loading_unloading_tech_type_sn]'), [], Dictionary::findDictItems('cranage_types'), ['class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[0][loading_unloading_method_sn]'), [], Dictionary::findDictItems('loading_unloading_methods'), ['class' => 'form-control multiselect', 'required' => true, 'multiple'=>'multiple']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                    <?= Html::textInput(Html::getInputName($model, 'cranage_types[0][capacity]'), null, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                    <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[0][capacity_type_sn]'), [], Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Количество', ['class' => 'control-label']) ?>
                        <?= Html::textInput(Html::getInputName($model, 'cranage_types[0][amount]'), null, ['class' => 'form-control', 'required' => true]) ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div style="margin-top: 1.5em; text-align: right;">
                        <div class="btn btn-danger cranage-delete delete-block" data-id="">Удалить</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="loading_unloading_tech_type lu-block">

    <?= Html::hiddenInput(Html::getInputName($model, 'loading_unloading_tech_types[0][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?= Html::label('Вид', ['class' => 'control-label']) ?>
                <?= Html::dropDownList(
                    Html::getInputName($model, 'loading_unloading_tech_types[0][loading_unloading_tech_type_sn]'),
                    [],
                    Dictionary::findDictItems('loading_unloading_tech_types'),
                    ['class' => 'form-control', 'required' => true]) ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                <?= Html::dropDownList(Html::getInputName($model, 'loading_unloading_tech_types[0][loading_unloading_method_sn]'), [], Dictionary::findDictItems('loading_unloading_methods'), ['class' => 'form-control multiselect', 'required' => true, 'multiple'=>'multiple']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">
                    <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                    <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[0][capacity]'), null, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>
                </div>
                <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">
                    <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                    <?= Html::dropDownList(Html::getInputName($model, 'loading_unloading_tech_types[0][capacity_type_sn]'), [], Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Количество', ['class' => 'control-label']) ?>
                        <?= Html::textInput(Html::getInputName($model, 'loading_unloading_tech_types[0][amount]'), null, ['class' => 'form-control', 'required' => true]) ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div style="margin-top: 1.5em; text-align: right;">
                        <div class="btn btn-danger equipment-delete delete-block">Удалить</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

/* @var $this View */
/* @var $model  app\modules\panel\models\forms\warehouse\DocumentsForm*/
/* @var $documents string[]  */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use app\models\Warehouse;

$scriptDeleteFile = <<< JS

    function deleteFile(path) {
        $.ajax({
            url: '/panel/warehouse/delete-file',
            method: "GET",
            data: {"path": path},
            success: function(data) {
                console.log(data);
                window.location.reload();
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function downloadFile(path, name) {
        fetch('/panel/warehouse/download-file?path=' + path)
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                a.download = name;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
                console.log('your file has downloaded!');
            })
            .catch((error) => console.log(error));
    }

    var elements = [];
    elements.push([
        document.getElementById('documents_path_1_delete'),
        document.getElementById('documents_path_1_value')
    ]);
    elements.push([
        document.getElementById('documents_path_2_delete'),
        document.getElementById('documents_path_2_value')
    ]);
    elements.push([
        document.getElementById('documents_path_3_delete'),
        document.getElementById('documents_path_3_value')
    ]);

    elements.map(function (elem) {
        if (elem[0]) {
            elem[0].addEventListener('click', function () {
                deleteFile(elem[1].dataset.path);
                elem[0].parentNode.style.display = 'none';
            });
        }
        if (elem[1]) {
            elem[1].addEventListener('click', function () {
                downloadFile(elem[1].dataset.path, elem[1].dataset.name);
            });
        }
    });

JS;
$this->registerJs($scriptDeleteFile, yii\web\View::POS_END);
?>

<?php $formWarehouseDocuments = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<p style="font-weight: bold;">Приложите документы</p>

<ul style="color: gray;">
    <li>Скан-копия должна быть цветной</li>
    <li>Формат - .pdf</li>
    <li>Один файл - один документ</li>
    <li>Срок действия документов - 30 календарных дней с момента выдачи</li>
</ul>

<?php if (isset($documents) && count($documents)) {
    $idx = 0;
    $doc_names = ['documents_file_1', 'documents_file_2', 'documents_file_3'];
    $doc_exists = [];
    foreach ($documents as $entity => $file) {
        echo '<div class="row file-row"><div class="col-sm-10" id="documents_path_' . ($idx + 1) . '_value" data-path="' .
        $file['path'] .
        '" data-name="' .
        $file['name'] .
        '"><a href="#">' .
        $file['name']  .
        '</a></div>' .
        '<div class="col-sm-2" style="text-align:center; color:red; cursor: pointer;"  id="documents_path_' . ($idx + 1) . '_delete">' .
        '&times;</div></div>';
        $idx++;
        $doc_exists[] = $entity;
    }
    $doc_last = array_diff($doc_names, $doc_exists);
    foreach ($doc_last as $doc) {
        echo '<div class="row file-row"><div class="col-sm-12">' . $formWarehouseDocuments->field($model, $doc)->fileInput() . '</div></div>';
    }
} else {
    echo '<div class="row file-row"><div class="col-sm-12">' . $formWarehouseDocuments->field($model, 'documents_file_1')->fileInput() . '</div></div>';
    echo '<div class="row file-row"><div class="col-sm-12">' . $formWarehouseDocuments->field($model, 'documents_file_2')->fileInput() . '</div></div>';
    echo '<div class="row file-row"><div class="col-sm-12">' . $formWarehouseDocuments->field($model, 'documents_file_3')->fileInput() . '</div></div>';
} ?>

<div class="form-group btns-wrapper">
        <?php
        if (Warehouse::findModel($model->id)->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            || Warehouse::findModel($model->id)->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
        ) {
            echo Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-next', 'disabled' => 'disabled']);
        } else {
            echo Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-next']);
        }
        ?>
<?php $formWarehouseDocuments::end(); ?>

    <?= Html::beginForm('/panel/warehouse/internal?id=' . $model->id) ?>
    <?= Html::submitButton('На предыдущий шаг', ['class' => 'btn btn-prev']) ?>
    <?= Html::endForm() ?>

</div>

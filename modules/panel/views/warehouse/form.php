<?php

use app\models\Warehouse;
use app\modules\panel\models\forms\warehouse\WarehouseForm;
use yii\bootstrap\Html;
use yii\web\View;
use yii\bootstrap\Alert;

/* @var $this View */
/* @var $current string */
/* @var $firstEmpty string */
/* @var $title string */
/* @var $model WarehouseForm */
/* @var $documents string[]  */

$this->title = $title ? : 'Новый склад';

$this->params['breadcrumbs'][] = ['label' => 'Склады', 'url' => ['/panel/warehouse/index']];
$this->params['breadcrumbs'][] = $this->title;

$tabs = [];
$firstEmptyPassed = false;

foreach (Warehouse::TABS as $alias => $title) {
    $content = Html::a($title);
    $options = [];

    if ($firstEmptyPassed) {
        $options['class'] = 'disabled';
    } else {
        $content = Html::a($title, [$alias, 'id' => $model->id]);
    }

    if ($alias == $current) {
        $options['class'] = 'active';
    }

    if ($alias == $firstEmpty) {
        $firstEmptyPassed = true;
    }

    $tabs[] = Html::tag('li', $content, $options);
}

?>

<div class="row" style="margin: 0 .5em;">
    <?php
    if ($model->id) {
        $warehouse = Warehouse::findModel($model->id);

        if ($warehouse->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            || $warehouse->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
        ) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-danger',
                ],
                'body' => "<h4>Заявка навсегда отклонена</h4>Причина: {$warehouse->verification_comment}",
            ]);

        } elseif ($warehouse->dsl_verification_status_sn == Warehouse::VERIFICATION_FAILED) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => "<h4>Заявка отклонена ДСЛ</h4>Причина: {$warehouse->verification_comment}",
            ]);

        } elseif ($warehouse->sb_verification_status_sn == Warehouse::VERIFICATION_FAILED) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => "<h4>Заявка отклонена СБ</h4>Причина: {$warehouse->verification_comment}",
            ]);

        } elseif (($warehouse->dsl_verification_status_sn == Warehouse::VERIFICATION_IN_PROGRESS
                    || $warehouse->sb_verification_status_sn == Warehouse::VERIFICATION_IN_PROGRESS)
                    && $warehouse->documents_is_set
        ) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-success',
                ],
                'body' => "<h5>Заявка на верификацию склада подана, ожидается проверка службой безопасности</h5>",
            ]);
        }
    }
    ?>
</div>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-8 col-md-10">

        <ul class="nav nav-tabs">
            <?= implode('', $tabs) ?>
        </ul>

        <div class="panel panel-default">
            <div class="panel-body">

                <?= $this->render($current, [
                    'model' => $model,
                    'documents' => $documents,
                ]) ?>

            </div>
        </div>

    </div>
</div>

<?php

use yii\bootstrap\Html;
use app\models\Dictionary;
use yii\web\View;
use app\models\Warehouse;
use dosamigos\multiselect\MultiSelect;

/* @var $this View */
/* @var $model app\modules\panel\models\forms\warehouse\InternalForm*/
/* @var $luct app\models\WhLoadUnloadCranage */

$internalIsSet = Warehouse::findOne(['id' => $model->id])->internal_is_set;

?>

<?php if (count($model->cranage_types)) { ?>

    <?php foreach ($model->cranage_types as $idx => $luct) { ?>

        <div class="cranage_types lu-block">
            <?= Html::hiddenInput(Html::getInputName($model, 'cranage_types[' . $idx . '][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Вид', ['class' => 'control-label']) ?>
                        <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[' . $idx . '][loading_unloading_tech_type_sn]'), $luct->loading_unloading_tech_type_sn, Dictionary::findDictItems('cranage_types'), ['class' => 'form-control', 'required' => true]) ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                        <?php
                        echo MultiSelect::widget([
                            "id" => $idx . "-multiselect-cranage",
                            "options" => ['multiple'=>"multiple", "class" => "form-control"],
                            "clientOptions" => ['nonSelectedText' => 'Ничего не выбрано', 'allSelectedText' => 'Все варианты'],
                            "data" => Dictionary::findDictItems('loading_unloading_methods'),
                            "value" =>  $luct->loading_unloading_method_sn,
                            "name" => Html::getInputName($model, 'cranage_types[' . $idx . '][loading_unloading_method_sn]'),
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">

                                <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                                <?= Html::textInput(Html::getInputName($model, 'cranage_types[' . $idx . '][capacity]'), $luct->capacity, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>

                        </div>
                        <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">

                                <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                                <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[' . $idx . '][capacity_type_sn]'), $luct->capacity_type_sn, Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= Html::label('Количество', ['class' => 'control-label']) ?>
                                <?= Html::textInput(Html::getInputName($model, 'cranage_types[' . $idx . '][amount]'), $luct->amount, ['class' => 'form-control', 'required' => true]) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div style="margin-top: 1.5em; text-align: right;">
                                <div class="btn btn-danger cranage-delete delete-block" data-id="<?= $luct->id ?>">
                                    Удалить
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>

    <?php } ?>

<?php } elseif (!$internalIsSet) { ?>

    <div class="cranage_types lu-block">

        <?= Html::hiddenInput(Html::getInputName($model, 'cranage_types[0][warehouse_id]'), $model->id, ['class' => 'hidden-control']) ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Вид', ['class' => 'control-label']) ?>
                    <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[0][loading_unloading_tech_type_sn]'), [], Dictionary::findDictItems('cranage_types'), ['class' => 'form-control', 'required' => true]) ?>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Способ загрузки', ['class' => 'control-label']) ?><br/>
                    <?php
                        echo MultiSelect::widget([
                            "id" => "0-multiselect-cranage",
                            "options" => ['multiple'=>"multiple", "class" => "form-control"],
                            "clientOptions" => ['nonSelectedText' => 'Ничего не выбрано', 'allSelectedText' => 'Все варианты'],
                            "data" => Dictionary::findDictItems('loading_unloading_methods'),
                            "value" =>  [],
                            "name" => Html::getInputName($model, 'cranage_types[0][loading_unloading_method_sn]'),
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-8" style="margin-right: 0; padding-right: 0;">

                            <?= Html::label('Грузоподъёмность', ['class' => 'control-label']) ?>
                            <?= Html::textInput(Html::getInputName($model, 'cranage_types[0][capacity]'), null, ['class' => 'form-control', 'required' => true, 'style' => 'border-right: none; border-top-right-radius: 0px; border-bottom-right-radius: 0px;']) ?>

                    </div>
                    <div class="col-sm-4" style="margin-left: 0; padding-left: 0;">

                            <?= Html::label('Ед. Изм', ['class' => 'control-label']) ?>
                            <?= Html::dropDownList(Html::getInputName($model, 'cranage_types[0][capacity_type_sn]'), [], Dictionary::findDictItems('unit', 'mass', true), ['class' => 'form-control', 'required' => true, 'style' => 'border-left: none; border-top-left-radius: 0px; border-bottom-left-radius: 0px;']) ?>

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= Html::label('Количество', ['class' => 'control-label']) ?>
                            <?= Html::textInput(Html::getInputName($model, 'cranage_types[0][amount]'), null, ['class' => 'form-control', 'required' => true]) ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div style="margin-top: 1.5em; text-align: right;">
                            <div class="btn btn-danger cranage-delete delete-block" data-id="">
                                Удалить
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\panel\models\forms\WarehouseSearchForm */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use app\models\Organization;

$this->title = 'Склады';
$this->params['breadcrumbs'][] = $this->title;

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var link = this.parentNode.dataset.link;
                a.setAttribute('href', link);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

    <div style="width: 20%; position:relative; top:2em; left:85%;">
        <?= Html::beginForm('/panel/warehouse/general', 'get') ?>
        <?php
            $org = Organization::findOne(['id' => Yii::$app->user->identity->organization_id]);
            if ($org && !$org->deleted_at && $org->verification_status_sn != Organization::VERIFICATION_RESTRICTED) {
                echo Html::submitButton('Новый склад', ['class' => 'btn btn-primary']);
            } else {
                echo Html::submitButton('Новый склад', ['class' => 'btn btn-primary', 'disabled' => 'disabled', 'title' => 'Организация удалена и/или ее верификация отменена навсегда']);
            }
        ?>
        <?= Html::endForm() ?>
    </div>


<div class="row" style="padding: 1em; text-align: center; margin-top:-2em;">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterPosition' => GridView::FILTER_POS_HEADER,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => function ($model, $key, $index, $grid) {
                if ($model->created_by == Yii::$app->user->identity->id) {
                    return [
                        'style' => 'border: none; cursor: pointer;',
                        'onmouseover' => 'this.style.backgroundColor="lightblue";',
                        'onmouseout' => 'this.style.backgroundColor="#F3F3F3";',
                        'data-link' => Url::to([$model->getFirstEmptyTab() . '?id=' . $model->id])
                    ];
                } else {
                    return [
                        'style' => 'border: none;',
                        'data-link' => '#'
                    ];
                }
            },
            'columns' => [
                [
                    'attribute' => 'title',
                    'headerOptions' => ['style' => 'width:30%;'],
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'filterInputOptions' => [
                        'placeholder' => 'Название склада',
                        'class' => 'form-control'
                    ],
                ],
                [
                    'attribute' => 'address_ym_geocoder',
                    'contentOptions' => [ 'style' => 'text-align: left;', 'class' => 'grid-table-row' ],
                    'filterInputOptions' => [
                        'placeholder' => 'Адрес склада',
                        'class' => 'form-control'
                    ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['style' => 'width:20%;'],
                    'template' => '{delete}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            return Url::to([$action . '?id=' . $model->id]);
                        }
                    },
                    'visibleButtons' => [
                        'delete' => function ($model, $key, $index) {
                            return $model->created_by == Yii::$app->user->identity->id;
                        },
                    ]
                ],
            ],
        ]
    ) ?>
</div>

</div>

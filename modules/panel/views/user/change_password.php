<?php

/* @var $this View */
/* @var $modelPassword app\modules\panel\models\forms\ChangePasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

?>
<!-- ChangePasswordForm begin-->


    <?php $formChangePassword = ActiveForm::begin([
        'action' =>['/panel/user/change-user-data'],
        'enableAjaxValidation' => true,
    ]); ?>


    <div class="form-group" style="padding-right: 2em;">
        <?= $formChangePassword->field($modelPassword, 'password')->passwordInput() ?>
    </div>
    <div class="form-group" style="padding-right: 2em;">
        <?= $formChangePassword->field($modelPassword, 'new_password')->passwordInput()?>
    </div>
    <div class="form-group" style="padding-right: 2em;">
        <?= $formChangePassword->field($modelPassword, 'new_password_confirm')->passwordInput() ?>
    </div>


    <div class="form-group" style="padding-right: 2em;">
        <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary', 'name' => 'change-password-button']) ?>
    </div>

    <?php $formChangePassword::end(); ?>

<!-- ChangePasswordForm end-->

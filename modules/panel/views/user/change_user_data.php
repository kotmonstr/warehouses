<?php

/* @var $this View */
/* @var $model app\modules\panel\models\forms\ChangeUserDataForm */
/* @var $modelPassword app\modules\panel\models\forms\ChangePasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\MaskedInput;

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
    function toggleForms() {
        $("#change_password_wrapper").toggle();
        $("#change_user_data_button").toggle();
    }
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

<h1><?= Html::encode($this->title) ?></h1>

<!-- ChangeUserDataForm begin-->
    <div class="container" style="background-color: #F3F3F3; padding: 1em;">

        <div class="container">

          <?php $formChangeUserData = ActiveForm::begin([
                'action' =>['/panel/user/change-user-data'],
                'enableAjaxValidation' => true,
            ]); ?>

            <div class="form-group" style="padding-right: 2em;">
                <?= $formChangeUserData->field($model, 'name')->textInput(['value' => $model->current_name ? $model->current_name : '']) ?>
            </div>
            <div class="form-group" style="padding-right: 2em;">
                <?= $formChangeUserData->field($model, 'organization_position')->textInput(['value' => $model->current_position ? $model->current_position : '']) ?>
            </div>
            <div class="form-group" style="padding-right: 2em;">
                <?= $formChangeUserData->field($model, 'phone_number')
                ->widget(MaskedInput::class,
                [
                    'mask' => '+7 (999) 999-99-99',
                    'options' => ['value' => $model->current_phone ? $model->current_phone : '']
                ]) ?>
            </div>

            <div class="form-group" style="padding-right: 2em;">
                <label for="change_user_data_email">Email</label>
                <input type="text" value="<?=Yii::$app->user->identity->email?>" readonly class="form-control" style="border: none; box-shadow: none; background-color: #F3F3F3; padding-left: 0;"/>
            </div>

            <div class="form-group" style="padding-right: 2em;">
                <label>Пароль</label>
                <p><a href="#" class="blue-text" onclick="toggleForms()">Изменить</a></p>
            </div>


            <div class="form-group" id="change_user_data_button" style="padding-right: 2em;">
                <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-primary', 'name' => 'change-user-data-button']) ?>
            </div>

            <?php $formChangeUserData::end(); ?>

        </div>

        <div id="change_password_wrapper" style="display: none;"  class="container">
            <?=$this->render('change_password', ['modelPassword' => $modelPassword])?>
        </div>
    </div>
<!-- ChangeUserDataForm end-->

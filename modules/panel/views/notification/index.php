<?php

/* @var $this View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Уведомления';
$this->params['breadcrumbs'][] = $this->title;

$scriptReadNotification = <<< JS

    function read(id) {
        $.ajax({
            url: '/panel/notification/read',
            method: "GET",
            data: {"id": id},
            success: function(data) {
                console.log(data);
            },
            error: function(error) {
                console.log(error);
            }
        });
        
        $('.notification-title[data-id=' + id + ']').removeClass('notification-title-unreaded');
    }

    var tds = document.getElementsByClassName('notification-td');
    for (var i = 0; i < tds.length; i++) {
        tds.item(i).addEventListener('click', function () {
            var wrapper = document.getElementById('notification-modal-wrapper');
            wrapper.innerHTML = '';
            wrapper.style.display = 'block';

            var modal = document.createElement('div');
            modal.classList.add('notification-modal');

            var h4 = document.createElement('h4');
            h4.innerText = this.innerText;

            var p = document.createElement('p');
            p.innerHTML = this.dataset.text;

            var close = document.createElement('div');
            close.classList.add('notification-close');
            close.innerHTML = '&times;';
            close.addEventListener('click', function () {
                wrapper.style.display = 'none';
            });

            modal.appendChild(close);
            modal.appendChild(h4);
            modal.appendChild(p);
            wrapper.appendChild(modal);
            
            wrapper.addEventListener('click', function () {
                wrapper.style.display = 'none';
            });

            read(this.dataset.id);
        });
    }

JS;
$this->registerJs($scriptReadNotification, yii\web\View::POS_END);
?>

<div id="notification-modal-wrapper"></div>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container notification-container">

    <div class="row notification-row">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'layout'=>"{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table',
            ],
            'rowOptions' => [
                'style' => 'border: none; cursor: pointer;',
                'onmouseover' => 'this.style.backgroundColor="lightblue";',
                'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
            ],
            'columns' => [
                [
                    'attribute' => 'title',
                    'headerOptions' => ['style' => 'width:70%;'],
                    'contentOptions' => function ($model, $key, $index, $column) { return [ 'style' => 'text-align: left; color: blue;', 'class' => 'notification-td', 'data' => ['id' => $model->id, 'text' => $model->text] ]; },
                    'content' => function ($model, $key, $index, $column) {
                        return '<div class="notification-title ' . (!$model->viewed_at ? 'notification-title-unreaded' : '') . '" data-id="' . $model->id . '">' . $model->title . '</div>';
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'format' => ['date', 'php:d-m-Y H:i:s']
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['style' => 'width:5%;'],
                    'template' => '{delete}',
                ],
            ],
        ]
    ) ?>
    </div>

</div>

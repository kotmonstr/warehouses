<?php
/* @var $model  app\models\Advert*/

use app\models\Advert;
use app\models\Dictionary;
use app\models\Warehouse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

$this->title = 'Объявление для склада "' . $model->warehouse->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Объявления', 'url' => ['/panel/advert/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin(); ?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container"  style="background-color: #F3F3F3; border-radius: 10px; padding: 2rem;">
    <div class="form-group">
        <?= $form->field($model, 'warehouse_id')->dropDownList(ArrayHelper::map(Warehouse::find()->where(
            [
                'created_by' => Yii::$app->user->id,
                'sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                'dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                'deleted_at' => null
            ]
            )->all(), 'id', 'title'))->label() ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'storage_type_sn')->dropDownList(Dictionary::findDictItems('storage_system'))->label() ?>
    </div>
    <div class="row">
        <div class="form-group col-sm-4">
            <?= $form->field($model, 'available_space')->textInput() ?>
        </div>
        <div class="form-group col-sm-2">
            <?= $form->field($model, 'available_space_type_sn')->dropDownList(Dictionary::findDictItems('unit', 'square', true))->label() ?>
        </div>
        <div class="form-group col-sm-6">
            <?= $form->field($model, 'unit_cost_per_day')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            <?= $form->field($model, 'available_from')->textInput(['type' => 'date']) ?>
        </div>
        <div class="form-group col-sm-6">
            <?= $form->field($model, 'available_to')->textInput(['type' => 'date']) ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-4">
            <?= $form->field($model, 'manager_name')->textInput() ?>
        </div>
        <div class="form-group col-sm-4">
            <?= $form->field($model, 'manager_phone_number')->widget(MaskedInput::class, ['mask' => '+7 (999) 999-99-99',
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->textInput(['placeholder'=>'+7 (999) 999 9999'])?>
        </div>
        <div class="form-group col-sm-4">
            <?= $form->field($model, 'manager_email')->textInput(['type' => 'email']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>
    </div>
</div>
<div class="form-group" style="margin-top: 2rem">
    <?= Html::submitButton('Сохранить и опубликовать', ['class' => 'btn btn-primary', 'name' => 'Advert[status_sn]', 'value' => Advert::STATUS_PUBLISHED]) ?>
    <?= Html::submitButton('Сохранить черновик', ['class' => 'btn btn-default', 'name' => 'Advert[status_sn]', 'value' => Advert::STATUS_DRAFT]) ?>
</div>
<?php ActiveForm::end(); ?>

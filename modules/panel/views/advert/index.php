
<?php

use app\models\Advert;
use app\models\Dictionary;
use app\models\User;
use app\models\Warehouse;
use app\modules\panel\models\forms\AdvertSearchForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Organization;

/* @var $this View */
/* @var $model AdvertSearchForm */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Объявления';
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'action' => ['/panel/advert/index'],
    'method' => 'get',
]);

$cityArray = Warehouse::getCities(Yii::$app->user->identity->organization_id);
$warehousesArray = ArrayHelper::map(
    Warehouse::find()->where([
        'organization_id' => Yii::$app->user->identity->organization_id,
        'deleted_at' => null
    ])->all(),
    'id',
    'title'
);

function getFilterFromArray($array, $labelText, $model, $attribute) {
    $filterArray = ['' => $labelText] + $array;
    return Html::activeDropDownList($model, $attribute, $filterArray, ['class' => 'form-control']);
}

$jsClickRow = <<<JS
    $(document).ready(function(){
        var cols = document.getElementsByClassName('grid-table-row');
        for (var i = 0; i < cols.length; i++) {
            cols[i].addEventListener('click', function() {
                var a = document.createElement('a');
                var id = this.parentNode.dataset.key;
                a.setAttribute('href', '/panel/advert/update?id=' + id);
                a.click();
            });
        }
    });
JS;

$this->registerJs($jsClickRow);

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="container" style="margin: 1em 5em 1em 0;padding: 1em;">
    <div class="row">
        <div class="col-md-12" style="padding-left: 0;">
            <div class="text-left">

                <?php
                /** @var User $identity */
                $identity = Yii::$app->user->identity;

                $organization = Organization::findOne(['id' => $identity->organization_id]);

                $warehousesCount = $identity->getWarehouse()->andWhere([
                    'sb_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                    'dsl_verification_status_sn' => Warehouse::VERIFICATION_VERIFIED,
                    'deleted_at' => null
                ])->count();

                if ($warehousesCount > 0 && $organization && !$organization->deleted_at && $organization->verification_status_sn != Organization::VERIFICATION_RESTRICTED) {
                    echo Html::a('Новое объявление', '/panel/advert/create', ['class' => 'btn btn-primary ']);
                } else {
                    echo Html::a('Новое объявление', '#', ['class' => 'btn btn-primary ', 'disabled' => 'disabled', 'title' => 'Сначала вам необходимо добавить склад и дождаться его верификации']);
                } ?>

            </div>
        </div>
    </div>
</div>

<div class="container" style="background-color: #F3F3F3; margin: 1em 5em 1em 0; border: 1px solid #F3F3F3; border-radius: 10px; padding: 1em;">

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $model,
                'filterPosition' => GridView::FILTER_POS_HEADER,
                'layout'=>"{items}\n{pager}",
                'tableOptions' => [
                    'class' => 'table',
                ],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->created_by == Yii::$app->user->id) {
                        return [
                            'style' => 'border: none; cursor: pointer;',
                            'onmouseover' => 'this.style.backgroundColor="lightblue";',
                            'onmouseout' => 'this.style.backgroundColor="#F3F3F3";'
                        ];
                    }
                    return ['style' => 'border: none;'];
                },
                'columns' => [
                    [
                        'attribute' => 'warehouse_id',
                        'filter' => getFilterFromArray($warehousesArray, 'Склады', $model, 'warehouse_id'),
                        'value' => 'warehouse.title',
                        'headerOptions' => ['style' => 'width:15%;'],
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:15%;'];
                            }
                            return ['style' => 'width:15%;'];
                        },
                    ],
                    [
                        'attribute' => 'city',
                        'headerOptions' => ['style' => 'width:15%;'],
                        'filter' => getFilterFromArray($cityArray, 'Города', $model, 'city'),
                        'content' => function ($model, $key, $index, $column) {
                            return $model->warehouse->address_city_ym_geocoder;
                        },
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:15%;'];
                            }
                            return ['style' => 'width:15%;'];
                        },
                    ],
                    [
                        'attribute' =>'storage_type_sn',
                        'filter'=> getFilterFromArray(Dictionary::findDictItems('storage_system'), 'Типы систем хранения', $model, 'storage_type_sn'),
                        'value' => 'storage_type.title',
                        'headerOptions' => ['style' => 'width:30%;'],
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:30%;'];
                            }
                            return ['style' => 'width:30%;'];
                        },
                    ],
                    [
                        'attribute' => 'available_from',
                        'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                        'format' => ['date', 'php:d.m.Y'],
                        'headerOptions' => ['style' => 'width:5%;'],
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:5%;'];
                            }
                            return ['style' => 'width:5%;'];
                        },
                    ],
                    [
                        'attribute' => 'available_to',
                        'format' => ['date', 'php:d.m.Y'],
                        'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null],
                        'headerOptions' => ['style' => 'width:5%;'],
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:5%;'];
                            }
                            return ['style' => 'width:5%;'];
                        },
                    ],
                    [
                        'attribute' => 'available_space',
                        'headerOptions' => ['style' => 'width:5%;'],
                        'value' => function (Advert $model) {
                            return $model->available_space . ' ' . $model->available_space_type->title_short;
                        },
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:5%;'];
                            }
                            return ['style' => 'width:5%;'];
                        },
                    ],
                    [
                        'attribute' => 'unit_cost_per_day',
                        'headerOptions' => ['style' => 'width:5%;'],
                        'value' => function (Advert $model) {
                            return $model->unit_cost_per_day . ' ₽';
                        },
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:5%;'];
                            }
                            return ['style' => 'width:5%;'];
                        },
                    ],
                    [
                        'attribute' => 'status_sn',
                        'filter'=> getFilterFromArray(Dictionary::findDictItems('advert_status'), 'Статус', $model, 'status_sn'),
                        'value' => 'status.title',
                        'headerOptions' => ['style' => 'width:10%;'],
                        'contentOptions' => function ($model, $key, $index, $grid) {
                            if ($model->created_by == Yii::$app->user->id) {
                                return ['class' => 'grid-table-row', 'style' => 'width:10%;'];
                            }
                            return ['style' => 'width:10%;'];
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'headerOptions' => ['style' => 'width:3em;'],
                        'contentOptions' => ['style' => 'width:3em;'],
                        'visibleButtons' => [
                            'delete' => function ($model, $key, $index) {
                                return $model->created_by == Yii::$app->user->id;
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'delete') {
                                return Url::to(['/panel/advert/delete?id=' . $model->id]);
                            }
                        },

                    ],

                ],
            ]);
            ?>
        </div>
    </div>
</div>

<?php

namespace app\modules\panel;

use yii\base\Module;

/**
 * panel module definition class
 */
class Panel extends Module
{
    /** {@inheritdoc} */
    public $controllerNamespace = 'app\modules\panel\controllers';
}

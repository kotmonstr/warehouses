<?php

namespace app\modules\panel\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * ChangeUserDataForm is the model behind the ChangeUserDataForm form.
 *
 * @property      string $password
 * @property      string $new_password
 * @property      string $new_password_confirm
 * @property-read User|null $user
 */
class ChangePasswordForm extends Model
{
    public $password;
    public $new_password;
    public $new_password_confirm;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['password', 'new_password', 'new_password_confirm'], 'required'],
            [['password', 'new_password', 'new_password_confirm'], 'string', 'max' => 255],
            [['password'], 'validatePassword'],
            [['new_password_confirm'], 'compare', 'compareAttribute' => 'new_password'],
        ];
    }

    /**
     * Validates the password,
     * this method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     *
     * @return null
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findByUsername(Yii::$app->user->identity->email);

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Wrong password.');
            }
        }
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'new_password_confirm' => 'Подтвердить новый пароль',
        ];
    }

    /**
     * Function for save changes user password
     *
     * @return bool whether the data is saved in successfully
     */
    public function save()
    {
        $user = User::findByUsername(Yii::$app->user->identity->email);
        if ($user && $this->validate()) {
            $user->setPassword($this->new_password);
            if ($user->save()) {
                return true;
            }
        }
        return false;
    }
}

<?php

namespace app\modules\panel\models\forms;

use yii\base\Model;

/**
 * Search Organization Form
 *
 * @property string $inn
 */
class OrgSearchForm extends Model
{
    public $inn;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            ['inn', 'required'],
            ['inn', 'string', 'length' => [10, 12]],
        ];
    }
}

<?php

namespace app\modules\panel\models\forms;


use app\models\Advert;
use Yii;
use yii\data\ActiveDataProvider;

class AdvertSearchForm extends Advert
{
    public $city;

    /** {@inheritDoc} */
    public function rules()
    {
        return [
            [['warehouse_id'], 'integer'],
            [['storage_type_sn', 'status_sn'], 'string'],
            [['available_from','available_to'], 'string'],
            [['available_to'], 'compare', 'compareAttribute' => 'available_from', 'operator' => '>=', 'skipOnEmpty' => true],
            [['city'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritDoc} */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'city'=>'Город',
        ]);
    }

    /**
     * @param $params
     * @return $this|ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()
            ->joinWith('warehouse.classification')
            ->where(['warehouse.organization_id' => Yii::$app->user->identity->organization_id]);
        $query
            ->joinWith('organization')
            ->andWhere([
                'organization.deleted_at' => null,
                'warehouse.deleted_at' => null,
                'advert.deleted_at' => null,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);

        if (!($this->load($params)) || !$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['warehouse_id' => $this->warehouse_id])
            ->andFilterWhere(['storage_type_sn' => $this->storage_type_sn])
            ->andFilterWhere(['status_sn' => $this->status_sn])
            ->andFilterWhere(['<=', 'available_from', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['>=', 'available_to', $this->available_from ? Yii::$app->formatter->format($this->available_from, 'timestamp') : null])
            ->andFilterWhere(['<=', 'available_from', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null])
            ->andFilterWhere(['>=', 'available_to', $this->available_to ? Yii::$app->formatter->format($this->available_to, 'timestamp') : null])
            ->andFilterWhere(['warehouse.address_city_ym_geocoder' => $this->city]);

        return $dataProvider;
    }
}

<?php

namespace app\modules\panel\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * ChangeUserDataForm is the model behind the ChangeUserDataForm form.
 *
 * @property      string $name
 * @property      string $organization_position
 * @property      string $phone_number
 * @property-read User|null $user
 * @property-read string|null $current_name
 * @property-read string|null $current_position
 * @property-read string|null $current_phone
 */
class ChangeUserDataForm extends Model
{
    public $name;
    public $organization_position;
    public $phone_number;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'organization_position'], 'string', 'max' => 255],
            [['phone_number'], 'filter', 'filter' => function ($value) {
                if (!empty($value)) {
                    return str_replace("_", "", $value);
                }
            }],
            [['phone_number'], 'string', 'length' => 18, 'message' => 'Введите номер телефона полностью'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'organization_position' => 'Должность',
            'phone_number' => 'Телефон',
        ];
    }

    /**
     * Function for save changes user data
     *
     * @return bool whether the data is saved in successfully
     */
    public function save()
    {
        $user = User::findByUsername(Yii::$app->user->identity->email);
        if ($user && $this->validate()) {
            $user->first_name = $this->name ? $this->name : $user->name;
            $user->organization_position = $this->organization_position;
            $user->phone_number = $this->phone_number;
            if ($user->save()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function for get name of current user
     *
     * @return null|string
     */
    public function getCurrent_name() {
        $user = User::findByUsername(Yii::$app->user->identity->email);
        if ($user && $user->first_name) {
            return $user->first_name;
        }
    }

    /**
     * Function for get position of current user
     *
     * @return null|string
     */
    public function getCurrent_position() {
        $user = User::findByUsername(Yii::$app->user->identity->email);
        if ($user && $user->organization_position) {
            return $user->organization_position;
        }
    }

    /**
     * Function for get phone of current user
     *
     * @return null|string
     */
    public function getCurrent_phone() {
        $user = User::findByUsername(Yii::$app->user->identity->email);
        if ($user && $user->phone_number) {
            return $user->phone_number;
        }
    }
}

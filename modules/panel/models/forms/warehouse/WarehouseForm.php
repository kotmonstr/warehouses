<?php

namespace app\modules\panel\models\forms\warehouse;

use app\models\Warehouse;
use yii\base\Model;

/**
 * Class WarehouseFormForm
 *
 * @property int $id
 */
class WarehouseForm extends Model
{
    public $id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return array_merge($scenarios, [
            self::SCENARIO_CREATE => $scenarios[self::SCENARIO_DEFAULT],
            self::SCENARIO_UPDATE => array_merge(['id'], $scenarios[self::SCENARIO_DEFAULT]),
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return (new Warehouse)->attributeLabels();
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
        ]);
    }
}
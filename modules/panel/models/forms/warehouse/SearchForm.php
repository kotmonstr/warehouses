<?php

namespace app\modules\panel\models\forms\warehouse;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\Warehouse;

/**
 * This is the model class for search in table 'warehouse'
 *
 * @property string $title
 * @property string $address_ym_geocoder
 */
class SearchForm extends Warehouse
{
    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'address_ym_geocoder'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @param string|array $params
     *
     * @return ActiveDataProvider $dataProvider
     */
    public function search($params)
    {
        $query = Warehouse::find()->where(['warehouse.deleted_at' => null]);
        $query->joinWith('organization')
            ->andFilterWhere(['organization.id' => Yii::$app->user->identity->organization_id])
            ->andFilterWhere(['organization.deleted_at' => null]);

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]
        );

        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ILIKE', 'warehouse.title', $this->title])
            ->andFilterWhere(['ILIKE', 'warehouse.address_ym_geocoder', $this->address_ym_geocoder]);

        return $dataProvider;
    }
}

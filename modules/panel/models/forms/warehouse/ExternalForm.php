<?php

namespace app\modules\panel\models\forms\warehouse;

use app\validators\NumberValidator;
use Yii;
use app\models\Warehouse;
use app\models\WhAccountingControlSystem;
use app\validators\DictItemValidator;
use app\models\Notification;
use app\models\User;
/**
 * Class ExternalForm
 * @package app\modules\panel\models\forms\warehouse
 *
 * @property string $accounting_system_sn
 * @property string $classification_sn
 * @property float $square_area
 * @property string $square_area_type_sn
 * @property float $built_up_area
 * @property string $built_up_area_type_sn
 * @property float $ceiling_height
 * @property string $ceiling_height_type_sn
 * @property int $floors
 * @property bool $maneuvering_area
 * @property int $entries_amount
 * @property bool $loading_ramp
 * @property int $automatic_gates_amount
 * @property bool $outer_lighting
 * @property bool $outer_security
 * @property bool $outer_beautification
 * @property bool $outer_fencing
 * @property int $outer_entries_amount
 * @property string $car_parking_sn
 * @property string $truck_parking_sn
 * @property bool $truck_maneuvering_area
 * @property bool $rail_road
 * @property float $distance_from_highway
 * @property string $distance_from_highway_type_sn
 * @property float $vehicle_max_length
 * @property string $vehicle_max_length_type_sn
 * @property float $vehicle_max_height
 * @property string $vehicle_max_height_type_sn
 * @property float $vehicle_max_width
 * @property string $vehicle_max_width_type_sn
 * @property float $vehicle_max_weight
 * @property string $vehicle_max_weight_type_sn
 * @property string[] $accounting_control_system_sn
 */
class ExternalForm extends WarehouseForm
{
    const ALIAS = 'external';

    public $accounting_system_sn;
    public $classification_sn;
    public $square_area;
    public $square_area_type_sn;
    public $built_up_area;
    public $built_up_area_type_sn;
    public $ceiling_height;
    public $ceiling_height_type_sn;
    public $floors;
    public $maneuvering_area;
    public $entries_amount;
    public $loading_ramp;
    public $automatic_gates_amount;
    public $outer_lighting;
    public $outer_security;
    public $outer_beautification;
    public $outer_fencing;
    public $outer_entries_amount;
    public $car_parking_sn;
    public $truck_parking_sn;
    public $truck_maneuvering_area;
    public $rail_road;
    public $distance_from_highway;
    public $distance_from_highway_type_sn;
    public $vehicle_max_length;
    public $vehicle_max_length_type_sn;
    public $vehicle_max_height;
    public $vehicle_max_height_type_sn;
    public $vehicle_max_width;
    public $vehicle_max_width_type_sn;
    public $vehicle_max_weight;
    public $vehicle_max_weight_type_sn;
    public $accounting_control_system_sn;

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['square_area'], 'required'],
            [
                [
                    'floors', 'entries_amount',
                    'automatic_gates_amount', 'outer_entries_amount',
                ], 'integer'
            ],
            [
                [
                    'accounting_system_sn', 'classification_sn',
                    'square_area_type_sn', 'built_up_area_type_sn',
                    'ceiling_height_type_sn',
                    'car_parking_sn', 'truck_parking_sn',
                    'distance_from_highway_type_sn',
                    'vehicle_max_length_type_sn', 'vehicle_max_height_type_sn',
                    'vehicle_max_width_type_sn', 'vehicle_max_weight_type_sn',
                ], 'string', 'max' => 255
            ],
            [['accounting_system_sn'], DictItemValidator::class, 'dictionary_sn' => 'accounting_system_type'],
            [['classification_sn'], DictItemValidator::class, 'dictionary_sn' => 'classification'],
            [
                [
                    'square_area_type_sn', 'built_up_area_type_sn'
                ], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'square'
            ],
            [
                [
                    'ceiling_height_type_sn', 'distance_from_highway_type_sn',
                    'vehicle_max_length_type_sn', 'vehicle_max_height_type_sn', 'vehicle_max_width_type_sn'
                ], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'distance'
            ],
            [
                [
                    'car_parking_sn', 'truck_parking_sn'
                ], DictItemValidator::class, 'dictionary_sn' => 'parking_type'
            ],
            [['vehicle_max_weight_type_sn'], DictItemValidator::class, 'dictionary_sn' => 'unit', 'type' => 'mass'],
            [
                [
                    'maneuvering_area', 'loading_ramp',
                    'outer_lighting', 'outer_security',
                    'outer_beautification', 'outer_fencing',
                    'truck_maneuvering_area', 'rail_road',
                ], 'boolean'
            ],
            [
                [
                    'square_area', 'built_up_area',
                    'ceiling_height', 'distance_from_highway',
                    'vehicle_max_length', 'vehicle_max_height',
                    'vehicle_max_width', 'vehicle_max_weight',
                ], NumberValidator::class,
                'min' => 0,
                'numberPattern' => '/^\d+([\.\,]\d{1,2})?$/',
                'message' => 'Положительное число максимум с двумя знаками после запятой'
            ],
            ['accounting_control_system_sn', 'each', 'rule' => ['string', 'max' => 255]],
            ['accounting_control_system_sn', 'default', 'value' => []],
            [['accounting_control_system_sn'], 'each', 'rule' => [
                DictItemValidator::class,
                'dictionary_sn' => 'accounting_control_systems'
            ]],
        ]);
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'accounting_control_system_sn' => 'Системы контроля и учета',
        ]);
    }

    /**
     * Function for save model data
     *
     * @return bool whether the data is saved in successfully
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем основную модель */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        $warehouse = Warehouse::findModel($this->id);
        $warehouse->dsl_verification_status_sn = Warehouse::VERIFICATION_IN_PROGRESS;
        $warehouse->sb_verification_status_sn = Warehouse::VERIFICATION_IN_PROGRESS;

        $warehouse->accounting_system_sn = $this->accounting_system_sn;
        $warehouse->classification_sn = $this->classification_sn;
        $warehouse->square_area = $this->square_area;
        $warehouse->square_area_type_sn = $this->square_area_type_sn;
        $warehouse->built_up_area = $this->built_up_area;
        $warehouse->built_up_area_type_sn =  $this->built_up_area_type_sn;
        $warehouse->ceiling_height = $this->ceiling_height;
        $warehouse->ceiling_height_type_sn = $this->ceiling_height_type_sn;
        $warehouse->floors = $this->floors;
        $warehouse->maneuvering_area = $this->maneuvering_area;
        $warehouse->entries_amount = $this->entries_amount;
        $warehouse->loading_ramp = $this->loading_ramp;
        $warehouse->automatic_gates_amount = $this->automatic_gates_amount;
        $warehouse->outer_lighting = $this->outer_lighting;
        $warehouse->outer_security = $this->outer_security;
        $warehouse->outer_beautification = $this->outer_beautification;
        $warehouse->outer_fencing = $this->outer_fencing;
        $warehouse->outer_entries_amount = $this->outer_entries_amount;
        $warehouse->car_parking_sn = $this->car_parking_sn;
        $warehouse->truck_parking_sn = $this->truck_parking_sn;
        $warehouse->truck_maneuvering_area = $this->truck_maneuvering_area;
        $warehouse->rail_road = $this->rail_road;
        $warehouse->distance_from_highway = $this->distance_from_highway;
        $warehouse->distance_from_highway_type_sn = $this->distance_from_highway_type_sn;
        $warehouse->vehicle_max_length = $this->vehicle_max_length;
        $warehouse->vehicle_max_length_type_sn = $this->vehicle_max_length_type_sn;
        $warehouse->vehicle_max_height = $this->vehicle_max_height;
        $warehouse->vehicle_max_height_type_sn = $this->vehicle_max_height_type_sn;
        $warehouse->vehicle_max_width = $this->vehicle_max_width;
        $warehouse->vehicle_max_width_type_sn = $this->vehicle_max_width_type_sn;
        $warehouse->vehicle_max_weight = $this->vehicle_max_weight;
        $warehouse->vehicle_max_weight_type_sn = $this->vehicle_max_weight_type_sn;

        $warehouse->external_is_set = true;

        if (!$warehouse->save()) {
            $transaction->rollBack();
            $this->addErrors($warehouse->errors);
            return false;
        }

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Сохраняем указанные "Системы контроля и учета" */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        WhAccountingControlSystem::deleteAll(['warehouse_id' => $warehouse->id]);

        foreach ($this->accounting_control_system_sn as $sn) {
            $wh_acs = new WhAccountingControlSystem();

            $wh_acs->warehouse_id = $warehouse->id;
            $wh_acs->accounting_control_system_sn = $sn;

            if (!$wh_acs->save()) {
                $transaction->rollBack();
                $this->addError('accounting_control_system_sn', 'Указано неверное значение');
                return false;
            }
        }

        /*~~~~~~~~~~~~~~~~~~~~~*/
        /* Уведомляем админов  */
        /*~~~~~~~~~~~~~~~~~~~~~*/

        if ($warehouse->internal_is_set && $warehouse->documents_is_set) {
            if (!Notification::notifyRoleGroup(
                    User::ROLE_ADMIN,
                    'Склад ожидает проверку',
                    "Склад " . '<a href="/admin/warehouse/external?id=' . $warehouse->id . '">' . "{$warehouse->title}</a> ожидает проверку внешних параметров"
            )
            ) {
                $transaction->rollBack();
                return false;
            }

            if (!Notification::notifyRoleGroup(
                User::ROLE_DSL,
                    'Склад ожидает проверку',
                    "Склад " . '<a href="/admin/warehouse/external?id=' . $warehouse->id . '">' . "{$warehouse->title}</a> ожидает проверку внешних параметров"
                )
            ) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }
}

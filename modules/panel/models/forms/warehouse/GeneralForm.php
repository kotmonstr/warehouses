<?php

namespace app\modules\panel\models\forms\warehouse;

use app\models\User;
use app\models\Warehouse;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Document;
use app\models\File;
use app\models\Notification;

/**
 * Class GeneralForm
 * @package app\modules\panel\models\forms\warehouse
 *
 * @property string $title
 * @property string $description
 *
 * @property string $address_ym_geocoder
 * @property string $address_lat
 * @property string $address_lon
 *
 * @property bool $work_hours_24_7
 * @property string $work_hours_weekdays_from
 * @property string $work_hours_weekdays_to
 * @property string $work_hours_weekends_from
 * @property string $work_hours_weekends_to
 *
 * @property UploadedFile $photo_1
 * @property UploadedFile $photo_2
 * @property UploadedFile $photo_3
 * @property UploadedFile $photo_4
 * @property UploadedFile $photo_5
 *
 * @property string $address_city_ym_geocoder
 */
class GeneralForm extends WarehouseForm
{
    const ALIAS = 'general';

    public $title;
    public $description;

    public $address_ym_geocoder;
    public $address_lat;
    public $address_lon;

    public $work_hours_24_7;
    public $work_hours_weekdays_from;
    public $work_hours_weekdays_to;
    public $work_hours_weekends_from;
    public $work_hours_weekends_to;

    public $photo_1;
    public $photo_2;
    public $photo_3;
    public $photo_4;
    public $photo_5;


    public $address_city_ym_geocoder;

    /** {@inheritdoc} */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title', 'description'], 'trim'],

            [['title', 'address_ym_geocoder', 'address_lat', 'address_lon', 'work_hours_24_7'], 'required'],

            [['title', 'address_ym_geocoder', 'address_lat', 'address_lon', 'address_city_ym_geocoder'], 'string', 'max' => 255],
            [['description', 'work_hours_weekdays_from', 'work_hours_weekdays_to', 'work_hours_weekends_from', 'work_hours_weekends_to'], 'string'],
            [['work_hours_24_7'], 'boolean'],

            [
                [
                    'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5'
                ], 'file',
                'skipOnEmpty' => true,
                'extensions' => ['png', 'jpg'],
                'maxSize' => 1024 * 1024 * 20,
            ],
        ]);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->work_hours_24_7 && (!$this->work_hours_weekdays_from || !$this->work_hours_weekdays_to)) {
            $this->addError('work_hours_weekdays_from', 'Необходимо указать рабочие часы');
            return false;
        }

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $model = Warehouse::findModel($this->id);
            $model->dsl_verification_status_sn = Warehouse::VERIFICATION_IN_PROGRESS;
            $model->sb_verification_status_sn = Warehouse::VERIFICATION_IN_PROGRESS;

        } else {
            /** @var User $identity */
            $identity = Yii::$app->user->identity;

            $model = new Warehouse();
            $model->organization_id = $identity->organization_id;
            $model->created_by = $identity->id;
        }

        $model->title = $this->title;
        $model->description = $this->description;

        $model->address_ym_geocoder = $this->address_ym_geocoder;
        $model->address_lat = $this->address_lat;
        $model->address_lon = $this->address_lon;
        $model->address_city_ym_geocoder = $this->address_city_ym_geocoder;

        $model->work_hours_24_7 = $this->work_hours_24_7;

        if (!$this->work_hours_24_7) {
            $model->work_hours_weekdays_from = $this->work_hours_weekdays_from;
            $model->work_hours_weekdays_to   = $this->work_hours_weekdays_to;
            $model->work_hours_weekends_from = $this->work_hours_weekends_from;
            $model->work_hours_weekends_to   = $this->work_hours_weekends_to;
        }

        $model->general_is_set = true;

        if (!$model->save()) {
            $transaction->rollBack();
            $this->addErrors($model->errors);
            return false;
        }

        $this->id = $model->id;

        /*~~~~~~~~~~~~~~~~*/
        /* Сохраняем фото */
        /*~~~~~~~~~~~~~~~~*/



        if ($this->photo_1) {
            $this->createDocWithFile($this->photo_1, 'photo_1', $transaction);
        }
        if ($this->photo_2) {
            $this->createDocWithFile($this->photo_2, 'photo_2', $transaction);
        }
        if ($this->photo_3) {
            $this->createDocWithFile($this->photo_3, 'photo_3', $transaction);
        }
        if ($this->photo_4) {
            $this->createDocWithFile($this->photo_4, 'photo_4', $transaction);
        }
        if ($this->photo_5) {
            $this->createDocWithFile($this->photo_5, 'photo_5', $transaction);
        }


        /*~~~~~~~~~~~~~~~~~~~~~*/
        /* Уведомляем админов  */
        /*~~~~~~~~~~~~~~~~~~~~~*/

        if ($model->internal_is_set && $model->external_is_set && $model->documents_is_set) {
            if (!Notification::notifyRoleGroup(
                    User::ROLE_ADMIN,
                    'Склад ожидает проверку',
                    "Склад " . '<a href="/admin/warehouse/general?id=' . $model->id . '">' . "{$model->title}</a> ожидает проверку основной вкладки"
            )
            ) {
                $transaction->rollBack();
                return false;
            }

            if (!Notification::notifyRoleGroup(
                User::ROLE_DSL,
                    'Склад ожидает проверку',
                    "Склад " . '<a href="/admin/warehouse/general?id=' . $model->id . '">' . "{$model->title}</a> ожидает проверку основной вкладки"
                )
            ) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }

    /**
     * Function for save document with file
     *
     * @param UploadedFile       $uploadedFile
     * @param string             $entity_field
     * @param yii\db\Transaction $transaction
     *
     * @return bool|null
     */
    function createDocWithFile($uploadedFile, $entity_field, $transaction)
    {
        if ($uploadedFile) {
            $document = new Document();
            $document->entity_classname = Warehouse::class;
            $document->entity_field = $entity_field;
            $document->entity_id = $this->id;
            $document->created_by = Yii::$app->user->identity->id;
            $document->file = $uploadedFile;

            if (!$document->save()) {
                $transaction->rollBack();
                $this->addErrors($document->errors);
                return false;
            }
        }
    }

    /**
     * Function for get paths of uploaded files
     *
     * @return string[]
     */
    public function findFilePaths()
    {
        $documents = Document::findAllByEntity($this->id, Warehouse::class);
        $photo_paths = [];
        if ($documents) {

            foreach ($documents as $document) {
                if ($document->entity_field && (substr_compare($document->entity_field, 'photo_', 0, 6) == 0)) {
                    $file = File::findOne(['document_id' => $document['id']]);
                    if ($file) {
                        $photo_paths[$document->entity_field] = $file->path;
                    }
                }
            }
        }
        return $photo_paths;
    }
}

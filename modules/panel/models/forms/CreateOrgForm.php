<?php

namespace app\modules\panel\models\forms;

use app\models\Document;
use app\models\User;
use Yii;
use yii\base\Model;
use app\models\Organization;
use Exception;
use yii\web\UploadedFile;
use app\models\Notification;

/**
 * Create Organization Form
 *
 * @property int    $id
 * @property string $title
 * @property string $legal_type
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $okpo
 * @property string $main_activity
 * @property string $taxation_type
 * @property string $address_ym_geocoder
 *
 * @property string $shareholders
 * @property string $edm_type
 * @property string $post_address
 * @property string $fact_address
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $start_capital
 * @property string $registration_date
 * @property int    $employee_count
 * @property string $bank_details
 * @property string $authorized_head_contact
 * @property string $authorized_sign_contact
 * @property string $authorized_security_contact
 * @property string $partner_type
 *
 * @property UploadedFile $arendatorsList
 * @property UploadedFile $noDebtsReference
 * @property UploadedFile $rentContractCopy
 * @property UploadedFile $warehouseCadastralPassport
 * @property UploadedFile $propertyRightsCopy
 * @property UploadedFile $taxStampCopy
 * @property UploadedFile $taxRegistrationCopyUL
 * @property UploadedFile $naznachenieIspolnitelyaCopy
 * @property UploadedFile $doverennostNaPodpisanieCopy
 * @property UploadedFile $headSignatureCopy
 * @property UploadedFile $financialReportCopy
 * @property UploadedFile $passportCopyIP
 * @property UploadedFile $taxRegistrationCopyIP
 *
 * @property string $verification_status_sn
 * @property string $verification_comment
 */
class CreateOrgForm extends Model
{
    public $id;
    public $title;
    public $legal_type;
    public $inn;
    public $ogrn;
    public $kpp;
    public $okpo;
    public $main_activity;
    public $taxation_type;
    public $address_ym_geocoder;

    public $shareholders;
    public $edm_type;
    public $post_address;
    public $fact_address;
    public $contact_phone;
    public $contact_email;
    public $start_capital;
    public $registration_date;
    public $employee_count;
    public $bank_details;
    public $authorized_head_contact;
    public $authorized_sign_contact;
    public $authorized_security_contact;
    public $partner_type;

    public $verification_status_sn;
    public $verification_comment;

    // Common Files
    // Список арендаторов, имеющих право пользоваться складом (если их несколько)
    public $arendatorsList;
    // Справка налогового органа об отсутствии задолженностей по налогам и сборам.
    public $noDebtsReference;
    // Копия договора аренды или свидетельства о собственности на помещение (либо выписка из ЕГРН) по месту регистрации и фактическому месту нахождения.
    public $rentContractCopy;
    // Кадастровый паспорт складского помещения, сдаваемого в аренду
    public $warehouseCadastralPassport;
    // Копия свидетельства о праве собственности на складское помещение, сдаваемое в аренду
    public $propertyRightsCopy;

    // UL Files
    // Копия устава со штампом налогового органа (актуальная редакция, с учетом внесенных изменений).
    public $taxStampCopy;
    // Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРЮЛ).
    public $taxRegistrationCopyUL;
    // Копия решения и копия приказа о назначении единоличного исполнительного органа (либо о передаче полномочий единоличного исполнительного органа управляющей компании).
    public $naznachenieIspolnitelyaCopy;
    // Копия доверенности на право подписи договоров и первичных документов: актов, накладных, счетов, счетов-фактур, УПД от имени контрагента (в случае подписания таких документов лицами, не имеющими права действовать от имени юридического лица без доверенности).
    public $doverennostNaPodpisanieCopy;
    // Копия образца подписи руководителя, заверенная нотариально или банком (заверенная копия банковской карточки) либо копии страниц 2, 3 паспорта руководителя.
    public $headSignatureCopy;
    // Копия бухгалтерской отчетности на последнюю отчетную дату перед заключением договора (форма 1,2,3,4, пояснения) с квитанций об электронной сдаче отчетности.
    public $financialReportCopy;

    // IP Files
    // Копии страниц 2, 3 паспорта индивидуального предпринимателя.
    public $passportCopyIP;
    // Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРИП).
    public $taxRegistrationCopyIP;

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'legal_type', 'inn', 'ogrn', 'main_activity', 'taxation_type', 'address_ym_geocoder'], 'required'],
            [
                [
                    'shareholders', 'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'employee_count', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'required', 'when' => function (self $model) {
                    return $model->legal_type == 'UL';
                }
            ],
            [
                [
                    'kpp', 'okpo', 'main_activity', 'taxation_type',
                    'address_ym_geocoder', 'legal_type',
                    'verification_status_sn',
                    'edm_type', 'post_address', 'fact_address', 'contact_phone', 'contact_email',
                    'start_capital', 'registration_date', 'bank_details',
                    'authorized_head_contact', 'authorized_sign_contact', 'authorized_security_contact', 'partner_type',
                ], 'string', 'max' => 255
            ],
            [
                [
                    'taxStampCopy',
                    'taxRegistrationCopyUL',
                    'naznachenieIspolnitelyaCopy',
                    'doverennostNaPodpisanieCopy',
                    'headSignatureCopy',
                    'financialReportCopy',
                    'arendatorsList',
                    'noDebtsReference',
                    'rentContractCopy',
                    'warehouseCadastralPassport',
                    'propertyRightsCopy',
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'pdf',
                'minSize' => 1,
                'when' => function ($model) {
                    return $model->legal_type == 'UL';
                }
            ],
            [
                [
                    'passportCopyIP',
                    'taxRegistrationCopyIP',
                    'arendatorsList',
                    'noDebtsReference',
                    'rentContractCopy',
                    'warehouseCadastralPassport',
                    'propertyRightsCopy',
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'pdf',
                'minSize' => 1,
                'when' => function ($model) {
                    return $model->legal_type == 'IP';
                }
            ],
            [['verification_comment', 'shareholders'], 'string'],
            [['id', 'employee_count'], 'integer'],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        // Todo: temp solution
        $orgModel = new Organization();

        return array_merge($orgModel->attributeLabels(), [
            'arendatorsList' => 'Список арендаторов, имеющих право пользоваться складом',
            'noDebtsReference' => 'Справка налогового органа об отсутствии задолженностей по налогам и сборам',
            'rentContractCopy' => 'Копия договора аренды или свидетельства о собственности на помещение (либо выписка из ЕГРН) по месту регистрации и фактическому месту нахождения',
            'warehouseCadastralPassport' => 'Кадастровый паспорт складского помещения',
            'propertyRightsCopy' => 'Копия свидетельства о праве собственности на складское помещение, сдаваемое в аренду',
            'taxStampCopy' => 'Копия устава со штампом налогового органа (актуальная редакция, с учетом внесенных изменений)',
            'taxRegistrationCopyUL' => 'Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРЮЛ)',
            'naznachenieIspolnitelyaCopy' => 'Копия решения и копия приказа о назначении единоличного исполнительного органа (либо о передаче полномочий единоличного исполнительного органа управляющей компании)',
            'doverennostNaPodpisanieCopy' => 'Копия доверенности на право подписи договоров и первичных документов: актов, накладных, счетов, счетов-фактур, УПД от имени контрагента (в случае подписания таких документов лицами, не имеющими права действовать от имени юридического лица без доверенности)',
            'headSignatureCopy' => 'Копия образца подписи руководителя, заверенная нотариально или банком (заверенная копия банковской карточки) либо копии страниц 2, 3 паспорта руководителя',
            'financialReportCopy' => 'Копия бухгалтерской отчетности на последнюю отчетную дату перед заключением договора (форма 1,2,3,4, пояснения) с квитанций об электронной сдаче отчетности',
            'passportCopyIP' => 'Копии страниц 2, 3 паспорта индивидуального предпринимателя',
            'taxRegistrationCopyIP' => 'Копия свидетельства о постановке на учет в налоговом органе (либо лист записи ЕГРИП)',
        ]);
    }

    /**
     * create new organization
     *
     * @return bool is organization saved successfully
     */
    public function createOrganization()
    {
        if (Yii::$app->user->identity->organization_id) {
            $org = Organization::findModel(Yii::$app->user->identity->organization_id);
            $org->verification_status_sn = Organization::VERIFICATION_IN_PROGRESS;
        } else {
            $org = new Organization();
            $org->created_by = Yii::$app->user->identity->id;
        }

        $org->title = $this->title;
        $org->legal_type = $this->legal_type;
        $org->inn = $this->inn;
        $org->ogrn = $this->ogrn;
        $org->kpp = $this->kpp;
        $org->okpo = $this->okpo;
        $org->main_activity = $this->main_activity;
        $org->taxation_type = $this->taxation_type;
        $org->address_ym_geocoder = $this->address_ym_geocoder;

        $org->shareholders = $this->shareholders;
        $org->edm_type = $this->edm_type;
        $org->post_address = $this->post_address;
        $org->fact_address = $this->fact_address;
        $org->contact_phone = $this->contact_phone;
        $org->contact_email = $this->contact_email;
        $org->start_capital = $this->start_capital;
        $org->registration_date = $this->registration_date;
        $org->employee_count = $this->employee_count;
        $org->bank_details = $this->bank_details;
        $org->authorized_head_contact = $this->authorized_head_contact;
        $org->authorized_sign_contact = $this->authorized_sign_contact;
        $org->authorized_security_contact = $this->authorized_security_contact;
        $org->partner_type = $this->partner_type;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $org->save();

            $this->id = $org->id;

            $this->createDocument($org->id, 'arendatorsList', $this->arendatorsList);
            $this->createDocument($org->id, 'noDebtsReference', $this->noDebtsReference);
            $this->createDocument($org->id, 'rentContractCopy', $this->rentContractCopy);
            $this->createDocument($org->id, 'warehouseCadastralPassport', $this->warehouseCadastralPassport);
            $this->createDocument($org->id, 'propertyRightsCopy', $this->propertyRightsCopy);

            if ($this->legal_type == 'UL') {
                $this->createDocument($org->id, 'taxStampCopy', $this->taxStampCopy);
                $this->createDocument($org->id, 'taxRegistrationCopyUL', $this->taxRegistrationCopyUL);
                $this->createDocument($org->id, 'naznachenieIspolnitelyaCopy', $this->naznachenieIspolnitelyaCopy);
                $this->createDocument($org->id, 'doverennostNaPodpisanieCopy', $this->doverennostNaPodpisanieCopy);
                $this->createDocument($org->id, 'headSignatureCopy', $this->headSignatureCopy);
                $this->createDocument($org->id, 'financialReportCopy', $this->financialReportCopy);
            } else {
                $this->createDocument($org->id, 'passportCopyIP', $this->passportCopyIP);
                $this->createDocument($org->id, 'taxRegistrationCopyIP', $this->taxRegistrationCopyIP);
            }

            if (!Yii::$app->user->identity->organization_id) {
                $user = User::findModel(Yii::$app->user->identity->id);
                $user->organization_id = $org->id;
                $user->save();
            }

            if (!Notification::notifyRoleGroup(
                    User::ROLE_ADMIN,
                    'Компания ожидает проверку',
                    "Компания " . '<a href="/admin/organization/organization?id=' . $this->id . '">' . "{$this->title}</a> ожидает проверку"
                )
            ) {
                $transaction->rollBack();
                return false;
            }

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * create new document
     *
     * @param  string $orgId organization id
     * @param  string $entityField relationship to field of the form
     * @param  UploadedFile $uploadedFile
     * @return void
     */
    public function createDocument($orgId, $entityField, $uploadedFile)
    {
        $doc = new Document();

        $doc->file = $uploadedFile;
        $doc->entity_field = $entityField;
        $doc->entity_classname = Organization::class;
        $doc->entity_id = $orgId;
        $doc->created_by = Yii::$app->user->identity->id;

        $doc->save();
    }
}

<?php

namespace app\modules\panel\models\forms\reserve;

use app\models\User;
use app\validators\NumberValidator;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reserve;

/**
 * ReserveSearchForm represents the model behind the search form of `app\models\Reserve`.
 */
class ReserveSearchForm extends Reserve
{
    public $warehouse_id;
    public $storage_type_sn;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'advert_id', 'created_at', 'deleted_at', 'updated_at', ], 'integer'],
            [['rent_from','rent_to', 'storage_type_sn', 'warehouse_id', 'status_sn'], 'string'],
            [['amount', 'cost'], NumberValidator::class],
            [['amount_type_sn', 'contact_fio', 'contact_phone_number', 'contact_email', 'comment'], 'safe'],
            [['rent_to'], 'compare', 'compareAttribute' => 'rent_from', 'operator' => '>=', 'skipOnEmpty' => true],
        ];
    }
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'warehouse_id'=>'Склад',
            'storage_type_sn' => 'Тип',
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()->where(['reserve.deleted_at' => null])
            ->joinWith('advert.warehouse')
            ->andWhere([
                'warehouse.organization_id' => Yii::$app->user->identity->organization_id
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);


        $this->load($params);

        if ($this->validate()) {
            $query->AndFilterWhere(['warehouse.id' => $this->warehouse_id]);

            $query->AndFilterWhere(['storage_type_sn' => $this->storage_type_sn]);

            $query->AndFilterWhere(['<=', 'rent_from', $this->rent_from ? Yii::$app->formatter->format($this->rent_from, 'timestamp') : null]);
            $query->AndFilterWhere(['>=', 'rent_to', $this->rent_from ? Yii::$app->formatter->format($this->rent_from, 'timestamp') : null]);

            $query->AndFilterWhere(['<=', 'rent_from', $this->rent_to ? Yii::$app->formatter->format($this->rent_to, 'timestamp') : null]);
            $query->AndFilterWhere(['>=', 'rent_to', $this->rent_to ? Yii::$app->formatter->format($this->rent_to, 'timestamp') : null]);
            $query->AndFilterWhere(['reserve.status_sn' => $this->status_sn]);


            return $dataProvider;
        }
        return $dataProvider;
    }
}

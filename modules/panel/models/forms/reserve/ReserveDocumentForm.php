<?php

namespace app\modules\panel\models\forms\reserve;

use app\models\Reserve;
use yii\helpers\ArrayHelper;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Yii;
use app\models\Document;
use yii\base\Model;
use yii\base\Exception;

/**
 * This is the model class for downloading files for reserve.
 *
 * @property Document[]   $documents
 *
 * @property UploadedFile $incomingOrder
 * @property UploadedFile $mx1
 * @property UploadedFile $mx1Complaint
 * @property UploadedFile $outgoingOrder
 * @property UploadedFile $mx3
 * @property UploadedFile $mx3Complaint
 * @property UploadedFile $completionCert
 */
class ReserveDocumentForm extends Model
{
    public $documents;

    public $incomingOrder;  // Задания на склад на привоз
    public $mx1;            // МХ-1 (постановка на хранение)
    public $mx1Complaint;   // МХ-1 рекламация
    public $outgoingOrder;  // Задание на склад на забор
    public $mx3;            // МХ-3 (снятие с хранения)
    public $mx3Complaint;   // МХ-3 рекламация
    public $completionCert; // АВР (акт выполненных работ)

    const DOC_TYPES = [
        'incomingOrder',
        'mx1',
        'mx1Complaint',
        'outgoingOrder',
        'mx3',
        'mx3Complaint',
        'completionCert',
    ];

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['incomingOrder', 'outgoingOrder', 'mx1', 'mx3', 'mx1Complaint', 'mx3Complaint', 'completionCert'], 'file',
                'extensions' => ['pdf', 'jpg', 'jpeg'], 'maxSize' => 10 * 1024 * 1024,
            ],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'incomingOrder'  => 'Задания на склад на привоз',
            'mx1'            => 'МХ-1 (постановка на хранение)',
            'mx1Complaint'   => 'Рекламация',
            'outgoingOrder'  => 'Задание на склад на забор',
            'mx3'            => 'МХ-3 (снятие с хранения)',
            'mx3Complaint'   => 'Рекламация',
            'completionCert' => 'АВР (акт выполненных работ)',
        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function upload($id)
    {
        if (!$this->validate()) {
            return false;
        }

        $documents   = Document::findAllByEntity($id, Reserve::class);
        $existTypes  = ArrayHelper::getColumn($documents, 'entity_field');

        $transaction = Yii::$app->db->beginTransaction();

        foreach (self::DOC_TYPES as $type) {
            if (empty($this->$type)) {
                continue;
            }

            if (in_array($type, $existTypes)) {
                Document::deleteAll([
                    'entity_classname' => Reserve::class,
                    'entity_field'     => $type,
                    'entity_id'        => $id,
                ]);
            }

            $doc = new Document();

            $doc->file             = $this->$type;
            $doc->entity_field     = $type;
            $doc->entity_classname = Reserve::class;
            $doc->entity_id        = $id;
            $doc->created_by       = Yii::$app->user->identity->id;

            if (!$doc->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }
}

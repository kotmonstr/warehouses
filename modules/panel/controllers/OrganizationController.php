<?php

namespace app\modules\panel\controllers;

use yii\filters\VerbFilter;
use Yii;
use app\modules\panel\models\forms\OrgSearchForm;
use app\modules\panel\models\forms\CreateOrgForm;
use app\models\Organization;
use app\models\Membership;
use app\models\Document;
use app\models\File;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\base\Exception;

/**
 * This is the controller class for organization
 * with index, search, create, sendMembershipRequest,
 * downloadFile actions.
 */
class OrganizationController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            // 'access' => [],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'search' => ['GET', 'POST'],
                    'create' => ['GET', 'POST'],
                    'download-file' => ['GET', 'POST'],
                    'delete-file' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * actionIndex
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $userMembershipRequest = MembershipRequest::findByCurrentUser();
        if ($userMembershipRequest) {
            Yii::$app->session->addFlash('bg-success', 'Заявка на присоединение к компании отправлена');
            $userOrganization = Organization::findByMembemshipRequest();
        } else {
            $userOrganization = Organization::findByCurrentUser();
        }

        if ($userOrganization) {

            if ($userOrganization->verification_status_sn == Organization::VERIFICATION_IN_PROGRESS) {
                Yii::$app->session->addFlash('bg-success', 'Заявка на верификацию подана, ожидается проверка службой безопасности');
            }

            $data = Document::findAllByEntity($userOrganization->id, Organization::class);
            $documents = [];
            foreach ($data as $doc) {
                $documents[$doc['entity_field']] = $doc;
            }

            if ($userOrganization->verification_status_sn == Organization::VERIFICATION_FAILED
                && $userOrganization->created_by == Yii::$app->user->identity->id) {
                Yii::$app->session->addFlash('bg-warning', "Заявка на верификацию отклонена с комментарием: {$userOrganization->verification_comment}");
                return $this->redirect(['create?id=' . $userOrganization->id]);
            }

            return $this->render('user-organization', [
                'model' => $userOrganization,
                'documents' => $documents
            ]);
        }

        return $this->render('index');
    }

    /**
     * actionSearch
     *
     * @return Response|string
     */
    public function actionSearch()
    {
        $model = new OrgSearchForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $existingOrg = Organization::findByINN($model->inn);

            if ($existingOrg) {
                return $this->render('search-result', ['existingOrg' => $existingOrg]);
            } else {
                $orgData = Yii::$app->konturApi->findOrganizationByINN($model->inn);

                if (isset($orgData['error'])) {
                    Yii::$app->session->addFlash('bg-danger', 'Некорректный ИНН');
                    return $this->redirect(['/panel/organization']);
                }

                $newOrg = new CreateOrgForm();
                $newOrg->inn = $orgData['inn'];
                $newOrg->legal_type = $orgData['legal_type'];
                $newOrg->title = $orgData['title'];
                $newOrg->ogrn = $orgData['ogrn'];
                $newOrg->kpp = $orgData['kpp'];
                $newOrg->okpo = $orgData['okpo'];
                $newOrg->main_activity = $orgData['mainActivity'];
                $newOrg->taxation_type = $orgData['taxationType'];
                $newOrg->address_ym_geocoder = isset($orgData['address']) ? $orgData['address'] : '';
                $newOrg->shareholders = isset($orgData['shareholders']) ? $orgData['shareholders'] : '';
                $newOrg->start_capital = isset($orgData['start_capital']) ? $orgData['start_capital'] : '';
                $newOrg->registration_date = $orgData['registration_date'];

                return $this->render('create', [
                    'model' => $newOrg,
                    'documents' => []
                ]);
            }
        }
    }

    /**
     * actionCreate
     *
     * @param null $id
     *
     * @return Response|string
     */
    public function actionCreate($id = null)
    {
        $model = new CreateOrgForm();
        $documents = [];

        if ($id) {
            $org = Organization::findModel($id);
            $model = new CreateOrgForm();
            $model->load($org->attributes, '');
            $data = Document::findAllByEntity($id, Organization::class);
            $documents = [];
            foreach ($data as $doc) {
                $documents[$doc['entity_field']] = $doc;
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($id && $org->verification_status_sn == Organization::VERIFICATION_RESTRICTED) {
                throw new Exception('Organization verification restricted');
            }

            $model->arendatorsList = UploadedFile::getInstance($model, 'arendatorsList');
            $model->noDebtsReference = UploadedFile::getInstance($model, 'noDebtsReference');
            $model->rentContractCopy = UploadedFile::getInstance($model, 'rentContractCopy');
            $model->warehouseCadastralPassport = UploadedFile::getInstance($model, 'warehouseCadastralPassport');
            $model->propertyRightsCopy = UploadedFile::getInstance($model, 'propertyRightsCopy');

            if ($model->legal_type == 'UL') {
                $model->taxStampCopy = UploadedFile::getInstance($model, 'taxStampCopy');
                $model->taxRegistrationCopyUL = UploadedFile::getInstance($model, 'taxRegistrationCopyUL');
                $model->naznachenieIspolnitelyaCopy = UploadedFile::getInstance($model, 'naznachenieIspolnitelyaCopy');
                $model->doverennostNaPodpisanieCopy = UploadedFile::getInstance($model, 'doverennostNaPodpisanieCopy');
                $model->headSignatureCopy = UploadedFile::getInstance($model, 'headSignatureCopy');
                $model->financialReportCopy = UploadedFile::getInstance($model, 'financialReportCopy');
            } else {
                $model->passportCopyIP = UploadedFile::getInstance($model, 'passportCopyIP');
                $model->taxRegistrationCopyIP = UploadedFile::getInstance($model, 'taxRegistrationCopyIP');
            }

            if ($model->validate()) {
                $success = $model->createOrganization();
                if (!$success) {
                    Yii::$app->session->addFlash('bg-danger', 'Не удалось создать компанию');
                }
                return $this->redirect(['/panel/organization']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'documents' => $documents
        ]);
    }

    /**
     * actionSendMembershipRequest
     *
     * @return Response|string
     */
    public function actionSendMembershipRequest()
    {
        $newMembershipRequest = new MembershipRequest();

        $newMembershipRequest->user_id = Yii::$app->user->identity->id;
        $newMembershipRequest->organization_id = Yii::$app->request->post('organizationId');

        $success = $newMembershipRequest->save();
        if (!$success) {
            Yii::$app->session->addFlash('bg-danger', 'Не удалось отправить заявку');
        }

        return $this->redirect(['/panel/organization']);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        $document = Document::findModel($id);
        $file = $document->file_db;

        if (!$file->exists()) {
            throw new NotFoundHttpException('File not found');
        }

        Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $file->path, $file->name);
    }

    /**
     * Delete file action.
     *
     * @param string $path to file
     *
     * @return Response|string
     */
    public function actionDeleteFile($id)
    {
        $document = Document::findOne(['id' => $id]);
        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }
        if ($document->created_by != Yii::$app->user->identity->id) {
            throw new Exception('This is not yours document');
        }
        $file = File::findOne(['document_id' => $id]);
        if (!$file) {
            throw new NotFoundHttpException('File not founded');
        }
        $path = $file->path;
        $orgId = $document->entity_id;

        if ($file->delete() && $document->delete()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Файл успешно удален');
            unlink(Yii::getAlias('@webroot') . $path);
        }

        return $this->redirect(['create?id=' . $orgId]);
    }
}

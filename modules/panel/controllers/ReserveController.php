<?php

namespace app\modules\panel\controllers;

use app\models\Document;
use app\modules\panel\models\forms\reserve\ReserveDocumentForm;
use app\modules\panel\models\forms\reserve\ReserveSearchForm;
use Yii;
use app\models\Reserve;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\File;
use yii\base\Exception;

/**
 * ReserveController implements the CRUD actions for Reserve model.
 */
class ReserveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['view', 'index', 'delete-file', 'download-file'],
                'rules' => [
                    [
                        'actions' => ['view', 'index', 'delete-file', 'download-file'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'view' => ['GET', 'POST'],
                    'index' => ['GET'],
                    'delete-file' => ['GET', 'POST'],
                    'download-file' => ['GET', 'POST'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Reserve models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReserveSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reserve model.
     * @param integer $id
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionView($id)
    {
        $reserve = Reserve::findOne($id);
        $documentsModel = new ReserveDocumentForm();

        $documents = ArrayHelper::index(Document::findAllByEntity($id, Reserve::class), 'entity_field');
        $documentsModel->documents = $documents;

        if ($documentsModel->load(Yii::$app->request->post())) {

            foreach (ReserveDocumentForm::DOC_TYPES as $type) {
                $documentsModel->$type = UploadedFile::getInstance($documentsModel, $type);
            }

            if ($documentsModel->upload($reserve->id)) {
                Yii::$app->session->addFlash('bg-success', "Документы успешно загружены");
                return $this->redirect('index');
            }
        }

        return $this->render('view', [
            'documentModel' => $documentsModel,
            'documents' => $documents,
            'model' => $reserve,
        ]);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        $document = Document::findOne($id);
        $file = $document->file_db;

        if (!$file->exists()) {
            throw new NotFoundHttpException('File not found');
        }

        Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $file->path, $file->name);
    }

    /**
     * @param $id
     * @param $reserveId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFile($id, $reserveId)
    {
        $document = Document::findOne(['id' => $id]);

        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }

        $path = $document->file_db->path;

        if ($document->delete()) {
            unlink(Yii::getAlias('@webroot') . $path);
            Yii::$app->session->addFlash('bg-info', "Документ успешно удален");
            return $this->redirect(['/panel/reserve/view?id=' . $reserveId]);
        }
    }
}

<?php

namespace app\modules\panel\controllers;

use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Notification;
use yii\web\NotFoundHttpException;
/**
 * Class NotificationController
 * @package app\modules\panel\controllers
 */
class NotificationController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'delete', 'read'],
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'read'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'read' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $query = Notification::find()->where(['user_id' => Yii::$app->user->identity->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Delete action.
     *
     * @param string|int $id notification
     *
     * @return Response|string
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $notification = Notification::findModel($id);

        if ($notification->delete()) {
            Yii::$app->session->addFlash('bg-success', 'Уведомление удалено');
        } else {
            Yii::$app->session->addFlash('bg-danger', 'Ошибка при удалении уведомления');
        }

        return $this->redirect(['index']);
    }

    /**
     * Read action.
     *
     * @param string|int $id notification
     *
     * @return Response|string
     */
    public function actionRead($id)
    {
        if (Notification::readNotification($id)) {
            $resp = (object) ['status' => 200, 'text' => 'success'];
        } else {
            $resp = (object) ['status' => 400, 'text' => 'error'];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Json::encode($resp);
    }
}

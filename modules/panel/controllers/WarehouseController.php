<?php

namespace app\modules\panel\controllers;

use app\models\Warehouse;
use app\modules\panel\models\forms\warehouse\GeneralForm;
use app\modules\panel\models\forms\warehouse\InternalForm;
use app\modules\panel\models\forms\warehouse\ExternalForm;
use app\modules\panel\models\forms\warehouse\DocumentsForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\modules\panel\models\forms\warehouse\SearchForm;
use yii\web\UploadedFile;
use app\models\File;
use app\models\Document;
use yii\web\NotFoundHttpException;
use yii\base\Exception;
use app\models\WhLoadUnloadCranageLuType;
use app\models\WhLoadUnloadEquipmentLuType;

/**
 * Class WarehouseController
 * @package app\modules\panel\controllers
 */
class WarehouseController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'index',
                    'general', 'internal', 'external', 'documents',
                    'delete', 'delete-file', 'download-file'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'general', 'internal', 'external', 'documents',
                            'delete', 'delete-file', 'download-file'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'general' => ['GET', 'POST'],
                    'internal' => ['GET', 'POST'],
                    'external' => ['GET', 'POST'],
                    'documents' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'delete-file' => ['GET', 'POST'],
                    'download-file' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchModel = new SearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]
        );
    }

    /**
     * @param null $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionGeneral($id = null)
    {
        $form = new GeneralForm();
        $form->scenario = GeneralForm::SCENARIO_CREATE;
        $documents = [];

        if ($id) {
            $model = Warehouse::findModel($id);
            if ($model->created_by != Yii::$app->user->identity->id) {
                return $this->redirect(['index']);
            }

            $form = new GeneralForm();
            $form->scenario = GeneralForm::SCENARIO_UPDATE;
            $form->load($model->attributes, '');
            $documents = $form->findFilePaths();
        }

        if ($form->load(Yii::$app->request->post())) {

            if ($id && ($model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                || $model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED)
            ) {
                throw new Exception('Warehouse verification restricted');
            }

            $form->photo_1 = UploadedFile::getInstance($form, 'photo_1');
            $form->photo_2 = UploadedFile::getInstance($form, 'photo_2');
            $form->photo_3 = UploadedFile::getInstance($form, 'photo_3');
            $form->photo_4 = UploadedFile::getInstance($form, 'photo_4');
            $form->photo_5 = UploadedFile::getInstance($form, 'photo_5');
            if ($form->save()) {
                return $this->redirect([ExternalForm::ALIAS, 'id' => $form->id]);
            }
        }

        return $this->render('form', [
            'current'    => GeneralForm::ALIAS,
            'firstEmpty' => isset($model) ? $model->getFirstEmptyTab() : GeneralForm::ALIAS,
            'title'      => isset($model) ? $model->title : null,
            'model'      => $form,
            'documents' => $documents,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws NotFoundHttpException
     */
    public function actionExternal($id)
    {
        $model = Warehouse::findModel($id);
        if ($model->created_by != Yii::$app->user->identity->id) {
            return $this->redirect(['index']);
        }

        $form = new ExternalForm();
        $form->scenario = ExternalForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');

        /* Предзагрузка связанных сущностей */
        $form->accounting_control_system_sn = ArrayHelper::getColumn($model->wh_accounting_control_system, 'accounting_control_system_sn');

        if ($form->load(Yii::$app->request->post())) {

            if ($model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                || $model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            ) {
                throw new Exception('Warehouse verification restricted');
            }

            if ($form->save()) {
                return $this->redirect([InternalForm::ALIAS, 'id' => $form->id]);
            }
        }

        return $this->render('form', [
            'current'    => ExternalForm::ALIAS,
            'firstEmpty' => $model->getFirstEmptyTab(),
            'title'      => $model->title,
            'model'      => $form,
            'documents' => [],
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws NotFoundHttpException
     */
    public function actionInternal($id)
    {
        $model = Warehouse::findModel($id);
        if ($model->created_by != Yii::$app->user->identity->id) {
            return $this->redirect(['index']);
        }

        $form = new InternalForm();
        $form->scenario = InternalForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');

        if ($form->load(Yii::$app->request->post())) {

            if ($model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                || $model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            ) {
                throw new Exception('Warehouse verification restricted');
            }

            if ($form->save()) {
                return $this->redirect([DocumentsForm::ALIAS, 'id' => $form->id]);
            }
        }

        /* Предзагрузка связанных сущностей */
        $form->storage_system_sns = ArrayHelper::getColumn($model->wh_storage_system, 'storage_system_sn');
        $form->loading_unloading_struct_types_sn = ArrayHelper::getColumn($model->wh_loading_unloading_struct_type, 'loading_unloading_struct_type_sn');
        $form->office_facility_types_inside_sn = ArrayHelper::getColumn(
            $model->wh_office_facility_type,
            function ($element) {
                if ($element['types'] == 'inside') {
                    return $element['office_facility_types_sn'];
                }
            }
        );
        $form->office_facility_types_outside_sn = ArrayHelper::getColumn(
            $model->wh_office_facility_type,
            function ($element) {
                if ($element['types'] == 'outside') {
                    return $element['office_facility_types_sn'];
                }
            }
        );

        $form->loading_unloading_tech_types = $model->wh_load_unload_equipment;
        foreach ($form->loading_unloading_tech_types as $tech) {
            $tech->loading_unloading_method_sn = ArrayHelper::getColumn(
                WhLoadUnloadEquipmentLuType::findAll(['equipment_id' => $tech->id]),
                'loading_unloading_method_sn'
            );
        }

        $form->cranage_types = $model->wh_load_unload_cranage;
        foreach ($form->cranage_types as $cranage) {
            $cranage->loading_unloading_method_sn = ArrayHelper::getColumn(
                WhLoadUnloadCranageLuType::findAll(['equipment_id' => $cranage->id]),
                'loading_unloading_method_sn'
            );
        }

        return $this->render('form', [
            'current'    => InternalForm::ALIAS,
            'firstEmpty' => $model->getFirstEmptyTab(),
            'title'      => $model->title,
            'model'      => $form,
            'documents' => [],
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws NotFoundHttpException
     */
    public function actionDocuments($id)
    {
        $model = Warehouse::findModel($id);
        if ($model->created_by != Yii::$app->user->identity->id) {
            return $this->redirect(['index']);
        }

        $form = new DocumentsForm();
        $form->scenario = DocumentsForm::SCENARIO_UPDATE;
        $form->load($model->attributes, '');
        $documents = $form->findFilePaths();

        if ($form->load(Yii::$app->request->post())) {

            if ($model->dsl_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
                || $model->sb_verification_status_sn == Warehouse::VERIFICATION_RESTRICTED
            ) {
                throw new Exception('Warehouse verification restricted');
            }

            $form->documents_file_1 = UploadedFile::getInstance($form, 'documents_file_1');
            $form->documents_file_2 = UploadedFile::getInstance($form, 'documents_file_2');
            $form->documents_file_3 = UploadedFile::getInstance($form, 'documents_file_3');
            if ($form->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('form', [
            'current'    => DocumentsForm::ALIAS,
            'firstEmpty' => $model->getFirstEmptyTab(),
            'title'      => $model->title,
            'model'      => $form,
            'documents' => $documents,
        ]);
    }

    /**
     * Delete action.
     *
     * @param $id
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = Warehouse::findModel($id);
        if ($model->created_by != Yii::$app->user->identity->id) {
            throw new Exception('This is not yours warehouse');
        }
        if ($model->delete()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Склад успешно удален');
        }

        return $this->redirect(['index']);
    }

    /**
     * Delete file action.
     *
     * @param string $path to file
     *
     * @return Response|string
     */
    public function actionDeleteFile($path)
    {
        $file = File::findOne(['path' => $path]);
        if (!$file) {
            throw new NotFoundHttpException('File not founded');
        }
        $document = Document::findOne(['id' => $file->document_id]);
        if (!$document) {
            throw new NotFoundHttpException('Document not founded');
        }
        if ($document->created_by != Yii::$app->user->identity->id) {
            throw new Exception('This is not yours document');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($file->delete() && $document->delete()) {
            unlink(Yii::getAlias('@webroot') . $path);
            Yii::$app->getSession()->addFlash('bg-info', 'Файл успешно удален');
            $resp = (object)['status' => 200, 'text' => 'success'];
        } else {
            $resp = (object)['status' => 400, 'text' => 'error'];
        }
        return Json::encode($resp);
    }

    /**
     * Download file action.
     *
     * @param string $path to file
     *
     * @return Response|string
     */
    public function actionDownloadFile($path)
    {
        if (file_exists(Yii::getAlias('@webroot') . $path)) {
            Yii::$app->response->sendFile(Yii::getAlias('@webroot') . $path);
        }
    }
}

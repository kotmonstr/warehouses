<?php

namespace app\modules\panel\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Membership;
use app\models\User;
use yii\web\NotFoundHttpException;

/**
 * Class TeamController
 * @package app\modules\panel\controllers
 */
class TeamController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [ 'index', 'update', 'delete' ],
                'rules' => [
                    [
                        'actions' => [ 'index', 'update', 'delete' ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    'update' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $queryRequest = Membership::find()->where(['organization_id' => Yii::$app->user->identity->organization_id]);

        $dataProviderRequests = new ActiveDataProvider([
            'query' => $queryRequest,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $queryTeam = User::find()->where(['organization_id' => Yii::$app->user->identity->organization_id]);

        $dataProviderTeam = new ActiveDataProvider([
            'query' => $queryTeam,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index', [
            'dataProviderRequests' => $dataProviderRequests,
            'dataProviderTeam' => $dataProviderTeam,
        ]);
    }

    /**
     * Update action.
     *
     * @param string|int $id MembershipRequest
     *
     * @return Response|string
     */
    public function actionUpdate($id)
    {
        if (Membership::addUser($id, Yii::$app->user->identity->organization_id)) {
            Yii::$app->session->addFlash('bg-success', 'Пользователь был успешно добавлен в вашу компанию');
        } else {
            Yii::$app->session->addFlash('bg-danger', 'Ошибка при попытке добавления пользователя');
        }
        return $this->redirect(['index']);
    }

    /**
     * Delete action.
     *
     * @param string|int $id MembershipRequest
     *
     * @return Response|string
     */
    public function actionDelete($id)
    {
        $membershipRequest = Membership::findOne(['id' => $id]);
        if (!$membershipRequest) {
            throw new NotFoundHttpException('Membership request not founded');
        }

        if ($membershipRequest->delete()) {
            Yii::$app->session->addFlash('bg-success', 'Вы отклонили заявку пользователя на вступление');
        } else {
            Yii::$app->session->addFlash('bg-danger', 'Ошибка при попытке отклонить заявку пользователя на вступление');
        }
        return $this->redirect(['index']);
    }

}

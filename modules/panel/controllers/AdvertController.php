<?php

namespace app\modules\panel\controllers;

use app\models\Advert;
use app\modules\panel\models\forms\AdvertSearchForm;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * This is the controller class for advert
 */
class AdvertController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'update', 'delete', 'index'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['GET', 'POST'],
                    'update' => ['GET', 'POST'],
                    'delete' => ['GET', 'POST'],
                    'index'  => ['GET'],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Index action.
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        $searchModel = new AdvertSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $advert = new Advert();

        if ($advert->load(Yii::$app->request->post())) {

            $advert->created_by = Yii::$app->user->identity->id;

            if ($advert->validate() && $advert->save()) {
                Yii::$app->getSession()->addFlash('bg-info', 'Изменения успешно сохранены');
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $advert
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $advert = Advert::findOne($id);

        if ($advert->created_by != Yii::$app->user->identity->id) {
            return $this->redirect(['index']);
        }

        if ($advert->load(Yii::$app->request->post())) {

            if ($advert->validate() && $advert->save()) {
                Yii::$app->getSession()->addFlash('bg-info', 'Изменения успешно сохранены');
                return $this->redirect('index');
            }
        }

        return $this->render('update', [
            'model' => $advert,
        ]);
    }

    /**
     * Delete action
     *
     * @param string|int $id advert
     *
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $advert = Advert::findModel($id);

        if ($advert->created_by != Yii::$app->user->identity->id) {
            return $this->redirect(['index']);
        }

        if ($advert->delete()) {
            Yii::$app->getSession()->addFlash('bg-info', 'Объявление успешно удаленно');
        }

        return $this->redirect(['index']);
    }
}

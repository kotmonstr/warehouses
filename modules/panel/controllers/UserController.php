<?php
/**
 * This is the file with controller class for holder
 * php version 7
 */
namespace app\modules\panel\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\modules\panel\models\forms\ChangeUserDataForm;
use app\modules\panel\models\forms\ChangePasswordForm;
use yii\widgets\ActiveForm;

/**
 * This is the controller class for holder
 * with changeUserData, changePassword actions.
 */
class UserController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['change-user-data'],
                'rules' => [
                    [
                        'actions' => ['change-user-data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'change-user-data' => ['GET', 'POST'],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * ChangeUserData action.
     *
     * @return Response|string
     */
    public function actionChangeUserData()
    {
        $modelData = new ChangeUserDataForm();
        $modelPassword = new ChangePasswordForm();

        if (Yii::$app->request->isAjax && $modelData->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($modelData);
        }

        if ($modelData->load(Yii::$app->request->post()) && $modelData->save()) {
            $this->refresh();
        }

        if (Yii::$app->request->isAjax && $modelPassword->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            /** @noinspection PhpFullyQualifiedNameUsageInspection */
            return ActiveForm::validate($modelPassword);
        }

        if ($modelPassword->load(Yii::$app->request->post()) && $modelPassword->save()) {
            Yii::$app->getSession()->addFlash('bg-success', 'Пароль успешно изменён.');
            $modelPassword = new ChangePasswordForm();
        }

        return $this->render('change_user_data', ['model' => $modelData, 'modelPassword' =>$modelPassword]);
    }
}

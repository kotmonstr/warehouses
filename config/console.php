<?php

$common = require __DIR__ . '/common.php';

$config = [
    'id' => 'basic-console',
    'controllerNamespace' => 'app\commands',
    'components' => [],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return array_merge_recursive($common, $config);

<?php

return [

    /*~~~~~~~~~~~~~~~~*/
    /* AUTH & PROFILE */
    /*~~~~~~~~~~~~~~~~*/

    // COMMON
    /* [+] */ 'POST v0/auth/sign-up'            => 'v0/auth/sign-up',
    /* [+] */ 'POST v0/auth/sign-in'            => 'v0/auth/sign-in',
    /* [+] */ 'POST v0/auth/logout'             => 'v0/auth/logout',
    /* [+] */ 'POST v0/auth/refresh-token'      => 'v0/auth/refresh-token',
    /* [+] */ 'POST v0/auth/reset-password'     => 'v0/auth/reset-password',
    /* [+] */ 'POST v0/auth/set-password'       => 'v0/auth/set-password',

    /* [+] */ 'POST v0/profile/view'            => 'v0/profile/view',
    /* [+] */ 'POST v0/profile/update'          => 'v0/profile/update',
    /* [+] */ 'POST v0/profile/change-password' => 'v0/profile/change-password',

    /*~~~~~~*/
    /* USER */
    /*~~~~~~*/

    // ADM
    /* [+] */ 'POST v0/adm/user/create'                   => 'v0/adm/user/create',
    /* [+] */ 'POST v0/adm/user/index'                    => 'v0/adm/user/index',
    /* [+] */ 'POST v0/adm/user/<id:\d+>/view'            => 'v0/adm/user/view',
    /* [+] */ 'POST v0/adm/user/<id:\d+>/update'          => 'v0/adm/user/update',
    /* [+] */ 'POST v0/adm/user/<id:\d+>/change-password' => 'v0/adm/user/change-password',
    /* [+] */ 'POST v0/adm/user/<id:\d+>/delete' => 'v0/adm/user/delete',

    /*~~~~~~~~~~~~*/
    /* DICTIONARY */
    /*~~~~~~~~~~~~*/

    // COMMON
    /* [!] */ 'POST v0/dictionary/<item:[\w-]+>/items' => 'v0/dictionary/items',
    /* [+] */ 'POST v0/geocoder/suggest'               => 'v0/geocoder/suggest',

    // TODO: TEMPORARY METHODS
    /* [+] */ 'POST v0/wh_cities/all' => 'v0/dictionary/all-cities',
    /* [+] */ 'POST v0/wh_cities/my'  => 'v0/dictionary/my-cities',

    /*~~~~~~~~~~~~~~~*/
    /* NOTIFICATIONS */
    /*~~~~~~~~~~~~~~~*/

    // COMMON
    /* [+] */ 'POST v0/notification/index'              => 'v0/notification/index',
    /* [+] */ 'POST v0/notification/unread-count'       => 'v0/notification/unread-count',
    /* [+] */ 'POST v0/notification/<id:\d+>/view'      => 'v0/notification/view',
    /* [+] */ 'POST v0/notification/<id:\d+>/mark-read' => 'v0/notification/mark-read',
    /* [+] */ 'POST v0/notification/mark-all-read'      => 'v0/notification/mark-all-read',
    /* [+] */ 'POST v0/notification/<id:\d+>/delete'    => 'v0/notification/delete',

    /*~~~~~~~~~~~~~~*/
    /* ORGANIZATION */
    /*~~~~~~~~~~~~~~*/

    // COMMON
    /* [+] */ 'POST v0/organization/check-inn'                     => 'v0/organization/check-inn',
    /* [+] */ 'POST v0/organization/create_document/<type:[\w-]+>' => 'v0/organization/create_document',

    // USER
    /* [+] */ 'POST v0/organization/create'    => 'v0/user/organization/create',
    /* [+] */ 'POST v0/organization/my/view'   => 'v0/user/organization/view',
    /* [~] */ 'POST v0/organization/my/update' => 'v0/user/organization/update',

    // ADM
    /* [+] */ 'POST v0/adm/organization/create'          => 'v0/adm/organization/create',
    /* [+] */ 'POST v0/adm/organization/index'           => 'v0/adm/organization/index',
    /* [+] */ 'POST v0/adm/organization/<id:\d+>/view'   => 'v0/adm/organization/view',
    /* [+] */ 'POST v0/adm/organization/<id:\d+>/update' => 'v0/adm/organization/update',

    /*~~~~~~~~~~~~~~*/
    /* TEAM MEMBERS */
    /*~~~~~~~~~~~~~~*/

    // USER
    /* [+] */ 'POST v0/team-members/request/index'           => 'v0/user/team-members/request-index',
    /* [+] */ 'POST v0/team-members/request/create'          => 'v0/user/team-members/request-create',
    /* [+] */ 'POST v0/team-members/request/my/view'         => 'v0/user/team-members/request-my',
    /* [+] */ 'POST v0/team-members/request/my/cancel'       => 'v0/user/team-members/request-cancel',
    /* [+] */ 'POST v0/team-members/request/<id:\d+>/accept' => 'v0/user/team-members/request-accept',
    /* [+] */ 'POST v0/team-members/request/<id:\d+>/reject' => 'v0/user/team-members/request-reject',

    /* [+] */ 'POST v0/team-members/index'                   => 'v0/user/team-members/index',
    /* [+] */ 'POST v0/team-members/<user_id:\d+>/delete'    => 'v0/user/team-members/delete',

    // ADM
    /* [-] */ 'POST v0/adm/team-members/request/index'           => 'v0/adm/team-members/request-index',
    /* [-] */ 'POST v0/adm/team-members/request/create'          => 'v0/adm/team-members/request-create',
    /* [-] */ 'POST v0/adm/team-members/request/<id:\d+>/view'   => 'v0/adm/team-members/request-view',
    /* [-] */ 'POST v0/adm/team-members/request/<id:\d+>/cancel' => 'v0/adm/team-members/request-cancel',
    /* [-] */ 'POST v0/adm/team-members/request/<id:\d+>/accept' => 'v0/adm/team-members/request-accept',
    /* [-] */ 'POST v0/adm/team-members/request/<id:\d+>/reject' => 'v0/adm/team-members/request-reject',

    /* [-] */ 'POST v0/adm/team-members/index'                   => 'v0/adm/team-members/index',
    /* [-] */ 'POST v0/adm/team-members/<user_id:\d+>/delete'    => 'v0/adm/team-members/delete',

    /*~~~~~~~~~~~*/
    /* WAREHOUSE */
    /*~~~~~~~~~~~*/

    // COMMON
    /* [+] */ 'POST v0/warehouse/new_photo'                 => 'v0/warehouse/new_photo',

    // USER
    /* [+] */ 'POST v0/warehouse/create'                    => 'v0/user/warehouse/create',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/update/general'   => 'v0/user/warehouse/update-general',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/update/external'  => 'v0/user/warehouse/update-external',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/update/internal'  => 'v0/user/warehouse/update-internal',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/update/documents' => 'v0/user/warehouse/update-documents',

    /* [+] */ 'POST v0/warehouse/index'                     => 'v0/user/warehouse/index',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/view/general'     => 'v0/user/warehouse/view-general',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/view/external'    => 'v0/user/warehouse/view-external',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/view/internal'    => 'v0/user/warehouse/view-internal',
    /* [+] */ 'POST v0/warehouse/<id:\d+>/view/documents'   => 'v0/user/warehouse/view-documents',

    /* [+] */ 'POST v0/warehouse/<id:\d+>/delete'           => 'v0/user/warehouse/delete',

    // ADM
    /* [-] */ 'POST v0/adm/warehouse/create'                    => 'v0/adm/warehouse/create',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/update/general'   => 'v0/adm/warehouse/update-general',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/update/external'  => 'v0/adm/warehouse/update-external',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/update/internal'  => 'v0/adm/warehouse/update-internal',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/update/documents' => 'v0/adm/warehouse/update-documents',

    /* [-] */ 'POST v0/adm/warehouse/index'                     => 'v0/adm/warehouse/index',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/view/general'     => 'v0/adm/warehouse/view-general',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/view/external'    => 'v0/adm/warehouse/view-external',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/view/internal'    => 'v0/adm/warehouse/view-internal',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/view/documents'   => 'v0/adm/warehouse/view-documents',

    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/delete'           => 'v0/adm/warehouse/delete',
    /* [-] */ 'POST v0/adm/warehouse/<id:\d+>/organization/report/url'  => 'v0/adm/warehouse/organization-report-url',

    /*~~~~~~~~*/
    /* ADVERT */
    /*~~~~~~~~*/

    // USER
    /* [+] */ 'POST v0/advrt/create'           => 'v0/user/advert/create',
    /* [+] */ 'POST v0/advrt/<id:\d+>/update'  => 'v0/user/advert/update',
    /* [+] */ 'POST v0/advrt/<id:\d+>/publish' => 'v0/user/advert/publish',

    /* [+] */ 'POST v0/advrt/index'            => 'v0/user/advert/index',
    /* [+] */ 'POST v0/advrt/<id:\d+>/view'    => 'v0/user/advert/view',
    /* [+] */ 'POST v0/advrt/<id:\d+>/delete'  => 'v0/user/advert/delete',

    // ADM
    /* [-] */ 'POST v0/adm/advrt/create'           => 'v0/adm/advert/create',
    /* [-] */ 'POST v0/adm/advrt/<id:\d+>/update'  => 'v0/adm/advert/update',
    /* [-] */ 'POST v0/adm/advrt/<id:\d+>/publish' => 'v0/adm/advert/publish',

    /* [-] */ 'POST v0/adm/advrt/index'            => 'v0/adm/advert/index',
    /* [-] */ 'POST v0/adm/advrt/<id:\d+>/view'    => 'v0/adm/advert/view',
    /* [-] */ 'POST v0/adm/advrt/<id:\d+>/delete'  => 'v0/adm/advert/delete',

    /*~~~~~~~~~*/
    /* SWAGGER */
    /*~~~~~~~~~*/

    /* [+] */ 'docs'               => 'docs/docs/index',
    /* [+] */ 'docs/<name:[\w-]+>' => 'docs/docs/view',

    /*~~~~~~~*/
    /* OTHER */
    /*~~~~~~~*/

    /* [+] */ 'OPTIONS <path:.*>' => 'v0/default/options',
];

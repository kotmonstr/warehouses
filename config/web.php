<?php

$common = require __DIR__ . '/common.php';

$config = [
    'id' => 'basic',
    'modules' => [
        'v0'      => 'app\modules\api\v0\Module',
        'docs'    => 'app\modules\api\docs\Module',
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'i7pDewmVukpAV3AgD0yShAIT2Heyr-XG',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser'
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return array_merge_recursive($common, $config);

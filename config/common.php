<?php

$params = require __DIR__ . '/params.php';
$routes = require __DIR__ . '/routes.php';

$config = [
    'name' => 'Агрегатор складов',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => 5,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'konturApi' => [
            'class' => 'app\components\KonturApi',
            'key' => env('KONTUR_API_KEY'),
        ],
        'ymaps' => [
            'class' => 'app\components\YMaps',
            'key' => env('YANDEX_API_KEY'),
        ],
        'db' => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'pgsql:host=' . env('DB_HOST') . ';dbname=' . env('DB_NAME'),
            'username' => env('DB_USER'),
            'password' => env('DB_PASSWORD'),
            'charset'  => 'utf8',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => $routes,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'encryption' => 'tls',
                'host' => 'smtp.yandex.com',
                'port' => '587',
                'username' => env('SERVICE_MAIL_USER'),
                'password' => env('SERVICE_MAIL_PASSWORD'),
            ],
        ],
    ],
    'params' => $params,
];

return $config;

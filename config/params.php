<?php

return [
    'domainUrl'  => env('DOMAIN_URL'),
    'adminEmail' => 'no-reply@wa.globus-ltd.com',
];

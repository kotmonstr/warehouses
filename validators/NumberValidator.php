<?php

namespace app\validators;

/**
 * Class NumberValidator
 * @package app\validators
 */
class NumberValidator extends \yii\validators\NumberValidator
{
    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        $model->$attribute = str_replace(',', '.', $model->$attribute);

        parent::validateAttribute($model, $attribute);
    }
}
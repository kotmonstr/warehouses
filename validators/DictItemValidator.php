<?php

namespace app\validators;

use yii\validators\Validator;
use app\models\DictionaryItem;

/**
 * Validate if dictionary item exist
 *
 * @property string $dictionary_sn
 * @property string|null $type
 */
class DictItemValidator extends Validator
{
    public $dictionary_sn;
    public $type;

    public function validateAttribute($model, $attribute)
    {
        $query = ['system_name' => $model->$attribute, 'dictionary_sn' => $this->dictionary_sn, 'deleted_at' => null];

        if ($this->type) {
            $query['type'] = $this->type;
        }

        $isValid = DictionaryItem::find()->where($query)->exists();

        if (!$isValid) {
            $this->addError($model, $attribute, 'Неверное значение справочника');
        }
    }
}

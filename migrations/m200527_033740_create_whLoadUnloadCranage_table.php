<?php
/**
 * This is the file with handles the creation of table `wh_load_unload_cranage`.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the creation of table `wh_load_unload_cranage`.
 * Has foreign key to the table:
 *
 * - `warehouse`
 */
class m200527_033740_create_whLoadUnloadCranage_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->createTable(
            'wh_load_unload_cranage',
            [
                'id' => $this->primaryKey(),
                'warehouse_id' => $this->integer()->notNull(),
                'loading_unloading_tech_type_sn' => $this->string(),
                'amount' => $this->integer(),
                'capacity' => $this->decimal(19, 2),
                'capacity_type_sn' => $this->string(),
                'loading_unloading_method_sn' => $this->string(),
                'created_at' => $this->bigInteger()->notNull(),
                'updated_at' => $this->bigInteger(),
            ]
        );

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whLoadUnloadCranage-warehouse_id',
            'wh_load_unload_cranage',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whLoadUnloadCranage-warehouse_id',
            'wh_load_unload_cranage',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whLoadUnloadCranage-warehouse_id',
            'wh_load_unload_cranage'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whLoadUnloadCranage-warehouse_id',
            'wh_load_unload_cranage'
        );

        $this->dropTable('wh_load_unload_cranage');
    }
}

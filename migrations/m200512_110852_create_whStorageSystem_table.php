<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_storage_system`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 */
class m200512_110852_create_whStorageSystem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_storage_system', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'storage_system_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whStorageSystem-warehouse_id',
            'wh_storage_system',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whStorageSystem-warehouse_id',
            'wh_storage_system',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whStorageSystem-warehouse_id',
            'wh_storage_system'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whStorageSystem-warehouse_id',
            'wh_storage_system'
        );

        $this->dropTable('wh_storage_system');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m200512_110338_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'organization_id' => $this->integer(),
            'organization_position' => $this->string(),
            'email' => $this->string()->notNull(),
            'email_verification_token' => $this->string(),
            'email_verified' => $this->boolean(),
            'phone_number' => $this->string(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'verification_status_sn' => $this->string(),
            'verification_comment' => $this->text(),
            'verified_at' => $this->bigInteger(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `document`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m200512_111127_create_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'entity_classname' => $this->string()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-document-created_by',
            'document',
            'created_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-document-created_by',
            'document',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-document-created_by',
            'document'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-document-created_by',
            'document'
        );

        $this->dropTable('document');
    }
}

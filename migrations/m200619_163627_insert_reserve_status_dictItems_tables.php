<?php

use yii\db\Migration;

/**
 * Class m200619_163627_insert_reserve_status_dictItems_tables
 */
class m200619_163627_insert_reserve_status_dictItems_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $timestamp  = time();
        //Добавление статуса в таблицу dict
        $this->batchInsert(
            'dictionary',
            ['system_name', 'title', 'created_at'],
            [
                ['reserve_status', 'Cтатус брони', $timestamp]
            ]
        );
        //Добавление статуса в таблицу dictItems
        $this->batchInsert(
            'dictionary_item',
            ['dictionary_sn', 'type', 'system_name', 'title', 'created_at'],
            [
                ['reserve_status', 'default', 'active', 'Активна', $timestamp],
                ['reserve_status', 'default', 'canceled', 'Отменена', $timestamp],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('dictionary', ['system_name' => 'reserve_status']);
        $this->delete('dictionary_item', ['dictionary_sn' => 'reserve_status']);
    }

}

<?php

use yii\db\Migration;

/**
 * Class m200619_164954_addColumnsStatus_reserve_table
 */
class m200619_164954_addColumnsStatus_reserve_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Добавление поля статус в модель Reserve
        $this->addColumn('reserve', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('reserve', 'status');
    }
}

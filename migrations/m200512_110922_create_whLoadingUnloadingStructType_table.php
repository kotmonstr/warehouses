<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_loading_unloading_struct_type`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 */
class m200512_110922_create_whLoadingUnloadingStructType_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_loading_unloading_struct_type', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'loading_unloading_struct_type_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whLoadingUnloadingStructType-warehouse_id',
            'wh_loading_unloading_struct_type',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whLoadingUnloadingStructType-warehouse_id',
            'wh_loading_unloading_struct_type',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whLoadingUnloadingStructType-warehouse_id',
            'wh_loading_unloading_struct_type'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whLoadingUnloadingStructType-warehouse_id',
            'wh_loading_unloading_struct_type'
        );

        $this->dropTable('wh_loading_unloading_struct_type');
    }
}

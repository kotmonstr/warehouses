<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_accounting_control_system`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 */
class m200601_081006_create_whAccountingControlSystem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_accounting_control_system', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'accounting_control_system_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whAccountingControlSystem-warehouse_id',
            'wh_accounting_control_system',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whAccountingControlSystem-warehouse_id',
            'wh_accounting_control_system',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whAccountingControlSystem-warehouse_id',
            'wh_accounting_control_system'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whAccountingControlSystem-warehouse_id',
            'wh_accounting_control_system'
        );

        $this->dropTable('wh_accounting_control_system');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m200528_222018_rename_type_column_to_entity_field_in_document_table
 */
class m200528_222018_rename_type_column_to_entity_field_in_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('document', 'type', 'entity_field');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('document', 'entity_field', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200528_222018_rename_type_column_to_entity_field_in_document_table cannot be reverted.\n";

        return false;
    }
    */
}

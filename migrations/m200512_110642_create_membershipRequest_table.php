<?php

use yii\db\Migration;

/**
 * Handles the creation of table `membershipRequest`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `organization`
 */
class m200512_110642_create_membershipRequest_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('membership_request', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'organization_id' => $this->integer()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-membershipRequest-user_id',
            'membership_request',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-membershipRequest-user_id',
            'membership_request',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `organization_id`
        $this->createIndex(
            'idx-membershipRequest-organization_id',
            'membership_request',
            'organization_id'
        );

        // add foreign key for table `organization`
        $this->addForeignKey(
            'fk-membershipRequest-organization_id',
            'membership_request',
            'organization_id',
            'organization',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-membershipRequest-user_id',
            'membership_request'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-membershipRequest-user_id',
            'membership_request'
        );

        // drops foreign key for table `organization`
        $this->dropForeignKey(
            'fk-membershipRequest-organization_id',
            'membership_request'
        );

        // drops index for column `organization_id`
        $this->dropIndex(
            'idx-membershipRequest-organization_id',
            'membership_request'
        );

        $this->dropTable('membership_request');
    }
}

<?php
/**
 * This is the file with handles the adding columns in `warehouse` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding columns in `warehouse` table.
 * Column:
 *
 * - `dsl_verification_status_sn`
 */
class m200618_194002_addColumsVerificationDsl_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('warehouse', 'dsl_verification_status_sn', $this->string());
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropColumn('warehouse', 'dsl_verification_status_sn');
    }
}

<?php
/**
 * This is the file with handles rename columns in table `warehouse`.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles rename columns in table `warehouse`.
 * verification_status_sn => sb_verification_status_sn
 */
class m200618_194001_rename_verification_columns_to_sb_berification_in_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->renameColumn('warehouse', 'verification_status_sn', 'sb_verification_status_sn');
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->renameColumn('warehouse', 'sb_verification_status_sn', 'verification_status_sn');
    }
}

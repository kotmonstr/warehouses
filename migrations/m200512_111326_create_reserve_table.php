<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reserve`.
 * Has foreign keys to the tables:
 *
 * - `advert`
 */
class m200512_111326_create_reserve_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reserve', [
            'id' => $this->primaryKey(),
            'advert_id' => $this->integer()->notNull(),
            'amount' => $this->decimal(19, 2),
            'amount_type_sn' => $this->string(),
            'cost' => $this->decimal(19, 2),
            'rent_from' => $this->bigInteger(),
            'rent_to' => $this->bigInteger(),
            'contact_fio' => $this->string(),
            'contact_phone_number' => $this->string(),
            'contact_email' => $this->string()->notNull(),
            'comment' => $this->text(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `advert_id`
        $this->createIndex(
            'idx-reserve-advert_id',
            'reserve',
            'advert_id'
        );

        // add foreign key for table `advert`
        $this->addForeignKey(
            'fk-reserve-advert_id',
            'reserve',
            'advert_id',
            'advert',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `advert`
        $this->dropForeignKey(
            'fk-reserve-advert_id',
            'reserve'
        );

        // drops index for column `advert_id`
        $this->dropIndex(
            'idx-reserve-advert_id',
            'reserve'
        );

        $this->dropTable('reserve');
    }
}

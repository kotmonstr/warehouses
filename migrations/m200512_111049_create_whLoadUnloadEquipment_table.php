<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_load_unload_equipment`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 */
class m200512_111049_create_whLoadUnloadEquipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_load_unload_equipment', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'type' => $this->string(),
            'amount' => $this->integer(),
            'capacity' => $this->decimal(19, 2),
            'capacity_type_sn' => $this->string(),
            'load_unload_method' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whLoadUnloadEquipment-warehouse_id',
            'wh_load_unload_equipment',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whLoadUnloadEquipment-warehouse_id',
            'wh_load_unload_equipment',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whLoadUnloadEquipment-warehouse_id',
            'wh_load_unload_equipment'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whLoadUnloadEquipment-warehouse_id',
            'wh_load_unload_equipment'
        );

        $this->dropTable('wh_load_unload_equipment');
    }
}

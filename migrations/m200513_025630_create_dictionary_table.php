<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dictionary`.
 */
class m200513_025630_create_dictionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('dictionary', [
            'id' => $this->primaryKey(),
            'system_name' => $this->string()->notNull()->unique(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        $timestamp  = time();

        $this->batchInsert('dictionary', ['system_name', 'title', 'created_at'], [
            ['accounting_system_type', 'Типы систем складского учета', $timestamp],
            ['classification', 'Классы помещений', $timestamp],
            ['unit', 'Единицы измерения', $timestamp],
            ['accounting_control_systems', 'Системы контроля и учета', $timestamp],
            ['parking_type', 'Типы парковки', $timestamp],
            ['storage_system', 'Системы хранения', $timestamp],
            ['floor_covering', 'Материал полового покрытия', $timestamp],
            ['temperature_mode', 'Температурные режимы', $timestamp],
            ['fire_ext_system_type', 'Типы систем автоматического пожаротушения', $timestamp],
            ['electricity_grid_type', 'Типы электросетей', $timestamp],
            ['heating_system_type', 'Типы систем отопления', $timestamp],
            ['loading_unloading_struct_types', 'Типы разгрузочно-погрузочных конструкций', $timestamp],
            ['office_facility_types', 'Типы офисных и пособных помещений', $timestamp]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dictionary');
    }
}

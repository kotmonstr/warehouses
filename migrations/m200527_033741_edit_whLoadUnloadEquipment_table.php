<?php
/**
 * This is the file with handles the editing of table `wh_load_unload_equipment`.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the editing of table `wh_load_unload_equipment`:
 * - rename type and load_unload_method fields
 */
class m200527_033741_edit_whLoadUnloadEquipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn(
            'wh_load_unload_equipment',
            'type',
            'loading_unloading_tech_type_sn'
        );

        $this->renameColumn(
            'wh_load_unload_equipment',
            'load_unload_method',
            'loading_unloading_method_sn'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn(
            'wh_load_unload_equipment',
            'loading_unloading_method_sn',
            'load_unload_method'
        );

        $this->renameColumn(
            'wh_load_unload_equipment',
            'loading_unloading_tech_type_sn',
            'type'
        );
    }
}

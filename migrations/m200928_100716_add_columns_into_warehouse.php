<?php

use yii\db\Migration;

/**
 * Class m200928_100716_add_columns_into_warehouse
 */
class m200928_100716_add_columns_into_warehouse extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%warehouse}}', 'inn', $this->string()->null());
        $this->addColumn('{{%warehouse}}', 'kpp', $this->string()->null());
        $this->addColumn('{{%warehouse}}', 'from_the_owner', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%warehouse}}', 'inn');
        $this->dropColumn('{{%warehouse}}', 'kpp');
        $this->dropColumn('{{%warehouse}}', 'from_the_owner');
    }

}

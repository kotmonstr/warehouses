<?php

use yii\db\Migration;

/**
 * Class m200812_122622_alter_user_table
 */
class m200812_122622_alter_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%user}}', 'name', 'first_name');
        $this->addColumn('{{%user}}', 'last_name', $this->string(255));
        $this->addColumn('{{%user}}', 'middle_name', $this->string(255));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%user}}', 'first_name', 'name');
        $this->dropColumn('{{%user}}', 'last_name');
        $this->dropColumn('{{%user}}', 'middle_name');

    }


}

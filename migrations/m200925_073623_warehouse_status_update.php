<?php

use yii\db\Migration;

/**
 * Class m200925_073623_warehouse_status_update
 */
class m200925_073623_warehouse_status_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%warehouse}}', 'status_sn', $this->string(255));

        $this->batchInsert('{{dictionary}}', ['system_name', 'title', 'created_at'], [
            ['warehouse_verification_status', 'Cтатус верификации склада', time()],
        ]);

        $this->delete('{{dictionary_item}}', ['in', 'dictionary_sn', [
            'warehouse_status'
        ]]);

        $this->batchInsert('{{dictionary_item}}', ['dictionary_sn', 'type', 'system_name', 'title', 'created_at'], [
            ['warehouse_status', 'default', 'draft', 'Черновик', time()],
            ['warehouse_status', 'default', 'verification', 'На верификации', time()],
            ['warehouse_status', 'default', 'verified', 'Активен', time()],
            ['warehouse_status', 'default', 'restricted', 'Отклонён', time()],
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%warehouse}}', 'status_sn');

        $this->delete('{{dictionary}}', ['in', 'system_name', [
            'warehouse_verification_status',
        ]]);

        $this->delete('{{dictionary_item}}', ['in', 'system_name', [
            'draft','verification','verified','restricted'
        ]]);
    }

}

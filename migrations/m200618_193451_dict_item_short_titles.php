<?php

use yii\db\Migration;

/**
 * Class m200618_193451_dict_item_short_titles
 */
class m200618_193451_dict_item_short_titles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dictionary_item', 'title_short', $this->string()->after('title'));

        $titles = [
            'unit' => [
                'celsius'      => '°C',
                'kilogram'     => 'кг',
                'meter'        => 'м',
                'square_meter' => 'м²',
                'ton'          => 'т',
            ]
        ];

        foreach ($titles as $dictionary_sn => $system_names) {
            foreach ($system_names as $system_name => $title) {
                $this->update('dictionary_item', [
                    'title_short' => $title
                ], [
                    'dictionary_sn' => $dictionary_sn,
                    'system_name' => $system_name,
                ]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dictionary_item', 'title_short');
    }
}

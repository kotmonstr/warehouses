<?php
/**
 * This is the file with handles the insert into table `dictionary_item`
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the insert into table `dictionary_item`
 */
class m200517_025703_insert_dictionaryItem_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $timestamp  = time();

        $this->batchInsert(
            'dictionary_item',
            [
                'dictionary_sn', 'type', 'system_name', 'title', 'created_at'
            ],
            [
                ['floor_covering', 'default', 'laminate', 'Ламинат', $timestamp],
                ['floor_covering', 'default', 'linoleum', 'Линолеум', $timestamp],
                ['floor_covering', 'default', 'polymeric', 'Полимерный', $timestamp],
                ['floor_covering', 'default', 'reinforced_concrete', 'Железобетон', $timestamp],
                ['floor_covering', 'default', 'self_leveling', 'Наливной', $timestamp],
                ['floor_covering', 'default', 'tile', 'Плитка', $timestamp],
                ['floor_covering', 'default', 'wood', 'Деревянный', $timestamp],
                ['cranage_types', 'default', 'beam', 'Кран-балка', $timestamp],
                ['cranage_types', 'default', 'gantry', 'Козловой кран', $timestamp],
                ['cranage_types', 'default', 'overhead', 'Мостовой кран', $timestamp],
                ['cranage_types', 'default', 'railway', 'Ж/д кран', $timestamp],
                ['loading_unloading_methods', 'default', 'top', 'Верхняя', $timestamp],
                ['loading_unloading_methods', 'default', 'side', 'Боковая', $timestamp],
                ['loading_unloading_methods', 'default', 'rear', 'Задняя', $timestamp],
                ['loading_unloading_tech_types', 'default', 'trolley', 'Складские тележки', $timestamp],
                ['loading_unloading_tech_types', 'default', 'lift_table', 'Подъемные столы', $timestamp],
                ['loading_unloading_tech_types', 'default', 'manual_piler', 'Штабелеры ручные', $timestamp],
                ['loading_unloading_tech_types', 'default', 'auto_piler', 'Штабелеры электрические', $timestamp],
                ['loading_unloading_tech_types', 'default', 'richtrak', 'Ричтраки', $timestamp],
                ['loading_unloading_tech_types', 'default', 'loader', 'Погрузчики', $timestamp],
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->delete(
            'dictionary_item',
            [
                'dictionary_sn' => 'floor_covering',
                'system_name' => [
                    'laminate',
                    'linoleum',
                    'polymeric',
                    'reinforced_concrete',
                    'self_leveling',
                    'tile',
                    'wood'
                ]
            ]
        );
        $this->delete(
            'dictionary_item',
            [
                'dictionary_sn' => 'cranage_types',
                'system_name' => [
                    'beam',
                    'gantry',
                    'overhead',
                    'railway'
                ]
            ]
        );
        $this->delete(
            'dictionary_item',
            [
                'dictionary_sn' => 'loading_unloading_methods',
                'system_name' => [
                    'top',
                    'side',
                    'rear'
                ]
            ]
        );
        $this->delete(
            'dictionary_item',
            [
                'dictionary_sn' => 'loading_unloading_tech_types',
                'system_name' => [
                    'trolley',
                    'lift_table',
                    'manual_piler',
                    'auto_piler',
                    'richtrak',
                    'loader'
                ]
            ]
        );
    }
}

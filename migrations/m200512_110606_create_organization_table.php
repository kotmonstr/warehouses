<?php

use yii\db\Migration;

/**
 * Handles the creation of table `organization`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m200512_110606_create_organization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('organization', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'inn' => $this->string()->notNull(),
            'ogrn' => $this->string()->notNull(),
            'kpp' => $this->string(),
            'okpo' => $this->string(),
            'main_activity' => $this->string(),
            'taxation_type' => $this->string(),
            'address_ym_geocoder' => $this->string(),
            'verification_status_sn' => $this->string(),
            'verification_comment' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'verified_at' => $this->bigInteger(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-organization-created_by',
            'organization',
            'created_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-organization-created_by',
            'organization',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-organization-created_by',
            'organization'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-organization-created_by',
            'organization'
        );

        $this->dropTable('organization');
    }
}

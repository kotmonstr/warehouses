<?php

use yii\db\Migration;

/**
 * Class m200604_083921_rename_status_column_to_status_sn_in_advert_table
 */
class m200604_083921_rename_status_column_to_status_sn_in_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('advert', 'status', 'status_sn');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('advert', 'status_sn', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200604_083921_rename_status_column_to_status_sn_in_advert_table cannot be reverted.\n";

        return false;
    }
    */
}

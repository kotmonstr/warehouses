<?php
/**
 * This is the file with handles the separating values hooks and others in `dictionary_item` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the separating values in `dictionary_item` table.
 * Values:
 *
 * - hooks_and_other
 */
class m200715_092311_separateHooksAndOthers_dictionaryItem_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $time = time();

        $this->update(
            'dictionary_item',
            [
                'title' => 'Другие складские системы для специфических видов продукции',
                'created_at' => $time + 1,
            ],
            [
                'system_name' => 'hooks_and_other',
                'title' => 'Крюки и другие складские системы для специфических видов продукции'
            ]
        );

        $this->insert(
            'dictionary_item',
            [
                'dictionary_sn' => 'storage_system',
                'type' => 'default',
                'system_name' => 'hooks',
                'title' => 'Крюки',
                'created_at' => $time
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->update(
            'wh_storage_system',
            [
                'storage_system_sn' => 'hooks_and_other'
            ],
            [
                'storage_system_sn' => 'hooks'
            ]
        );

        $this->update(
            'advert',
            [
                'storage_type_sn' => 'hooks_and_other'
            ],
            [
                'storage_type_sn' => 'hooks'
            ]
        );

        $this->delete(
            'dictionary_item',
            [
                'system_name' => 'hooks',
                'title' => 'Крюки'
            ]
        );

        $this->update(
            'dictionary_item',
            [
                'title' => 'Крюки и другие складские системы для специфических видов продукции'
            ],
            [
                'system_name' => 'hooks_and_other',
                'title' => 'Другие складские системы для специфических видов продукции'
            ]
        );
    }
}

<?php

use yii\db\Migration;

/**
 * Class m200713_212452_add_few_dictionary_items
 */
class m200713_212452_add_few_dictionary_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('dictionary', ['system_name', 'title', 'created_at'], [
            ['organization_status', 'Cтатус организации', time()],
            ['warehouse_status', 'Cтатус склада', time()],
        ]);

        $this->batchInsert('dictionary_item', ['dictionary_sn', 'type', 'system_name', 'title', 'created_at'], [
            ['organization_status', 'default', 'in_progress', 'Ожидает', time()],
            ['organization_status', 'default', 'verified', 'Верифицирована', time()],
            ['organization_status', 'default', 'failed', 'Отменена', time()],
            ['organization_status', 'default', 'restricted', 'Заблокирована', time()],

            ['warehouse_status', 'default', 'in_progress', 'Ожидает', time()],
            ['warehouse_status', 'default', 'verified', 'Верифицирован', time()],
            ['warehouse_status', 'default', 'failed', 'Отменен', time()],
            ['warehouse_status', 'default', 'restricted', 'Заблокирован', time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('dictionary', [
            'system_name' => ['organization_status', 'warehouse_status'],
        ]);

        $this->delete('dictionary_item', [
            'dictionary_sn' => ['organization_status', 'warehouse_status'],
        ]);
    }
}

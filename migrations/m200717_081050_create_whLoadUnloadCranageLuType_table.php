<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_load_unload_cranage_lu_type`.
 * Has foreign keys to the tables:
 *
 * - `wh_load_unload_cranage`
 */
class m200717_081050_create_whLoadUnloadCranageLuType_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_load_unload_cranage_lu_type', [
            'id' => $this->primaryKey(),
            'equipment_id' => $this->integer()->notNull(),
            'loading_unloading_method_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `equipment_id`
        $this->createIndex(
            'idx-whLoadUnloadCranageLuType-equipment_id',
            'wh_load_unload_cranage_lu_type',
            'equipment_id'
        );

        // add foreign key for table `wh_load_unload_cranage`
        $this->addForeignKey(
            'fk-whLoadUnloadCranageLuType-equipment_id',
            'wh_load_unload_cranage_lu_type',
            'equipment_id',
            'wh_load_unload_cranage',
            'id',
            'CASCADE'
        );

        // insert values from wh_load_unload_cranage
        $this->execute(
            "INSERT INTO wh_load_unload_cranage_lu_type (equipment_id, loading_unloading_method_sn, created_at)
            SELECT id, loading_unloading_method_sn, created_at
            FROM wh_load_unload_cranage"
        );

        //drop column load_unload_method in wh_load_unload_cranage table
        $this->dropColumn('wh_load_unload_cranage', 'loading_unloading_method_sn');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //add column load_unload_method to wh_load_unload_cranage table
        $this->addColumn('wh_load_unload_cranage', 'loading_unloading_method_sn', $this->string());

        // insert values from wh_load_unload_cranage_lu_type
        $this->execute(
            "UPDATE wh_load_unload_cranage SET loading_unloading_method_sn=
            (SELECT loading_unloading_method_sn
            FROM wh_load_unload_cranage_lu_type
            WHERE wh_load_unload_cranage.id=wh_load_unload_cranage_lu_type.equipment_id
            LIMIT 1)"
        );

        // drops foreign key for table `wh_load_unload_cranage`
        $this->dropForeignKey(
            'fk-whLoadUnloadCranageLuType-equipment_id',
            'wh_load_unload_cranage_lu_type'
        );

        // drops index for column `equipment_id`
        $this->dropIndex(
            'idx-whLoadUnloadCranageLuType-equipment_id',
            'wh_load_unload_cranage_lu_type'
        );

        $this->dropTable('wh_load_unload_cranage_lu_type');
    }
}

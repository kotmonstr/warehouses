<?php
/**
 * This is the file with handles the adding columns in `warehouse` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding columns in `warehouse` table.
 * Columns:
 *
 * - `car_parking_sn`
 * - `truck_parking_sn`
 */
class m200527_033743_addColumsParking_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('warehouse', 'car_parking_sn', $this->string());
        $this->addColumn('warehouse', 'truck_parking_sn', $this->string());
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropColumn('warehouse', 'car_parking_sn');
        $this->dropColumn('warehouse', 'truck_parking_sn');
    }
}

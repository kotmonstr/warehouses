<?php

use yii\db\Migration;

/**
 * Handles the creation of foreign keys to the tables:
 * - `user`
 * - `organization`
 */
class m200512_110607_create_fkUserOrganization_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-user-organization_id', 'user', 'organization_id');

        $this->addForeignKey('fk-user-organization_id', 'user', 'organization_id', 'organization', 'id', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `organization`
        $this->dropForeignKey(
            'fk-user-organization_id',
            'user'
        );

        // drops index for column `organization_id`
        $this->dropIndex(
            'idx-user-organization_id',
            'user'
        );
    }
}

<?php
/**
 * This is the file with handles adding users SB and DSL to table `user`.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles adding users to table `user`.
 */
class m200618_194000_add_users_SBandDSL_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->insert(
            'user', [
                'name' => 'SB',
                'email' => 'sb@wa.ru',
                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password'),
                'email_verified' => 1,
                'role' => 'sb',
                'created_at' => time(),
            ]
        );

        $this->insert(
            'user', [
                'name' => 'DSL',
                'email' => 'dsl@wa.ru',
                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password'),
                'email_verified' => 1,
                'role' => 'dsl',
                'created_at' => time(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->delete('user', ['email' => 'dsl@wa.ru', 'name' => 'DSL']);
        $this->delete('user', ['email' => 'sb@wa.ru', 'name' => 'SB']);
    }
}

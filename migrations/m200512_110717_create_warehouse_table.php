<?php

use yii\db\Migration;

/**
 * Handles the creation of table `warehouse`.
 * Has foreign keys to the tables:
 *
 * - `organization`
 * - `user`
 */
class m200512_110717_create_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('warehouse', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'description' => $this->text(),
            'address_ym_geocoder' => $this->string(),
            'address_lat' => $this->string(),
            'address_lon' => $this->string(),
            'work_hours_24_7' => $this->boolean(),
            'work_hours_weekdays_from' => $this->integer(),
            'work_hours_weekdays_to' => $this->integer(),
            'work_hours_weekends_from' => $this->integer(),
            'work_hours_weekends_to' => $this->integer(),
            'accounting_system_sn' => $this->string(),
            'classification_sn' => $this->string(),
            'square_area' => $this->decimal(19, 2),
            'square_area_type_sn' => $this->string(),
            'built_up_area' => $this->decimal(19, 2),
            'built_up_area_type_sn' => $this->string(),
            'ceiling_height' => $this->decimal(19, 2),
            'ceiling_height_type_sn' => $this->string(),
            'floors' => $this->integer(),
            'maneuvering_area' => $this->boolean(),
            'entries_amount' => $this->integer(),
            'loading_ramp' => $this->boolean(),
            'automatic_gates_amount' => $this->integer(),
            'outer_lighting' => $this->boolean(),
            'outer_security' => $this->boolean(),
            'outer_beautification' => $this->boolean(),
            'outer_fencing' => $this->boolean(),
            'outer_entries_amount' => $this->integer(),
            'truck_maneuvering_area' => $this->boolean(),
            'rail_road' => $this->boolean(),
            'distance_from_highway' => $this->decimal(19, 2),
            'distance_from_highway_type_sn' => $this->string(),
            'vehicle_max_length' => $this->decimal(19, 2),
            'vehicle_max_length_type_sn' => $this->string(),
            'vehicle_max_height' => $this->decimal(19, 2),
            'vehicle_max_height_type_sn' => $this->string(),
            'vehicle_max_width' => $this->decimal(19, 2),
            'vehicle_max_width_type_sn' => $this->string(),
            'vehicle_max_weight' => $this->decimal(19, 2),
            'vehicle_max_weight_type_sn' => $this->string(),
            'column_distance' => $this->decimal(19, 2),
            'column_distance_type_sn' => $this->string(),
            'span_distance' => $this->decimal(19, 2),
            'span_distance_type_sn' => $this->string(),
            'column_protection' => $this->boolean(),
            'floor_covering_sn' => $this->string(),
            'anti_dust_covering' => $this->boolean(),
            'height_above_ground' => $this->decimal(19, 2),
            'height_above_ground_type_sn' => $this->string(),
            'load_per_square_meter' => $this->decimal(19, 2),
            'load_per_square_meter_type_sn' => $this->string(),
            'ventilation' => $this->boolean(),
            'conditioning' => $this->boolean(),
            'temperature_mode_sn' => $this->string(),
            'min_temperature' => $this->decimal(19, 2),
            'min_temperature_type_sn' => $this->string(),
            'max_temperature' => $this->decimal(19, 2),
            'max_temperature_type_sn' => $this->string(),
            'security_guard' => $this->boolean(),
            'security_video' => $this->boolean(),
            'security_alarm' => $this->boolean(),
            'fire_alarm' => $this->boolean(),
            'fire_ext_system_sn' => $this->string(),
            'fire_charging' => $this->boolean(),
            'fire_tanks_and_pump' => $this->boolean(),
            'electricity_grid_sn' => $this->string(),
            'heating_system_sn' => $this->string(),
            'water_hot' => $this->boolean(),
            'water_cold' => $this->boolean(),
            'water_sewerage' => $this->boolean(),
            'internet' => $this->boolean(),
            'phone' => $this->boolean(),
            'fiber_optic' => $this->boolean(),
            'created_by' => $this->integer()->notNull(),
            'verified_at' => $this->bigInteger(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `organization_id`
        $this->createIndex(
            'idx-warehouse-organization_id',
            'warehouse',
            'organization_id'
        );

        // add foreign key for table `organization`
        $this->addForeignKey(
            'fk-warehouse-organization_id',
            'warehouse',
            'organization_id',
            'organization',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-warehouse-created_by',
            'warehouse',
            'created_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-warehouse-created_by',
            'warehouse',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `organization`
        $this->dropForeignKey(
            'fk-warehouse-organization_id',
            'warehouse'
        );

        // drops index for column `organization_id`
        $this->dropIndex(
            'idx-warehouse-organization_id',
            'warehouse'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-warehouse-created_by',
            'warehouse'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-warehouse-created_by',
            'warehouse'
        );

        $this->dropTable('warehouse');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m200512_110527_create_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->bigInteger()->notNull(),
            'viewed_at' => $this->bigInteger(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-notification-user_id',
            'notification',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-notification-user_id',
            'notification',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-notification-user_id',
            'notification'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-notification-user_id',
            'notification'
        );

        $this->dropTable('notification');
    }
}

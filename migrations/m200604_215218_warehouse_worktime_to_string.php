<?php

use yii\db\Migration;

/**
 * Class m200604_215218_warehouse_worktime_to_string
 */
class m200604_215218_warehouse_worktime_to_string extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('warehouse', 'work_hours_weekdays_from', $this->string());
        $this->alterColumn('warehouse', 'work_hours_weekdays_to', $this->string());
        $this->alterColumn('warehouse', 'work_hours_weekends_from', $this->string());
        $this->alterColumn('warehouse', 'work_hours_weekends_to', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('warehouse', 'work_hours_weekdays_from IS NOT NULL');
        $this->delete('warehouse', 'work_hours_weekdays_to IS NOT NULL');
        $this->delete('warehouse', 'work_hours_weekends_from IS NOT NULL');
        $this->delete('warehouse', 'work_hours_weekends_to IS NOT NULL');
        $this->execute('ALTER TABLE warehouse ALTER COLUMN work_hours_weekdays_from TYPE integer USING (work_hours_weekdays_from::integer);');
        $this->execute('ALTER TABLE warehouse ALTER COLUMN work_hours_weekdays_to TYPE integer USING (work_hours_weekdays_from::integer);');
        $this->execute('ALTER TABLE warehouse ALTER COLUMN work_hours_weekends_from TYPE integer USING (work_hours_weekdays_from::integer);');
        $this->execute('ALTER TABLE warehouse ALTER COLUMN work_hours_weekends_to TYPE integer USING (work_hours_weekdays_from::integer);');
    }
}

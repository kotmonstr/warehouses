<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dictionary_item`.
 * Has foreign keys to the tables:
 *
 * - `dictionary`
 */
class m200513_025703_create_dictionaryItem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('dictionary_item', [
            'id' => $this->primaryKey(),
            'dictionary_sn' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'system_name' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `dictionary_sn`
        $this->createIndex(
            'idx-dictionaryItem-dictionary_sn',
            'dictionary_item',
            'dictionary_sn'
        );

        // add foreign key for table `dictionary`
        $this->addForeignKey(
            'fk-dictionaryItem-dictionary_sn',
            'dictionary_item',
            'dictionary_sn',
            'dictionary',
            'system_name',
            'CASCADE'
        );

        $timestamp  = time();

        $this->batchInsert('dictionary_item', ['dictionary_sn', 'type', 'system_name', 'title', 'created_at'], [
            ['accounting_system_type', 'default', 'auto', 'Автоматизированная', $timestamp],
            ['accounting_system_type', 'default', 'manual', 'Неавтоматизированная', $timestamp],
            ['classification', 'default', 'a_plus', 'A+', $timestamp],
            ['classification', 'default', 'a', 'A', $timestamp],
            ['classification', 'default', 'b_plus', 'B+', $timestamp],
            ['classification', 'default', 'b', 'B', $timestamp],
            ['classification', 'default', 'c', 'C', $timestamp],
            ['classification', 'default', 'd', 'D', $timestamp],
            ['unit', 'mass', 'kilogram', 'Килограмм', $timestamp],
            ['unit', 'mass', 'ton', 'Тонна', $timestamp],
            ['unit', 'distance', 'meter', 'Метр', $timestamp],
            ['unit', 'temperature', 'celsius', 'Градус Цельсия', $timestamp],
            ['unit', 'square', 'square_meter', 'Квадратный метр', $timestamp],
            ['accounting_control_systems', 'default', 'bms', 'BMS', $timestamp],
            ['accounting_control_systems', 'default', 'check_mode', 'Контрольно-пропускной режим', $timestamp],
            ['accounting_control_systems', 'default', 'employee_access_control', 'Контроль доступа сотрудников', $timestamp],
            ['accounting_control_systems', 'default', 'contractors_nomenclature_database', 'База контрагентов и номенклатуры', $timestamp],
            ['accounting_control_systems', 'default', 'auto_cargo_movement_acc', 'Автоматизированный учет движения грузов', $timestamp],
            ['parking_type', 'default', 'no_parking', 'Нет', $timestamp],
            ['parking_type', 'default', 'inside', 'На территории склада', $timestamp],
            ['parking_type', 'default', 'outside', 'За территорией склада', $timestamp],
            ['storage_system', 'default', 'shelving', 'Стеллажи', $timestamp],
            ['storage_system', 'default', 'floor_storage', 'Напольное хранение', $timestamp],
            ['storage_system', 'default', 'hung_up', 'Вешала', $timestamp],
            ['storage_system', 'default', 'liquid_capacity', 'Емкости для жидкостей', $timestamp],
            ['storage_system', 'default', 'hooks_and_other', 'Крюки и другие складские системы для специфических видов продукции', $timestamp],
            ['floor_covering', 'default', 'concrete', 'Бетон', $timestamp],
            ['floor_covering', 'default', 'asphalt', 'Асфальт', $timestamp],
            ['temperature_mode', 'default', 'manual_adjustment', 'Регулируется, в зависимости от требований к условиям хранения груза', $timestamp],
            ['temperature_mode', 'default', 'auto_microclimate', 'Поддерживается постоянный микроклимат', $timestamp],
            ['fire_ext_system_type', 'default', 'powder', 'Порошковая', $timestamp],
            ['fire_ext_system_type', 'default', 'sprinkler', 'Спринклерная', $timestamp],
            ['fire_ext_system_type', 'default', 'hydrate', 'Гидратная', $timestamp],
            ['electricity_grid_type', 'default', 'own_system', 'Собственная автономная электроподстанция', $timestamp],
            ['electricity_grid_type', 'default', 'common', 'Общие источники энергии', $timestamp],
            ['heating_system_type', 'default', 'own_system', 'Собственный тепловой узел', $timestamp],
            ['heating_system_type', 'default', 'common', 'Общее отопление', $timestamp],
            ['loading_unloading_struct_types', 'default', 'trucks_ramps', 'Пандусы для грузовых машин', $timestamp],
            ['loading_unloading_struct_types', 'default', 'dock_shelters', 'Dock shelters', $timestamp],
            ['loading_unloading_struct_types', 'default', 'ramps', 'Рампы', $timestamp],
            ['loading_unloading_struct_types', 'default', 'rail_ramps', 'Ж/Д рампы', $timestamp],
            ['loading_unloading_struct_types', 'default', 'dock_levellers', 'Dock levellers', $timestamp],
            ['loading_unloading_struct_types', 'default', 'freight_elevators', 'Грузовые лифты', $timestamp],
            ['office_facility_types', 'default', 'office_rooms', 'Офисные помещения', $timestamp],
            ['office_facility_types', 'default', 'conference_rooms', 'Конференц-залы', $timestamp],
            ['office_facility_types', 'default', 'dining_rooms', 'Пункты питания', $timestamp],
            ['office_facility_types', 'default', 'lounges', 'Комнаты отдыха', $timestamp],
            ['office_facility_types', 'default', 'staff_lounges', 'Комнаты отдыха персонала', $timestamp],
            ['office_facility_types', 'default', 'staff_changing_rooms', 'Раздевалки для персонала', $timestamp],
            ['office_facility_types', 'default', 'bathrooms', 'Санитарные узлы', $timestamp],
            ['office_facility_types', 'default', 'server_rooms', 'Серверные комнаты', $timestamp],
            ['office_facility_types', 'default', 'other', 'Другое', $timestamp]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `dictionary`
        $this->dropForeignKey(
            'fk-dictionaryItem-dictionary_sn',
            'dictionary_item'
        );

        // drops index for column `dictionary_sn`
        $this->dropIndex(
            'idx-dictionaryItem-dictionary_sn',
            'dictionary_item'
        );

        $this->dropTable('dictionary_item');
    }
}

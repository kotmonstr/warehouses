<?php
/**
 * This is the file with handles the adding index into table `dictionary_item`
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding index into table `dictionary_item`:
 * - dictionary_sn with system_name
 */
class m200527_033738_addIdx_dictionaryItem_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-dictionaryItem-system_name',
            'dictionary_item',
            ['dictionary_sn', 'system_name'],
            true
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropIndex('idx-dictionaryItem-system_name', 'dictionary_item');
    }
}

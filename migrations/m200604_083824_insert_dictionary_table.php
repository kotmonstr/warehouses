<?php

use yii\db\Migration;

/**
 * Class m200604_083824_insert_dictionary_table
 */
class m200604_083824_insert_dictionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $timestamp  = time();

        $this->batchInsert(
            'dictionary',
            ['system_name', 'title', 'created_at'],
            [
                ['advert_status', 'Cтатус объявления', $timestamp]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('dictionary', ['system_name' => 'advert_status']);
    }
}

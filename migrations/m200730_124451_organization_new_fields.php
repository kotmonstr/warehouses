<?php

use yii\db\Migration;

/**
 * Class m200730_124451_organization_new_fields
 */
class m200730_124451_organization_new_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('organization', 'shareholders', $this->text());
        $this->addColumn('organization', 'edm_type', $this->string());
        $this->addColumn('organization', 'post_address', $this->string());
        $this->addColumn('organization', 'fact_address', $this->string());
        $this->addColumn('organization', 'contact_phone', $this->string());
        $this->addColumn('organization', 'contact_email', $this->string());
        $this->addColumn('organization', 'start_capital', $this->string());
        $this->addColumn('organization', 'registration_date', $this->string());
        $this->addColumn('organization', 'employee_count', $this->integer());
        $this->addColumn('organization', 'bank_details', $this->string());
        $this->addColumn('organization', 'authorized_head_contact', $this->string());
        $this->addColumn('organization', 'authorized_sign_contact', $this->string());
        $this->addColumn('organization', 'authorized_security_contact', $this->string());
        $this->addColumn('organization', 'partner_type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('organization', 'shareholders');
        $this->dropColumn('organization', 'edm_type');
        $this->dropColumn('organization', 'post_address');
        $this->dropColumn('organization', 'fact_address');
        $this->dropColumn('organization', 'contact_phone');
        $this->dropColumn('organization', 'contact_email');
        $this->dropColumn('organization', 'start_capital');
        $this->dropColumn('organization', 'registration_date');
        $this->dropColumn('organization', 'employee_count');
        $this->dropColumn('organization', 'bank_details');
        $this->dropColumn('organization', 'authorized_head_contact');
        $this->dropColumn('organization', 'authorized_sign_contact');
        $this->dropColumn('organization', 'authorized_security_contact');
        $this->dropColumn('organization', 'partner_type');
    }
}

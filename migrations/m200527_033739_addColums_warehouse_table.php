<?php
/**
 * This is the file with handles the adding columns in `warehouse` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding columns in `warehouse` table.
 * Columns:
 *
 * - `general_is_set`
 * - `external_is_set`
 * - `internal_is_set`
 * - `documents_is_set`
 */
class m200527_033739_addColums_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('warehouse', 'general_is_set', $this->boolean());
        $this->addColumn('warehouse', 'external_is_set', $this->boolean());
        $this->addColumn('warehouse', 'internal_is_set', $this->boolean());
        $this->addColumn('warehouse', 'documents_is_set', $this->boolean());
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropColumn('warehouse', 'documents_is_set');
        $this->dropColumn('warehouse', 'internal_is_set');
        $this->dropColumn('warehouse', 'external_is_set');
        $this->dropColumn('warehouse', 'general_is_set');
    }
}

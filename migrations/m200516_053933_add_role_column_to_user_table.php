<?php
/**
 * This is the file with handles adding columns to table `user`.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles adding columns to table `user`.
 */
class m200516_053933_add_role_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->string());

        $this->insert(
            'user', [
                'name' => 'admin',
                'email' => 'admin@admin.ru',
                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password'),
                'email_verified' => 1,
                'role' => 'admin',
                'created_at' => time(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->delete('user', ['email' => 'admin@admin.ru', 'name' => 'admin']);
        $this->dropColumn('user', 'role');
    }
}

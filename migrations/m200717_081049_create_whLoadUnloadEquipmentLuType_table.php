<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_load_unload_equipment_lu_type`.
 * Has foreign keys to the tables:
 *
 * - `wh_load_unload_equipment`
 */
class m200717_081049_create_whLoadUnloadEquipmentLuType_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_load_unload_equipment_lu_type', [
            'id' => $this->primaryKey(),
            'equipment_id' => $this->integer()->notNull(),
            'loading_unloading_method_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `equipment_id`
        $this->createIndex(
            'idx-whLoadUnloadEquipmentLuType-equipment_id',
            'wh_load_unload_equipment_lu_type',
            'equipment_id'
        );

        // add foreign key for table `wh_load_unload_equipment`
        $this->addForeignKey(
            'fk-whLoadUnloadEquipmentLuType-equipment_id',
            'wh_load_unload_equipment_lu_type',
            'equipment_id',
            'wh_load_unload_equipment',
            'id',
            'CASCADE'
        );

        // insert values from wh_load_unload_equipment
        $this->execute(
            "INSERT INTO wh_load_unload_equipment_lu_type (equipment_id, loading_unloading_method_sn, created_at)
            SELECT id, loading_unloading_method_sn, created_at
            FROM wh_load_unload_equipment"
        );

        //drop column load_unload_method in wh_load_unload_equipment table
        $this->dropColumn('wh_load_unload_equipment', 'loading_unloading_method_sn');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //add column load_unload_method to wh_load_unload_equipment table
        $this->addColumn('wh_load_unload_equipment', 'loading_unloading_method_sn', $this->string());

        // insert values from wh_load_unload_equipment_lu_type
        $this->execute(
            "UPDATE wh_load_unload_equipment SET loading_unloading_method_sn=
            (SELECT loading_unloading_method_sn
            FROM wh_load_unload_equipment_lu_type
            WHERE wh_load_unload_equipment.id=wh_load_unload_equipment_lu_type.equipment_id
            LIMIT 1)"
        );

        // drops foreign key for table `wh_load_unload_equipment`
        $this->dropForeignKey(
            'fk-whLoadUnloadEquipmentLuType-equipment_id',
            'wh_load_unload_equipment_lu_type'
        );

        // drops index for column `equipment_id`
        $this->dropIndex(
            'idx-whLoadUnloadEquipmentLuType-equipment_id',
            'wh_load_unload_equipment_lu_type'
        );

        $this->dropTable('wh_load_unload_equipment_lu_type');
    }
}

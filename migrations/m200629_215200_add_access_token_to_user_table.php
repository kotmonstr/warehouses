<?php

use yii\db\Migration;

/**
 * Class m200629_215200_add_access_token_to_user_table
 */
class m200629_215200_add_access_token_to_user_table extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->addColumn('user', 'access_token', $this->string());
        $this->addColumn('user', 'access_token_expired_at', $this->integer());

        $this->addColumn('user', 'refresh_token', $this->string());
        $this->addColumn('user', 'refresh_token_expired_at', $this->integer());
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'access_token');
        $this->dropColumn('user', 'access_token_expired_at');

        $this->dropColumn('user', 'refresh_token');
        $this->dropColumn('user', 'refresh_token_expired_at');
    }
}

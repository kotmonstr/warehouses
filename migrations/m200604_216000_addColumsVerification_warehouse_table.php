<?php
/**
 * This is the file with handles the adding columns in `warehouse` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding columns in `warehouse` table.
 * Columns:
 *
 * - `verification_status_sn`
 * - `verification_comment`
 */
class m200604_216000_addColumsVerification_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('warehouse', 'verification_status_sn', $this->string());
        $this->addColumn('warehouse', 'verification_comment', $this->text());
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropColumn('warehouse', 'verification_comment');
        $this->dropColumn('warehouse', 'verification_status_sn');
    }
}

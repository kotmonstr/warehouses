<?php

use yii\db\Migration;

/**
 * Class m200604_083847_insert_dictionaryItem_table
 */
class m200604_083847_insert_dictionaryItem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $timestamp  = time();

        $this->batchInsert(
            'dictionary_item',
            ['dictionary_sn', 'type', 'system_name', 'title', 'created_at'],
            [
                ['advert_status', 'default', 'published', 'Опубликовано', $timestamp],
                ['advert_status', 'default', 'draft', 'Черновик', $timestamp],
                ['advert_status', 'default', 'adjustment', 'Корректировка', $timestamp],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('dictionary_item', ['dictionary_sn' => 'advert_status']);
    }

}

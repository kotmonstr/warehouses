<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advert`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 * - `user`
 */
class m200512_111250_create_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('advert', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'storage_type_sn' => $this->string(),
            'available_space' => $this->decimal(19, 2),
            'available_space_type_sn' => $this->string(),
            'unit_cost_per_day' => $this->decimal(19, 2),
            'available_from' => $this->bigInteger(),
            'available_to' => $this->bigInteger(),
            'manager_name' => $this->string(),
            'manager_phone_number' => $this->string(),
            'manager_email' => $this->string(),
            'comment' => $this->text(),
            'status' => $this->string(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'deleted_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-advert-warehouse_id',
            'advert',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-advert-warehouse_id',
            'advert',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-advert-created_by',
            'advert',
            'created_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-advert-created_by',
            'advert',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-advert-warehouse_id',
            'advert'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-advert-warehouse_id',
            'advert'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-advert-created_by',
            'advert'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-advert-created_by',
            'advert'
        );

        $this->dropTable('advert');
    }
}

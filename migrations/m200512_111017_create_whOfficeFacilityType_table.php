<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wh_office_facility_type`.
 * Has foreign keys to the tables:
 *
 * - `warehouse`
 */
class m200512_111017_create_whOfficeFacilityType_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wh_office_facility_type', [
            'id' => $this->primaryKey(),
            'warehouse_id' => $this->integer()->notNull(),
            'types' => $this->string(),
            'office_facility_types_sn' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `warehouse_id`
        $this->createIndex(
            'idx-whOfficeFacilityType-warehouse_id',
            'wh_office_facility_type',
            'warehouse_id'
        );

        // add foreign key for table `warehouse`
        $this->addForeignKey(
            'fk-whOfficeFacilityType-warehouse_id',
            'wh_office_facility_type',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `warehouse`
        $this->dropForeignKey(
            'fk-whOfficeFacilityType-warehouse_id',
            'wh_office_facility_type'
        );

        // drops index for column `warehouse_id`
        $this->dropIndex(
            'idx-whOfficeFacilityType-warehouse_id',
            'wh_office_facility_type'
        );

        $this->dropTable('wh_office_facility_type');
    }
}

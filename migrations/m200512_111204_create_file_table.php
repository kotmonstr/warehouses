<?php

use yii\db\Migration;

/**
 * Handles the creation of table `file`.
 * Has foreign keys to the tables:
 *
 * - `document`
 */
class m200512_111204_create_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'document_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'path' => $this->string(),
            'size' => $this->integer(),
            'mime_type' => $this->string(),
            'created_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `document_id`
        $this->createIndex(
            'idx-file-document_id',
            'file',
            'document_id'
        );

        // add foreign key for table `document`
        $this->addForeignKey(
            'fk-file-document_id',
            'file',
            'document_id',
            'document',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `document`
        $this->dropForeignKey(
            'fk-file-document_id',
            'file'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            'idx-file-document_id',
            'file'
        );

        $this->dropTable('file');
    }
}

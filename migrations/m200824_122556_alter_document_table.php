<?php

use yii\db\Migration;

/**
 * Class m200824_122556_alter_document_table
 */
class m200824_122556_alter_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%document}}', 'entity_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%document}}', 'entity_id', $this->integer()->notNull());
    }

}

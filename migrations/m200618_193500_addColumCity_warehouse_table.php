<?php
/**
 * This is the file with handles the adding columns in `warehouse` table.
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the adding columns in `warehouse` table.
 * Column:
 *
 * - `address_city_ym_geocoder`
 */
class m200618_193500_addColumCity_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('warehouse', 'address_city_ym_geocoder', $this->string());
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->dropColumn('warehouse', 'address_city_ym_geocoder');
    }
}

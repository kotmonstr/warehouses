<?php

use yii\db\Migration;

/**
 * Class m200703_162311_rename_reserve_status_column
 */
class m200703_162311_rename_reserve_status_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('reserve', 'status', 'status_sn');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('reserve', 'status_sn', 'status');
    }
}

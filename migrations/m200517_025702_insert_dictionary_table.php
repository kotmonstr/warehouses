<?php
/**
 * This is the file with handles the insert into table `dictionary`
 * php version 7
 */
use yii\db\Migration;

/**
 * Handles the insert into table `dictionary`
 */
class m200517_025702_insert_dictionary_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeUp()
    {
        $timestamp  = time();

        $this->batchInsert(
            'dictionary',
            [
                'system_name', 'title', 'created_at'
            ],
            [
                ['cranage_types', 'Крановое оборудование', $timestamp],
                ['loading_unloading_methods', 'Способ загрузки', $timestamp],
                ['loading_unloading_tech_types', 'Погрузочно-разгрузочная техника', $timestamp],
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function safeDown()
    {
        $this->delete('dictionary', ['system_name' => 'cranage_types']);
        $this->delete('dictionary', ['system_name' => 'loading_unloading_methods']);
        $this->delete('dictionary', ['system_name' => 'loading_unloading_tech_types']);
    }
}

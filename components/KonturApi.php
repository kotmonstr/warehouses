<?php
// Docs: https://developer.kontur.ru/focus

namespace app\components;

use yii\httpclient\Client;
use yii\base\Component;

/**
 * Class KonturApi
 * @package app\components
 *
 * @property Client|null $httpClient
 * @property string $key kontur api key
 */
class KonturApi extends Component
{
    private $httpClient;
    public $key;

    const BASE_URL = 'https://focus-api.kontur.ru/api3/';
    const METHOD_REQ = 'req';
    const METHOD_EGR_DETAILS = 'egrDetails';
    const METHOD_ANALYTICS = 'analytics';
    const METHOD_BRIEF_REPORT = 'briefReport';

    /**
     * find organization info by inn
     *
     * @param  string $inn
     * @return array
     */
    public function findOrganizationByINN($inn)
    {
        $requisites = $this->getHttpClient()
            ->get(self::METHOD_REQ, ['key' => $this->key, 'inn' => $inn])
            ->send();

        if (!$requisites->isOk) {
            return ['error' => $requisites->content];
        }

        if (count($requisites->data) == 0) {
            return ['error' => 'Данных не найдено'];
        }

        $extendedRequisites = $this->getHttpClient()
            ->get(self::METHOD_EGR_DETAILS, ['key' => $this->key, 'inn' => $inn])
            ->send();

        $analytics = $this->getHttpClient()
            ->get(self::METHOD_ANALYTICS, ['key' => $this->key, 'inn' => $inn])
            ->send();

        $requisites = $requisites->data[0];
        $extendedRequisites = $extendedRequisites->data[0];
        $analytics = $analytics->data[0]['analytics'];

        if (array_key_exists('UL', $requisites)) {
            $ulReq = $requisites['UL'];
            $ulExtReq = $extendedRequisites['UL'];
            $commonInfo = $this->getCommonInfo($requisites, $ulReq);

            return array_merge($commonInfo, [
                'legal_type' => 'UL',
                'title' => $ulReq['legalName']['short'],
                'mainActivity' => $this->getActivity($ulExtReq['activities']['principalActivity']),
                'address' => $this->getAddress($ulReq['legalAddress']['parsedAddressRF']),
                'taxationType' => $this->getTaxationType($analytics),
                'shareholders' => $this->getShareholders($ulExtReq),
                'start_capital' => @$ulExtReq['statedCapital']['sum'] ?: null,
                'registration_date' => $ulReq['registrationDate'],
            ]);
        }

        if (array_key_exists('IP', $requisites)) {
            $ipReq = $requisites['IP'];
            $ipExtReq = $extendedRequisites['IP'];
            $commonInfo = $this->getCommonInfo($requisites, $ipReq);

            return array_merge($commonInfo, [
                'legal_type' => 'IP',
                'title' => 'ИП '. $ipReq['fio'],
                'mainActivity' => $this->getActivity($ipExtReq['activities']['principalActivity']),
                'taxationType' => $this->getTaxationType($analytics),
                'registration_date' => $ipReq['registrationDate'],
            ]);
        }
    }

    /**
     * get http client
     *
     * @return Client
     */
    private function getHttpClient()
    {
        if (!is_object($this->httpClient)) {
            $this->httpClient = new Client(['baseUrl' => self::BASE_URL]);
        }

        return $this->httpClient;
    }

    /**
     * get common info for IP and UL
     *
     * @param  array $requisites
     * @param  array $typeRequisites IP or UL requisites
     * @return array
     */
    private function getCommonInfo($requisites, $typeRequisites)
    {
        return [
            'inn' => $requisites['inn'],
            'ogrn' => $requisites['ogrn'],
            'kpp' => isset($typeRequisites['kpp']) ? $typeRequisites['kpp'] : '',
            'okpo' => isset($typeRequisites['okpo']) ? $typeRequisites['okpo'] : '',
        ];
    }

    /**
     * get address
     *
     * @param  array $address
     * @return string
     */
    private function getAddress($address)
    {
        if (isset($address['city'])) {
            $city = "{$address['city']['topoShortName']}. {$address['city']['topoValue']}";
        } elseif (isset($address['settlement'])) {
            $city = "{$address['settlement']['topoShortName']}. {$address['settlement']['topoValue']}";
        } else {
            $city = "{$address['regionName']['topoShortName']}. {$address['regionName']['topoValue']}";
        }

        $zipCode = $address['zipCode'];
        $house = $address['house']['topoValue'];
        $street = "{$address['street']['topoShortName']}. {$address['street']['topoValue']}";

        return "$zipCode, $city, $street, $house";
    }

    /**
     * getActivity
     *
     * @param  array $activity
     * @return string
     */
    private function getActivity($activity)
    {
        return "{$activity['code']} {$activity['text']}";
    }

    /**
     * getTaxationType
     *
     * @param  array $data
     * @return string
     */
    private function getTaxationType($data)
    {
        if (isset($data['m7027']) && $data['m7027']) {
            return 'УСН';
        } else {
            return 'ОСНО';
        }
    }

    /**
     * Get express report by inn or ogrn
     *
     * @param  string[] $params ['inn' => '123456789111'] || ['ogrn' => '123456789111']
     * @return string
     */
    public function downloadBriefReport($params)
    {
        $urlBriefReport = self::BASE_URL . self::METHOD_BRIEF_REPORT. '?key=' . $this->key . '&pdf=False&';
        foreach ($params as $name => $value) {
            $urlBriefReport .= $name . '=' . $value . '&';
        }

        $response = $this->getHttpClient()
            ->get($urlBriefReport)
            ->send();

        if (!$response->isOk || count($response->data) < 1
            || !isset($response->data[0]['briefReport'])
            || ! isset($response->data[0]['briefReport']['href'])
        ) {
            return ['error' => $response->content];
        }

        return $response->data[0]['briefReport']['href'];
    }

    /**
     * @param $ulExtReq
     * @return string
     */
    public function getShareholders($ulExtReq)
    {
        $result = [];

        if (!array_key_exists('shareholders', $ulExtReq)) {
            return null;
        }

        if (!empty($ulExtReq['shareholders']['shareholdersUL'])) {
            foreach ($ulExtReq['shareholders']['shareholdersUL'] as $holder) {
                $result[] = $holder['fullName'] . ' (' . $holder['inn'] . ')';
            }
        }

        if (!empty($ulExtReq['shareholders']['shareholdersFL'])) {
            foreach ($ulExtReq['shareholders']['shareholdersFL'] as $holder) {
                $result[] = $holder['fio'];
            }
        }

        if (empty($ulExtReq['shareholders']['shareholdersFL']) && empty($ulExtReq['shareholders']['shareholdersUL'])) {
            foreach ($ulExtReq['shareholders']['shareholdersOther'] as $holder) {
                $result[] = $holder['fullName'];
            }
        }

        return implode(', ', $result);
    }
}

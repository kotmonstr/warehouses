<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Exception;

/**
 * Class YMaps
 * @package app\components
 *
 * @property-read string $key
 * @property-read Client $_client
 */
class YMaps extends Component
{
    public $key;

    protected $_client;

    const SUGGEST_URL = 'https://suggest-maps.yandex.ru/suggest-geo';

    /** {@inheritDoc} */
    public function init()
    {
        parent::init();

        $this->_client = new Client();
    }

    /**
     * @param string $query
     * @param float|null $lat
     * @param float|null $lon
     * Todo: $limit
     * Todo: $offset
     * @return array
     * @throws Exception
     */
    public function suggest(string $query, float $lat = null, float $lon = null): array
    {
        $params = [
            'text' => $query,
            'bases' => 'geo',
            'lang' => 'ru_RU',
            'callback' => '',
            'results' => 10,
            'v' => 9,
        ];

        if (isset($lat, $lon)) {
            $params['ll'] = implode(',', [$lat, $lon]);
        }

        $response = $this->_client->get(array_merge([self::SUGGEST_URL], $params))->send();
        $response = Json::decode($response->content);

        if (!$response || !array_key_exists('results', $response) || empty($response['results'])) {
            return [];
        }

        $result = [];

        foreach ($response['results'] as $suggest) {
            $item = [];

            if (array_key_exists('text', $suggest)) {
                $item['address'] = $suggest['text'];
            } elseif (array_key_exists('title', $suggest) && array_key_exists('text', $suggest['title'])) {
                $item['address'] = $suggest['title']['text'];
            } else {
                continue;
            }

            if (isset($lat, $lon) && array_key_exists('distance', $suggest)) {
                $item['distance'] = $suggest['distance']['value'];
            }

            $result[] = $item;
        }

        if (isset($lat, $lon)) {
            usort($result, function ($a, $b) {
                return $a['distance'] > $b['distance'];
            });
        }

        return ArrayHelper::getColumn($result, 'address');
    }

    /**
     * @param string $query
     * @return array
     */
    public function search(string $query): array
    {
        // Todo

        return [];
    }
}
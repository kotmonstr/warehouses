<?php

/* @var $this yii\web\View */
/* @var $model app\models\forms\SignInForm */
/* @var $token string */

use yii\helpers\Html;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <div>

        <?= Html::beginForm(['/user/reset-password-confirm'], 'post') ?>

        <?= Html::textInput('tokenReset', $token, ['class' => 'fade']) ?>

        <div class="form-group">
            <?= Html::passwordInput('passwordReset', '', ['class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Пароль']) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сбросить', ['class' => 'btn btn-primary col-sm-12', 'name' => 'reset-password-confirm-button']) ?>
        </div>



        <?= Html::endForm() ?>

    </div>


</div>

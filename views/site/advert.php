<?php

/* @var $this \yii\web\View */
/* @var $model app\models\Advert */

use app\models\User;
use app\models\File;
use app\models\Document;
use app\models\Warehouse;
use yii\helpers\Html;

$entityId = $model->warehouse->id;
$documents = Document::findAllByEntity($entityId, Warehouse::class);
$filePaths = [];
foreach ($documents as $document) {
    if (substr_compare($document->entity_field, 'photo_', 0, 6) == 0) {
        $filePaths[] = File::findOne(['document_id' => $document->id])->path;
    }
}
$filePath1 = count($filePaths) ? $filePaths[0] : '';
?>

<div class="ob-container">
    <div class="row">

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-4">
                    <div style="height: 250px; background: url(<?= $filePath1 ? $filePath1 : '/img/camera.svg' ?>) center no-repeat; background-size: cover;"></div>
                </div>
                <div class="col-sm-8" style="height: 250px; position: relative;">
                    <h3><?= Html::a($model->warehouse->title, ['advert-info?id=' . $model->id]) ?></h3>
                    <div><i class="fa fa-map-marker" aria-hidden="true"></i> Адрес склада: <?= $model->warehouse->address_ym_geocoder ?></div>
                    <div class="ob-comment" style="height: 100px; margin-top:15px; overflow: hidden;">
                        <?php if (empty($model->comment) && empty($model->warehouse->description)) { ?>
                            <div>Нет информации</div>
                        <?php } ?>

                        <?php if ($model->comment) { ?>
                            <div><b>Комментарий:</b><br><?= $model->comment ?></div>
                        <?php } ?>

                        <?php if ($model->warehouse->description) { ?>
                            <div><b>Описание:</b><br><?= $model->warehouse->description ?></div>
                        <?php } ?>
                    </div>
                    <div style="position: absolute; bottom: 0; left: 15px; right: 15px;">
                        <span class="ob-span bg-primary">
                            <?= $model->available_space ?> <?= $model->available_space_type->title_short; ?>
                        </span>

                        <?php if ($model->warehouse->classification) { ?>
                            <span class="ob-span bg-primary" style="margin-left: 10px;">
                                Классификация: <?= $model->warehouse->classification->title ?>
                            </span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!(Yii::$app->user->isGuest || (Yii::$app->user->identity->role == User::ROLE_HOLDER))) { ?>

        <div class="col-sm-12">
            <hr style="border-color: #777;">
            <div class="row">
                <div class="col-sm-6">
                    <h4 style="font-weight: bold;"><?= $model->unit_cost_per_day ?>&nbsp;&#8381;/<?= $model->available_space_type->title_short; ?>/день</h4>
                </div>
                <div class="col-sm-6" style="text-align:right;">
                    <div style="display:flex; flex-direction:row; align-items:center; justify-content:flex-end;">
                        <a href='mailto: <?= $model->manager_email ?>'><div class="text-primary btn"><?= $model->manager_email ?></div></a>

                        <?= $model->manager_phone_number ? "<div style='width:3em;'></div><a href='callto: {$model->manager_phone_number}'><div class='btn btn-primary'>{$model->manager_phone_number}</div></a>" : "" ?>
                    </div>
                </div>
            </div>
        </div>

        <?php } ?>
    </div>
</div>

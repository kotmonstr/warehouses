<?php

/**
 * @var $this View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 */

use yii\helpers\Html;
use yii\web\View;

$this->title = $name;

?>

<div class="site-error">
    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
        <p class="lead"><?= nl2br(Html::encode($message)) ?></p>
    </div>
</div>

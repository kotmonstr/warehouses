<?php

/* @var $this \yii\web\View */
/* @var $model app\models\Advert */
/* @var $photos string[] */

use app\widgets\geocoder\GeocoderAsset;
use app\models\User;
use yii\helpers\Json;
use app\models\Dictionary;
use app\models\DictionaryItem;
use yii\bootstrap\Carousel;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->warehouse->title;
$this->params['breadcrumbs'][] = ['label' => 'Поиск', 'url' => ["/site/adverts?AdvertsSearchForm[city]={$model->warehouse->address_city_ym_geocoder}"]];
$this->params['breadcrumbs'][] = $this->title;

GeocoderAsset::register($this);

if (Yii::$app->user->isGuest || Yii::$app->user->identity->role == User::ROLE_HOLDER) {

    $contentString = '<div class="row"><p style="margin-left: 3em;">Дополнительная информация видна только зарегистрированным арендаторам.</p></div>';
    $iconString = '!';

} else {

    $costString = '<p style="font-weight: bold;">' .
            $model->unit_cost_per_day . ' &#8381; / ' .
            $model->available_space_type->title_short . '/ день</p>';

    $spaceString = '<p>' . $model->available_space . ' ' .
            $model->available_space_type->title_short . '</p>';

    $contactString = '<span style="padding:.3em;color:white;border:1px solid white;border-radius:5px; margin-left: -5em;" class="bg-primary">';
    if ($model->manager_phone_number) {
        $contactString .= $model->manager_phone_number;
    } else {
        $contactString .= $model->manager_email;
    }
    $contactString .= '</span>';

    $iconString = $model->unit_cost_per_day . ' ₽ / ' . $model->available_space_type->title_short . ' / день';

    $contentString = '<div style="width:30em;"><div class="row"><div class="col-sm-5">' .
                        $costString .
                        $spaceString .
                    '</div><div class="col-sm-7" style="margin-top:2em;text-align:center;">' .
                        $contactString .
                    '</div></div></div>';
}

$placemark = [
    'geometry' => [
        'type' => 'Point',
        'coordinates' => [$model->warehouse->address_lon, $model->warehouse->address_lat],
    ],
    'properties' => [
        'balloonContentHeader' => '<h4 style="text-align: center;" class="text-primary">' . $model->warehouse->title . '</h4>',
        'balloonContentBody' => $contentString,
        'balloonContentFooter' => '',
        'hintContent' => $model->warehouse->title,
        'iconContent' => $iconString,
    ],
];

$placemarkJson = Json::encode($placemark);
$lon = $model->warehouse->address_lon;
$lat = $model->warehouse->address_lat;

$mapAdvertInit = <<<JS
    ymaps.ready(initAdvertMap);

    function initAdvertMap() {
        var myMap = new ymaps.Map('containerMapAdInfo', {
            center: [$lon, $lat],
            controls: [],
            zoom: 15
        });

        var myGeoObject = new ymaps.GeoObject($placemarkJson, {
            preset: 'islands#redStretchyIcon',
            draggable: false
        });

        myMap.geoObjects.add(myGeoObject);
    }
JS;

$this->registerJs($mapAdvertInit, yii\web\View::POS_END);

$warehouse = $model->warehouse;

function showWithDictionary($warehouse, $field, $dictionary_sn) {
    if ($warehouse[$field]) {
        echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$warehouse->attributeLabels()[$field]}</div><div class='col-sm-6'>";
        echo Dictionary::findDictItems($dictionary_sn)[$warehouse[$field]];
        echo "</div></div>";
    }
}

function showWithDictionaryItem($warehouse, $field, $dictionary_sn, $dictionary_type ) {
    if ($warehouse[$field]) {
        echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$warehouse->attributeLabels()[$field]}</div><div class='col-sm-6'>";
        echo "{$warehouse[$field]} ";
        echo DictionaryItem::findOne(['title' => Dictionary::findDictItems($dictionary_sn, $dictionary_type)[$warehouse[$field . '_type_sn']]])->title_short;
        echo "</div></div>";
    }
}

function showSingleValue($warehouse, $field) {
    if ($warehouse[$field]) {
        echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$warehouse->attributeLabels()[$field]}</div><div class='col-sm-6'>";
        echo "{$warehouse[$field]} ";
        echo "</div></div>";
    }
}

function showBoolValue($warehouse, $field) {
    if ($warehouse[$field]) {
        echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$warehouse->attributeLabels()[$field]}</div><div class='col-sm-6'>";
        if ($warehouse[$field]) { echo "есть"; } else {echo "нет"; }
        echo "</div></div>";
    }
}

function showRelationsSimple($title, $warehouse, $relations, $field, $dictionary_sn) {
    echo "<div class='row' style='margin-top: 1em;'><div class='col-sm-6'><h5>{$title}</h5></div><div class='col-sm-6'>";
    if ($warehouse[$relations]) {
        foreach ($warehouse[$relations] as $relation) {
            if ($relation[$field]) {
                echo "<p>" . Dictionary::findDictItems($dictionary_sn)[$relation[$field]] . "</p>";
            }
        }
    } else {
        echo "нет";
    }
    echo "</div></div>";
}

function showRelationsOffice($title, $warehouse, $relations, $field, $type) {
    echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$title}</div><div class='col-sm-6'>";
    if ($warehouse[$relations]) {
        foreach ($warehouse[$relations] as $relation) {
            if ($relation->types == $type) {
                echo "<p>" . Dictionary::findDictItems('office_facility_types')[$relation[$field]] . "</p>";
            }
        }
    } else {
        echo "нет";
    }
    echo "</div></div>";
}

function showRelationsTech($title, $warehouse, $relations, $dictionary_sn) {
    echo "<div class='row'><h5 style='margin-left: 1em;'>{$title}</h5>";
    if ($warehouse[$relations]) {
        foreach ($warehouse[$relations] as $relation) {
            echo "<div class'row' style='margin-left: 1em; margin-bottom: 1em;'>";

            if ($relation->loading_unloading_tech_type_sn) {
                echo "<div class='row'><div class='col-sm-6' style='color: gray;'>{$relation->attributeLabels()['loading_unloading_tech_type_sn']}</div><div class='col-sm-6'>";
                echo Dictionary::findDictItems($dictionary_sn)[$relation->loading_unloading_tech_type_sn];
                echo "</div></div>";
            }

            showSingleValue($relation, 'amount');
            showWithDictionaryItem($relation, 'capacity', 'unit', 'mass');
            showWithDictionary($relation, 'loading_unloading_method_sn', 'loading_unloading_methods');
            echo "</div>";
        }
    } else {
        echo "<p style='margin-left: 1em; color: gray;'>нет</p>";
    }
    echo "</div>";
}

$showImageModal = <<<JS
    var images = document.getElementsByClassName('thumbs_image_warehouse');
    for (var i = 0; i < images.length; i++) {
        images[i].addEventListener('click', function(){
            var wrapper = document.getElementById('notification-modal-wrapper');
            wrapper.innerHTML = '';
            wrapper.style.display = 'block';

            var modal = document.createElement('div');
            modal.classList.add('notification-modal');
            modal.style.width = '570px';
            modal.style.marginTop = '5%';

            var img = document.createElement('img');
            img.setAttribute('src', this.getAttribute('src'));
            img.style.width = '500px';
            img.style.heigth = 'auto';
            img.style.border = '1px solid white';
            img.style.borderRadius = '10px';

            var close = document.createElement('div');
            close.classList.add('notification-close');
            close.innerHTML = '&times;';
            close.addEventListener('click', function () {
                wrapper.style.display = 'none';
            });

            modal.appendChild(close);
            modal.appendChild(img);
            wrapper.appendChild(modal);
        });
    }
JS;

$this->registerJs($showImageModal, yii\web\View::POS_END);
?>

<div id="notification-modal-wrapper"></div>

<h1><?=Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-sm-12" id="containerMapAdInfo" style="height:250px;"></div>
</div>

<div style="margin-top: 15px;">
    <div class="row">
        <div class="col-sm-8">

            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <h4><?= $model->warehouse->title ?></h4>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> Адрес склада: <?= $model->warehouse->address_ym_geocoder ?></p>

                    <div class="col-sm-3">
                        <h5>Параметр</h5>
                        <p>Значение</p>
                    </div>

                    <div class="col-sm-3">
                        <h5>Параметр</h5>
                        <p>Значение</p>
                    </div>

                    <div class="col-sm-3">
                        <h5>Параметр</h5>
                        <p>Значение</p>
                    </div>

                    <div class="col-sm-3">
                        <h5>Параметр</h5>
                        <p>Значение</p>
                    </div>
                </div>
            </div>


            <?php
                $images = [];
                $thumbs = [];
                $length = count($photos);
                if ($length) {
                    echo '<div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;"><div class="row">';

                    foreach ($photos as $key => $path) {
                        $image = [];
                        $image['content'] = "<img src='{$path}' style='width: 300px; height: 300px; border: 1px solid gray; border-radius: 10px; margin: 0 auto;'/>";
                        $key++;
                        $image['caption'] = "{$key} из {$length}";
                        $images[] = $image;
                        $thumbs[] = "<img src='{$path}' class='thumbs_image_warehouse' style='width: 100px; height: 100px; border: 1px solid #F3F3F3; border-radius: 10px; margin: .5em; cursor: pointer;'/>";
                    }

                    echo Carousel::widget(
                        [
                            'items' => $images,
                            'options' => [
                                'style' => 'width: 100%; heigth: 320px; margin: 10px auto; border: 1px solid gray; border-radius: 10px; background-color: gray;',
                                'data-interval' => 0,
                            ],
                        ]
                    );

                    echo "<div class='row' style='text-align: center;'>";
                    foreach ($thumbs as $thumb) {
                        echo $thumb;
                    }
                    echo "</div>";

                    echo "</div>";
                    echo "</div>";
                }
            ?>

            <?php if ($model->warehouse->description) { ?>
            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <h4>Описание</h4>
                    <p><?= $model->warehouse->description ?></p>
                </div>
            </div>
            <?php } ?>

            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <h4>Детальная информация</h4>

                    <?php
                        echo "<h4>Основное</h4>";

                        echo "<div class='row'><div class='col-sm-6' style='color: gray;'>Часы работы</div><div class='col-sm-6''>";
                        if ($warehouse->work_hours_24_7) {
                            echo "круглосуточно";
                        } else {
                            echo "по будням:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$warehouse->work_hours_weekdays_from} - {$warehouse->work_hours_weekdays_to}";
                            if ($warehouse->work_hours_weekends_from && $warehouse->work_hours_weekends_to) {
                                echo "<br>по выходным:&nbsp;{$warehouse->work_hours_weekends_from} - {$warehouse->work_hours_weekends_to}";
                            }
                        }
                        echo "</div></div>";

                        echo "<h4>Внешние параметры</h4>";

                        showWithDictionary($warehouse, 'accounting_system_sn', 'accounting_system_type');
                        showWithDictionary($warehouse, 'classification_sn', 'classification');

                        echo "<h5>Характеристики здания</h5>";
                        showWithDictionaryItem($warehouse, 'square_area', 'unit', 'square');
                        showWithDictionaryItem($warehouse, 'built_up_area', 'unit', 'square');
                        showWithDictionaryItem($warehouse, 'ceiling_height', 'unit', 'distance');
                        showSingleValue($warehouse, 'floors');

                        echo "<h5>Организация движения</h5>";
                        showBoolValue($warehouse, 'maneuvering_area');
                        showSingleValue($warehouse, 'entries_amount');

                        echo "<h5>Зона разгрузки</h5>";
                        showBoolValue($warehouse, 'loading_ramp');
                        showSingleValue($warehouse, 'automatic_gates_amount');

                        showRelationsSimple('Системы контроля и учета', $warehouse, 'wh_accounting_control_system', 'accounting_control_system_sn', 'accounting_control_systems');

                        echo "<h5>Прилегающая территория</h5>";
                        showBoolValue($warehouse, 'outer_lighting');
                        showBoolValue($warehouse, 'outer_security');
                        showBoolValue($warehouse, 'outer_beautification');
                        showBoolValue($warehouse, 'outer_fencing');
                        showSingleValue($warehouse, 'outer_entries_amount');

                        echo "<h5>Площадки для автотранспорта</h5>";
                        showWithDictionary($warehouse, 'car_parking_sn', 'parking_type');
                        showWithDictionary($warehouse, 'truck_parking_sn', 'parking_type');
                        showBoolValue($warehouse, 'truck_maneuvering_area');

                        echo "<h5>ЖД</h5>";
                        showBoolValue($warehouse, 'rail_road');

                        echo "<h5>Ограничения для ТС</h5>";
                        showWithDictionaryItem($warehouse, 'distance_from_highway', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'vehicle_max_length', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'vehicle_max_height', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'vehicle_max_width', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'vehicle_max_weight', 'unit', 'mass');

                        echo "<h4>Внутренние параметры</h4>";

                        showRelationsTech('Погрузочно-разгрузочная техника', $warehouse, 'wh_load_unload_equipment', 'loading_unloading_tech_types');

                        showRelationsTech('Крановое оборудование', $warehouse, 'wh_load_unload_cranage', 'cranage_types');

                        showRelationsSimple('Системы хранения', $warehouse, 'wh_storage_system', 'storage_system_sn', 'storage_system');

                        echo "<h5>Внутренние конструкции</h5>";
                        showWithDictionaryItem($warehouse, 'column_distance', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'span_distance', 'unit', 'distance');
                        showBoolValue($warehouse, 'column_protection');

                        echo "<h5>Пол</h5>";
                        showWithDictionary($warehouse, 'floor_covering_sn', 'floor_covering');
                        showBoolValue($warehouse, 'anti_dust_covering');
                        showWithDictionaryItem($warehouse, 'height_above_ground', 'unit', 'distance');
                        showWithDictionaryItem($warehouse, 'load_per_square_meter', 'unit', 'mass');

                        echo "<h5>Системы вентиляции и кондиционирования</h5>";
                        showBoolValue($warehouse, 'ventilation');
                        showBoolValue($warehouse, 'conditioning');

                        echo "<h5>Температурный режим</h5>";
                        showWithDictionary($warehouse, 'temperature_mode_sn', 'temperature_mode');
                        showWithDictionaryItem($warehouse, 'min_temperature', 'unit', 'temperature');
                        showWithDictionaryItem($warehouse, 'max_temperature', 'unit', 'temperature');

                        echo "<h5>Система безопасности</h5>";
                        showBoolValue($warehouse, 'security_guard');
                        showBoolValue($warehouse, 'security_video');
                        showBoolValue($warehouse, 'security_alarm');

                        echo "<h5>Пожарная безопасность</h5>";
                        showBoolValue($warehouse, 'fire_alarm');
                        showBoolValue($warehouse, 'fire_charging');
                        showBoolValue($warehouse, 'fire_tanks_and_pump');
                        showWithDictionary($warehouse, 'fire_ext_system_sn', 'fire_ext_system_type');

                        echo "<h5>Электросеть</h5>";
                        showWithDictionary($warehouse, 'electricity_grid_sn', 'electricity_grid_type');

                        echo "<h5>Система отопления</h5>";
                        showWithDictionary($warehouse, 'heating_system_sn', 'heating_system_type');

                        echo "<h5>Водоснабжение</h5>";
                        showBoolValue($warehouse, 'water_hot');
                        showBoolValue($warehouse, 'water_cold');
                        showBoolValue($warehouse, 'water_sewerage');

                        showRelationsSimple('Разгрузочно/погрузочные конструкции', $warehouse, 'wh_loading_unloading_struct_type', 'loading_unloading_struct_type_sn', 'loading_unloading_struct_types');

                        echo "<h5>Офисные и подсобные помещения</h5>";
                        showRelationsOffice('на территории', $warehouse, 'wh_office_facility_type', 'office_facility_types_sn', 'inside');
                        showRelationsOffice('не на территории', $warehouse, 'wh_office_facility_type', 'office_facility_types_sn', 'outside');

                        echo "<h5>Телекоммуникации</h5>";
                        showBoolValue($warehouse, 'internet');
                        showBoolValue($warehouse, 'phone');
                        showBoolValue($warehouse, 'fiber_optic');
                    ?>

                </div>
            </div>

        </div>

        <div class="col-sm-4">

            <?php if (
                !Yii::$app->user->isGuest
                && (
                    in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_DSL, User::ROLE_SB])
                    || (
                        Yii::$app->user->identity->role == User::ROLE_HOLDER &&
                        $model->created_by == Yii::$app->user->identity->id &&
                        $model->warehouse->created_by == Yii::$app->user->identity->id
                    )
                )
            ) {

                $module = Yii::$app->user->identity->role == User::ROLE_HOLDER ? 'panel' : 'admin';
                $advAction = Yii::$app->user->identity->role == User::ROLE_HOLDER ? 'update' : 'advert'; // >_<

                ?>
                <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>Редактировать</h4>
                            <a class="btn btn-primary" href="<?= Url::to([$module . '/advert/' . $advAction, 'id' => $model->id]) ?>" target="_blank">Объявление</a>
                            <a class="btn btn-primary" href="<?= Url::to([$module . '/warehouse/general', 'id' => $model->warehouse_id]) ?>" target="_blank">Склад</a>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <?php if (Yii::$app->user->isGuest || (Yii::$app->user->identity->role == User::ROLE_HOLDER)) { ?>

                <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 style="text-align: center; line-height: 26px; color: #999;">
                                Информация о ценах,<br>а так же контактная информация,<br>доступны только для арендаторов
                            </h4>
                        </div>
                    </div>
                </div>

            <?php } else { ?>

            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <div class="col-xs-12">
                        <h4>
                            ≈ <?= $model->unit_cost_per_day * $model->available_space ?> ₽
                            <span style="font-size: .6em;">за <?= $model->available_space ?> м3 на 1 день</span>
                        </h4>
                        <p style="color: gray; font-size: .8em;">Стоимость 1 м3 в день <?= $model->unit_cost_per_day ?> ₽</p>
                    </div>
                </div>
            </div>

            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <div class="col-xs-12"><h5 style="color: gray;">Контакты</h5></div>
                    <div class="col-xs-4"><?= $model->manager_name ?></div>
                    <div class="col-xs-8" style="text-align: right;">
                        <a href='mailto: <?= $model->manager_email ?>'><span class="text-primary"><?= $model->manager_email ?></span></a>
                        <?php if ($model->manager_phone_number) {
                            echo "<a href='callto: {$model->manager_phone_number}'><span class='btn btn-primary' style='margin-top: 15px;'>{$model->manager_phone_number}</span></a>";
                        } ?>

                    </div>
                </div>
            </div>

            <div style="background-color: #F3F3F3; border-radius: 5px; padding: 1em 2em; margin-bottom: 1em;">
                <div class="row">
                    <div class="col-xs-12">
                        <p>Если хотите, Вы можете заказать доставку на этот склад</p>
                        <div style="text-align: right">
                            <button type="button" class="btn btn-primary">Заказать перевозку</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php } ?>
        </div>
    </div>
</div>

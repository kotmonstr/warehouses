<?php

/* @var $this \yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderMap yii\data\ActiveDataProvider */
/* @var $searchModel app\models\forms\AdvertsSearchForm */

use app\models\Advert;
use app\widgets\geocoder\GeocoderAsset;
use yii\helpers\Json;
use yii\widgets\ListView;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Поиск склада';
$this->params['breadcrumbs'][] = $this->title;

GeocoderAsset::register($this);

$scriptToggleListMap = <<< JS

    function showListAd(btnShow, btnHide, idShow, idHide) {
        document.getElementById(idShow).style.display = 'block';
        document.getElementById(idHide).style.display = 'none';
        btnShow.classList.add('btn-primary');
        btnShow.classList.remove('btn-default');
        btnHide.classList.add('btn-default');
        btnHide.classList.remove('btn-primary');
    }

    var btnShowListAdverts = document.getElementById('btnShowListAd');
    var btnShowMapAdverts = document.getElementById('btnShowMapAd');
    if (btnShowListAdverts) {
        btnShowListAdverts.addEventListener('click', function(){
            showListAd(this, btnShowMapAdverts, 'containerListAd', 'containerMapAd');
        });
    }
    if (btnShowMapAdverts) {
        btnShowMapAdverts.addEventListener('click', function(){
            showListAd(this, btnShowListAdverts, 'containerMapAd', 'containerListAd');
        });
    }

    $(document).on('pjax:complete', function() {
        showListAd(btnShowListAdverts, btnShowMapAdverts, 'containerListAd', 'containerMapAd');
    });
JS;

$this->registerJs($scriptToggleListMap, yii\web\View::POS_END);

/** @var Advert[] $models */
$models = $dataProviderMap->getModels();

$marks = [
    'type' => 'FeatureCollection',
    'features' => [],
];

foreach ($models as $model) {

    if (Yii::$app->user->isGuest || Yii::$app->user->identity->role == User::ROLE_HOLDER) {

        $contentString = '<div class="row"><p style="margin-left: 3em;">' .
                        'Дополнительная информация видна только зарегистрированным арендаторам.' .
                        '</p><p style="padding-right: 4em; text-align: right;">' .
                        Html::a('Просмотр', ['advert-info?id=' . $model->id]) . '</p></div>';
        $iconString = '!';

    } else {

        $costString = '<p style="font-weight: bold;">' .
                $model->unit_cost_per_day . ' &#8381; / ' .
                $model->available_space_type->title_short . '/ день</p>';

        $spaceString = '<p>' . $model->available_space . ' ' .
                $model->available_space_type->title_short . '</p>';

        $contactString = '<span style="padding:.3em;color:white;border:1px solid white;border-radius:5px; margin-left: -5em;" class="bg-primary">';
        if ($model->manager_phone_number) {
            $contactString .= $model->manager_phone_number;
        } else {
            $contactString .= $model->manager_email;
        }
        $contactString .= '</span>';


        $iconString = $model->unit_cost_per_day . ' ₽ / ' . $model->available_space_type->title_short . ' / день';

        $contentString = '<div style="width:30em;"><div class="row"><div class="col-sm-5">' .
                            $costString .
                            $spaceString .
                        '</div><div class="col-sm-7" style="margin-top:2em;padding-right: 5em; text-align: right;">' .
                            $contactString .
                        '</div></div><p style="padding-right: 4em; text-align: right;">' .
                        Html::a('Просмотр', ['advert-info?id=' . $model->id]) . '</p></div>';
    }

    $marks['features'][] = [
        'type' => 'Feature',
        'id' => $model->id,
        'geometry' => [
            'type' => 'Point',
            'coordinates' => [$model->warehouse->address_lon, $model->warehouse->address_lat],
        ],
        'properties' => [
            'balloonContentHeader' => '<h4 style="text-align: center;" class="text-primary">' . $model->warehouse->title . '</h4>',
            'balloonContentBody' => $contentString,
            'balloonContentFooter' => '',
            'clusterCaption' => '<h5 style="text-align: center;" class="text-primary">' . $model->warehouse->title . '</h5>',
            'hintContent' => $model->warehouse->title,
            'iconContent' => $iconString,
        ],
    ];
}

$marksJson = Json::encode($marks);

$mapInit = <<<JS
    ymaps.ready(initMap);

    function initMap() {
        // Создание экземпляра карты.
        var myMap = new ymaps.Map('containerMapAd', {
            center: [55.76, 37.64],
            controls: [],
            zoom: 3
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#grayStretchyIcon');
        objectManager.clusters.options.set('preset', 'islands#grayClusterIcons');

        myMap.geoObjects.add(objectManager);
        objectManager.add($marksJson);

        myMap.setBounds(myMap.geoObjects.getBounds());
    }
JS;

$this->registerJs($mapInit, yii\web\View::POS_END);

function getVariableString($countRows) {
    $variablesString = '';
    if ($countRows >= 5 && $countRows <= 20 || substr(strval($countRows), -2) == '11'
        || substr(strval($countRows), -2) == '12' || substr(strval($countRows), -2) == '13' || substr(strval($countRows), -2) == '14'
    ) {
        $variablesString = 'вариантов';
    } elseif (substr(strval($countRows), -1) == '1') {
        $variablesString = 'вариант';
    } elseif (substr(strval($countRows), -1) == '2' || substr(strval($countRows), -1) == '3' || substr(strval($countRows), -1) == '4') {
        $variablesString = 'варианта';
    } else {
        $variablesString = 'вариантов';
    }
    return $variablesString;
}
?>
<h1><?=Html::encode($this->title) ?></h1>

<?= $this->render('search_form', ['model' => $searchModel]) ?>

<br>

<div class="row">
    <div class="col-sm-6">
        <h4><?= $dataProviderMap->getCount() . ' ' . getVariableString($dataProviderMap->getCount()) ?></h4>
    </div>
    <div style="text-align: right;" class="col-sm-6">
        <button type="button" class="btn btn-primary" id="btnShowMapAd" style="width: 10em;">Карта</button>
        <button type="button" class="btn btn-default" id="btnShowListAd" style="width: 10em;">Список</button>
    </div>
</div>

<div id="containerListAd" style="display: none">

<?php Pjax::begin(); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => 'advert',
        'layout' => '{items}{pager}'
    ]); ?>

<?php Pjax::end(); ?>

</div>

<div id="containerMapAd" style="height:500px; margin-top: 1em;"></div>

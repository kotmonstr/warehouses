<?php

/* @var $this yii\web\View */
/* @var $model AdvertsSearchForm */

use app\models\forms\AdvertsSearchForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\Warehouse;

$cityArray = Warehouse::getCities(null, true);
$minDateFrom = date('Y-m-d', time() - (60 * 60 * 24 * 365));
$minDateTo = date('Y-m-d');
$maxDate = date('Y-m-d', time() + (60 * 60 * 24 * 365 * 5));

$scriptDateChange = <<< JS

    var dateFrom = document.getElementById('advertssearchform-available_from');
    var dateTo = document.getElementById('advertssearchform-available_to');

    function checkDates(dateFrom, dateTo) {
        if (dateFrom.value && dateTo.value && dateFrom.value > dateTo.value) {
            dateTo.value = '';
            alert('Дата "Доступно по" должна быть меньше или равна "Доступно с"');
        }
    }

    if (dateFrom && dateTo) {
        dateFrom.addEventListener('blur', function(){checkDates(dateFrom, dateTo);});
        dateTo.addEventListener('blur', function(){checkDates(dateFrom, dateTo);});
    }

JS;
$this->registerJs($scriptDateChange, yii\web\View::POS_END);
?>

<div style="background-color: #F3F3F3; width: 100%; margin: 0; padding: 0; border: 1px solid #F3F3F3; border-radius: 5px;">
    <div class="row" style="padding: 3em;">
        <div class="col-sm-12" style="background-color: white; border: 1px solid white; border-radius: 5px; padding: 1em;">
            <?php $indexForm = ActiveForm::begin(['method' => 'GET', 'action' => ['site/adverts']]); ?>
                <div style="display: flex; flex-direction: row; justify-content:space-between;">
                    <?= $indexForm->field($model, 'city')->dropDownList($cityArray) ?>
                    <?= $indexForm->field($model, 'available_from')->textInput(['type' => 'date', 'min' => $minDateFrom, 'max' => $maxDate]) ?>
                    <?= $indexForm->field($model, 'available_to')->textInput(['type' => 'date', 'min' => $minDateTo, 'max' => $maxDate]) ?>
                    <?= $indexForm->field($model, 'unit_cost_from')->textInput(['value']) ?>
                    <?= $indexForm->field($model, 'unit_cost_to')->textInput(['value']) ?>
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'style' => 'height: 2.5em; margin-top: 1.7em;']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

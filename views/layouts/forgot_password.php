<?php

/* @var $this View */

use yii\helpers\Html;
use yii\web\View;

?>
<!-- Forgot password modal begin-->
<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModalLabel" aria-hidden="true">
  <div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">
      <div class="options text-right">
        <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">&#10005;</button>
      </div>
      <h4 style="text-align:center;">Введите адрес для отправки нового пароля</h4>

    <?= Html::beginForm(['/user/forgot-password'], 'get') ?>

      <div class="modal-body mb-1">
        <?= Html::textInput('email', '', ['class' => 'form-control', 'placeholder' => 'Email']) ?>
      </div>

      <div class="modal-footer">
        <?= Html::submitButton('Сбросить', ['class' => 'btn btn-primary col-sm-12', 'name' => 'forgot-password-button']); ?>
      </div>

    <?= Html::endForm() ?>

    </div>
  </div>
</div>
<!-- Forgot password modal end-->

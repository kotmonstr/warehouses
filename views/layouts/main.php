<?php

/* @var $this View */
/* @var $content string */

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\forms\SignInForm;
use app\models\forms\SignUpForm;
use yii\bootstrap\Alert;
use app\models\Notification;

AppAsset::register($this);

/** @var User $identity */
$identity = Yii::$app->user->identity;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<?= $this->render('forgot_password') ?>

<?= $this->render('sign_modal', ['modelSignIn' => new SignInForm(), 'modelSignUp' => new SignUpForm()]) ?>

<div class="wrap">

    <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        $navItems = [];

        if (Yii::$app->user->isGuest) {
            $navItems = [
                [
                    'label' => 'Вход и регистрация',
                    'url' => '#',
                    'options' => ['data-toggle' => 'modal', 'data-target' => '#loginModal'],
                ],
            ];
        }
        else {
            switch ($identity->role) {
                case User::ROLE_HOLDER: {
                    $navItems = [
                        [
                            'label' => 'Компания',
                            'url' => ['/panel/organization/index'],
                        ],
                        [
                            'label' => 'Команда',
                            'url' => ['/panel/team/index'],
                            'visible' => isset($identity->organization_id),
                        ],
                        [
                            'label' => 'Склады',
                            'url' => ['/panel/warehouse/index'],
                            'visible' => isset($identity->organization_id),
                        ],
                        [
                            'label' => 'Объявления',
                            'url' => ['/panel/advert/index'],
                            'visible' => isset($identity->organization_id),
                        ],
                        [
                            'label' => 'Брони',
                            'url' => ['/panel/reserve/index'],
                            'visible' => isset($identity->organization_id),
                        ],
                        [
                            'label' => 'Уведомления',
                            'options' => Notification::isSetUnReaded() ? ['class' => 'notification-label'] : [],
                            'url' => ['/panel/notification/index'],
                        ],
                    ];
                } break;
                case User::ROLE_ADMIN:
                case User::ROLE_SB:
                case User::ROLE_DSL: {
                    $navItems = [
                        [
                            'label' => 'Пользователи',
                            'url' => ['/admin/user/index'],
                        ],
                        [
                            'label' => 'Компании',
                            'url' => ['/admin/organization/index'],
                        ],
                        [
                            'label' => 'Склады',
                            'url' => ['/admin/warehouse/index'],
                        ],
                        [
                            'label' => 'Объявления',
                            'url' => ['/admin/advert/index'],
                        ],
                        [
                            'label' => 'Брони',
                            'url' => ['/admin/reserve/index'],
                        ],
                        [
                            'label' => 'Уведомления',
                            'options' => Notification::isSetUnReaded() ? ['class' => 'notification-label'] : [],
                            'url' => ['/admin/notification/index'],
                        ],
                    ];
                } break;
            }

            $navItems[] = [
                'label' => $identity->first_name,
                'items' => [
                    [
                        'label' => 'Профиль',
                        'url' => ['/panel/user/change-user-data'],
                    ],
                    Html::tag('li', '', ['class' => 'divider']),
                    [
                        'label' => 'Выход',
                        'url' => ['/user/logout'],
                        'linkOptions' => ['data' => ['method' => 'post']],
                    ]
                ],
            ];
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $navItems,
        ]);

        NavBar::end();
    ?>

    <div class="container">

        <?php
            foreach (Yii::$app->session->getAllFlashes() as $class => $values) {
                foreach ($values as $value) {
                    echo Alert::widget(['options' => ['class' => $class], 'body' => $value]);
                }
            }
        ?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

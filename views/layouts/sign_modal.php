<?php

/* @var $this View */
/* @var $modelSignIn SignInForm */
/* @var $modelSignUp SignUpForm */

use app\models\forms\SignInForm;
use app\models\forms\SignUpForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
?>
<!-- SignIn SignUp modal begin-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 15%;">
    <div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">


      <div class="modal-c-tabs">
        <div class="options text-right">
          <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">&#10005;</button>
        </div>


        <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
          <li class="nav-item active">
            <a class="nav-link" data-toggle="tab" href="#panelSignIn" role="tab">
              Вход</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panelSignUp" role="tab">
              Регистрация</a>
          </li>
        </ul>


        <div class="tab-content">

        <!-- SignIn modal begin-->

          <div class="tab-pane fade in show active" id="panelSignIn" role="tabpanel">

          <?php $formSignIn = ActiveForm::begin([
                'action' =>['/user/sign-in'],
                'enableAjaxValidation' => true,
            ]); ?>

            <div class="modal-body mb-1">

                <?= $formSignIn->field($modelSignIn, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Email'])->label(false) ?>
                <?= $formSignIn->field($modelSignIn, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>
                <?= $formSignIn->field($modelSignIn, 'rememberMe')->checkbox() ?>

                <p><a href="#" class="blue-text" data-toggle="modal" data-target="#forgotPasswordModal"  data-dismiss="modal">Не помню</a></p>


            </div>

            <div class="modal-footer">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary col-sm-12', 'name' => 'sign-in-button']) ?>
            </div>
            <?php $formSignIn::end(); ?>
          </div>

          <!-- SignIn modal end-->

          <!-- SignUp modal begin-->

          <div class="tab-pane fade" id="panelSignUp" role="tabpanel" style="margin-top: -19em;">

          <?php $formSignUp = ActiveForm::begin([
                'action' =>['/user/sign-up'],
                'enableAjaxValidation' => true,
            ]); ?>

            <div class="modal-body">

                <?= $formSignUp->field($modelSignUp, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Email'])->label(false) ?>
                <?= $formSignUp->field($modelSignUp, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>
                <?= $formSignUp->field($modelSignUp, 'name')->textInput(['placeholder' => 'ФИО'])->label(false) ?>
                <?= $formSignUp->field($modelSignUp, 'rememberMe')->checkbox() ?>

                <?= $formSignUp->field($modelSignUp, 'accept')->checkbox() ?>
                <p>Я ознакомился с <a href="user/policy" class="blue-text" target="blank">Политикой конфиденциальности</a> и условиями соглашения на обработку персональных данных.</p>


            </div>

            <div class="modal-footer">
              <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary col-sm-12', 'name' => 'sign-up-button']) ?>
            </div>
            <?php $formSignUp::end(); ?>
          </div>

          <!-- SignUp modal end-->

        </div>

      </div>
      </div>

    </div>
</div>
<!-- SignIn SignUp modal end-->


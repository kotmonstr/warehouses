<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View password-reset-html */
/* @var $user app\models\User instance of User for reset password*/
/* @var $confirmLink string link for reset password */

?>
<div>
    <p>Здравствуйте, <?= $user->first_name ?>,</p>
    <p>Проследуйте по ссылке для смены пароля</p>
    <p><?= Html::a('Сменить', $confirmLink) ?></p>
</div>

<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View password-reset-html */
/* @var $user app\models\User instance of newly created User */
/* @var $confirmLink link to confirm email */

?>
<div>
    <p>Здравствуйте, <?= $user->first_name ?>,</p>
    <p>Проследуйте по ссылке для подтверждения email: </p>
    <p><?= Html::a('Перейти', $confirmLink) ?></p>
</div>

<?php

/* @var $this View password-reset-html */

use yii\web\View;

/* @var $user app\models\User instance of newly created User */
/* @var $confirmLink link to confirm email */

?>
Здравствуйте, <?= $user->first_name ?>,
Проследуйте по ссылке для подтверждения email:
<?= $confirmLink ?>

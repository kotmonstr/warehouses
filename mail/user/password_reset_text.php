<?php

/* @var $this View password-reset-html */

use yii\web\View;

/* @var $user app\models\User instance of User for reset password*/
/* @var $confirmLink string link for reset password */
/* @var $password string new password */

?>
Здравствуйте, <?= $user->first_name ?>,
Проследуйте по ссылке для смены пароля
<?= $confirmLink ?>

<?php
/* @var $this View view component instance */

use yii\mail\MessageInterface;
use yii\web\View;

/* @var $message MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<?=  $content; ?>
<?php $this->endBody() ?>
<?php $this->endPage() ?>

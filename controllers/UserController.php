<?php
/**
 * This is the file with controller class for user
 * php version 7
 */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\SignInForm;
use app\models\forms\SignUpForm;
use yii\widgets\ActiveForm;
use app\models\User;

/**
 * This is the controller class for user
 * with signIn, signUp actions.
 */
class UserController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'sign-in', 'sign-up', 'sign-up-confirm', 'forgot-password', 'reset-password-confirm', 'policy'],
                'rules' => [
                    [
                        'actions' => ['sign-in', 'sign-up', 'sign-up-confirm', 'forgot-password', 'reset-password-confirm', 'policy'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['POST'],
                    'sign-in' => ['POST'],
                    'sign-up' => ['POST'],
                    'sign-up-confirm' => ['GET'],
                    'forgot-password' => ['GET'],
                    'reset-password-confirm' => ['GET', 'POST'],
                    'policy' => ['GET'],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * SignIn action.
     *
     * @return array|Response
     */
    public function actionSignIn()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignInForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->role == User::ROLE_ADMIN
                || Yii::$app->user->identity->role == User::ROLE_DSL
                || Yii::$app->user->identity->role == User::ROLE_SB
            ) {
                return $this->redirect(['admin/organization']);
            }
            $this->redirect('/panel/organization/index');
        }
    }

    /**
     * SignUp action.
     *
     * @return array|Response
     */
    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignUpForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->getSession()->addFlash('bg-success', 'На указанный e-mail адрес было отправлено сообщения для верификации.<br>Пожалуйста, перейдите по ссылке в письме для подтверждения вашего почтового адреса.');
            $this->redirect(['/panel/organization/index']);
        }
    }

    /**
     * SignUpConfirm action.
     *
     * @param string $token for confirm
     *
     * @return Response|string
     */
    public function actionSignUpConfirm($token)
    {
        $model = new SignUpForm();
        if (empty($token)) {
            Yii::$app->getSession()->addFlash('bg-danger', 'Что-то пошло не так.');
        }

        if ($model->confirm($token)) {
            Yii::$app->getSession()->addFlash('bg-success', 'Почтовый адрес успешно подтверджён, спасибо.');
        } else {
            Yii::$app->getSession()->addFlash('bg-danger', 'Во время подтверждения почтового адреса произошла ошибка. Попробуйте перейти по ссылке ещё раз или обратитесь в службу поддержки.');
        }

        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
        } else {
            $this->redirect(['/panel/user/change-user-data']);
        }
    }

    /**
     * ForgotPassword action.
     *
     * @param string $email for sending reseted password
     *
     * @return Response|string
     */
    public function actionForgotPassword($email)
    {
        $model = new SignInForm();
        if (empty($email)) {
            Yii::$app->getSession()->addFlash('bg-warning', 'Пожалуйста, укажите email.');
        }

        if ($model->resetPassword($email)) {
            Yii::$app->getSession()->addFlash('bg-success', 'Письмо с инструкцией по восстановлению пароля было отправлено на указанный электронный адрес.');
        } else {
            Yii::$app->getSession()->addFlash('bg-danger', 'Пользователь с указанным email не найден.');
        }

        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
        } else {
            $this->redirect(['/panel/user/change-user-data']);
        }
    }

    /**
     * ResetPasswordConfirm action.
     *
     * @param string $token for confirm
     *
     * @return Response|string
     */
    public function actionResetPasswordConfirm($token = null)
    {
        $model = new SignInForm();

        if (isset($_POST['tokenReset']) && isset($_POST['passwordReset'])) {
            if ($model->confirm($_POST['tokenReset'], $_POST['passwordReset'])) {
                Yii::$app->getSession()->addFlash('bg-success', 'Новый пароль успешно установлен.');
            } else {
                Yii::$app->getSession()->addFlash('bg-danger', 'Произошла ошибка при восстановлении пароля. Попробуйте повторить попытку.');
            }

            if (Yii::$app->user->isGuest) {
                $this->redirect(['/site/index']);
            } else {
                $this->redirect(['/panel/user/change-user-data']);
            }

        } else {
            return $this->render('reset_password', ['token' => $token]);
        }

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Policy action.
     *
     * @return Response
     */
    public function actionPolicy()
    {
        return $this->render('policy');
    }
}

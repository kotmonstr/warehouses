<?php

namespace app\controllers;

use app\models\Advert;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\forms\AdvertsSearchForm;
use app\models\Document;
use app\models\File;
use app\models\Warehouse;

class SiteController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'about', 'adverts', 'advert-info'],
                'rules' => [
                    [
                        'actions' => ['index', 'about', 'adverts', 'advert-info'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['POST', 'GET'],
                    'about' => ['POST', 'GET'],
                    'adverts' => ['POST', 'GET'],
                    'advert-info' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /** {@inheritdoc} */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchForm = new AdvertsSearchForm();

        return $this->render('index', ['model' => $searchForm]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Adverts action.
     *
     * @return string
     */
    public function actionAdverts()
    {
        $searchForm = new AdvertsSearchForm();
        $dataProvider = $searchForm->search(Yii::$app->request->queryParams);
        $dataProviderMap = $searchForm->search(Yii::$app->request->queryParams);
        $dataProviderMap->setPagination(false);

        return $this->render('adverts', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchForm,
            'dataProviderMap' => $dataProviderMap
        ]);
    }

    /**
     * Advert info action.
     *
     * @param string|int $id advert
     *
     * @return string
     */
    public function actionAdvertInfo($id)
    {
        $model = Advert::findModel($id);
        $documents = Document::findAllByEntity($model->warehouse->id, Warehouse::class);
        $filePaths = [];
        foreach ($documents as $document) {
            if (substr_compare($document->entity_field, 'photo_', 0, 6) == 0) {
                $filePaths[] = File::findOne(['document_id' => $document->id])->path;
            }
        }
        return $this->render('advert_info', ['model' => $model, 'photos' => $filePaths]);
    }
}

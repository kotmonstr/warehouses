<?php

/* @var $this View */

use yii\web\View;

$scriptGeo = <<< JS

function init() {

    if (document.getElementById('btn_set_address')) {
        var address, coordinates, city;
        var myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: coordinates,
            },
                properties: {
                    iconContent: '!',
                    hintContent: address,
            }
            }, {
                preset: 'islands#blackStretchyIcon',
                draggable: true
            }
        );

        function setFormValue() {
            if(document.getElementById('input_address_ym_geocoder')){
                document.getElementById('input_address_ym_geocoder').value = address;
            }
            if(document.getElementById('input_address_lat')){
                document.getElementById('input_address_lat').value = coordinates[1];
            }
            if(document.getElementById('input_address_lon')){
                document.getElementById('input_address_lon').value = coordinates[0];
            }
            if(document.getElementById('input_address_city_ym_geocoder')){
                document.getElementById('input_address_city_ym_geocoder').value = city;
            }
        }

        var btn = document.getElementById('btn_set_address');
        var lon = btn.dataset.lon;
        var lat = btn.dataset.lat;
        if(lon && lat){
            coordinates = [lon, lat];
            ymaps.geocode(coordinates, {
                results: 1
            }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                address = firstGeoObject.getAddressLine();
                myGeoObject = setPlacemark(myGeoObject);

                if (myGeoObject) {
                    myGeoObject.events.add('dragend', function () {
                        getAddress(myGeoObject.geometry.getCoordinates());
                    });

                    myMap.geoObjects.add(myGeoObject);
                }
            });

            ymaps.geocode(coordinates, {
                results: 1,
                kind: 'locality',
            }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                city = firstGeoObject.getAddressLine().split(',');
                city = city[city.length - 1].trim();
            });
        }

        var myMap = new ymaps.Map('map', {
            center: coordinates || [55.753994, 37.622093],
            zoom: 15,
            controls: []
        });

        var searchControl = new ymaps.control.SearchControl({
            options: {
            provider: 'yandex#search'
            }
        });

        myMap.controls.add(searchControl);

        searchControl.events.add('resultselect', function(e) {
            var index = e.get('index');
            searchControl.getResult(index).then(function(res) {
                coordinates = res.geometry.getCoordinates();

                ymaps.geocode(coordinates, {
                    results: 1
                }).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);
                    address = firstGeoObject.getAddressLine();
                    myGeoObject = setPlacemark(myGeoObject);

                    if (myGeoObject) {
                        myGeoObject.events.add('dragend', function () {
                            getAddress(myGeoObject.geometry.getCoordinates());
                        });

                        myMap.geoObjects.add(myGeoObject);
                    }
                });

                ymaps.geocode(coordinates, {
                    results: 1,
                    kind: 'locality',
                }).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);
                    city = firstGeoObject.getAddressLine().split(',');
                    city = city[city.length - 1].trim();
                });
            });
        })

        myMap.events.add('click', function (e) {
            coordinates = e.get('coords');
            myGeoObject = setPlacemark(myGeoObject);

            if (myGeoObject) {
                myGeoObject.events.add('dragend', function () {
                    getAddress(myGeoObject.geometry.getCoordinates());
                });

                myMap.geoObjects.add(myGeoObject);
            }
        });

        function setPlacemark(myGeoObject) {
            myGeoObject.geometry.setCoordinates(coordinates);
            getAddress(coordinates);
            return myGeoObject;
        }

        function getAddress(coords) {
            myGeoObject.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                var addressHint = [
                    firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                    firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                ].filter(Boolean).join(', ');

                address = firstGeoObject.getAddressLine();

                myGeoObject.properties.set({
                    iconCaption: addressHint,
                    balloonContent: address,
                    hintContent: addressHint,
                });

                setFormValue();
            });

            ymaps.geocode(coords, {
                    results: 1,
                    kind: 'locality',
                }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                city = firstGeoObject.getAddressLine().split(',');
                city = city[city.length - 1].trim();
                setFormValue();
            });
        }
    }
}

ymaps.ready(init);
JS;
$this->registerJs($scriptGeo, yii\web\View::POS_END);

$lat = '';
$lon = '';
if (isset($model) && isset($model->address_lat)) { $lat = $model->address_lat; }
if (isset($model) && isset($model->address_lon)) { $lon = $model->address_lon; }

?>
<div class="row" style="position: relative; top: 3em; left: 80%; z-index: -2;">

    <div
        class="btn btn-warning"
        id="btn_set_address"
        data-lat="<?= $lat ?>"
        data-lon="<?= $lon ?>"
        style="width: 140px;"
    >Подтвердить</div>

</div>

<div id="map" style="width: 100%; height: 470px; border: 2px solid #F3F3F3; padding-right: 2em;"></div>



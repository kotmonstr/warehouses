<?php
/**
 * This is the file with class for assets Geocoder widget
 * php version 7
 */
namespace app\widgets\geocoder;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * This is the class for assets Geocoder widget
 */
class GeocoderAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $js = [
    ];

    public $css = [];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->js[] = "https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=" . env('YANDEX_API_KEY');
        parent::init();
    }
}

<?php
/**
 * This is the file with widget class for yandex geocoder
 * php version 7
 */
namespace app\widgets\geocoder;

use yii\base\Widget;

/**
 * This is the widget class for yandex geocoder
 */
class Geocoder extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        GeocoderAsset::register($this->getView());
        return $this->render('index', ['model' => $this->model]);
    }
}

